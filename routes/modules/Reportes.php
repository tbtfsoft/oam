<?php

/*
|--------------------------------------------------------------------------
| Reportes Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "Reportes" middleware group. Now create something great!
|
*/

/**
 * El name router comienza con Reportes.*
 */
Route::middleware('auth')->namespace('Modules\Reportes')->name('reportes.')->group(function() {	
	Route::middleware('role_or_permission:super-admin|o&m')->group(function() {
		Route::get('/reportes-electrico','ReportesController@homeElectrico')->name('electrico');
		Route::post('/generarReporteElectrico','ReportesController@generarReporteElectrico')->name('generarReporteElectrico');
		Route::get('/generarReporteElectrico','ReportesController@generarReporteElectrico')->name('generarReporteElectrico');
	});
});