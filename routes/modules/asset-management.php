<?php

/*
|--------------------------------------------------------------------------
| asset-management Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "asset-management" middleware group. Now create something great!
|
*/

Route::middleware('auth')->namespace('Modules\AssetManagement')->group(function() {

	/* Dashboard */
	Route::prefix('generales')->name('generales.')->group(function(){
		Route::post('/empresasPadres','GeneralesController@EmpresasPadre')->name('empresasPadres');
		Route::get('/empresas/getbyids','GeneralesController@getEmpresasByIds')->name('empresas.gerById');
  	});

	/* Dashboard */
	Route::middleware(['maintenace_permission:dashboard'])->prefix('dashboard')->name('dashboard.')->group(function(){
	  Route::post('/energiaMensual','DashboardController@GraficaEnergiaMensual')->name('energiaMensual');
	  Route::post('/energiaCLPMensual','DashboardController@GraficaEnergiaCLPMensual')->name('energiaCLPMensual');
	  Route::post('/potenciaCLPMensual','DashboardController@GraficaPotenciaCLPMensual')->name('potenciaCLPMensual');
	  Route::post('/servCompCLPMensual','DashboardController@GraficaServCompCLPMensual')->name('servCompCLPMensual');
	  Route::post('/potenciaDiaria','DashboardController@GraficaPotenciaDiaria')->name('potenciaDiaria');
	  Route::post('/energiaDiaria','DashboardController@GraficaEnergiaDiaria')->name('energiaDiaria');
	  Route::post('/energiaMensual2','DashboardController@GraficaEnergiaMes')->name('energiaMensual2');
	  Route::post('/energiaMensual2Ant','DashboardController@GraficaEnergiaMesAnt')->name('energiaMensual2Ant');
	});

	/* CARGA DE DATOS */
	Route::prefix('carga-datos')->name('cargar-data.')->group(function() {
		Route::prefix('energia-medidores')->group(function(){
			//carga-datos.energia-medidores-pfv
			Route::get('/pfv','EnergiaMedidoresControllerPFV@energiaMedidores')->name('energia-medidores-pfv');
			Route::post('/pfv/ajaxCSV','EnergiaMedidoresControllerPFV@ajaxCSV')->name('energia-medidores-pfv.ajaxCSV');
			//carga-datos.energia-medidores-cen
			Route::get('/cen','EnergiaMedidoresControllerCEN@energiaMedidores')->name('energia-medidores-cen');
			Route::post('/cen/ajaxCSV','EnergiaMedidoresControllerCEN@ajaxCSV')->name('energia-medidores-cen.ajaxCSV');
		});

		Route::get('/template-email','TemplateEmailController@index')->name('template-email.index');
        Route::post('/template-email/filter','TemplateEmailController@filter')->name('template-email.filter');
        Route::post('/template-email/sentEmail','TemplateEmailController@sentEmail')->name('template-email.send');

        Route::get('/cmg','CMGController@cmg')->name('cmg');
		Route::post('/cmg/ajaxCSV','CMGController@ajaxCSV')->name('cmg.ajaxCSV');

		Route::get('it-energia','ITEnergiaController@index')->name('it-energia');
		Route::post('it-energia/ajaxCSV','ITEnergiaController@energiaAjaxCSV')->name('energiaCSV');
		Route::get('it-energia/{month}/{year}/{id_tipo_doc_cen}','ITEnergiaController@existData')->name('energiaCSV.isdata');

		Route::get('it-potencia','ITPotenciaController@index')->name('it-potencia');
		Route::post('it-potencia/ajaxCSV','ITPotenciaController@potenciaAjaxCSV')->name('potenciaCSV');
		Route::get('it-potencia/{month}/{year}/{id_tipo_doc_cen}','ITPotenciaController@existData')->name('potenciaCSV.isdata');

		Route::get('balance-potencia','CargarDatosController@balancePotencia')->name('balance-potencia');

		Route::get('/facturas','FacturacionController@index')->name('facturas');
		Route::get('/facturas/eliminar','FacturacionController@eliminar')->name('facturas.eliminar');
		Route::post('/facturas/eliminar','FacturacionController@eliminarPost')->name('facturas.eliminar.post');

	  	Route::get('/facturas-emitidas','FacturacionController@emitidas')->name('facturas-emitidas');
		Route::post('/facturas-emitidas/ajaxCSV','FacturacionController@ajaxCSV')->name('facturas-emitidas.ajaxCSV');

	  	Route::get('/facturas-recibidas','FacturacionController@recibidas')->name('facturas-recibidas');
		Route::post('/facturas-recibidas/ajaxCSV','FacturacionController@ajaxCSV')->name('facturas-recibidas.ajaxCSV');

		Route::get('servicios-complementarios','ServicioComplementarioController@index')->name('servicios-complementarios');
		Route::post('servicios-complementarios/ajaxCSV','ServicioComplementarioController@servicioComplementarioAjaxCSV')->name('servicio-complementarioCSV');
		Route::get('servicios-complementarios/{month}/{year}/{id_tipo_doc_cen}','ServicioComplementarioController@existData')->name('servicio-complementarioCSV.isdata');

		// Route::get('empresas', 'EmpresaController@index')->name('empresas');
    Route::post('empresas/ajaxCSV','EmpresaController@postAjaxSaveCsv')->name('empresaCSV');

		Route::get('factor-referencia','FactorReferenciaController@index')->name('factor-referencia');
		Route::post('factor-referencia/ajaxCSV','FactorReferenciaController@ajaxCSV')->name('factor-referencia.ajaxCSV');

		Route::get('/pagos', 'PagosController@index')->name('pagos');
		Route::post('/pagos', 'PagosController@ajaxCSV')->name('pagos.ajaxCSV');
	});

	// COMERCIALIZACION
	Route::middleware(['maintenace_permission:comercializacion', 'role_or_permission:super-admin|comercializacion'])->group(function() {
		/* Balance de Energía */
		Route::prefix('balance-energia')->name('balance-energia.')->group(function(){
			Route::get('/','BalanceEnergiaController@index')->name('index');
	  		Route::post('/checkperiodo','BalanceEnergiaController@AjaxCheckPerido')->name('check');
		});

		/* CUADROS DE PAGOS */
		Route::prefix('cuadros-pago')->name('cuadros-pago.')->group(function() {
		  Route::get('matriz-acredor','CuadrosPagoController@matrizAcredor')->name('matriz-acredor');
		  Route::get('matriz-deudor/data','CuadrosPagoController@getDataMatrizDeudor')->name('matriz-deudor-data');
		  Route::get('matriz-deudor','CuadrosPagoController@matrizDeudor')->name('matriz-deudor');
		});

		/* FACTURACION */
		Route::prefix('facturacion')->name('facturacion.')->group(function(){
		  Route::get('/','FacturacionController@index')->name('index');
		  Route::post('/','FacturacionController@postAjaxFacturacion')->name('postAjax');
		  Route::get('/recibidas','FacturacionController@recibidas')->name('recibidas');
		  Route::get('/emitidas','FacturacionController@emitidas')->name('emitidas');
		  Route::get('/revisar','FacturacionController@revisar')->name('revisar');
			Route::post('/emit-pagos', 'FacturacionController@emitPagos')->name('emit-pagos');
		});

		/* pagos */
		Route::prefix('pagos')->name('pagos.')->group(function() {
			Route::get('/emitidos', 'PagosController@emitidos')->name('emitidos');
			Route::get('/recibidos', 'PagosController@recibidos')->name('recibidos');
			Route::post('/update', 'PagosController@ajaxUpdatePago')->name('updated');
			Route::post('/cancel', 'PagosController@cancelarFactura')->name('cancel');
		});

		/* OPERACIÓN */
		Route::namespace('Operaciones')->prefix('operaciones')->name('operaciones.')->group(function() {
		Route::get('ERP','OperacionesErpController@index')->name('ERP');
	    Route::get('ERP/getDocumentsDirectory/{id}','OperacionesErpController@getDocumentsDirectory')->name('ERP.getDocumentsDirectory');

	  	Route::post('/ERP/export','OperacionesErpController@export')->name('ERP.export');
	    Route::get('ERP/download/{id_empresa}/{empresa}/{modulo}/{tipo_documento}/{periodo}','OperacionesErpController@downloadDocumentHTTP')->name('ERP.download.file');
		Route::get('CEN','OperacionesCenController@index')->name('CEN');
		Route::post('/CEN/export','OperacionesCenController@export')->name('CEN.export');

		Route::get('facturasPPCEN','OperacionesFacturasPPECEN@index')->name('facturasPPCEN');
		Route::get('generarPlanillaDTEPPCEN','OperacionesFacturasPPECEN@generarPlanillaDTEPPCEN')->name('generarPlanillaDTEPPCEN');
		});

		/* FILE MASIVOS XML */
		Route::namespace('RenameXML')->prefix('rename-xml')->name('rename-xml.')->group(function() {
		  Route::get('/','RenameXMLController@index')->name('index');
		  Route::post('/execute','RenameXMLController@execute')->name('execute');
		  Route::get('/getFiles','RenameXMLController@getFiles')->name('getFiles');
		});
	});

	// SCADA WEB
	Route::middleware(['maintenace_permission:o&m', 'role_or_permission:super-admin|o&m'])->group(function() {
		/* Graficos Comercial */
		Route::prefix('graficos-comercial')->name('graficos-comercial.')->group(function(){
			Route::post('/energiaRange','GraficosComercialController@GraficaEnergiaRange')->name('energiaRange');
			Route::post('/energiaMes','GraficosComercialController@GraficaEnergiaMes')->name('energiaMes');
			Route::post('/energiaVSMes','GraficosComercialController@GraficaEnergiaVSMes')->name('energiaVSMes');
			Route::post('/energiaAnio','GraficosComercialController@GraficaEnergiaAnio')->name('energiaAnio');
			Route::post('/energiaVSAnio','GraficosComercialController@GraficaEnergiaVSAnio')->name('energiaVSAnio');
		});

		/* Graficos Electrico */
		Route::prefix('graficos-electrico')->name('graficos-electrico.')->group(function(){
			Route::post('/potenciaRange','GraficosElectricoController@GraficaPotenciaRange')->name('potenciaRange');
			Route::post('/voltajesRange','GraficosElectricoController@GraficaVoltajesRange')->name('voltajesRange');
			Route::post('/inversoresRange','GraficosElectricoController@GraficaInversoresRange')->name('inversoresRange');
		});
	});

	// ASESOR-TECNICO
	Route::middleware(['role_or_permission:super-admin|asesor-tecnico', 'maintenace_permission:asesor-tecnico'])
				->prefix('asesor-tecnico')
				->namespace('AsesorTecnico')
				->group(function() {
		Route::get('/','AsesorTecnicoController@index')->name('asesor-tecnico.index');
		Route::resource('correspondencias', 'CorrespondenciasController');
		Route::post('correspondencias/changeStatus/{id}', 'CorrespondenciasController@changeStatus')->name('correspondencias.change.status');
		Route::resource('repositorios', 'RepositoriosController');
		Route::post('repositorios/changeStatus/{id}', 'RepositoriosController@changeStatus')->name('repositorios.change.status');
	});
	Route::middleware(['role_or_permission:super-admin|PMRTE', 'maintenace_permission:PRMTE'])
		->prefix('prmte')
		->namespace('AsesorTecnico')
		->group(function() {
			Route::resource('incidencias', 'IncidenciasController');
			Route::post('incidencias/changeStatus/{id}', 'IncidenciasController@changeStatus')->name('incidencias.change.status');
		});

	Route::middleware(['role_or_permission:super-admin|PMRTE', 'maintenace_permission:PRMTE'])
	->group(function() {
		Route::post('comentarios/destroy', 'ComentariosController@destroy')->name('comentarios.delete');
		Route::post('comentarios/edit', 'ComentariosController@edit')->name('comentarios.edit');
		Route::post('comentarios/update', 'ComentariosController@update')->name('comentarios.update');
		Route::post('message/files/{module}/{id}/{file}', 'MessageFilesController@destroy')->name('message-files.delete');
	});

	// PRMTE
	Route::middleware(['role_or_permission:super-admin|PMRTE', 'maintenace_permission:PRMTE'])->group(function() {
		Route::get('/prmte/status','PrmteController@index')->name('prmte.status');
		Route::post('/potenciaDiariaPRMTE','PrmteController@GraficaPotenciaDiariaPRMTE')->name('prmte.potenciaDiariaPRMTE');
	});
});
