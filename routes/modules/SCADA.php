<?php

	/*
	|--------------------------------------------------------------------------
	| SCADA Routes
	|--------------------------------------------------------------------------
	|
	| Here is where you can register web routes for your application. These
	| routes are loaded by the RouteServiceProvider within a group which
	| contains the "SCADA" middleware group. Now create something great!
	|
	*/

	/**
	 * El name router comienza con scada.*
	 */
	Route::middleware('auth')->namespace('Modules\Scada')->prefix('o&m')->group(function() {	
		Route::middleware('role_or_permission:super-admin|o&m')->group(function() {
			Route::name('scada-web.')->group(function() {
				Route::get('scada-web/comercial','ScadaWebComercialController@home')->name('comercial');
				Route::get('scada-web/electrico','ScadaWebElectricoController@home')->name('electrico');
			});
			Route::resource('repositorios', 'ScadaWebRepositoriosController', [
				'names' => [
					'index' => 'o&m.repositorios.index',
					'store' => 'o&m.repositorios.store',
					'create' => 'o&m.repositorios.create',
					'update' => 'o&m.repositorios.update',
					'edit' => 'o&m.repositorios.edit',
					'destroy' => 'o&m.repositorios.destroy',
					'show' => 'o&m.repositorios.show'
				]
			]);
			Route::post('repositorios/changeStatus/{id}', 'ScadaWebRepositoriosController@changeStatus')->name('o&m.repositorios.change.status');
		});
	});