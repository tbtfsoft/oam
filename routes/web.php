<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes(['register' => false, 'logout' => false]);

Route::get('/', 'HomeController@index')->name('home');

Route::get('lang/{locale}', 'LocalizationController@index')->name('lang');

// Route::get('pusher', function(){
// 	$user = \Auth::user();
// 	// $email = new CreateUserMail($user);
// 	// \Mail::to('carlosanselmi2@gmail.com')->send(new App\Mail\CreateUserMail($user));
// 	\Mail::to('carlosanselmi2@gmail.com')->send(new CreateUserMail(\Auth::user()));
// 	// new \App\Mail\CreateUserMail(\Auth::user());
// 	// event(new App\Events\CreateUserEvent());
//     // event(new App\Events\TestEvent('Broadcasting in Laravel using Pusher!'));

// 	return view('pusher');
// })->name('home');
// 

Route::middleware('auth')->group(function() {

	/* Profile */
	Route::namespace('Auth')->group(function() {

		Route::middleware('role:super-admin|client-admin')->group(function() {
			Route::resource('users', 'ProfileController');
		});

		Route::prefix('profile')->name('profile.')->group(function() {

			Route::get('/', 'ProfileController@profile')->name('index');

			Route::post('/update/{id}', 'ProfileController@updateProfile')->name('update');

			Route::post('/updateAuthUserPassword', 'ProfileController@updateAuthUserPassword')->name('update.password');
		});
	});

	Route::middleware('role:super-admin')->namespace('CRUD')->group(function(){
		Route::resource('permissions', 'PermissionsController')->except(['show']);
		// Route::resource('conexiones_empresas', 'ConexionesEmpresasController')->except(['show']);
		Route::resource('empresas', 'EmpresasController')->except(['show']);
		Route::resource('conexiones', 'ConexionesController')->except(['show']);
		Route::resource('dispositivos', 'DispositivoController')->except(['show']);
		Route::resource('precio_estabilizado', 'PrecioEstabilizadoController')->except(['show']);
		Route::resource('barras', 'BarrasController')->except(['show']);
		Route::resource('barras_empresas', 'BarraEmpresaController')->except(['show']);
		Route::resource('modalidad_calculo_empresas', 'ModalidadCalculoEmpresasController')->except(['show']);
		Route::resource('periodos_cen', 'PeriodoCenController')->except(['show']);
	});	

});	

Route::get('/recursos', function () {
	return view('layouts.recursos');
});

Route::get('ajaxlogin','Auth\LoginController@ajaxLogin')->name('is_users');

Route::get('model1','Modules\AssetManagement\EnergiaMedidoresController@getMaxAndMinTBL');

Route::prefix('ppagos')->name('ppagos.')->namespace('PPagoCen')->group(function() {
	Route::get('/','PpController@index')->name('index'); // Descarga el .csv
	Route::get('/empresas','PpController@empresas')->name('empresas'); // Aquí sube todo el .json de empresas que se encuentra en el /public/test
	Route::get('/bancos','PpController@bancos')->name('bancos');// Aquí sube todo el .json de bancos que se encuentra en el /public/test
	Route::get('/empresas/get', 'PpController@getEmpresas')->name('getEmpresas');
	Route::get('/bancos/get', 'PpController@getBancos')->name('getEmpresas');
	Route::get('/bancos/export/{option?}', 'PpController@export')->name('export');
	Route::get('/process', 'PpController@process')->name('process');
});


Route::get('/403', function() {
	return view('errors.403');
})->name('errors.403');

Route::get('/503', function() {
	return view('errors.503');
})->name('errors.503');