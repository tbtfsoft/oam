<?php

return [
    'sidebar' => [
        [
            'label' => 'Dashboard',
            'icon'   =>  'icon-dashboard',
            'route' => 'home'
        ],
        [
            'label'  =>  'app.cargar-data',
            'icon'   =>  'icon-cargar-data',
            'roles'  =>  ['super-admin'],
            'routes' => [
                ['route' => 'cargar-data.it-energia'],
                ['route' => 'cargar-data.it-potencia'],
                ['route' => 'cargar-data.servicios-complementarios'],
                ['route' => 'cargar-data.energia-medidores-cen'],
                ['route' => 'cargar-data.energia-medidores-pfv'],
                ['route' => 'cargar-data.factor-referencia'],
                ['route' => 'cargar-data.cmg'],
                ['route' => 'precio_estabilizado.index'],
                ['route' => 'empresas.index'],
                ['route' => 'ppagos.index'],
                ['route' => 'barras.index'],
                ['route' => 'barras_empresas.index'],
                ['route' => 'cargar-data.facturas', 'active' => 'carga-datos/facturas/facturas'],
                ['route' => 'cargar-data.facturas.eliminar', 'active' => 'carga-datos/facturas/eliminar'],
                ['route' => 'modalidad_calculo_empresas.index'],
                ['route' => 'periodos_cen.index']
            ]
        ],
        [
            'label' =>  'O&M',
            'icon'  =>  'icon-scada-web',
            'routes'    =>  [
                [
                    'label' =>  'Scada Web',
                    'routes' =>  [
                        ['route' => 'scada-web.comercial'],
                        ['route' => 'scada-web.electrico'],
                        ['route' => 'reportes.electrico']
                    ]
                ],
                ['route' =>  'o&m.repositorios.index']
            ]
        ],
        [
            'label' => 'app.comercializacion',
            'icon'  => 'icon-comercializacion',
            'routes'    =>  [
                ['route'    =>  'balance-energia.index'],
                [
                    'label' =>  'app.cuadros-pago',
                    'routes'    =>  [
                        ['route'    =>  'cuadros-pago.matriz-acredor'],
                        ['route'    =>  'cuadros-pago.matriz-deudor']
                    ]
                ],
                [
                    'label' =>  'app.facturas',
                    'icon'  =>  'icon-facturas',
                    'routes'    =>  [
                        [
                            'route'    =>  'facturacion.emitidas',
                            'parameters'  =>  '?empresa_xls=414&mes=06&anio=2020&module=0'
                        ],
                        ['route'    =>  'facturacion.recibidas']
                    ]
                ],
                [
                    'label' =>  'app.pagos',
                    'icon'  =>  'icon-pagos',
                    'routes'    =>  [
                        ['route'    =>  'pagos.recibidos'],
                        ['route'    =>  'pagos.emitidos']
                    ]
                ],
                [
                    // 'label' =>  'app.operaciones',
                    'label' =>  'app.interfaz-erp',
                    'icon'  =>  'icon-operaciones',
                    'routes'    =>  [
                        ['route'    =>  'operaciones.ERP'],
                        [
                            'route'    =>  'rename-xml.index',
                            'roles' =>  ['super-admin']
                        ],
                        [
                            'route'    =>  'operaciones.facturasPPCEN',
                            'roles' =>  ['super-admin']
                        ]
                    ]
                ],
            ]
        ],
        [
            'label'     =>  'app.asesor-tecnico',
            'icon'  =>  'icon-asesor-tecnico',
            'routes'    =>  [
                ['route'    =>  'correspondencias.index'],
                ['route'    =>  'repositorios.index']
            ]
        ],
        [
            'label' =>  'app.pmrte',
            'icon'  =>  'icon-pmrte',
            'routes'    =>  [
                ['route'    =>  'prmte.status'],
                ['route'    =>  'incidencias.index']
            ]
        ],
        [
            'label' =>  'app.administracion',
            'icon'  =>  'icon-user-settings',
            'class' =>  'separation',
            'routes'    =>  [
                ['route' => 'profile.index', 'icon' =>  'fas fa-user mr-2'],
                ['route' => 'users.index', 'icon'   =>  'fas fa-users-cog', 'roles'   =>  ['super-admin|client-admin']],
                ['route' => 'permissions.index', 'icon'   =>  'mr-1 fas fa-user-lock', 'roles'   =>  ['super-admin']],
                ['route' => 'conexiones.index', 'icon' =>  'mr-1 fas fa-project-diagram', 'roles'   =>  ['super-admin']]
            ]
        ]

    ]
];