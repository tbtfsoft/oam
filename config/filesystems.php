<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "sftp", "s3", "rackspace"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public/uploads/'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        'file' => [
            'driver' => 'local',
            'root' => storage_path('app/public/uploads/'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        'logos' => [
            'driver' => 'local',
            'root' => storage_path('app/public/uploads/logos'),
            'url' => env('APP_URL').'/storage/logos',
            'visibility' => 'public',
        ],
        'renameXML' => [
            'driver' => 'local',
            'root' => storage_path('app/public/uploads/REPORTE_XML'),
            'visibility' => 'public',
        ],
        'energiaXML' => [
            'driver' => 'local',
            'root' => storage_path('app/public/uploads/cen_xml/energia'),
            'visibility' => 'public',
        ],
        'potenciaXML' => [
            'driver' => 'local',
            'root' => storage_path('app/public/uploads/cen_xml/potencia'),
            'visibility' => 'public',
        ],
        'correspondencias' => [
            'driver' => 'local',
            'root' => storage_path('app/public/correspondencias'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],
        'informes' => [
            'driver' => 'local',
            'root' => storage_path('app/public/informes'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],
        'incidencias' => [
            'driver' => 'local',
            'root' => storage_path('app/public/incidencias'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],
        'om_informes' => [
            'driver' => 'local',
            'root' => storage_path('app/public/om_informes'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],
        'cen' => [
            'driver' => 'local',
            'root' => storage_path('app/public/uploads/cen'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],
        'erp' => [
            'driver' => 'local',
            'root' => storage_path('app/public/uploads/erp'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],
        's3' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
            'url' => env('AWS_URL'),
        ],

    ],

];
