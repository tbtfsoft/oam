
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('tail.select');
import Swal from 'sweetalert2';
window.Swal = Swal;

window.formatCliente = function(cliente) {
    cliente.value = cliente.value.replace(/[.-]/g, '').replace( /^(\d{1,2})(\d{3})(\d{3})(\w{1})$/, '$1.$2.$3-$4')
}

window.checkRut = function(cadena2) {
    var cadena = cadena2; 
    if(cadena.indexOf('-') == -1){
        return false;
    }else{
        var array_rut = cadena.split('-');
        var rut = array_rut[0];
        rut += '';
        rut = rut.replace(/[^0-9]/g, '');
        var verificador = array_rut[1];
        verificador = verificador.toUpperCase();
        var array_rut = new Array();
        var array_multiplica = new Array();

        var valor = 9;

        for(var i=0;i<rut.length;i++){
            array_rut[i] = rut[i];
            array_multiplica[i] = valor;
            valor--;

            valor= (valor<4)?9:valor;
        }

        array_rut = array_rut.reverse();
        var suma = 0;
        for (var i=0;i<array_rut.length;i++){
            suma +=(array_rut[i]*array_multiplica[i]);
        }

        var verificador_real = suma%11;
        verificador_real = (verificador_real==10)?'K':verificador_real;

        if (verificador_real != verificador){
            return false;
        }
        else{
            return true;
        }
    }
}

window.serialize = function (form) {

  // Setup our serialized data
  var serialized = [];

  // Loop through each field in the form
  for (var i = 0; i < form.elements.length; i++) {

    var field = form.elements[i];

    // Don't serialize fields without a name, submits, buttons, file and reset inputs, and disabled fields
    if (!field.name || field.disabled || field.type === 'file' || field.type === 'reset' || field.type === 'submit' || field.type === 'button') continue;

    // If a multi-select, get all selections
    if (field.type === 'select-multiple') {
      for (var n = 0; n < field.options.length; n++) {
        if (!field.options[n].selected) continue;
        serialized.push(encodeURIComponent(field.name) + "=" + encodeURIComponent(field.options[n].value));
      }
    }

    // Convert field data to a query string
    else if ((field.type !== 'checkbox' && field.type !== 'radio') || field.checked) {
      serialized.push(encodeURIComponent(field.name) + "=" + encodeURIComponent(field.value));
    }
  }

  return serialized.join('&');
}
