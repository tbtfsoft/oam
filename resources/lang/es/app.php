<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'users' => 'Usuarios',
    'conexiones' => 'Conexiones',
    'dispositivos' => 'Dispositivos',
    'precio_estabilizado' => 'Precio estabilizado',
    'barras_empresas' => 'Barras empresas',
    'barras' => 'Barras',
    'modalidad_calculo_empresas' => 'Modalidad cálculo empresas',
    'empresas' => 'Empresas',
    'empresa' => 'Empresa',
    'periodos_cen' => 'Períodos CEN',
    'permissions' => 'Permisos',
    'correspondencias' => 'correspondencias',
    'correspondencia' => 'correspondencia',
    'incidencia' => 'incidencia',
    'incidencias' => 'incidencias',
    'informe' => 'Repositorio',
    'informes' => 'Repositorios',
    'repositorios' => 'Repositorios',
    'om_informes' => 'Repositorios O&M',


    'cargar-data'    =>  'Cargar data',
    'cargar-data.it-energia'    =>  'Balance de energía',
    'cargar-data.it-potencia'   =>  'Balance de potencia',
    'cargar-data.servicios-complementarios' =>  'Servicios complementarios',
    'cargar-data.energia-medidores-cen' =>  'Energía medidores CEN',
    'cargar-data.energia-medidores-pfv' =>  'Energía medidores PFV',
    'cargar-data.factor-referencia' =>  'Factor de referencia',
    'cargar-data.cmg'   =>  'CMg',
    'precio_estabilizado.index' =>  'Precio estabilizado',
    'empresas.index'    =>  'Empresas',
    'ppagos.index'  =>  'Empresas PPCEN',
    'barras.index'   =>  'Barras',
    'barras_empresas.index' =>  'Barras empresas',
    'cargar-data.facturas'  =>  'Facturas XML',
    'cargar-data.facturas.eliminar'  =>  'Facturas XML D',
    'cargar-data.template-email.index'  =>  'Template email',
    'modalidad_calculo_empresas.index'  =>  'Modalidad calculo empresas',
    'periodos_cen.index'    =>  'Período CEN',

    'o&m'    =>  'O&M',
    'scada-web'    =>  'Scada web',
    'scada-web.comercial'   =>  'Gráficos comerciales',
    'scada-web.electrico'   =>  'Gráficos eléctricos',
    'reportes.electrico' =>  'Reportes eléctricos',
    'o&m.informes.index'    =>  'Informes',
    'o&m.repositorios'    =>  'Repositorios O&M',
    'o&m.repositorios.index'    =>  'Repositorios',

    'comercializacion'  =>  'Comercialización',
    'balance-energia.index' =>  'Balance energía',
    'cuadros-pago' =>  'Cuadros de pago',
    'cuadros-pago.matriz-acredor'    =>  'Matriz acreedor',
    'cuadros-pago.matriz-deudor'    =>  'Matriz deudor',

    'factura'   =>  'Factura',
    'facturas'   =>  'Facturas',
    'facturacion.emitidas'  =>  'Facturas emitidas',
    'facturacion.recibidas' =>  'Facturas recibidas',

    'interfaz-erp'  =>  'Interfaz ERP',
    'Emisión masiva de documentos a ERP' => 'Emisión masiva de documentos a ERP',

    'pagos'   =>  'Pagos',
    'pagos.recibidos'   =>  'Pagos recibidos',
    'pagos.emitidos'    =>  'Pagos emitidos',

    'operaciones'   =>  'Operaciones',
    'operaciones.ERP'   =>  'ERP',
    'rename-xml.index'  =>  'Rename XML',
    'operaciones.facturasPPCEN' =>  'Facturas PPCEN',
    'asesor-tecnico'    =>  'Asesor técnico',
    'correspondencias.index'    =>  'Correspondencias',
    'informes.index'    =>  'Informes',
    'repositorios.index'    =>  'Repositorios',

    'pmrte' =>  'PRMTE',
    'prmte.pmrte'   =>  'PRMTE',
    'prmte.status'   =>  'Status',
    'incidencias.index' =>  'Incidencias',

    'administracion' =>  'Administración',
    'profile.index' =>  'Perfil',
    'users.index'   =>  'Usuarios',
    'permissions.index' =>  'Permisos',
    'conexiones.index'  =>  'Conexiones',

    "rename xml"    =>  "Rename XML",
    "descargar"     =>  "Descargar",
    "procesar"  =>  "Procesar",

    "Emisión masiva de documentos a CEN"    =>  "Emisión masiva de documentos a CEN",

    // SELECTS
    'Coordinado' => 'Coordinado',
    'Concepto' => 'Concepto',

    //DATAPICKER
    "actualizar" => "Actualizar",
    "eliminar" => "Eliminar",
    "editar" => "Editar",
    "agregar" => "Agregar",
    "siguiente" =>  "Siguiente",
    "si" => "Si",
    "no" => "no",
    "de" => "De",
    "hasta" => "Hasta",
    "filtrar" => "Filtrar",
    "aplicar" => "Aplicar",
    "aceptar" => "Aceptar",
    "cancelar" => "Cancelar",

    // MESES
    'enero' => 'Enero',
    'febrero' => 'Febrero',
    'marzo' => 'Marzo',
    'abril' => 'Abril',
    'mayo' => 'Mayo',
    'junio' => 'Junio',
    'julio' => 'Julio',
    'agosto' => 'Agosto',
    'septiembre' => 'Septiembre',
    'octubre' => 'Octubre',
    'noviembre' => 'Noviembre',
    'diciembre' => 'Diciembre',

    'dom'   => 'Dom',
    'lun'   => 'Lun',
    'mar'   => 'Mar',
    'mie'   => 'Mie',
    'jue'   => 'Hue',
    'vie'   => 'Vie',
    'sab'   => 'Sab',

    // Tipo de documento
    'preliminar' => 'Preliminar',
    'definitivo' => 'Definitivo',
    'reliquidacion' => 'Reliquidación',

    // Dashboard
    'Dashboard' => 'Dashboard',
    'Mes actual' => 'Mes actual',
    'Mes en proceso' => 'Mes en proceso',
    'Año en proceso - Mes cerrado' => 'Año en proceso - Mes cerrado',
    // >>>>ERROR:VERIFICAR CONCURENCIAS
    'Calculo basados en valores instantáneos' => 'Cálculo basados en valores instantáneos',
    'Cálculo basados en valores instantáneos' => 'Cálculo basados en valores instantáneos',
    'Calculos basados en valores acumulativos' => 'Cálculos basados en valores acumulativos',
    "Cálculos basados en valores acumulativos" => "Calculos basados en valores acumulativos",

    // JS
    'Potencia Activa kW - Radiación W/m2' => 'Potencia activa kW - Radiación W/m2',
    'Generación kWh Total' => 'Generación kWh total',
    'kWh - Generación CLP total' => 'kWh - Generación CLP total',
    'Producción y Consumo Mensual de Energía (kWh)' => 'Producción y Consumo Mensual de Energía (kWh)',
    'Energía (kWh)' => 'Energía (kWh)',
    'Transferencia Mensual de Energia (CLP)' => 'Transferencia Mensual de Energia (CLP)',
    'Energía (CLP)' => 'Energía (CLP)',
    'Potencia Transferida (CLP)' => 'Potencia Transferida (CLP)',
    'Producción Total' => 'Producción Total',
    'Transferencia Mensual de Potencia (CLP)' => 'Transferencia Mensual de Potencia (CLP)',
    'Potencia (CLP)' => 'Potencia (CLP)',
    'Servicios Complementarios (CLP)' => 'Servicios Complementarios (CLP)',
    'Servicios Complementarios Total' => 'Servicios Complementarios Total',
    'Servicios Complementarios Mensual (CLP)' => 'Servicios Complementarios Mensual (CLP)',
    'Energía Inyectada (kWh)' => 'Energía Inyectada (kWh)',
    'Energía inyectada' => 'Energía inyectada',
    'Energía' => 'Energía',
    "Servicios complementarios" => "Servicios complementarios",
    'Potencia' => 'Potencia',
    'Todos' => 'Todos',
    'Energía Generada (kWh - CLP)' => 'Energía Generada (kWh - CLP)',
    'Generación CLP' => 'Generación CLP',
    'Generación kWh' => 'Generación kWh',
    'kWh - Generacion' =>  'kWh - Generacion',
    'Energía Consumida (kWh)' => 'Energía Consumida (kWh)',
    'Energía Producida (CLP)' => 'Energía Producida (CLP)',
    'Transferencia Mensual de Energía (CLP)' => 'Transferencia Mensual de Energía (CLP)',
    'Total' => 'Total',
    'Energía Generada Acumulada (kWh)' => 'Energía Generada Acumulada (kWh)',
    'kWh - Generacion CLP Total' => 'kWh - Generación CLP Total',
    'año' => 'Año',
    'mes' => 'Mes',
    'dia' => 'Día',

    // carga de data
    "guardar" => "Guardar",
    "potencía" => "Potencía",
    "tipo de documento" => "Tipo de documento",
    "ver balance de energía" => "Ver balance de energía",
    "generar balance"   =>  "Generar balance",
    "para procesar la data de CEN"   =>  "para procesar la data de CEN",
    "Error en el balance de energía."   => "Error en el balance de energía.",
    "Balance de energía exitoso."   => "Balance de energía exitoso.",
    "Energía consumida" =>  "Energía consumida",
    "Error en el balance de energía." =>  "Error en el balance de energía.",
    "Balance de energía exitoso." =>  "Balance de energía exitoso.",
    "Data obtenida" =>  "Data obtenida",
    "Energía inyectada" =>  "Energía inyectada",
    "Energía consumida" =>  "Energía consumida",
    "Medidor" => "Medidor",
    'Generación'    => 'Generación',
    "DELTA" => "DELTA",
    'Para la empresa :empresa en el período :periodo en nuestra base de datos de oEnergy.' => 'Para la empresa :empresa en el período :periodo en nuestra base de datos de oEnergy.',
    "No sé pudo procesar estas empresas" => "No sé pudo procesar estas empresas",

    "La data de Energía Medidores CEN aun no esta disponible para el periodo seleccionado." => "La data de Energía Medidores CEN aun no esta disponible para el periodo seleccionado.",
    "La data de Energía Medidores PFV aun no está disponible para el periodo seleccionado." => "La data de Energía Medidores PFV aun no está disponible para el periodo seleccionado.",


    "modalidad" => "Modalidad",
    "barra" => "Barra",
    "rango de fechas" => "Rango de fechas",
    "Variables Eléctricas y Económicas" => "Variables Eléctricas y Económicas",
    "Variables Eléctricas" => "Variables Eléctricas",
    "Balance de Transferencias Económicas CEN (Mes Cerrado)" => "Balance de Transferencias Económicas CEN (Mes Cerrado)",

    "Medidor" => "Medidor",
    "Inversores" => "Inversores",
    "Combiner Box" => "Combiner Box",
    "Trackers" => "Trackers",
    "Calidad" => "Calidad",
    "Generar" => "Generar",
    "generar" => "generar",
    "general" => "General",
    "publicado por" => "Publicado por",
    "seleccione por lo menos una empresa" => "Seleccione por lo menos una empresa",
    "Debe escribir un comentario" => "Debe escribir un comentario",
    "seleccione las empresas" => "Seleccione las empresas",
    "reporte" => "Reporte",
    "publicado por" => "Publicado por :name el :date",
    "setBy" => "por :name",
    "sin comentarios" => "Sin comentarios",
    "comentarios" => "Comentarios",
    "comentario" => "Comentario",
    "archivos" => "Archivos",
    "sin archivos" => "Sin archivos",
    "historial" => "Historial",
    "de eliminar el comentario" => "De eliminar el comentario",
    "esta seguro?" => "¿Está seguro?",
    "si, eliminar!" => "Si, eliminar!",
    "complete los campos requeridos" => "Complete los campos requeridos",
    "editar comentario" => "Editar comentario",
    "ocurrio algo inesperado, intente mas tarde." => "Ocurrio algo inesperado, intente mas tarde.",
    "Status actual" => "Status actual",
    "Registros Recibidos" => "Registros Recibidos",
    "Disponibilidad" => "Disponibilidad",
    "Data obtained via PRMTE" => "Data obtained via PRMTE",
    "MONTO TOTAL A COBRAR" => "MONTO TOTAL A COBRAR",
    "SIN REGISTROS OBTENIDOS" => "SIN REGISTROS OBTENIDOS",
    "Fecha de recepción SII" => "Fecha de recepción SII",
    "Razón social" => "Razón social",
    "deudor" => "Deudor",
    "acreedor" => "Acreedor",
    "monto"  => "Monto",
    "documento"  => "Documento",
    "periodo"  => "Período",

    'no se encontro la empresa correspondiente' =>  'No se encontro la empresa correspondiente.',
    'error al leer el xml'  =>  'Error al leer el xml.',
    'archivos procesados correctamente' =>  'Archivos procesados correctamente.',
    'debe subir por lo menos un archivo'    =>  'Debe subir por lo menos un archivo.',
    'actualización de las facturas correctamente'   =>  'Actualización de las facturas correctamente.',
    'facturas eliminadas correctamente' =>  'Facturas eliminadas correctamente.',
    'archivos no procesados'    =>  'Archivos no procesados.',

    "Se eliminará la fecha de pago de esta factura" => 'Se eliminará la fecha de pago de esta factura',
    "La factura no se encuentra pagada" =>  "La factura no se encuentra pagada",
    "se restableció"  =>  "Se restableció",
    "complete los campos requeridos"  =>  "Complete los campos requeridos",
    "codigo de pago"    =>  "Código de pago",
    "Data procesada correctamente"  =>  "Data procesada correctamente",
    "Se actualizó el estado correctamente"  =>  "Se actualizó el estado correctamente.",
    "No se encontró el registro"  =>  "No se encontró el registro.",

    "La data de Energía Medidores CEN aun no está disponible para el periodo seleccionado"  =>  "La data de Energía Medidores CEN aun no está disponible para el periodo seleccionado.",
    "La data de Energía Medidores PFV aun no está disponible para el periodo seleccionado"  =>  "La data de Energía Medidores PFV aun no está disponible para el periodo seleccionado.",
    "No se adjunto el archivo. Intentelo de nuevo"  =>  "No se adjunto el archivo. Intentelo de nuevo.",
    "No se encontró el comentario"  =>  "No se encontró el comentario",
    "El comentario se eliminó correctamente"  =>  "El comentario se eliminó correctamente.",
    "El comentario se actualizó correctamente"  =>  "El comentario se actualizó correctamente.",
    "Archivo no valido"  =>  "Archivo no valido",
    "Archivo importado exitosamente"  =>  "Archivo importado exitosamente",
    "Se requiere el ID de la empresa"  =>  "Se requiere el ID de la empresa",
    "Debe ingresar el archivo CSV para poder procesar la data"  =>  "Debe ingresar el archivo CSV para poder procesar la data",
    "Factura procesada"  =>  "Factura procesada",
    "Archivo eliminado correctamente"  =>  "Archivo eliminado correctamente",

    'Energía Generada y Consumida (kWh)' => 'Energía Generada y Consumida (kWh)',
    'Potencia activa total Combiner Box' => 'Potencia activa total Combiner Box',
    'Potencia activa total Inversores'   => 'Potencia activa total Inversores',
    'Inversor'  =>  'Inversor',
    'Combiner box'  =>  'Combiner box',
    'Consumo kWh'   =>  'Consumo kWh',
    'Generación Total'  =>  'Generación Total',
    'Consumo Total'  =>  'Consumo Total',
	'kWh - ConsumoTotal'  =>  'kWh - ConsumoTotal',
    'Energía Generada (kWh)'  =>  'Energía Generada (kWh)',
    'Voltaje de Línea'  =>  'Voltaje de Línea',
    'Voltaje AB'    =>  'Voltaje AB',
    'Voltaje BC'    =>  'Voltaje BC',
    'Voltaje CA'    =>  'Voltaje CA',
    'Voltaje CA'    =>  'Voltaje CA',
];
