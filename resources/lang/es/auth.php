<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Estas credenciales no coinciden con nuestros registros.',
    'throttle' => 'Demasiados intentos fallidos en muy poco tiempo. Por favor intente de nuevo en :seconds segundos.',
    'username'  =>  'RUT/USERNAME',
    'password'  =>  'Contraseña',
    'ok'        =>  'Entrar',
    'modules'   =>  'Modulos',
    'profile'   =>  'Profile',
    'name'      =>  'Nombre',
    'save'      =>  'Guardar',
    'details'   =>  'Detalles de la cuenta',
    'change_password'   =>  'Cambiar contraseña',

    'update'  =>  'Actualizar',
    'password_current'  =>  'Contraseña (actual)',
    'password_new'    =>  'Contraseña (nueva)',
    'password_confirmation'    =>  'Contraseña (confirmar)',

    'profile_updated'   =>  'Se actualizó exitosamente su perfil',
    'password_updated'   =>  'Su contraseña fue cambiada exitosamente!',
    'error.current_password'   =>  'La contraseña actual no coincide',
    'error.check_fields'   =>  'Verifique los campos para actualizar la contraseña.',
];
