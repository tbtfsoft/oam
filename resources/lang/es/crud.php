<?php

return [

    /*
    |--------------------------------------------------------------------------
    | CRUD Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    'destroy' => 'Registro eliminado exitosamente.',
    'store' => 'Registro creado exitosamente.',
    'update' => 'Registro actualizado exitosamente.',
    'error' => 'Whoops! Hubo algunos problemas con su entrada.',

    'action' => 'Acciones',
    'cancel' => 'Cancelar',
    'accept' => 'Aceptar',
    'edit'   => 'Editar',
    'new'    => 'Crear',
    'back'   => 'Atras',
    'save'   => 'Guardar',

];
