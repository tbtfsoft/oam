<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'username'  =>  'RUT/USERNAME',
    'password'  =>  'Password',
    'ok'        =>  'acept',
    'modules'   =>  'Modules',
    'profile'   =>  'Profile',
    'name'      =>  'Name',
    'save'      =>  'Save',
    'details'   =>  'Account details',
    'change_password'   =>  'change password',
    'update'  =>  'Update',
    'password_current'  =>  'Current password',
    'password_new'    =>  'New password',
    'password_confirmation'    =>  'Confirmation password',
    'profile_updated'   =>  'Your profile was successfully updated',
    'password_updated'   =>  'Your password was successfully changed!',
    'error.current_password'   =>  'The current password does not match',
    'error.check_fields'   =>  'Check the fields to update the password.',

];
