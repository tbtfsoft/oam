<?php

return [

    /*
    |--------------------------------------------------------------------------
    | CRUD Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    'destroy' => 'Registry successfully deleted.',
    'store' => 'Registry created successfully.',
    'update' => 'Register successfully updated.',
    'error' => 'Whoops! There were some problems with your input.',

    'action' => 'Actions',
    'cancel' => 'Cancel',
    'accept' => 'Accept',
    'edit'   => 'Edit',
    'new'    => 'New',
    'back'   => 'Back',
    'save'   => 'Save',
];
