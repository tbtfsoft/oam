@foreach (session('flash_notification', collect())->toArray() as $message)
    @if ($message['overlay'])
        @include('flash::modal', [
            'modalClass' => 'flash-modal',
            'title'      => $message['title'],
            'body'       => $message['message']
        ])
    @else
        <div class="mb-0 alert alert-{{ $message['level'] }} {{ $message['important'] ? 'alert-important' : '' }}" role="alert">
            <button type="button" class="close text-white" data-dismiss="alert" aria-hidden="true">&times;</button>

            {!! $message['message'] !!}
   
            @if ($errors->any())                    
                <ul class="py-0 mb-0">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
        </div>
    @endif
@endforeach

{{ session()->forget('flash_notification') }}
