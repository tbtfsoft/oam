@extends('layouts.app')

@content
	<div class="col-12 col-lg-10 mx-auto">
		<div class="row mt-5">
			<div class="col-12 align-self-center text-center">
				<img src="{{ asset('img/errors/503.png') }}" alt="503" style="max-height: 50vh; max-width: 100%; margin: auto; align-items: center;">
			</div>
			<div class="col-12 align-self-center text-center mx-auto">
				<div class="error-content text-center">
					<p class="h5">
						Estamos actualizando este módulo, disculpe las molestias.
					</p>
					<p class="h5"> Volver al inicio <a href="{{ route('home') }}" class="font-weight-bold">Inicio</a></p>
				</div>
			</div>
		</div>
	</div>
@endcontent