@extends('errors.layout')

@section('content')
	<div class="error-content text-center">
		{{-- <img src="{{ asset('images/errors/404-error.svg') }}" width="100px"> --}}
		<h1 class="mb-0">ERROR <span class="bg-light px-2 rounded text-dark">404</span></h1>
		<p class="mb-0 line-height">
			Esta página no la hemos encontrado
		</p>
		<p class="line-height mb-2">
			Por favor contactar al administrador del sistema
		</p>
		<a class="btn btn-dark text-uppercase" href="{{ url()->previous() }}"> regresar </a>
		<a class="btn btn-dark text-uppercase" href="{{ route('home') }}"> inicio </a>
	</div>
@endsection