@extends('layouts.app')

@content
	<div class="col-12 col-lg-10 mx-auto">
		<div class="row mt-5">
			<div class="col-12 align-self-center text-center">
				<img src="{{ asset('img/errors/403.png') }}" alt="503" style="max-height: 50vh; max-width: 100%; margin: auto; align-items: center;">
			</div>
			<div class="col-12 align-self-center text-center mx-auto">
				<div class="error-content text-center">
					<h4>Usted no tiene contratado este Módulo</h4>
					<p class="h5">
						Para mayor información o adquisición del módulo le invitamos a contactarnos.
					</p>
				</div>
			</div>
		</div>
	</div>
@endcontent