<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name') }}</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <style>
            html,body{
                width: 100%;
                height: 100%;
                margin: auto;
            }
            body{
                background-image: url({{ asset('img/errors/hiw-hero.svg') }});
                background-repeat: no-repeat;
                background-position: 0 0;
                display: table;
                background-color: #8f8f8f;
                color: white;
                position: relative;
                padding-bottom: 45px;
            }
            body::before{
                content: "";
                background-image: url({{ asset('img/logo/isotipo.png') }});
                background-repeat: no-repeat;
                background-position: 0 0;
                background-size: contain;
                opacity: .8;
                width: 100%;
                height: 100%;
                top: 0;
                left: 0;
                z-index: 0;
                position: absolute;
            }
            p{ margin-bottom: 0px; }
            h1{ margin-bottom: 0px; margin-top: 0px }
            @media screen and (max-width: 768px){
                body::before{ opacity: 0.5; }
            }
            @media screen and (max-width: 425px){
                body::before{ width: 50%; }
            }
            a{ color: white; }
            a:hover{ font-weight: 500; color: white }
            #wrapper{
                display: table-cell;
                vertical-align: middle;
                width: 100%;
            }
            footer{
                position: absolute;
                bottom: 0;
                text-align: center;
                width: 100%;
            }
        </style>
    @yield('styles')

</head>
<body>
    <div id="wrapper">
        <!-- Start right Content here -->
        <div class="content-page" style="margin: 0px">
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card-box card-box-energia">
                                @yield('content')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Start footer -->
        <footer class="footer">
            <a href="http://www.amigosolar.cl/" target="_blank" class="line-height d-block">
                <small>&copy; {{ date('Y')-1 }}-{{ date('Y') }} oEnergy</small>
            </a>
            <a href="http://amigo-solar.local/documentos/T%C3%A9rminos%20de%20Uso.pdf" class="line-height d-inline-block" target="_blank">
                <small>Términos de uso</small>
            </a> | 
            <a href="http://amigo-solar.local/documentos/Politica%20de%20Privacidad.pdf" target="_blank">
                <small>Política de privacidad y seguridad </small>
            </a>
        </footer>
    </div>
</body>
</html>
