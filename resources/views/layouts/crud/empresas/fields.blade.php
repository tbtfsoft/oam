<div class="row">
  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('rut', 'RUT*') !!}
      {!! Form::text('rut',null, ['class' => $errors->has('rut') ? 'form-control text-uppercase error' : 'form-control text-uppercase', 'placeholder' => 'RUT','required' => true, 'data-rule' => 'required|rut', 'onkeyup'=>'formatCliente(this)', 'maxlength'=> '12' ]) !!}
      @if ($errors->has('rut'))
        <div class="error" data-type="validator-error">{{ $errors->first('rut') }}</div>
      @endif
    </div>
  </div>

  <div class="col-12 col-sm-6">    
    <div class="form-group">
      {!! Form::label('nombre_xls', 'Nombre XLS') !!}
      {!! Form::text('nombre_xls',null, ['class' => $errors->has('nombre_xls') ? 'form-control error' : 'form-control', 'placeholder' => 'nombre_xls','required' => true, 'data-rule' => 'required|minlength-3']) !!}
      @if ($errors->has('nombre_xls'))
        <div class="error" data-type="validator-error">{{ $errors->first('nombre_xls') }}</div>
      @endif
    </div>
  </div>

  <div class="col-12 col-sm-6">    
    <div class="form-group">
      {!! Form::label('label', 'Label') !!}
      {!! Form::text('label',null, ['class' => $errors->has('label') ? 'form-control error' : 'form-control', 'placeholder' => 'label', 'data-rule' => 'minlength-3']) !!}
      @if ($errors->has('label'))
        <div class="error" data-type="validator-error">{{ $errors->first('label') }}</div>
      @endif
    </div>
  </div>

  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('order', 'ORDEN') !!}
      {!! Form::number('order', !isset($model) ? 100000 : null, ['class' => $errors->has('order') ? 'form-control error' : 'form-control', 'placeholder' => 'order', 'data-rule' => 'numeric', 'onkeyup'=>'formatCliente(this)']) !!}
      @if ($errors->has('order'))
        <div class="error" data-type="validator-error">{{ $errors->first('order') }}</div>
      @endif
    </div>
  </div>

  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('potencia_instalada', 'Potencia instalada') !!}
      {!! Form::text('potencia_instalada',null, ['class' => $errors->has('potencia_instalada') ? 'form-control error' : 'form-control', 'placeholder' => 'potencia_instalada', 'onkeyup'=>'formatCliente(this)', 'maxlength'=> '12' ]) !!}
      @if ($errors->has('potencia_instalada'))
        <div class="error" data-type="validator-error">{{ $errors->first('potencia_instalada') }}</div>
      @endif
    </div>
  </div>

  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('coordenadas_lat', 'coordenadas_lat') !!}
      {!! Form::text('coordenadas_lat',null, ['class' => $errors->has('coordenadas_lat') ? 'form-control text-uppercase error' : 'form-control', 'placeholder' => 'coordenadas_lat','required' => true, 'data-rule' => 'minlength-5', 'maxlength'=> '30' ]) !!}
      @if ($errors->has('coordenadas_lat'))
        <div class="error" data-type="validator-error">{{ $errors->first('coordenadas_lat') }}</div>
      @endif
    </div>
  </div>

  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('coordenadas_lng', 'coordenadas_lng') !!}
      {!! Form::text('coordenadas_lng',null, ['class' => $errors->has('coordenadas_lng') ? 'form-control text-uppercase error' : 'form-control', 'placeholder' => 'coordenadas_lng','required' => true, 'data-rule' => 'minlength-5', 'maxlength'=> '30' ]) !!}
      @if ($errors->has('coordenadas_lng'))
        <div class="error" data-type="validator-error">{{ $errors->first('coordenadas_lng') }}</div>
      @endif
    </div>
  </div>

  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('url_gmaps', 'url_gmaps') !!}
      {!! Form::text('url_gmaps',null, ['class' => $errors->has('url_gmaps') ? 'form-control text-uppercase error' : 'form-control', 'placeholder' => 'url_gmaps','required' => true, 'data-rule' => 'minlength-5', 'maxlength'=> '100' ]) !!}
      @if ($errors->has('url_gmaps'))
        <div class="error" data-type="validator-error">{{ $errors->first('url_gmaps') }}</div>
      @endif
    </div>
  </div>

  <div class="col-12 col-sm-6">
    @php
      $fecha_inicio = old('fecha_inicio') ? old('fecha_inicio') : date('d/m/Y');
      if (isset($model)) {
        $fecha_inicio = old('fecha_inicio') ? old('fecha_inicio') : date('d/m/Y H:m:s', strtotime($model->fecha_inicio));
      }
    @endphp
    <div class="form-group">
      {!! Form::label('fecha_inicio', 'Fecha inicio') !!}
      {!! Form::text('fecha_inicio', $fecha_inicio, ['class' => $errors->has('fecha_inicio') ? 'form-control text-uppercase error' : 'form-control text-uppercase', 'placeholder' => 'fecha_inicio','required' => true, 'data-rule' => 'minlength-5', 'onkeyup'=>'formatCliente(this)', 'maxlength'=> '12' ]) !!}
      @if ($errors->has('fecha_inicio'))
        <div class="error" data-type="validator-error">{{ $errors->first('fecha_inicio') }}</div>
      @endif
    </div>
  </div>

  <div class="col-12 mt-3">
    {!! link_to_route($class_basename.'.index', Lang::get('crud.back'),[], ['class'=>'btn btn-outline-danger btn-sm text-uppercase load-link']); !!}
    {!! Form::submit(Lang::get('crud.save'),['class' => 'btn btn-primary btn-sm text-uppercase']) !!}
  </div>
</div>

@section('scripts')
  <script src="{{ asset('plugins/js-form-validator-2.1/js-form-validator.js') }}"></script>
  @component('layouts.partials.assets.components.datapicker')
  <script>
    /*
     Documentacion màs detallada: https://www.daterangepicker.com/ 
    */
    $('input[name="fecha_inicio"]').daterangepicker({
      singleDatePicker: true,
      showDropdowns: true,
      autoclose: true,
      locale: {
        format: 'DD/M/Y',
        separator: " - ",
        applyLabel: "Aplicar",
        cancelLabel: "Cancelar",
        fromLabel: "DE",
        toLabel: "HASTA",
        customRangeLabel: "Custom",
        daysOfWeek: [
            "Dom",
            "Lun",
            "Mar",
            "Mie",
            "Jue",
            "Vie",
            "Sáb"
        ],
        monthNames: [
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"
        ],
        firstDay: 1
      },
    });
  </script>
  @endcomponent
  <script>
    var f = document.querySelector('.form-validator');
    if(f){
      f.setAttribute('novalidate', true);
      var options = {
        rules: {
          rut: function (value) {
            return checkRut(value);
          }
        },
        messages: {
          es: {
            rut: {
              incorrect: 'RUT INVALIDO'
            }
          }
        },
        locale: "{{ session()->has('locale') ? session()->get('locale') : 'es' }}"
      };

      new Validator(f, function (err, res) {
        return res;
      },options);
    }
  </script>
@endsection
