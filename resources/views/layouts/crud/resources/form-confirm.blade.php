<script>
  function confirmar(form,e) {
    e.preventDefault();
    $.confirm({
      title: '¿Desea eliminarlo?',
      content: 'Esta operación no se puede deshacer',
      theme: 'supervan',
      buttons: { 
        ok: {
            text: "ok!",
            btnClass: 'btn-primary',
            keys: ['enter'],
            action: function() {
              form.submit();
              return true;
            }
        },
        cancel: function() { }
      }
    });
  }
</script>