@isset ($class_basename)
	@php
		$showDelete = $role != 'super-admin' && Auth::id() != $id;
	@endphp
	<div class="w-100 text-center">
		@if ($role != 'super-admin' || Auth::user()->id == $id)
			<a href="{{ route($class_basename.'.edit',$id) }}" class="btn btn-sm btn-primary load-link {{ !$showDelete? 'd-block' : '' }}" onclick="document.body.classList.add('loading')">
				<i class="fa fa-edit"></i>
			</a>
		@else
			<a href="{{ route($class_basename.'.edit',$id) }}" class="btn btn-sm btn-warning load-link {{ !$showDelete? 'd-block' : '' }}">
				<i class="fa fa-search" aria-hidden="true" onclick="document.body.classList.add('loading')"></i>
			</a>
		@endif

		@if ($showDelete)
		    <form id="form-destroy_{{$id}}" action="{{ route($class_basename.'.destroy',$id) }}" method="POST" onsubmit="return confirmar(this,event);" class="d-inline">
		    	@csrf
		    	@method('DELETE')
				<button class="btn btn-sm btn-danger btn-confirm load-link" type="submit" data-toggle="confirmation" data-singleton="true">
					<i class="fa fa-trash-o"></i>
				</button>
		    </form>
		@endif
	</div>
@endisset
