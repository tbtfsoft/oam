@php
  // iS Super Admin
  $isSuperAdmin = false;
  // Si el modelo soy yo
  $isMe = false;
  // Si el modelo posee parent
  $parent = false;
  // Si client Admin
  $isClientAdmin = Auth::user()->hasRole(['client-admin']);
  // Verifica si existe el model (:create User)
  if(isset($model)) {
    // Verifica si es SuperAdmin
    $isSuperAdmin = $model->hasRole(['super-admin']);
    // Verifica si el model soy yo (Es decir, el que va a editar el $model(user) es mio)
    $isMe = Auth::id() == $model->id;
    // Se verifica si el modelo tiene parent ( client-admin )
    $parent = $model->parent->first();
  }else {
    $parent = Auth::user();
  }
  // NOTA: $model es el modelo del User a editar
@endphp

<div class="row">

  <div class="col-12 col-sm-6">    
    <div class="form-group">
      {!! Form::label('name', 'Nombre*') !!}
      {!! Form::text('name',null, ['class' => $errors->has('name') ? 'form-control error' : 'form-control', 'placeholder' => 'name','required' => true, 'data-rule' => 'required|minlength-3', 'disabled' => $isSuperAdmin && !$isMe]) !!}
      @if ($errors->has('name'))
        <div class="error" data-type="validator-error">{{ $errors->first('name') }}</div>
      @endif
    </div>
  </div>

  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('rut', 'RUT*') !!}
      {!! Form::text('rut',null, ['class' => $errors->has('rut') ? 'form-control text-uppercase error' : 'form-control text-uppercase', 'placeholder' => 'RUT','required' => true, 'data-rule' => 'required|rut', 'disabled' => $isSuperAdmin && !$isMe, 'onkeyup'=>'formatCliente(this)', 'maxlength'=> '12' ]) !!}
      @if ($errors->has('rut'))
        <div class="error" data-type="validator-error">{{ $errors->first('rut') }}</div>
      @endif
    </div>
  </div>

  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('email', 'Email*') !!}
      {!! Form::text('email',null, ['class' => $errors->has('email') ? 'form-control error' : 'form-control', 'placeholder' => 'Email','required' => true, 'data-rule' => 'required|email','disabled' => $isSuperAdmin && !$isMe ]) !!}
      @if ($errors->has('email'))
        <div class="error" data-type="validator-error">{{ $errors->first('email') }}</div>
      @endif
    </div>
  </div>

  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('roles[]', 'Rol*') !!}
      @php
        // Obtiene los roles permitidos para este usuario. Si posee como padre un (client-admin) solo se mostrara el rol 'client'
        $roles = $isClientAdmin ? \App\Models\Role::where('name','client')->pluck('name','id') : \App\Models\Role::all()->pluck('name','id');
      @endphp
      {!! Form::select('roles[]', $roles, null, ['class' => $errors->has('roles') ? 'form-control error' : 'form-control', 'placeholder' => 'Rol','required' => true, 'disabled' => $isSuperAdmin,'data-rule' => 'required' ]) !!}
    </div>
  </div>
  
  @if (Auth::user()->hasRole(['super-admin|client-admin']) && !$isSuperAdmin )
    @php
      // Validación del Password por defecto (:create User)
      $validatorPassword = 'required|minlenght-5';
      // Verifica si no existe el modelo ( si es :create )
      if(isset($model)) {
        // El password lo pone null, ya que trae el hash del User.
        $model->password = null;
        // Cambia la validación por defecto (:create User) por la de (:Update User)
        $validatorPassword = 'minlenght-5';
      }
    @endphp
    <div class="col-12">
      <div class="row">
        <div class="col-12 col-sm-6">
          <div class="form-group">
            {!! Form::label('password', 'Contraseña') !!}
            <input type="password" class="form-control {{ $errors->has('password') ?? 'error'}}" placeholder="Contraseña" data-rule='{{$validatorPassword}}' value="{{ old('password') }}" name="password" id="password">
            {{-- {!! Form::text('password',null, ['class' => $errors->has('password') ? 'form-control error' : 'form-control', 'placeholder' => 'Contraseña','required' => true, 'data-rule' => $validatorPassword]) !!} --}}
            @if ($errors->has('password'))
              <div class="error" data-type="validator-error">{{ $errors->first('password') }}</div>
            @endif
          </div>
        </div>

        <div class="col-12 col-sm-6">
          <div class="form-group">
            {!! Form::label('password_confirmation', 'Confirmacion contraseña') !!}
            <input type="password" class="form-control {{ $errors->has('password_confirmation') ?? 'error'}}" placeholder="Contraseña" data-rule='{{$validatorPassword}}' value="{{ old('password_confirmation') }}" name="password_confirmation" id="password_confirmation">
            {{-- {!! Form::text('password_confirmation',null, ['class' => $errors->has('password_confirmation') ? 'form-control error' : 'form-control', 'placeholder' => 'Contraseña','required' => true, 'data-rule' => $validatorPassword]) !!} --}}
            @if ($errors->has('password_confirmation'))
              <div class="error" data-type="validator-error">{{ $errors->first('password_confirmation') }}</div>
            @endif
          </div>
        </div>
      </div>
    </div>
  @endif
  
  @if (isset($model) && $parent)
  <div class="col-12">
    <div class="form-group">
      <div class="alert alert-warning alert-dismissible fade show mb-0" role="alert">
        Este usuario, <b>{{ $model->name }}</b>. Fue creado por <b> <a href="{{ route($class_basename.'.edit',$parent->id) }}" class="text-white"><u>{{ $parent->name }}</u></a> </b> así que solo posee los permisos y empresas de él.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    </div>
  </div>
  @endif
  @php
    $empresas = $isClientAdmin ? Auth::user()->empresas : \App\Models\AssetManagement\Empresa::all();

    $permissions = [];

    $empresasSelect = [];

    $modulos = \App\Models\Permission::all()->where('module','!=',NULL)->where('module','!=','Empresa');

    // Si tiene parent (Client-admin) se obtiene los permisos de los Modulos.
    $modulos = $isClientAdmin ? $parent->permissions->where('module','!=',NULL)->where('module','!=','Empresa') : $modulos;

    // Si existe el model es :Update. De lo contrarío es :Create.
    if(isset($model)) {
      $empresasSelect = $isSuperAdmin ? $empresas : $model->empresas;
      $empresasSelect = $empresasSelect->pluck('id')->toArray();
      // Se obtiene los permisos del model(Usuario a edit)
      $permissions = $model->permissions->pluck('name')->toArray();
    }

    // old('permissions') obtiene los permisos del FormRequest (al dar aceptar y arroja error retordas los permisos que se selecciono). sino, los permisos que tenía.
    $empresasSelect = old('empresas') ? old('empresas') : $empresasSelect;

    $permissions = old('permissions') ? old('permissions') : $permissions;

  @endphp

  @if (count($empresas))
    <div class="col-12">
      <div class="form-group treeMultiselect--max">
        {!! Form::label('empresas[]', 'Empresas') !!}
        <select class="treeMultiselect" multiple="multiple" name="empresas[]">
          @foreach ($empresas as $empresa)
            <option value="{{$empresa->id}}" data-section="Empresas"
              {{ in_array($empresa->id, $empresasSelect) ? "selected" : '' }}>
              {{ $empresa->label ? $empresa->label : $empresa->nombre_xls }}
            </option>
          @endforeach
        </select>
      </div>
    </div>
  @endif

  <div class="col-12">
    <div class="form-group treeMultiselect--max">
      {!! Form::label('permissions[]', 'Permisos') !!}
      <select class="permissionModulo" multiple="multiple" name="permissions[]">
        @foreach ($modulos as $permission)
          <option value="{{$permission->name}}" 
              {{ in_array($permission->name,$permissions) ? "selected" : '' }}
              data-index="{{$permission->id}}">
            {{ $permission->display_name }}
          </option>
        @endforeach
      </select>
    </div>
  </div>

  <div class="col-12 mt-3">
    {!! link_to_route($class_basename.'.index', Lang::get('crud.back'),[], ['class'=>'btn btn-outline-danger btn-sm text-uppercase load-link']); !!}
    @if(!$isSuperAdmin || $isMe)
      {!! Form::submit(Lang::get('crud.save'),['class' => 'btn btn-primary btn-sm text-uppercase']) !!}
    @endif
  </div>
</div>

@multiselect
  <script>
    $(".permissionModulo").treeMultiselect({
      searchable: true
    });
    
    $(".treeMultiselect").treeMultiselect({
      searchable: true,
      freeze: {{ $isSuperAdmin ? 'true' : 'false' }}
    });
  </script>
@endmultiselect

@section('scripts')
  <script src="{{ asset('plugins/js-form-validator-2.1/js-form-validator.js') }}"></script>
  <script>
    var f = document.querySelector('.form-validator');
    if(f){
      f.setAttribute('novalidate', true);
      var options = {
        rules: {
          rut: function (value) {
            return checkRut(value);
          }
        },
        messages: {
          es: {
            rut: {
              incorrect: 'RUT INVALIDO'
            }
          }
        },
        locale: "{{ session()->has('locale') ? session()->get('locale') : 'es' }}"
      };

      new Validator(f, function (err, res) {
        return res;
      },options);
    }
  </script>
@endsection