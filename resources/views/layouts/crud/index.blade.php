@extends('layouts.app')

@content
  @slot('breadcrumbItems')
    <li class="breadcrumb-item active" aria-current="page">
      <span class="text-uppercase">
        {{ AppHelpers::trans_choice($class_basename,1) }}  - 
      </span>
      <span class="text-lowercase text-uppercase">
        @lang('crud.new')
      </span>
    </li>
  @endslot

  @slot('title')
    <div class="row align-items-center">
      <div class="col">
        <h1 class="display-6 text-uppercase">
          {{ AppHelpers::trans_choice($class_basename,1) }}
        </h1>
      </div>
      <div class="col-auto">
        <a href="{{ route($class_basename.'.create') }}" class="btn btn-danger btn-sm load-link">
          @lang('crud.new')
        </a>
      </div>
    </div>
  @endslot

  <div class="row">
    <div class="col-12 mb-4">
      {{$dataTable->table(['id' => 'datatable'])}}
    </div>
  </div>
@endcontent

@section('scripts')
  @parent
  @jqueryConfirm
  @datatables
  <script>
    $(function() {
      {{$dataTable->generateScripts()}}
    })
  </script>
@endsection