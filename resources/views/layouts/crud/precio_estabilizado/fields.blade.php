<div class="row">
  @php
    $barras =  \App\Models\Barras::all();
    $oldBarra = isset($model) ? $model->id_barra : null;
    $oldBarra = old('id_barra') ? old('id_barra') : $oldBarra;
  @endphp
  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('id_barra', 'ID Barras') !!}
      <select class="form-control" name="id_barra" data-rule="required">
        @foreach ($barras as $barra)
          <option value="{{$barra->id}}" {{ $barra->id === $oldBarra ? "selected" : '' }}>
            {{ $barra->barra_xls }}
          </option>
        @endforeach
      </select>
      @if ($errors->has('id_barra'))
        <div class="error" data-type="validator-error">{{ $errors->first('id_barra') }}</div>
      @endif
    </div>
  </div>

  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('precio_energia', 'Precio energía*') !!}
      {!! Form::text('precio_energia',null, ['class' => $errors->has('precio_energia') ? 'form-control error' : 'form-control', 'placeholder' => 'precio_energia','required' => true, 'data-rule' => 'required']) !!}
      @if ($errors->has('precio_energia'))
        <div class="error" data-type="validator-error">{{ $errors->first('precio_energia') }}</div>
      @endif
    </div>
  </div>

  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('precio_potencia', 'Precio potencia*') !!}
      {!! Form::text('precio_potencia',null, ['class' => $errors->has('precio_potencia') ? 'form-control error' : 'form-control', 'placeholder' => 'precio_potencia','required' => true, 'data-rule' => 'required']) !!}
      @if ($errors->has('precio_potencia'))
        <div class="error" data-type="validator-error">{{ $errors->first('precio_potencia') }}</div>
      @endif
    </div>
  </div>

  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('dates', 'Fechas*') !!}
      {!! Form::text('dates', null , ['class' => $errors->has('final_date') || $errors->has('inicial_date') ? 'form-control error' : 'form-control', 'placeholder' => 'Y-m-d','required' => true, 'data-rule' => 'required']) !!}
      @if ($errors->has('final_date') || $errors->has('inicial_date'))
        <div class="error" data-type="validator-error">{{ $errors->first('final_date') }}</div>
        <div class="error" data-type="validator-error">{{ $errors->first('inicial_date') }}</div>
      @endif
    </div>
  </div>

  <div class="col-12 mt-3">
    {!! link_to_route('precio_estabilizado.index', Lang::get('crud.back'),[], ['class'=>'btn btn-outline-danger btn-sm text-uppercase load-link']); !!}
    {!! Form::submit(Lang::get('crud.save'),['class' => 'btn btn-primary btn-sm text-uppercase']) !!}
  </div>

  	@php
      if (isset($model)) {
		    $startDate = old('inicial_date') ? old('inicial_date') : date('d/m/Y H:i:s', strtotime($model->inicial_date));
		    $endtDate = old('final_date') ? old('final_date') : date('d/m/Y H:i:s', strtotime($model->final_date));
      } else {
        $startDate = old('inicial_date') ? old('inicial_date') : date('d/m/Y');
        $endtDate = old('final_date') ? old('final_date') : date('d/m/Y');
      }
  	@endphp
</div>

@section('scripts')
  <script src="{{ asset('plugins/js-form-validator-2.1/js-form-validator.js') }}"></script>
  @component('layouts.partials.assets.components.datapicker')
  <script>
    /*
     Documentacion màs detallada: https://www.daterangepicker.com/ 
    */
    $('input[name="dates"]').daterangepicker({
      timePicker: true,
      timePicker24Hour: true,
      showDropdowns: true,
      minYear: 2010, // El año mminimo.
      startDate: "{{ $startDate }}", // Obtiene la fecha actual
      endDate: "{{ $endtDate }}", // El fin de fecha es un dia adicional a la fecha actual.
      autoclose: false,
      locale: {
        format: 'DD/M/Y HH:mm',
          separator: " - ",
          applyLabel: "Aplicar",
          cancelLabel: "Cancelar",
          fromLabel: "DE",
          toLabel: "HASTA",
          customRangeLabel: "Custom",
          daysOfWeek: [
              "Dom",
              "Lun",
              "Mar",
              "Mie",
              "Jue",
              "Vie",
              "Sáb"
          ],
          monthNames: [
              "Enero",
              "Febrero",
              "Marzo",
              "Abril",
              "Mayo",
              "Junio",
              "Julio",
              "Agosto",
              "Septiembre",
              "Octubre",
              "Noviembre",
              "Diciembre"
          ],
          firstDay: 1
      },
    });
  </script>
  @endcomponent
  <script>
    var f = document.querySelector('.form-validator');
    if(f){
      f.setAttribute('novalidate', true);
      var options = {
        locale: "{{ session()->has('locale') ? session()->get('locale') : 'es' }}"
      };

      new Validator(f, function (err, res) {
        return res;
      },options);
    }
  </script>
@endsection