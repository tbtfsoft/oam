<div class="row">
  @php
    $oldDB = isset($model) ? $model->db : old('db');
  @endphp
  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('db', 'Base de datos') !!}
      {!! Form::text('db', null, ['class' => $errors->has('db') ? 'form-control error' : 'form-control', 'placeholder' => 'Base de datos','required' => true, 'data-rule' => 'required|minlength-1']) !!}
      {{-- <select class="form-control" name="db" data-rule="required">
        <option value="PARKS" {{"PARKS" == $oldDB ? "selected" : ''}}>PARKS</option>
        <option value="PARKS2" {{"PARKS2" == $oldDB ? "selected" : ''}}>PARKS2</option>
      </select> --}}
    </div>
  </div>

  @php
    $empresas =  \App\Models\AssetManagement\Empresa::all();
    $oldEmpresa = old('id_empresa') ? old('id_empresa') : [];
  @endphp

  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('empresa', 'Empresa') !!}
      <select class="form-control tail-select" name="id_empresa" data-rule="required">
        @foreach ($empresas as $empresa)
          <option value="{{$empresa->id}}" {{ isset($model->id_empresa) && $empresa->id === $model->id_empresa ? "selected" : '' }}>
            {{ $empresa->nombre_xls }}
          </option>
        @endforeach
      </select>

      @if ($errors->has('empresa'))
        <div class="error" data-type="validator-error">{{ $errors->first('empresa') }}</div>
      @endif
    </div>
  </div>

  {{-- id_parq --}}
  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('id_parq', 'ID PARQ*') !!}
      {!! Form::text('id_parq', null, ['class' => $errors->has('id_parq') ? 'form-control error' : 'form-control', 'placeholder' => 'ID_PARQ','required' => true, 'data-rule' => 'required|minlength-1']) !!}
      @if ($errors->has('id_parq'))
        <div class="error" data-type="validator-error">{{ $errors->first('id_parq') }}</div>
      @endif
    </div>
  </div>

  {{-- id_medidor --}}
  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('id_medidor', 'ID MEDIDOR*') !!}
      {!! Form::text('id_medidor', null, ['class' => $errors->has('id_disp') ? 'form-control error' : 'form-control', 'placeholder' => 'id_medidor','required' => true, 'data-rule' => 'required|minlength-1']) !!}
      @if ($errors->has('id_medidor'))
        <div class="error" data-type="validator-error">{{ $errors->first('id_medidor') }}</div>
      @endif
    </div>
  </div>

  {{-- id_radsensor --}}
  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('id_radsensor', 'ID RADSENSOR*') !!}
      {!! Form::text('id_radsensor', null, ['class' => $errors->has('id_radsensor') ? 'form-control error' : 'form-control', 'placeholder' => 'id_radsensor', 'required' => true, 'data-rule' => 'required|minlength-1']) !!}
      @if ($errors->has('id_radsensor'))
        <div class="error" data-type="validator-error">{{ $errors->first('id_radsensor') }}</div>
      @endif
    </div>
  </div>

  {{-- host --}}
  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('host', 'Host') !!}
      {!! Form::text('host', null, ['class' => $errors->has('host') ? 'form-control error' : 'form-control', 'placeholder' => 'host', 'required' => true, 'data-rule' => 'required|minlength-1']) !!}
      @if ($errors->has('host'))
        <div class="error" data-type="validator-error">{{ $errors->first('host') }}</div>
      @endif
    </div>
  </div>

  {{-- Nombre'Parque --}}
  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('nombre_parq', 'NOMBRE_PARQ*') !!}
      {!! Form::text('nombre_parq', null, ['class' => $errors->has('nombre_parq') ? 'form-control error' : 'form-control', 'placeholder' => 'nombre_parq', 'required' => true, 'data-rule' => 'required|minlength-1']) !!}
      @if ($errors->has('nombre_parq'))
        <div class="error" data-type="validator-error">{{ $errors->first('nombre_parq') }}</div>
      @endif
    </div>
  </div>

  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('db_prmte', 'DB PRMTE') !!}
      {!! Form::text('db_prmte', null, ['class' => $errors->has('db_prmte') ? 'form-control error' : 'form-control', 'placeholder' => 'DB PRMTE', 'data-rule' => 'minlength-1']) !!}
    </div>
  </div>

  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('id_parq_prmte', 'ID_PARQ_PRMTE') !!}
      {!! Form::text('id_parq_prmte', null, ['class' => $errors->has('id_parq_prmte') ? 'form-control error' : 'form-control', 'placeholder' => 'ID_PARQ_PRMTE', 'data-rule' => 'minlength-1']) !!}
    </div>
  </div>

  <div class="col-12 col-sm-6">
    <div class="form-group">
<<<<<<< HEAD
      {!! Form::label('id_medidor_prmte', 'ID_PARQ_PRMTE') !!}
=======
      {!! Form::label('id_medidor_prmte', 'ID_MEDIDOR_PRMTE') !!}
>>>>>>> 6bba57b09923102656598136b90d82f9927ccb19
      {!! Form::text('id_medidor_prmte', null, ['class' => $errors->has('id_medidor_prmte') ? 'form-control error' : 'form-control', 'placeholder' => 'ID_MEDIDOR_PRMTE', 'data-rule' => 'minlength-1']) !!}
    </div>
  </div>

  <div class="col-12 mt-3">
    {!! link_to_route($class_basename.'.index', Lang::get('crud.back'),[], ['class'=>'btn btn-outline-danger btn-sm text-uppercase load-link']); !!}
    {!! Form::submit(Lang::get('crud.save'),['class' => 'btn btn-primary btn-sm text-uppercase']) !!}
  </div>

</div>

@section('scripts')
  <script src="{{ asset('plugins/js-form-validator-2.1/js-form-validator.js') }}"></script>
  <script>
    var f = document.querySelector('.form-validator');
    if(f){
      f.setAttribute('novalidate', true);
      var options = {
        locale: "{{ session()->has('locale') ? session()->get('locale') : 'es' }}"
      };
      new Validator(f, function (err, res) {
        return res;
      },options);
    }
  </script>
@endsection
