@inject('modules', 'App\Injections\PermissionTrait')

<div class="row">

  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('name', 'Nombre*') !!}
      {!! Form::text('name', null, ['class' => $errors->has('name') ? 'form-control error' : 'form-control', 'placeholder' => 'name','required' => true, 'data-rule' => 'required']) !!}
      @if ($errors->has('name'))
        <div class="error" data-type="validator-error">{{ $errors->first('name') }}</div>
      @endif
    </div>
  </div>

  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('display_name', 'Display Name*') !!}
      {!! Form::text('display_name', null, ['class' => $errors->has('display_name') ? 'form-control error' : 'form-control', 'placeholder' => 'Display Name','required' => true, 'data-rule' => 'required']) !!}
      @if ($errors->has('display_name'))
        <div class="error" data-type="validator-error">{{ $errors->first('display_name') }}</div>
      @endif
    </div>
  </div>

  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('module', 'Module*') !!}
      {!! Form::select('module', $modules->modules(), null, ['class' => $errors->has('module') ? 'form-control error' : 'form-control', 'placeholder' => 'Modulo','required' => true, 'data-rule' => 'required']) !!}
      @if ($errors->has('module'))
        <div class="error" data-type="validator-error">{{ $errors->first('module') }}</div>
      @endif
    </div>
  </div>

  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('description', 'Descripcion') !!}
      {!! Form::text('description', null, ['class' => $errors->has('description') ? 'form-control error' : 'form-control', 'placeholder' => 'Descripcion','required' => true, 'data-rule' => 'required']) !!}
      @if ($errors->has('description'))
        <div class="error" data-type="validator-error">{{ $errors->first('description') }}</div>
      @endif
    </div>
  </div>


  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('guard_name', 'guard_name*') !!}
      {!! Form::text('guard_name','web', ['class' => $errors->has('guard_name') ? 'form-control error' : 'form-control', 'placeholder' => 'Guard Name','required' => true, 'readonly', 'data-rule' => 'required']) !!}
      @if ($errors->has('guard_name'))
        <div class="error" data-type="validator-error">{{ $errors->first('guard_name') }}</div>
      @endif
    </div>
  </div>

  <div class="col-12 mt-3">
    {!! link_to_route($class_basename.'.index', Lang::get('crud.back'),[], ['class'=>'btn btn-outline-danger btn-sm text-uppercase load-link']); !!}
    {!! Form::submit(Lang::get('crud.save'),['class' => 'btn btn-primary btn-sm text-uppercase']) !!}
  </div>
</div>

@section('scripts')
  <script src="{{ asset('plugins/js-form-validator-2.1/js-form-validator.js') }}"></script>
  <script>
    var f = document.querySelector('.form-validator');
    if(f){
      f.setAttribute('novalidate', true);
      var options = {
        rules: {
          rut: function (value) {
            return checkRut(value);
          }
        },
        locale: "{{ session()->has('locale') ? session()->get('locale') : 'es' }}"
      };

      new Validator(f, function (err, res) {
        return res;
      },options);
    }
  </script>
@endsection