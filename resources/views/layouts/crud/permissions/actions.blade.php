@isset ($class_basename)
	<div class="w-100 text-center">
		<a href="{{ route($class_basename.'.edit',$id) }}" class="btn btn-sm btn-primary load-link" onclick="document.body.classList.add('loading')">
			<i class="fa fa-edit"></i>
		</a>

    <form id="form-destroy_{{$id}}" action="{{ route($class_basename.'.destroy',$id) }}" method="POST" onsubmit="return confirmar(this,event);" class="d-inline">
    	@csrf
    	@method('DELETE')

			<button class="btn btn-sm btn-danger btn-confirm load-link" type="submit" data-toggle="confirmation" data-singleton="true">
				<i class="fa fa-trash"></i>
			</button>
    </form>
	</div>
@endisset
