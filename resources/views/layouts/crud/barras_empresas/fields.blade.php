@inject('modules', 'App\Injections\PermissionTrait')

<div class="row">
  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('id_barras', 'ID Barras') !!}
      {!! Form::select('id_barras', $modules->barras(), null, ['class' => $errors->has('id_barras') ? 'form-control error' : 'form-control', 'placeholder' => 'ID BARRAS','required' => true, 'data-rule' => 'required']) !!}
      @if ($errors->has('id_barras'))
        <div class="error" data-type="validator-error">{{ $errors->first('id_barras') }}</div>
      @endif
    </div>
  </div>

  <div class="col-6">
    <div class="form-group">
      {!! Form::label('id_empresa', 'Empresa') !!}
      {!! Form::select('id_empresa', $modules->empresas(), null, ['class' => $errors->has('id_empresa') ? 'form-control error  tail-select' : 'form-control  tail-select', 'placeholder' => 'EMPRESA','required' => true, 'data-rule' => 'required']) !!}
      @if ($errors->has('empresa'))
        <div class="error" data-type="validator-error">{{ $errors->first('empresa') }}</div>
      @endif
    </div>
  </div>

  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('tipo_calculo', 'Tipo de calculo*') !!}
      {!! Form::text('tipo_calculo', null, ['class' => $errors->has('tipo_calculo') ? 'form-control error' : 'form-control', 'placeholder' => 'tipo_calculo','required' => true, 'data-rule' => 'required']) !!}
      @if ($errors->has('tipo_calculo'))
        <div class="error" data-type="validator-error">{{ $errors->first('tipo_calculo') }}</div>
      @endif
    </div>
  </div>

  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('dates', 'Fechas*') !!}
      {!! Form::text('dates', isset($value) ? $value : null , ['class' => $errors->has('final_date') || $errors->has('inicial_date') ? 'form-control error' : 'form-control', 'placeholder' => 'Y-m-d','required' => true, 'data-rule' => 'required']) !!}
      @if ($errors->has('final_date') || $errors->has('inicial_date'))
        <div class="error" data-type="validator-error">{{ $errors->first('final_date') }}</div>
        <div class="error" data-type="validator-error">{{ $errors->first('inicial_date') }}</div>
      @endif
    </div>
  </div>

  <div class="col-12 mt-3">
    {!! link_to_route('barras_empresas.index', Lang::get('crud.back'),[], ['class'=>'btn btn-outline-danger btn-sm text-uppercase load-link']); !!}
    {!! Form::submit(Lang::get('crud.save'),['class' => 'btn btn-primary btn-sm text-uppercase']) !!}
  </div>
</div>

@section('scripts')
  @component('layouts.partials.assets.components.datapicker')
  @php
    if (isset($model)) {
      $startDate = old('inicial_date') ? old('inicial_date') : date('d/m/Y H:i:s', strtotime($model->inicial_date));
      $endtDate = old('final_date') ? old('final_date') : date('d/m/Y H:i:s', strtotime($model->final_date));
    } else {
      $startDate = old('inicial_date') ? old('inicial_date') : date('d/m/Y');
      $endtDate = old('final_date') ? old('final_date') : date('d/m/Y');
    }
  @endphp
  <script>
    /*
     Documentacion màs detallada: https://www.daterangepicker.com/ 
    */
    $('input[name="dates"]').daterangepicker({
      timePicker: true,
      timePicker24Hour: true,
      showDropdowns: true,
      minYear: 2010, // El año mminimo.
      startDate: '{{ $startDate }}', // Obtiene la fecha actual
      endDate: '{{ $endtDate }}', // El fin de fecha es un dia adicional a la fecha actual.
      autoclose: false,
      locale: {
        format: 'DD/M/Y HH:mm',
          separator: " - ",
          applyLabel: "Aplicar",
          cancelLabel: "Cancelar",
          fromLabel: "DE",
          toLabel: "HASTA",
          customRangeLabel: "Custom",
          daysOfWeek: [
              "Dom",
              "Lun",
              "Mar",
              "Mie",
              "Jue",
              "Vie",
              "Sáb"
          ],
          monthNames: [
              "Enero",
              "Febrero",
              "Marzo",
              "Abril",
              "Mayo",
              "Junio",
              "Julio",
              "Agosto",
              "Septiembre",
              "Octubre",
              "Noviembre",
              "Diciembre"
          ],
          firstDay: 1
      },
    });
  </script>
  @endcomponent
  <script src="{{ asset('plugins/js-form-validator-2.1/js-form-validator.js') }}"></script>
  <script>
    var f = document.querySelector('.form-validator');
    if(f){
      f.setAttribute('novalidate', true);
      var options = {
        locale: "{{ session()->has('locale') ? session()->get('locale') : 'es' }}"
      };

      new Validator(f, function (err, res) {
        return res;
      },options);
    }
  </script>
@endsection