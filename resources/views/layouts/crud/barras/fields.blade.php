<div class="row">

  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('barra_xls', 'BARRA XLS*') !!}
      {!! Form::text('barra_xls',null, ['class' => $errors->has('barra_xls') ? 'form-control error' : 'form-control', 'placeholder' => 'barra_xls','required' => true, 'data-rule' => 'required']) !!}
      @if ($errors->has('barra_xls'))
        <div class="error" data-type="validator-error">{{ $errors->first('barra_xls') }}</div>
      @endif
    </div>
  </div>

  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('barra_human', 'Barra human*') !!}
      {!! Form::text('barra_human',null, ['class' => $errors->has('barra_human') ? 'form-control error' : 'form-control', 'placeholder' => 'barra_human','required' => true, 'data-rule' => 'required']) !!}
      @if ($errors->has('barra_human'))
        <div class="error" data-type="validator-error">{{ $errors->first('barra_human') }}</div>
      @endif
    </div>
  </div>

  <div class="col-12 mt-3">
    {!! link_to_route($class_basename.'.index', Lang::get('crud.back'),[], ['class'=>'btn btn-outline-danger btn-sm text-uppercase load-link']); !!}
    {!! Form::submit(Lang::get('crud.save'),['class' => 'btn btn-primary btn-sm text-uppercase']) !!}
  </div>

</div>

@multiselect
  <script>
    $(".treeMultiselect").treeMultiselect({
      searchable: true
    });
  </script>
@endmultiselect

@section('scripts')
  <script src="{{ asset('plugins/js-form-validator-2.1/js-form-validator.js') }}"></script>
  <script>
    var f = document.querySelector('.form-validator');
    if(f){
      f.setAttribute('novalidate', true);
      var options = {
        locale: "{{ session()->has('locale') ? session()->get('locale') : 'es' }}"
      };

      new Validator(f, function (err, res) {
        return res;
      },options);
    }
  </script>
@endsection