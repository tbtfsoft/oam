{{-- 
  periodo
  tipo_doc
  tipo_item
  glosa
  folio
  fecha
--}}
<div class="row">

  {{-- periodo --}}
  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('periodo', 'Período*') !!}
      {!! Form::text('periodo', null, ['class' => $errors->has('periodo') ? 'form-control error' : 'form-control', 'placeholder' => 'periodo','required' => true, 'data-rule' => 'required|minlength-1']) !!}
      @if ($errors->has('periodo'))
        <div class="error" data-type="validator-error">{{ $errors->first('periodo') }}</div>
      @endif
    </div>
  </div>

  {{-- tipo_doc --}}
  @php
    $typeDoc = old('tipo_doc') ? old('tipo_doc') : isset($model->tipo_doc) ? $model->tipo_doc : null;
  @endphp
  @SelectTypeDocument(['class'=>"col-12 col-sm-12", 'selected' =>  $typeDoc, 'name' => 'tipo_doc', 'classSelect' => 'form-control'])

  @php
    $tipo_item = old('tipo_item') ? old('tipo_item') : isset($model->tipo_item) ? $model->tipo_item : null;
  @endphp
  @SelectModules(['class'=>"col-12 col-sm-6", 'selected' =>  $tipo_item, 'name' => 'tipo_item', 'type' => 'select', 'classSelect' => 'form-control', 'modules' => [
    [
      'label' => 'Energía',
      'value' => 1
    ],
    [
      'label' => 'Potencía',
      'value' => 2
    ],
    [
      'label' => 'Servicios complementarios',
      'value' => 3
    ]
  ]])

  {{-- glosa --}}
  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('glosa', 'Glosa*') !!}
      {!! Form::text('glosa', null, ['class' => $errors->has('glosa') ? 'form-control error' : 'form-control', 'placeholder' => 'glosa','required' => true, 'data-rule' => 'required|minlength-1']) !!}
      @if ($errors->has('glosa'))
        <div class="error" data-type="validator-error">{{ $errors->first('glosa') }}</div>
      @endif
    </div>
  </div>

  {{-- folio --}}
  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('folio', 'Folio*') !!}
      {!! Form::text('folio', null, ['class' => $errors->has('folio') ? 'form-control error' : 'form-control', 'placeholder' => 'folio','required' => true, 'data-rule' => 'required|minlength-1']) !!}
      @if ($errors->has('folio'))
        <div class="error" data-type="validator-error">{{ $errors->first('folio') }}</div>
      @endif
    </div>
  </div>

  {{-- fecha --}}
  @php
    $fecha = isset($model) ? date('d/m/Y', strtotime($model->fecha)) : date('d/m/Y');
    $fecha = old('fecha') ? old('fecha') : $fecha;
  @endphp
  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('fecha', 'Fecha*') !!}
      {!! Form::text('fecha', $fecha, ['class' => $errors->has('fecha') ? 'form-control error' : 'form-control', 'placeholder' => 'fecha','required' => true, 'data-rule' => 'required|minlength-1']) !!}
      @if ($errors->has('fecha'))
        <div class="error" data-type="validator-error">{{ $errors->first('fecha') }}</div>
      @endif
    </div>
  </div>

  <div class="col-12 mt-3">
    {!! link_to_route($class_basename.'.index', Lang::get('crud.back'),[], ['class'=>'btn btn-outline-danger btn-sm text-uppercase load-link']); !!}
    {!! Form::submit(Lang::get('crud.save'),['class' => 'btn btn-primary btn-sm text-uppercase']) !!}
  </div>
</div>

@section('scripts')
  @parent
  @component('layouts.partials.assets.components.datapicker')
  @endcomponent
  <script>
    $(function() {
      $('input[name="fecha"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        autoclose: true,
        locale: {
          format: 'DD/M/Y',
          separator: " - ",
          applyLabel: "Aplicar",
          cancelLabel: "Cancelar",
          fromLabel: "DE",
          toLabel: "HASTA",
          customRangeLabel: "Custom",
          daysOfWeek: [
              "Dom",
              "Lun",
              "Mar",
              "Mie",
              "Jue",
              "Vie",
              "Sáb"
          ],
          monthNames: [
              "Enero",
              "Febrero",
              "Marzo",
              "Abril",
              "Mayo",
              "Junio",
              "Julio",
              "Agosto",
              "Septiembre",
              "Octubre",
              "Noviembre",
              "Diciembre"
          ],
          firstDay: 1
        }
      });
    });
  </script>
  <script src="{{ asset('plugins/js-form-validator-2.1/js-form-validator.js') }}"></script>
  <script>
    var f = document.querySelector('.form-validator');
    if(f){
      f.setAttribute('novalidate', true);
      var options = {
        locale: "{{ session()->has('locale') ? session()->get('locale') : 'es' }}"
      };
      new Validator(f, function (err, res) {
        return res;
      },options);
    }
  </script>
@endsection