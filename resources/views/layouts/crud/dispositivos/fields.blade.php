<div class="row">

  <div class="col-12 col-sm-6">
    <div class="form-group">
      {!! Form::label('id_disp', 'ID DISPOSITIVO*') !!}
      {!! Form::text('id_disp',null, ['class' => $errors->has('id_disp') ? 'form-control error' : 'form-control', 'placeholder' => 'id_disp','required' => true, 'data-rule' => 'required']) !!}
      @if ($errors->has('id_disp'))
        <div class="error" data-type="validator-error">{{ $errors->first('id_disp') }}</div>
      @endif
    </div>
  </div>

  @php
    $tipos =  \App\Models\Permission::all()->where('module','!=',NULL)->where('module','!=','Empresa');
    $oldTipos = old('permissions') ? old('permissions') : [];
  @endphp
  <div class="col-6">
    <div class="form-group">
      {!! Form::label('tipo', 'Permisos') !!}
      <select class="form-control" name="tipo" data-rule="required">
        @foreach ($tipos as $tipo)
          <option value="{{$tipo->name}}" {{in_array($tipo->name,$oldTipos) ? "selected" : '' }}>
            {{ $tipo->display_name }}
          </option>
        @endforeach
      </select>
      @if ($errors->has('tipo'))
        <div class="error" data-type="validator-error">{{ $errors->first('tipo') }}</div>
      @endif
    </div>
  </div>

  <div class="col-12 mt-3">
    {!! link_to_route($class_basename.'.index', Lang::get('crud.back'),[], ['class'=>'btn btn-outline-danger btn-sm text-uppercase load-link']); !!}
    {!! Form::submit(Lang::get('crud.save'),['class' => 'btn btn-primary btn-sm text-uppercase']) !!}
  </div>

</div>

@multiselect
  <script>
    $(".treeMultiselect").treeMultiselect({
      searchable: true
    });
  </script>
@endmultiselect

@section('scripts')
  <script src="{{ asset('plugins/js-form-validator-2.1/js-form-validator.js') }}"></script>
  <script>
    var f = document.querySelector('.form-validator');
    if(f){
      f.setAttribute('novalidate', true);
      var options = {
        locale: "{{ session()->has('locale') ? session()->get('locale') : 'es' }}"
      };

      new Validator(f, function (err, res) {
        return res;
      },options);
    }
  </script>
@endsection