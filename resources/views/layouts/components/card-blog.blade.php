<!-- {{$title}} -->
<figure class="card__blog col col-12 col-sm-4">
	<div class="row">
		<div class="col-12"><img class="lazy" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="{{ $img }}" width="100%" alt="image_post_net_billing"/> </div>
		<div class="col-12">	
			<div class="date"><small class="day text-uppercase">{{ date('M d, Y',strtotime($date)) }}</small></div>
			<figcaption>
		    	<a href="{{ $route or url('error404') }}"><h4 class="border__bottom">{{$title}}</h4></a>
		    	{{$slot}}
		    	<div class="w-100">
		    		<a href="{{ $route or url('error404') }}" class="btn btn-outline-primary rounded-0 pull-right">@lang('app.read_more')</a>
		    	</div>
			</figcaption>
		</div>
	</div>
</figure>
<!-- {{$title}}-END -->