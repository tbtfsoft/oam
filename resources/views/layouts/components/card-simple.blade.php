<div class="col-12 col-sm-6 mb-3">
	<div class="row align-items-center">
		@if (isset($inv))
			<div class="col-12 col-md-8"><div class="service-content text-right">{{$slot}}</div></div>
			<div class="col text-center"><img src="{{ asset($img) }}" alt="apreton-de-manos" width="90px" height="90px"></div>
		@else
			<div class="col text-center"><img src="{{ asset($img) }}" alt="apreton-de-manos" width="90px" height="90px"></div>
			<div class="col-12 col-md-8"><div class="service-content">{{$slot}}</div></div>
		@endif
	</div>
</div>