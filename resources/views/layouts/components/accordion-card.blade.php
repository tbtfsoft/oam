@php
	$show  = isset($show)? $show : false
@endphp

<div class="accordion-title" id="{{$accordion}}_{{$num}}">
	<button class="{{ $show ? null : 'collapsed'}}" data-toggle="collapse" data-target="#{{$accordion}}collapse{{$num}}"
	 aria-expanded="{{ $show ? true : false}}" aria-controls="{{$accordion}}collapse{{$num}}">
	  {{ $title }}
	</button>
</div>

<div id="{{$accordion}}collapse{{$num}}" class="collapse {{ $show ? 'show' : ''}}" aria-labelledby="{{$accordion}}_{{$num}}" data-parent="#{{$accordion}}">
	<div class="card-body">
    	{{$slot}}
	</div>
</div>