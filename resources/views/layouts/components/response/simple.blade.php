<div class="jumbotron bg-{{$data['type']}} {{ $data['type'] == 'danger' ? 'text-white' : '' }} py-3 px-4" id="warning-energia">
	@if ( isset($data['title']) && !is_null($data['title']))
  	<h4 class="my-0">
			{{$data['title']}}
  	</h4>
  	<hr class="my-1">
  @endif
  <p class="my-0">
  	@isset ($data['message'])
  		{!! $data['message'] !!}
  	@endisset
  </p>
</div>