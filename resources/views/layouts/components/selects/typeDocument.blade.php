@php
    $documents = App\Models\AssetManagement\TipoDocCen::all();
    $selected = isset($selected) ? $selected : Input::get('tipo_doc_cen');
@endphp
@auth
    <div class="{{ $class ?? null }} w-100">
        <div class="form-group w-100">
            {!! Form::label('SelectDocumento', __('app.tipo de documento')) !!}
            <select class="{{ isset($classSelect) ? $classSelect : 'tail-select' }} w-100" name="{{ isset($name) ? $name : 'tipo_doc_cen' }}" id="{{ isset($name) ? $name : 'tipo_doc_cen' }}" required data-rule='required'>
                <option value="" class="text-truncate"><span class="d-none">{{__('app.tipo de documento')}}</span></option>
                @foreach ($documents as $element)
                    <option value="{{ $element->id }}"
                            class="text-truncate"
                            {{ $element->id == $selected ? 'selected' : '' }}>
                            {{ $element->nombre }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
@endauth
