@php
    $meses = Array(
        array('id' => '01','name' => __('app.enero')),
        array('id' => '02','name' => __('app.febrero')),
        array('id' => '03','name' => __('app.marzo')),
        array('id' => '04','name' => __('app.abril')),
        array('id' => '05','name' => __('app.mayo')),
        array('id' => '06','name' => __('app.junio')),
        array('id' => '07','name' => __('app.julio')),
        array('id' => '08','name' => __('app.agosto')),
        array('id' => '09','name' => __('app.septiembre')),
        array('id' => '10','name' => __('app.octubre')),
        array('id' => '11','name' => __('app.noviembre')),
        array('id' => '12','name' => __('app.diciembre'))
    );
    $selected = isset($selected) ? $selected : Input::get('mes');
@endphp
<div class="{{ $class ?? null }} w-100">
    <div class="form-group w-100">
        <label for="mes" class="w-100">{{__('app.mes') }}</label>
        <select id="mes" name="mes" class="selectpicker form-control" data-live-search="true" required data-rule="required">
            <option value="">{{__('app.mes') }}</option>
            @foreach($meses as $mes)
                <option value="{!!$mes['id']!!}" {{ $mes['id'] == $selected ? 'selected' : '' }}>{!!$mes['name']!!}</option>
            @endforeach
        </select>
    </div>
</div>