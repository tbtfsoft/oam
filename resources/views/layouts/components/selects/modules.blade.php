@php
  $modules = isset($modules) ? $modules : [
    [
      'label' => __('app.Energía'),
      'value' => 1,
    ],
    [
      'label' => __('app.Potencia'),
      'value' => 2,
    ]
  ];
  $selectedModules = Input::has('modules') ? Input::get('modules') : [1,2,3];
  $label = isset($label) ? $label : __('app.Concepto');
@endphp
@if (isset($type) && $type == 'select')
  <div class="{{ $class ?? null }}">
    <div class="form-group">
      {!! Form::label('SelectModules', $label) !!}
      <select class="{{ isset($classSelect) ? $classSelect : 'tail-select' }} w-100" name="{{ isset($name) ? $name : 'module' }}" id="{{ isset($name) ? $name : 'module' }}" required data-rule="required">
          @foreach ($modules as $element)
              <option value="{{ $element['value'] }}" {{ isset($selected) && $selected == $element['value'] ? 'selected' : null }}>
                {{ $element['label'] }}
              </option>
          @endforeach
      </select>
    </div>
  </div>
@else
  <div class="col-12">
    <div class="row">
      @foreach ($modules as $module)
        <div class="col-auto {{ isset($module['hidden']) ? "hidden" : '' }}">
          <div class="form-check form-check-inline ml-0" style="line-height: 1">
            <input class="form-check-input" type="checkbox" name="modules[]" {{ in_array($module['value'],$selectedModules) ? 'checked' : '' }} value="{{ $module['value'] }}" id="{{ $module['label'] }}" {{ isset($module['disabled']) && $module['disabled'] ? 'disabled' : '' }}>
            <label class="form-check-label" for="{{ $module['label'] }}">{{ $module['label'] }}</label>
          </div>
        </div>
      @endforeach
    </div>
  </div>
@endif

<style>
  .hidden {
    height: 0;
    width: 0;
    visibility: hidden;
    padding: 0;
  }
</style>