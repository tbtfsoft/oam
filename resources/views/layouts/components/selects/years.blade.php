@php
  $selected = isset($selected) ? $selected : Input::get('anio');
@endphp
<div class="{{ $class ?? null }}">
    <div class="form-group">
        <label for="anio" class="w-100">{{ __('app.año') }}</label>
        <select name="anio" id="{{ isset($id) ? $id : 'anio' }}" class="selectpicker form-control {{ isset($noSearch) ? 'no-search' : '' }}" title="{{__('app.año')}}" required data-rule="required">
            <option value="">{{ __('app.año') }}</option>
            @for ($i = date('Y'); $i > (date('Y')-30); $i--)
                <option value="{!!$i!!}" {{ $i == $selected ? 'selected' : '' }}>
                        {!!$i!!}</option>
            @endfor
        </select>
    </div>
</div>