@php
    $empresas = Auth::user()->hasRole(['backoffice|super-admin'])
                ? App\Models\AssetManagement\Empresa::orderBy('order', 'asc')->get()
                : Auth::user()->empresas;

    $valueDefault = Auth::user()->hasRole(['backoffice|super-admin']) ? null : -1;

    $selected = isset($selected)
                ? $selected
                : Input::get('empresa_xls');

    $label = isset($label) ? $label : strtoupper(__('app.Coordinado'));
@endphp
@auth
    <div class="{{ $class ?? null }}">
        <div class="form-group">
            {!! Form::label('SelectEmpresas', $label) !!}
            <select class="tail-select w-100" name="{{ isset($name) ? $name : 'empresa_xls' }}" id="{{ isset($name) ? $name : 'empresa_xls' }}" required data-rule="required">
                <option value="{{ $valueDefault }}">{{ $label }}</option>
                @foreach ($empresas as $element)
                    <option value="{{ $element->id }}"
                            data-description="RUT: {{ $element->rut }}"
                            {{ $element->id == $selected ? 'selected' : '' }}
                            {{ session('empresa_xls') == $element->nombre_xls ? 'selected' : '' }}>
                            {{ $element->label ? $element->label : $element->nombre_xls }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
@endauth
