<div class="row">
	<div class="col-12">
	    <h1  class="border__bottom text-uppercase">{{$slot}}</h1>
		@isset($subtitle) <p>{{ $subtitle }}</p> @endisset
	</div>
</div>