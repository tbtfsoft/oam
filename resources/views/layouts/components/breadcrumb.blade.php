<nav aria-label="breadcrumb">
	<ol class="breadcrumb mb-0 pl-0">
		<li class="breadcrumb-item text-uppercase">
			<a href="{{ route('home') }}" class="load-link">Dashboard</a>
		</li>
    	{{ $slot }}
	</ol>
</nav>