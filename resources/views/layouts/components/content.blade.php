@section('content')
    <div class="container container-padding">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="jumbotron pt-1 pb-1">
                	@isset ($breadcrumbItems)
                    	@component('layouts.components.breadcrumb')
                    		{{ $breadcrumbItems }}
                    	@endcomponent
                	@endisset
                	@isset ($title)
                		{{ $title }}
                		<hr class="my-1">
                	@endisset
                    {{ $slot }}
                </div>
            </div>
        </div>
    </div>
@endsection