@if (isset($route))
	<li class="nav-item">
		<a class="nav-link load-link {{ isset($clase)? $clase : null }}" href="{{ route($route) }}">{{ $slot }}<span class="sr-only">(current)</span></a>
	</li>
@elseif (isset($routes))
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown{{$dropDown}}" role="button" data-toggle="dropdown" aria-haspopup="true"aria-expanded="false">
          {{$slot}}
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown{{$dropDown}}">
        	{{$routes}}
        </div>
    </li>
@elseif (isset($url))
	<li class="nav-item">
		<a class="nav-link load-link {{ isset($clase)? $clase : null }}" href="{{ $url }}">{{ $slot }}</a>
	</li>
@endif