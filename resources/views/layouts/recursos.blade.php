@extends('layouts.app')

@content

    <div class="row">
        <div class="col-12 col-sm-4 col-md-3 col-lg-3">
            <div class="list bg-primary shadow-sm rounded overflow-hidden">
                <div class="list-item">
                    <div class="list-thumb bg-primary-active rounded-circle shadow-sm p-2 h3 mx-auto mb-0">
                        <i class="fa fa-envelope"></i>
                    </div>
                    <div class="list-body text-right">
                        <span class="list-title text-white">4598</span>
                        <span class="list-content text-white">New Orders</span>

                    </div>
                </div>
                <a class="d-flex text-white flex text-right flex-row align-items-center justify-content-end pt-2 pb-2 pl-4 pr-4 bg-primary-active">
                    View Detail <i class="fa fa-chevron-right ml-2 fs12 mt-1"></i>
                </a>
            </div>
        </div>
        <div class="col-12 col-sm-4 col-md-3 col-lg-3">
            <div class="list bg-warning shadow-sm rounded overflow-hidden">
                <div class="list-item">
                    <div class="list-thumb bg-warning-active rounded-circle shadow-sm p-2 h3 mx-auto mb-0">
                        <i class="fa fa-envelope"></i>
                    </div>
                    <div class="list-body text-right">
                        <span class="list-title text-white">4598</span>
                        <span class="list-content text-white">New Orders</span>

                    </div>
                </div>
                <a class="d-flex text-white flex text-right flex-row align-items-center justify-content-end pt-2 pb-2 pl-4 pr-4 bg-warning-active">
                    View Detail <i class="fa fa-chevron-right ml-2 fs12 mt-1"></i>
                </a>
            </div>
        </div>
        <div class="col-12 col-sm-4 col-md-3 col-lg-3">
            <div class="list bg-danger shadow-sm rounded overflow-hidden">
                <div class="list-item">
                    <div class="list-thumb bg-danger-active rounded-circle shadow-sm p-2 h3 mx-auto mb-0">
                        <i class="fa fa-envelope"></i>
                    </div>
                    <div class="list-body text-right">
                        <span class="list-title text-white">4598</span>
                        <span class="list-content text-white">New Orders</span>

                    </div>
                </div>
                <a class="d-flex text-white flex text-right flex-row align-items-center justify-content-end pt-2 pb-2 pl-4 pr-4 bg-danger-active">
                    View Detail <i class="fa fa-chevron-right ml-2 fs12 mt-1"></i>
                </a>
            </div>
        </div>
        <div class="col-12 col-sm-4 col-md-3 col-lg-3">
            <div class="list bg-secondary shadow-sm rounded overflow-hidden">
                <div class="list-item">
                    <div class="list-thumb bg-secondary-active rounded-circle shadow-sm p-2 h3 mx-auto mb-0">
                        <i class="fa fa-envelope"></i>
                    </div>
                    <div class="list-body text-right">
                        <span class="list-title text-white">4598</span>
                        <span class="list-content text-white">New Orders</span>

                    </div>
                </div>
                <a class="d-flex text-white flex text-right flex-row align-items-center justify-content-end pt-2 pb-2 pl-4 pr-4 bg-secondary-active">
                    View Detail <i class="fa fa-chevron-right ml-2 fs12 mt-1"></i>
                </a>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-12 col-sm-4 col-md-3 col-lg-3">
            <div class="list bg-primary shadow-sm rounded overflow-hidden">
                <div class="list-item">
                    <div class="list-thumb bg-primary-active rounded-circle shadow-sm p-2 h3 mx-auto mb-0">
                        <i class="fa fa-envelope"></i>
                    </div>
                    <div class="list-body text-right">
                        <span class="list-title text-white">4598</span>
                        <span class="list-content text-white">New Orders</span>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-4 col-md-3 col-lg-3">
            <div class="list bg-warning shadow-sm rounded overflow-hidden">
                <div class="list-item">
                    <div class="list-thumb bg-warning-active rounded-circle shadow-sm p-2 h3 mx-auto mb-0">
                        <i class="fa fa-envelope"></i>
                    </div>
                    <div class="list-body text-right">
                        <span class="list-title text-white">4598</span>
                        <span class="list-content text-white">New Orders</span>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-4 col-md-3 col-lg-3">
            <div class="list bg-danger shadow-sm rounded overflow-hidden">
                <div class="list-item">
                    <div class="list-thumb bg-danger-active rounded-circle shadow-sm p-2 h3 mx-auto mb-0">
                        <i class="fa fa-envelope"></i>
                    </div>
                    <div class="list-body text-right">
                        <span class="list-title text-white">4598</span>
                        <span class="list-content text-white">New Orders</span>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-4 col-md-3 col-lg-3">
            <div class="list bg-secondary shadow-sm rounded overflow-hidden">
                <div class="list-item">
                    <div class="list-thumb bg-secondary-active rounded-circle shadow-sm p-2 h3 mx-auto mb-0">
                        <i class="fa fa-envelope"></i>
                    </div>
                    <div class="list-body text-right">
                        <span class="list-title text-white">4598</span>
                        <span class="list-content text-white">New Orders</span>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-12 col-sm-4 col-md-3 col-lg-3">
            <div class="list bg-primary shadow-sm rounded overflow-hidden">
                <div class="list-item">
                    <div class="list-body text-right">
                        <span class="list-title text-white">4598</span>
                        <span class="list-content text-white">New Orders</span>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-4 col-md-3 col-lg-3">
            <div class="list bg-warning shadow-sm rounded overflow-hidden">
                <div class="list-item">
                    <div class="list-body text-right">
                        <span class="list-title text-white">4598</span>
                        <span class="list-content text-white">New Orders</span>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-4 col-md-3 col-lg-3">
            <div class="list bg-danger shadow-sm rounded overflow-hidden">
                <div class="list-item">
                    <div class="list-body text-right">
                        <span class="list-title text-white">4598</span>
                        <span class="list-content text-white">New Orders</span>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-4 col-md-3 col-lg-3">
            <div class="list bg-secondary shadow-sm rounded overflow-hidden">
                <div class="list-item">
                    <div class="list-body text-right">
                        <span class="list-title text-white">4598</span>
                        <span class="list-content text-white">New Orders</span>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-12 col-sm-4 col-md-3 col-lg-3">
            <div class="list bg-primary shadow-sm rounded overflow-hidden">
                <div class="list-item">
                    <div class="list-body text-left">
                        <span class="list-title text-white">4598</span>
                        <span class="list-content text-white">New Orders</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-4 col-md-3 col-lg-3">
            <div class="list bg-warning shadow-sm rounded overflow-hidden">
                <div class="list-item">
                    <div class="list-body text-left">
                        <span class="list-title text-white">4598</span>
                        <span class="list-content text-white">New Orders</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-4 col-md-3 col-lg-3">
            <div class="list bg-danger shadow-sm rounded overflow-hidden">
                <div class="list-item">
                    <div class="list-body text-left">
                        <span class="list-title text-white">4598</span>
                        <span class="list-content text-white">New Orders</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-4 col-md-3 col-lg-3">
            <div class="list bg-secondary shadow-sm rounded overflow-hidden">
                <div class="list-item">
                    <div class="list-body text-left">
                        <span class="list-title text-white">4598</span>
                        <span class="list-content text-white">New Orders</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endcontent