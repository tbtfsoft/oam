<!DOCTYPE html>
@php
    use Jenssegers\Agent\Agent;
    $agent = new Agent();
@endphp
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="{{ asset('fonts/fontawesome-5/js/all.min.js') }}"></script>
    <link href="{{ asset('fonts/icons/styles.css') }}" rel="stylesheet">
    
    @section('section-styles')
        <!-- .assets-styles -->
        @include('layouts.partials.assets.styles')
        <!-- .assets-styles -->
    @show

</head>
<body class="loading">
    @include('layouts.partials.loanding')
    <div class="page-wrapper {{ $agent->isMobile() || !Auth::check() ? '' : 'toggled' }}">
        @section('navbar')
            <!-- .navbar -->
            @include('layouts.partials.navbar')
            <!-- / .navbar -->
        @show

        @section('sidebar')
            <!-- NAV:SIDEBAR-->
            @include('layouts.partials.sidebar')
            <!-- /NAV:SIDEBAR-->
        @show

        <!-- PAGE:CONTENT  -->
        <main id="content" class="page-content h-100 {{ Auth::check() ? 'd-flex align-items-start flex-column ' : 'p-0' }}" >

            <!-- .alert -->
            @include('flash::message')
            <!-- / .alert -->

            <!-- .content -->
            @yield('content')
            <!-- / .content -->

            @section('footer')
                <!-- .footer -->
                @include('layouts.partials.footer')
                <!-- / .footer -->
            @show
        </main>
    </div>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>
    
    @section('section-scripts')
        <!-- .assets-scripts -->
        @include('layouts.partials.assets.scripts')
        <!-- / .assets-scripts -->
    @show
    <style>
        .progress {
          height: 4px;
        }
    </style>
    <script>
        let LoadLinks = document.getElementsByClassName('load-link');

        for (var i = 0; i < LoadLinks.length; i++) {
            LoadLinks[i].addEventListener('click', function(){
                document.body.classList.add("loading");
            }, false);
        }

        document.addEventListener('DOMContentLoaded', function(event) {
            document.body.classList.remove("loading");
        })

        jQuery(function ($) {
            $("#toggled-sidebar").click(function() {
              $(".page-wrapper").toggleClass("toggled");
            });
            $(function () {
              $('[data-toggle="tooltip"]').tooltip()
            })
        });
    </script>
</body>
</html>
