<?php date_default_timezone_set('America/Santiago');?>
<!-- Filtros pre cargados links sidebar -->

@php
    $empresas = null;
    $mes = date('m') - 1;
    $mes =  $mes < 10 ?  '0'. $mes : $mes;
    $anio = date('Y');
    if (Auth::user()) {
      $empresa =  Auth::user()->hasRole(['super-admin']) || Auth::user()->empresas ? 414 : Auth::user()->empresas->first()->id;
    }
@endphp
<nav class="sidebar-wrapper">
  <div class="sidebar-content">
    <div class="sidebar-menu">
        @guest
        <ul>
          <li>
            <a href="{{ route('login') }}">
            	<span>{{ __('Login') }}</span>
            </a>
          </li>
        </ul>
        @else
          <ul class="menu">
            <li class="slider-header navbarDropdownUser">
              @include('layouts.partials.assets.userLabel', ['type' => 'sidebar'])
            </li>
          </ul>
          @include('layouts.partials.variablesMenu')
          <!-- LOGOUT -->
          <ul class="menu border-top border-bottom">
            <li>
              <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <span class="icon">
                  <i class="icon-enter"></i>
                </span>
                <span>{{ __('Logout') }}</span>
              </a>
            </li>
          </ul>
        @endguest
    </div>
  </div>
</nav>

@section('scripts')
  @parent
  <script>
    let elements = $('.sidebar-wrapper .active');
    $.each(elements, function( index ) {
      let count = 0;
      let parent = $(elements[index])
      do {
        count = count + 1;
        parent = parent.parent()
        if (parent.length) {
          $(parent).addClass('active show')
        } else {
          break;
        }
      } while (!$(parent).hasClass('menu') && count < 6);
    });
  </script>
@endsection
