<nav class="navbar navbar-expand-lg navbar-default">
    <div class="col-12 col-sm-auto">
        <div class="sidebar-navbar-menu">
            <a href="{{ route('home') }}" class="load-link">
                <div class="logo-svg mw-100">
                    @include('layouts.components.logo-svg')
                </div>
            </a>
            <button id="toggled-sidebar">
                <i class="text-white fas fa-align-right"></i>
                <i class="text-white fas fa-align-left"></i>
            </button>
        </div>
    </div>

    {{-- 
    @auth
        <ul class="navbar-nav ml-auto d-flex d-lg-none col-6 col-sm-auto" style="flex-direction: row;">
            <li class="nav-item dropdown">
                <a id="sidebarCollapse"  class="navbarDropdownUser nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="display: grid !important; grid-template-columns: auto 1fr auto;">
                    @include('layouts.partials.assets.userLabel')
                </a>
            </li>
        </ul>
    @endauth
    --}}

    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                @if (!app()->environment('production'))
                    <li class="nav-item dropdown">
                        <a id="navbarDropdownLang"
                           class="nav-link dropdown-toggle dropdown-toggle--no-icon text-uppercase"
                           href="#"
                           role="button"
                           data-toggle="dropdown"
                           aria-haspopup="true"
                           aria-expanded="false">
                            <span>{{ \App::getLocale('locatel') }}</span>
                        </a>
                        <div
                            class="dropdown-menu dropdown-menu-right animate slideIn w-100"
                            aria-labelledby="navbarDropdownLang"
                            style="min-width: auto"
                        >
                            @if (\App::getLocale('locatel') !== 'es')
                                <a href="{{ route('lang', 'es') }}" class="p-2 border-bottom d-block">ES</a>
                            @endif
                            @if (\App::getLocale('locatel') !== 'en')
                                <a href="{{ route('lang', 'en') }}" class="p-2 border-bottom d-block">EN</a>
                            @endif
                        </div>
                    </li>
                @endif
                @include('layouts.partials.navbar-items')
            </ul>
        </div>
    </div>

    @progressBar
    
</nav>