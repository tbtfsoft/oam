<!-- font-awesome 4.7 -->
<link rel="stylesheet" href="{{ asset('plugins/font-awesome-4.7.0/css/font-awesome.min.css') }}">
<!-- tail.select -->

@yield('styles-treeMultiselect')

@yield('styles-datatables')

@yield('styles')