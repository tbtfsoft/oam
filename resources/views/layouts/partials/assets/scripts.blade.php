@section('scripts-app')
	<!-- Scripts -->
	<script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/resouces.js') }}"></script>

    <script type="text/javascript" src="{{ asset('plugins/tail.select/langs/tail.select-'.App::getLocale().'.js') }}"></script>
    <script> 
        document.addEventListener("DOMContentLoaded", function(){
            var da = tail.select(".tail-select, .selectpicker:not(.no-search)", {
                search: true,
                descriptions: true,
                locale: "es"
            });
            tail.select(".selectpicker.no-search", {
                descriptions: true,
                locale: "es"
            });
        });
        var urlEmpresasPadre = "{!!route('generales.empresasPadres')!!}";
    </script>
    <script src="{{ asset('js/empresas_padre.js') }}"></script>    
@show

@yield('scripts-treeMultiselect')

@yield('scripts-multiselect')

@yield('scripts-datatables')

@yield('scripts-datapicker')

@yield('scripts')

@section('scripts')

@endsection