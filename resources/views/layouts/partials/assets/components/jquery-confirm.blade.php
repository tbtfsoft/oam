@section('styles')
	@parent
	<link rel="stylesheet" href="{{ asset('plugins/jquery-confirm/jquery-confirm.min.css') }}">
@endsection

@section('scripts')
	@parent
	<script src="{{ asset('plugins/jquery-confirm/jquery-confirm.min.js') }}"></script>
	@isset ($slot)
		{{ $slot }}
	@else
		<script>
			function confirmar(form,e) {
			    e.preventDefault();
			    $.confirm({
			      title: '¿Desea eliminarlo?',
			      content: 'Esta operación no se puede deshacer',
			      theme: 'supervan',
			      buttons: { 
			        ok: {
			            text: "ok!",
			            btnClass: 'btn-primary',
			            keys: ['enter'],
			            action: function() {
			              form.submit();
			              return true;
			            }
			        },
			        cancel: function() { }
			      }
			    });
		  	}
		</script>
	@endisset
@endsection