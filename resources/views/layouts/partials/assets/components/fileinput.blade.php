@section('styles')
	@parent
	<link rel="stylesheet" href="{{ asset('plugins/bootstrap-fileinput/css/fileinput.min.css') }}">
	<style>

	</style>
@endsection

	{{ $slot }}

@section('scripts')
	@parent
	<script src="{{ asset('plugins/bootstrap-fileinput/js/fileinput.js') }}"></script>
	<script src="{{ asset('plugins/bootstrap-fileinput/themes/fas/theme.min.js') }}"></script>
	<script src="{{ asset('plugins/bootstrap-fileinput/js/locales/LANG.js') }}"></script>
	<script src="{{ asset('plugins/bootstrap-fileinput/js/locales/'.App::getLocale().'.js') }}"></script>
	
	@isset ($script_file_input)
		{{ $script_file_input }}
	@else
		<script>
		  $("#fileCSV").fileinput({
		    language: "{{App::getLocale()}}",
		    theme: "fas",
		    overwriteInitial: false,
		    browseClass: "btn btn-primary",
		    previewFileIcon: '<i class="fas fa-file"></i>',
		    showCaption: true,
			  uploadAsync: false,
		    showRemove: true,
		    showUpload: false,
	    	previewFileIconSettings: {
	    		'docx': '<i class="fas fa-file-word text-primary"></i>',
	    		'xlsx': '<i class="fas fa-file-excel text-success"></i>',
	    		'pptx': '<i class="fas fa-file-powerpoint text-danger"></i>',
	    		'jpg': '<i class="fas fa-file-image text-warning"></i>',
	    		'pdf': '<i class="fas fa-file-pdf text-danger"></i>',
	    		'zip': '<i class="fas fa-file-archive text-muted"></i>',
	    	}
		  });
		</script>
	@endisset
@endsection