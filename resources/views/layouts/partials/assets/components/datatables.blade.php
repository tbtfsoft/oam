@section('styles-datatables')
  <link rel="stylesheet" href="{{ asset('plugins/datatables/DataTables-1.10.18/css/jquery.dataTables.min.css') }}">
	<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.2.6/css/rowReorder.dataTables.min.css">
@show

@section('scripts-datatables')
	<script src="{{ asset('plugins/datatables/DataTables-1.10.18/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('plugins/datatables/DataTables-1.10.18/js/dataTables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('plugins/datatables/Responsive-2.2.2/js/dataTables.responsive.min.js') }}"></script>

	<script src="{{ asset('plugins/datatables/Buttons-1.5.4/js/dataTables.buttons.min.js') }}"></script>

	<script src="{{ asset('plugins/datatables/Buttons-1.5.4/js/buttons.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('plugins/datatables/Buttons-1.5.4/js/buttons.print.js') }}"></script>
	<script src="{{ asset('plugins/datatables/Buttons-1.5.4/js/buttons.html5.min.js') }}"></script>

	<script src="{{ asset('plugins/datatables/pdfmake-0.1.36/pdfmake.min.js') }}"></script>
	<script src="{{ asset('plugins/datatables/pdfmake-0.1.36/vfs_fonts.js') }}"></script>

	<script src="{{ asset('plugins/datatables/JSZip-2.5.0/jszip.min.js') }}"></script>
@show