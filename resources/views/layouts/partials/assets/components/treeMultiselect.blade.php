@section('scripts-treeMultiselect')
	@parent
	<script src="{{ asset('plugins/jquery.tree-multiselect/dist/jquery.tree-multiselect.min.js') }}"></script>
	@isset ($slot)
		{{ $slot }}
	@endisset
@endsection