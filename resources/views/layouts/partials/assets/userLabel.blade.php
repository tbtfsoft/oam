@php
	$name = isset($parent) ? $parent->nombre : 'Holding de Empresas';
	$logotipo = isset($parent) ? "/img/empresas/$parent->logo" : 'img/empresas/default.svg';
@endphp

@if (isset($type))
		<div class="nav-item-user-icon" style="background-image: url({{ asset($logotipo) }})"></div>
		<div class="d-inline-block text-truncate" class="nombreEmpresaPadre" title="{{ $name }}">
			{{ $name }}
			@isset ($child)
				<small class="d-block line-height">{{ $child->label ? $child->label : $child->nombre_xls }}</small>
			@endisset
		</div>
@else
		<li class="nav-item dropdown">
		    <a id="navbarDropdownUser"
		       class="navbarDropdownUser nav-link dropdown-toggle"
		       href="#"
		       role="button"
		       data-toggle="dropdown"
		       aria-haspopup="true"
		       aria-expanded="false">
					<div class="nav-item-user-icon" style="background-image: url({{ asset($logotipo) }})"></div>
					<div class="d-inline-block text-truncate line-height" class="nombreEmpresaPadre" title="{{ $name }}">
						{{ $name }}
						@isset ($child)
							<div class="d-block menu-label">{{ $child->label ? $child->label : $child->nombre_xls }}</div>
						@endisset
					</div>
		    </a>
		    <div class="dropdown-menu dropdown-menu-right animate slideIn" aria-labelledby="navbarDropdownUser" style="width: 250px">
		        <div class="p-2 border-bottom text-center">
		            <div class="d-block text-truncate line-height">Hola! <span class="font-weight-bold">{{ Auth::user()->name }}</span></div>
		            <small class="d-block text-muted mb-0 line-height">Administrator</small>
		        </div>
		        @isset ($child)
		        <div class="block border-bottom px-3 pt-0 pb-2">
		        	<div class="d-block mt-1 pt-1 line-height" title="RUT" data-toggle="tooltip" data-placement="top">
		        		<small class="d-block text-truncate text-muted">
		        			{{ $child->label ? $child->label : $child->nombre_xls }}
		        		</small>
		        		{{ $child->rut }}
		        	</div>
		        	@if ($child->potencia_instalada)
		        		<div class="d-block border-top mt-1 pt-1 line-height" title="Potencia instalada" data-toggle="tooltip" data-placement="top">
			        		<small class="d-block text-truncate text-muted">Potencia Activa Instalada</small> 
			        		{{ $child->potencia_instalada }}
		        		</div>
		        	@endif
		        	@if ($child->fecha_inicio)
		        		<div class="d-block border-top mt-1 pt-1 line-height" title="Fecha inicio - {{ $child->fecha_inicio }}" data-toggle="tooltip" data-placement="top">
			        		<small class="d-block text-truncate text-muted">Fecha Inicio</small> 
			        		{{ date('d/m/Y', strtotime($child->fecha_inicio)) }}
		        		</div>
		        		<div class="d-block border-top mt-1 pt-1 line-height" title="Edad PFV" data-toggle="tooltip" data-placement="top">
			        		<small class="d-block text-truncate text-muted">Edad PFV</small> 
		        			{{ AppHelpers::edad_pfv($child->fecha_inicio) }}
		        		</div>
		        	@endif
		        	@if ($child->url_gmaps)
								<a href="{{ $child->url_gmaps ? $child->url_gmaps : '#' }}" target="_blank" class="menu-coordenadas" style="text-decoration: none;">
									<i class="fas fa-map-marked-alt icon"></i>
									<span>
										Ver en el mapa<br>
		        				@if ($child->coordenadas_lat && $child->coordenadas_lng)
											<small>{{ $child->coordenadas_lat }}, {{ $child->coordenadas_lng }}</small>
		        				@endif
									</span>
								</a>
		        	@endif
		        </div>
		        @endisset
		        <a class="dropdown-item load-link" href="{{ route('logout') }}"
		           onclick="event.preventDefault();
		                         document.getElementById('logout-form').submit();">
		            <i class="fas fa-sign-in-alt"></i>
		            {{ __('Logout') }}
		        </a>
		    </div>
		</li>
@endif
<style>
	.menu-label {
		opacity: .8;
    font-size: .9rem;
    line-height: 1;
    display: block;
	}
	.menu-coordenadas {
		display: grid;
    grid-template-columns: 30px auto;
    align-items: center;
    border-top: 1px solid #dee2e6;
    padding-top: 5px;
    margin-top: 5px;
	}
	.menu-coordenadas .icon { margin: auto }
	.menu-coordenadas span { line-height: 1 }
	.menu-coordenadas span small { color: #333333b8; }
</style>