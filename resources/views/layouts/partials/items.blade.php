@if (isset($nameTarget))
    <ul class="collapse" id="{{ $nameTarget }}">
@else
    <ul class="menu">
@endif
    @foreach ($elements as $item)
        @if (!isset($item['roles']) || (isset($item['roles']) && Auth::user()->hasRole([$item['roles']])))
            @if (isset($item['routes']))
                <li class="sidebar-dropdown {{ isset($item['class']) ? $item['class'] : null }}">
                    @php
                        $nameTarget = 'collapse-' . preg_replace('([^A-Za-z0-9])', '', $item['label']);
                    @endphp
                    <a href="#" data-toggle="collapse" data-target="#{{ $nameTarget }}">
                        @isset($item['icon'])
                        <div class="icon">
                            <i class="{{ $item['icon'] }}"></i>
                        </div>
                        @endisset
                        <span class="pl-1">
                            {{ isset($item['label']) ? __($item['label']) : __('app.'.$item['route']) }}
                        </span>
                    </a>
                    @include('layouts.partials.items', ['elements' => $item['routes'], $nameTarget ])
                </li>
            @else
                <li>
                    <a 
                        <?php 
                            $route = '#';
                            if (isset($item['route'])) {
                                $additional = isset($item['additional']) ? $item['additional'] : null;
                                $route = route($item['route'] . $additional);
                            }
                         ?>
                        href="{{ $route }}{{ isset($item['parameters']) ? $item['parameters'] : null }}"
                        @if (isset($item['active']))
                            class="{{ Request::is($item['active']) ? 'active' : '' }}"
                        @else
                            class="{{ Request::is(Route::getRoutes()->getByName($item['route'])->uri() . '*') ? 'active' : '' }}"
                        @endif
                    >
                        @isset($item['icon'])
                        <div class="icon">
                            <i class="{{ $item['icon'] }}"></i>
                        </div>
                        @endisset
                        <span class="pl-1">
                            {{ isset($item['label']) ? __($item['label']) : __('app.'.$item['route']) }}
                        </span>
                    </a>
                </li>
            @endif
        @endif
    @endforeach
</ul>