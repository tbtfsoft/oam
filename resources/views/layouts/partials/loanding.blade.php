<div class="load">
    <div class="logo-load">
		@include('layouts.components.logo-svg')
    </div>
</div>

<style>
	.logo-load svg,
	.logo-load {
		position: absolute;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		margin: auto;
		width: 250px !important;
		max-width: 100%;
	}
	body.loading {
		overflow: hidden;
	}
	body .load {
		display: none;
	}
	body.loading .load{
		display: block;
		background-color: #f6f6f6;
		position: absolute;
		z-index: 10000000;
		width: 100%;
		height: 100%;
	}
	body.loading #content {
		overflow: hidden;
	}

	.logo-load {
	  filter: drop-shadow( 0px 0px 2px rgba(0, 0, 0, .1));
	}
	  .letter__e {
	    animation-name: lettere;
	    animation-duration: .8s;
	    animation-iteration-count: infinite;
	    animation-direction: alternate;
	  }

	  .letter__1 {
	    animation-name: letter1;
	    animation-duration: .8s;
	    animation-iteration-count: infinite;
	    animation-direction: alternate;
	  }

	  .letter__2 {
	    animation-name: letter2;
	    animation-duration: .8s;
	    animation-iteration-count: infinite;
	    animation-direction: alternate;
	  }

	  .letter__3 {
	    animation-name: letter3;
	    animation-duration: .8s;
	    animation-iteration-count: infinite;
	    animation-direction: alternate;
	  }

	  .letter__4 {
	    animation-name: letter4;
	    animation-duration: .8s;
	    animation-iteration-count: infinite;
	    animation-direction: alternate;
	  }

@keyframes lettere {
  from {
    transform: rotate(0) translateY(0) translateX(0);
  }
  to {
    transform: rotate(10deg) translateY(-87px) translateX(17px);
  }
}

@keyframes letter1 {
  from {
    transform: translateY(0px) translateX(0px);
  }
  to {
    transform: translateY(-8px);
  }
}

@keyframes letter2 {
  from {
    transform: translateY(0px) translateX(0px);
  }
  to {
    transform: translateY(-5px) translateX(-10px);
  }
}

@keyframes letter3 {
  from {
    transform: translateY(0px) translateX(0px);
  }
  to {
    transform: translateY(0px) translateX(-12px);
  }
}

@keyframes letter4 {
  from {
    transform: translateY(0px) translateX(0px);
  }
  to {
    transform: translateY(5px) translateX(-5px);
  }
}
</style>