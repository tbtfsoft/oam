<footer class="mt-auto">
	{{-- <div class="container-fluid"> --}}
		{{-- <div class="row"> --}}
			{{-- <div class="col-6"> --}}
				{{-- Copyright © {{ date('Y') }} <a href="http://www.oenergy.cl/" target="_blank">oEnergy</a> v.1.0 --}}
			{{-- </div> --}}
		{{-- </div> --}}
	{{-- </div> --}}
	<style>
		footer {
			width: 100%;
			padding-top: .5rem;
			padding-bottom: .5rem;
			background: #F8F9FF;
    	color: #adb5bd;
		}
		body .alert {
			width: 100%;
		}
		.container-padding,
		.container-padding > .row,
		.container-padding > .row > div > .jumbotron{
			min-height: 100%;
		}
		.container-padding {
			/*min-height: calc(100% - 43px);*/
			min-height: calc(100% - 20px);
		}
	</style>
</footer>