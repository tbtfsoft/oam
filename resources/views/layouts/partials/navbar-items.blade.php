@guest
    <li class="nav-item">
        <a class="nav-link load-link" href="{{ route('login') }}">
            {{ __('Login') }}
        </a>
    </li>
@else
  <div id="empresa-menu">
    @include('layouts.partials.assets.userLabel')
  </div>
@endguest