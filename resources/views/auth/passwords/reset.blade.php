@extends('auth.layout')

@section('content-auth')
    <div class="container">
        <div class="row row-center justify-content-center w-100">
            <div class="col-12 col-sm-8 col-md-7 col-lg-5 mx-auto">
                <div class="auto-form-wrapper">
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf
                        <div class="text-center mb-2">
                            <a href="{{ route('login') }}">
                                <img src="{{ asset('img/logo.svg') }}" alt="logo oenergy" class="mb-2" width="200px" style="max-width: 100%">
                            </a>
                        </div>
                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group row">
                            <div class="col-12">
                                <label for="email" class="col-form-label">{{ __('E-Mail Address') }}</label>
                            </div>

                            <div class="col-12">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus placeholder="{{ __('E-Mail Address') }}">

                                @if ($errors->has('email'))
                                    <div class="alert alert-danger fade show px-1 pt-1 my-1 rounded" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-12">
                                <label for="password">{{ __('Password') }}</label>
                            </div>

                            <div class="col-12">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="{{ __('Password') }}">

                                @if ($errors->has('password'))
                                    <div class="alert alert-danger fade show px-1 pt-1 my-1 rounded" role="alert">
                                        <strong class="alert__text">{{ $errors->first('password') }}</strong>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-12">
                                <label for="password-confirm" class="col-form-label">{{ __('Confirm Password') }}</label>
                            </div>

                            <div class="col-12">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="{{ __('Confirm Password') }}">
                                
                                @if ($errors->has('password_confirmation'))
                                    <div class="alert alert-danger fade show px-1 pt-1 my-1 rounded" role="alert">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="row mt-4">
                            <div class="col-12 col-sm-8 mx-auto text-center">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <span class="text-white d-block text-center" style="font-size: .8rem">
                    &copy; Copyright © {{ date('Y') }} 
                    <a href="http://oenergy.cl/" target="_blank" class="text-white">
                        oEnergy
                    </a>
                    . All rights reserved.
                </span>
            </div>
        </div>
    </div>
@endsection
