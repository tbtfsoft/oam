@extends('auth.layout')

@section('content-auth')
    @if (session('status'))
        <div class="alert alert-success" style="position: absolute;top: 0; width: 100%" role="alert">
            {{ session('status') }}
        </div>
    @endif
    <div class="container">
        <div class="row row-center justify-content-center">
            <div class="col-12 col-sm-8 col-md-6 col-lg-5 mx-auto">
                <div class="auto-form-wrapper">
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <div class="text-center mb-2">
                            <a href="{{ route('login') }}">
                                <img src="{{ asset('img/logo.svg') }}" alt="logo oenergy" class="mb-2" width="200px" style="max-width: 100%">
                            </a>
                        </div>
                        <p class="text-center text-uppercase text-muted">{{ __('Reset Password') }}</p>
                        <div class="form-group row mb-3">
                            <div class="col-12">
                                <input type="email" placeholder="{{ __('E-Mail Address') }}" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <div class="alert alert-danger fade show px-1 pt-1 my-1 rounded" role="alert">
                                        <strong class="alert__text">{{ $errors->first('email') }}</strong>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-12 col-lg-10 mx-auto text-center">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
