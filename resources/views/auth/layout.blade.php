@extends('layouts.app')

@section('navbar',false)
@section('footer',false)
@section('section-styles',false)
@section('styles')
  @parent
@stop
@section('sidebar', false)

@section('content')
	<div class="auth theme-one">
		@yield('content-auth')
	</div>
@stop

@section('scripts-app',false)
@section('scripts-validator',false)