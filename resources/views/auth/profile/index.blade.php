@extends('layouts.app')

@content
    @slot('breadcrumbItems')
        <li class="breadcrumb-item active text-uppercase" aria-current="page">{{__('auth.profile')}}</li>
    @endslot

    @slot('title')
        <h1 class="display-6 text-uppercase">{{__('auth.profile')}}</h1>
    @endslot
	
	<div class="container-padding">
		<div class="row">
			<div class="col">
				<div class="row align-items-center">
					<div class="col-4 col-lg-6 mx-auto">
						<div class="square">
							<div class="square__content">
								<img src="{{ asset('img/auth/user-dark.svg') }}" class="mw-100 mh-100">
							</div>
						</div>
					</div>
					@auth
						<div class="col-8 col-md-12 col-lg-10 text-center py-2 mx-auto">
							<h4 class="mb-0">{{ auth::user()->name }}</h4>
							<p class="mb-0 text-uppercase">
								@foreach (auth::user()->getRoleNames() as $rol)
									{{ $rol }}
								@endforeach
							</p>
							<div class="profile__description d-none d-md-block">
								<hr>
								<h5 class="mb-0">{{__('auth.modules')}}</h5>
								<span class="badge badge-primary text-uppercase">Dashboard</span>
								@foreach (auth::user()->getAllPermissions() as $element)
									@if ($element->module)
										<span class="badge badge-primary text-uppercase">{{ $element->display_name }}</span>
									@endif
								@endforeach
								
							</div>
						</div>
					@endauth
				</div>
			</div>
			<div class="col-12 col-md-7 col-lg-8">
				<hr class="d-block d-md-none">
				<form action="{{ route('profile.update',Auth::id()) }}" method="post" class="row form-validator">
					@csrf
					<div class="col-12">
						<h6 class="text-uppercase"> {{__('auth.details')}} </h6>
					</div>
					<div class="col-6">
  						<div class="form-group">
  						  	<label for="name">{{__('auth.name')}}</label>
  						  	<input type="text" class="form-control" placeholder="{{__('auth.name')}}" name="name" value="{{ auth::user()->name }}" data-rule='required|minlength-3'>
    						@if ($errors->has('name'))
    							<div class="error" data-type="validator-error">{{ $errors->first('name') }}</div>
    						@endif
  						</div>
					</div>

					<div class="col-6">
  						<div class="form-group">
  						  	<label for="rut">RUT</label>
  						  	<input type="text" class="form-control" placeholder="RUT" disabled value="{{ auth::user()->rut }}">
    						@if ($errors->has('rut'))
    							<div class="error" data-type="validator-error">{{ $errors->first('rut') }}</div>
    						@endif
  						</div>
					</div>

					<div class="col-12 col-sm-6">
  						<div class="form-group">
  						  	<label for="email">Email</label>
  						  	<input type="email" class="form-control" placeholder="email" disabled value="{{ auth::user()->email }}">
    						@if ($errors->has('email'))
    							<div class="error" data-type="validator-error">{{ $errors->first('email') }}</div>
    						@endif
  						</div>
					</div>

					<div class="col-12 col-sm-6">
  						<div class="form-group">
  						  	<label for="rol">ROL</label>
  						  	<input type="text"
  						  			class="form-control"
  						  			disabled 
  						  			placeholder="ROL"
  						  			value="{{ count(auth::user()->getRoleNames()) ? auth::user()->getRoleNames()[0] : 'N/D' }}">
  						</div>
					</div>
					<div class="col-12">
						<div class="my-2">
							<button type="submit" class="btn btn-primary text-uppercase">{{__('auth.save')}}</button>
						</div>
					</div>
				</form>
				<hr>
				<form action="{{ route('profile.update.password') }}" class="row form-validator-password" method="POST">
					@csrf
					<div class="col-12">
						<h6 class="text-uppercase"> {{__('auth.change_password')}} </h6>
					</div>
					<div class="col-12 col-sm-6 mx-auto">
						<div class="form-group">
						    <label for="password_current">{{__('auth.password_current')}}</label>
						    <input type="password"
						    		class="form-control {{ $errors->has('password_current') ? 'error' : '' }}"
						    		name="password_current"
									required 
						    		value="{{ old('password_current') }}"
						    		data-rule='required|minlength-5'
						    		placeholder="{{__('auth.password_current')}}">
    						@if ($errors->has('password_current'))
    							<div class="error text-truncate" data-type="validator-error" title="{{ $errors->first('password_current') }}">{{ $errors->first('password_current') }}</div>
    						@endif
						</div>
					</div>
					<div class="col-12">
						<div class="row">					
							<div class="col-12 col-sm-6">
								<div class="form-group">
								    <label for="password">{{__('auth.password_new')}}</label>
								    <input type="password"
								    		class="form-control {{ $errors->has('password') ? 'error' : '' }}"
								    		name="password"
								    		minlength="6" 
											required 
								    		value="{{ old('password') }}"
						    				data-rule='required|minlength-5'
								    		placeholder="{{__('auth.password_new')}}">
    								@if ($errors->has('password'))
    									<div class="error text-truncate" data-type="validator-error" title="{{ $errors->first('password') }}">
    										{{ $errors->first('password') }}
    									</div>
    								@endif
								</div>
							</div>
							<div class="col-12 col-sm-6">
								<div class="form-group">
							    <label for="password_confirmation">{{__('auth.password_confirmation')}}</label>
								<input type="password"
										class="form-control {{ $errors->has('password_confirmation') ? 'error' : '' }}"
										name="password_confirmation"
										minlength="6"
										required 
										value="{{ old('password_confirmation') }}"
						    			data-rule='required|minlength-5'
										placeholder="{{__('auth.password_confirmation')}}">
    								@if ($errors->has('password_confirmation'))
    									<div class="error text-truncate" data-type="validator-error" title="{{ $errors->first('password_confirmation') }}">
    										{{ $errors->first('password_confirmation') }}
    									</div>
    								@endif
								</div>
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="my-2">
							<button type="submit" class="btn btn-primary text-uppercase">{{__('auth.update')}}</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
@endcontent

@section('scripts')
	@parent
  <script src="{{ asset('plugins/js-form-validator-2.1/js-form-validator.js') }}"></script>
  <script>
    var fProfile = document.querySelector('.form-validator');
    if(fProfile){
      fProfile.setAttribute('novalidate', true);
        new Validator(fProfile, function (err, res) {
          return res;
        });
    }

    var f = document.querySelector('.form-validator-password');
    if(f){
      f.setAttribute('novalidate', true);
      var options = {
          rules: {
            rut: function (value) {
              return checkRut(value);
            },
			password : function (value) {
				// console.log($('input[nane=password_current]').val());
				return false;
			},
			password_confirmation : function (value) {
				return false;
			}
          },
          messages: {
            es: {
              	rut: {
                	incorrect: 'RUT INVALIDO'
            	},
				password: {
					incorrect: 'Verifique password'
				},
				password_confirmation: {
					incorrect: 'Verifique password'
				}
            }
          },
          locale: "{{ session()->has('locale') ? session()->get('locale') : 'es' }}"
        };

        new Validator(f, function (err, res) {
          return res;
        },options);
    }
  </script>
@endsection