@extends('auth.layout')

@section('content-auth')
    <div class="container">
        <div class="row row-center justify-content-center">
            <div class="col-10 col-sm-8 col-md-6 col-lg-5 mx-auto">
                <div class="auto-form-wrapper">
                    <form method="POST" id="LoginForm"  class="form-validator" action="{{ route('login') }}">
                        @csrf

                        <div class="text-center mb-2">
                            <img src="{{ asset('img/logo.svg') }}" alt="logo oenergy" class="mb-2 mw-100" width="200px">
                        </div>

                        <div class="form-group">
                            <label class="label" for="rut"> {{__('auth.username')}} </label>
                            <div class="input-group">
                                <input class="form-control text-uppercase" type="text" placeholder="{{__('auth.username')}}" id="rut" name="rut" value="{{ old('rut') }}" autofocus maxlength="12" onkeyup="formatCliente(this)" required data-rule="required|minlength-11|rut">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="label" for="passwords"> {{__('auth.password') }} </label>
                            <div class="input-group">
                                <input class="form-control" type="password" id="passwords" required placeholder="{{__('auth.password')}}" name="password" required data-rule="minlength-4|required">
                            </div>
                        </div>

                        <div class="text-center{{ $errors->has('rut') ? ' has-error' : '' }}">
                            @if ($errors->has('rut'))
                                <div class="alert alert-danger fade show px-1 pt-1 my-1 rounded" role="alert">
                                    <strong class="alert__text">{{ $errors->first('rut') }}</strong>
                                </div>
                            @endif
                        </div>

                        <div class="text-center d-none" id="message-alert">
                            <div class="alert alert-danger fade show px-1 pt-1 my-1 rounded" role="alert">
                                <strong class="alert__text"></strong>
                            </div>
                        </div>

                        <div class="form-group d-flex justify-content-between">
                            <div class="form-check form-check-flat mt-0">
                                <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} > {{ __('Remember Me') }}
                                <i class="input-helper"></i></label>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-12 mx-auto text-center">
                                <button type="submit" class="btn btn-primary button text-uppercase submit-btn btn-block">
                                    {{ __('auth.ok') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
                <span class="text-white d-block text-center" style="font-size: .8rem">
                    &copy; Copyright © {{ date('Y') }} 
                    <a href="http://oenergy.cl/" target="_blank" class="text-white">
                        oEnergy
                    </a>
                    . All rights reserved.
                </span>
            </div>
        </div>
    </div>
@endsection

@section('section-scripts')
    @parent
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('plugins/js-form-validator-2.1/js-form-validator.js') }}"></script>
    <style>
        .shake button[type=submit] {
            pointer-events: none;
        }
        .error[data-type="validator-error"] {
            width: 100% !important;
            bottom: -3px !important;
            border-radius: 5px !important;
            z-index: 1111111;
            border-top-left-radius: 0px !important;
            border-top-right-radius: 0px !important;
        }
    </style>
    <script>
        var f = document.querySelector('.form-validator'),
            form = $('form'),
            msg = false;

        if(f){
            var submitForm = function(err,res) {
                if(res) {
                    login(form);
                }else {
                    form.addClass('shake');
                    setTimeout(function() {
                       form.removeClass("shake");
                    }, 820)
                }
                return false;
            }

            f.setAttribute('novalidate', true);

            var options = {
              rules: {
                rut: function (value) {
                  return checkRut(value);
                }
              },
              messages: {
                es: {
                  rut: {
                    incorrect: 'RUT INVALIDO'
                  }
                }
              },
              locale: "{{ session()->has('locale') ? session()->get('locale') : 'es' }}"
            };

            new Validator(f,function (err, res) {
                submitForm(err, res);
            },options);
        }

        function formatCliente(cliente)
        {
            cliente.value=cliente.value.replace(/[.-]/g, '').replace( /^(\d{1,2})(\d{3})(\d{3})(\w{1})$/, '$1.$2.$3-$4')
        }

        function login(form) {
            form.removeClass('shake');
            var button = $('.button'),
                spinner = '<span class="spinner"></span>';
            $('input').prop('disabled', true);
            $('#message-alert').addClass('d-none').removeClass('d-block');
            let rut = $('#rut').val(),
                password = $('#passwords').val();
            button.toggleClass('loading').html(spinner);
            $.ajax({
                type: "GET",
                url: "{{ route('is_users') }}",
                data: {
                    rut: rut,
                    password: password
                },
                success: function(data)
                {
                    if(data.auth) {
                        window.location = data.path;
                    }
                    else {
                        $('input').prop('disabled', false);
                        $('#message-alert').addClass('d-block').removeClass('d-none').find('.alert__text').html('{{__("auth.failed")}}');
                        button.toggleClass('loading').html('{{ __('Entrar') }}');
                        form.addClass('shake');
                        setTimeout(function() {
                           form.removeClass("shake");
                        }, 820)
                    }
                },
                error: function(xhr,status,error)
                {
                    if(msg)
                        msg.dismiss();
                    alertify.set('notifier','position', 'top-right');
                    msg = alertify.error('Error message', 0);
                }
            });
        }
    </script>
@endsection
