@extends('layouts.app')

@content
  @slot('breadcrumbItems')
    <li class="breadcrumb-item active text-uppercase" aria-current="page">
      {{ __('app.scada-web.comercial') }}
    </li>
  @endslot
  @slot('title')
  <div class="row align-items-center">
    <div class="col">
        <h1 class="display-6 text-uppercase my-0 pt-md-3">
          {{ __('app.scada-web.comercial') }}
        </h1>
    </div>
    <div class="col-12 col-md-6">
        <div class="row justify-content-end">
            @if(Auth::user()->hasRole(['super-admin']))
                @SelectCompanys(['class'=>"col-12 col-sm-6", 'selected' => 414 ])
            @else
                @SelectCompanys(['class'=>"col-12 col-sm-12", 'selected' => Auth::user()->empresas->first()->id ])
            @endif
        </div>
    </div>
    </div> 

    <div class="row">
      <div class="col-12 col-sm-4 col-md-3 col-lg-3 mb-1">
            <div class="list bg-warning shadow-sm rounded overflow-hidden">
                <div class="list-item">
                    <div class="list-thumb bg-warning-active rounded-circle shadow-sm p-2 h3 mx-auto mb-0" style="color: #FFFFFF;">
                        <i class="fa fa-plug"></i>
                    </div>
                    <div class="list-body text-right"  style='font-family: "Courier New"'>
                        <span id="barra_span" class="list-title text-white" style='font-weight: bold;'></span>
                        <span class="list-content text-white">{{ __('app.barra') }}</span>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-4 col-md-3 col-lg-3 mb-1">
            <div class="list bg-warning shadow-sm rounded overflow-hidden">
                <div class="list-item">
                    <div class="list-thumb bg-warning-active rounded-circle shadow-sm p-2 h3 mx-auto mb-0" style="color: #FFFFFF;">
                        <i class="fa fa-search-dollar"></i>
                    </div>
                    <div class="list-body text-right"  style='font-family: "Courier New"'>
                        <span class="list-title text-white" style='font-weight: bold;'><span id="modalidad_span"></span><span id="precio_energia_span"></span></span>
                        <span class="list-content text-white">
                          {{ __('app.modalidad') }}
                        </span>

                    </div>
                </div>
            </div>
        </div>        
    </div>     
  @endslot

  <?php date_default_timezone_set('America/Santiago');?>

  <!-- Var -->
  @php 
    $mes = date('m');
    $anio = date('Y');
  @endphp
  

  <div class="row">
    <div class="col-12 col-md-12">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" id="dia-tab" data-toggle="tab" href="#dia" role="tab" aria-controls="dia" aria-selected="true">
          {{ __('app.dia') }}
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="mesTab-tab" data-toggle="tab" href="#mesTab" role="tab" aria-controls="mesTab" aria-selected="false">
          {{ __('app.mes') }}
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="anioTab-tab" data-toggle="tab" href="#anioTab" role="tab" aria-controls="anioTab" aria-selected="false">
          {{ __('app.año') }}
        </a>
      </li>
    </ul>
    <div class="tab-content" id="myTabContent">
      <div class="tab-pane fade show active" id="dia" role="tabpanel" aria-labelledby="dia-tab">
        <div class="row">
          <div class="col-12 col-md-6">
              <div class="row">    
                <div class="col-12 col-sm-7"> 
                  <div class="form-group">
                    <label for="dates">
                      {{ __('app.rango de fechas') }}
                    </label>
                    <input type="text" id="dateRangePicker" name="dates" class="form-control mb-2" />
                  </div>  
                </div>
              </div>
          </div>
        </div>  
        <div class="row">
          <div class="col-12 col-md-12">
          <div class="col-12 mt-lg-4">
                <div class="title-grafic">
                  <div>
                    {{ __('app.Variables Eléctricas y Económicas') }}
                  </div>
                  <button type="button" class="btn btn-secondary rounded-circle opacity-5 btn-sm" data-toggle="tooltip" data-placement="top" title="{{ __('Cálculo basados en valores instantáneos') }}">
                        <i class="fas fa-info-circle"></i>
                    </button>
                </div>
              </div>
            <div class="row">
                  <div class="col-12 col-md-12 my-12">
                      <div class="shadow rounded bg-white">
                          <div id="energiaRange" class="p-2"></div>
                      </div>
                  </div>          
              </div>                
          </div>
        </div>  
      </div>
      <div class="tab-pane fade" id="mesTab" role="tabpanel" aria-labelledby="mesTab-tab">
        <div class="col-12 col-md-6">
          <div class="row">    
            @SelectMonths(['class'=>"col-12 col-sm-4", "noSearch" => true,'id' => "mes", 'selected' => $mes])
            @SelectYears(['class'=>"col-12 col-sm-3", "noSearch" => true,'id' => "anio-mes", 'selected' => $anio])
          </div>
        </div>
        <div class="row">
          <div class="col-12 col-md-12">
          <div class="col-12 mt-lg-4">
                <div class="title-grafic">
                  <div>
                    {{ __('app.Variables Eléctricas y Económicas') }}
                  </div>
                  <button type="button" class="btn btn-secondary rounded-circle opacity-5 btn-sm" data-toggle="tooltip" data-placement="top" title="{{ __('app.Cálculo basados en valores instantáneos') }}">
                        <i class="fas fa-info-circle"></i>
                    </button>
                </div>
              </div>
            <div class="row">
                  <div class="col-12 col-md-12 my-12">
                      <div class="shadow rounded bg-white">
                          <div id="energiaMes" class="p-2"></div>
                      </div>
                  </div>          
              </div> 
              <div class="row">
                  <div class="col-12 col-md-12 my-12">
                      <div class="shadow rounded bg-white">
                          <div id="energiaVSMes" class="p-2"></div>
                      </div>
                  </div>          
              </div>                             
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="anioTab" role="tabpanel" aria-labelledby="anioTab-tab">
        <div class="col-12 col-md-6">
          <div class="row">    
            @SelectYears(['class'=>"col-12 col-sm-6", "noSearch" => true, 'selected' => $anio])
          </div>
        </div>
        <div class="row">
          <div class="col-12 col-md-12">
          <div class="row">
              <div class="col-12 mt-lg-4">
                <div class="title-grafic">
                  <div>
                    {{__('app.Variables Eléctricas y Económicas')}}
                  </div>
                  <button type="button" class="btn btn-secondary rounded-circle opacity-5 btn-sm" data-toggle="tooltip" data-placement="top" title="{{__('app.Cálculo basados en valores instantáneos') }}">
                        <i class="fas fa-info-circle"></i>
                    </button>
                </div>
              </div>         
            </div>
            <div class="row">
                  <div class="col-12 col-md-12 my-12">
                      <div class="shadow rounded bg-white">
                          <div id="energiaAnio" class="p-2"></div>
                      </div>
                  </div>          
              </div> 
              <div class="row">
                  <div class="col-12 col-md-12 my-12">
                      <div class="shadow rounded bg-white">
                          <div id="energiaVSAnio" class="p-2"></div>
                      </div>
                  </div>          
              </div>
              <div class="row">
              <div class="col-12 mt-lg-4">
                <div class="title-grafic">
                    <div>
                      {{__('app.Balance de Transferencias Económicas CEN (Mes Cerrado)')}}
                    </div>
                    <button type="button" class="btn btn-secondary rounded-circle opacity-5 btn-sm" data-toggle="tooltip" data-placement="top" title="{{ __('app.Cálculos basados en valores acumulativos') }}">
                        <i class="fas fa-info-circle"></i>
                    </button>
                </div>
              </div>         
            </div>
              <div class="row">
                  <div class="col-12 col-md-12 my-12">
                      <div class="shadow rounded bg-white">
                          <div id="produccionEnergiMensual" class="p-2"></div>
                      </div>
                  </div>          
              </div> 
              <div class="row">
                  <div class="col-12 col-md-12 my-12">
                      <div class="shadow rounded bg-white">
                          <div id="produccionEnergiaCLPMensual" class="p-2"></div>
                      </div>
                  </div>          
              </div> 
              <div class="row">
                  <div class="col-12 col-md-12 my-12">
                      <div class="shadow rounded bg-white">
                          <div id="produccionPotenciaCLPMensual" class="p-2"></div>
                      </div>
                  </div>          
              </div> 
              <div class="row">
                  <div class="col-12 col-md-12 my-12">
                      <div class="shadow rounded bg-white">
                          <div id="servCompCLPMensual" class="p-2"></div>
                      </div>
                  </div>          
              </div>               
          </div>
        </div>
      </div>
    </div>
    </div>
  </div>  
@endcontent

@datapicker
  <script>
    startDate = "{{ date('d/m/Y')}}"+" 00:00";
    endtDate = "{{ date('d/m/Y')}}"+" 23:59";
  </script>

  <script>
    /*
     Documentacion màs detallada: https://www.daterangepicker.com/ 
    */
    $('#dateRangePicker').daterangepicker({
      timePicker: true,
      timePicker24Hour: true,
      showDropdowns: true,
      minYear: 2010, // El año mminimo.
      startDate: startDate, // Obtiene la fecha actual
      endDate: endtDate, // El fin de fecha es un dia adicional a la fecha actual.
      autoclose: false,
      locale: {
        format: 'DD/M/Y HH:mm',
          separator: " - ",
          applyLabel: "{{ __('app.aplicar') }}",
          cancelLabel: "{{ __('app.cancelar') }}",
          fromLabel: "{{ __('app.de') }}",
          toLabel: "{{ __('app.hasta') }}",
          customRangeLabel: "Custom",
          daysOfWeek: [
            "{{ __('app.dom') }}",
            "{{ __('app.lun') }}",
            "{{ __('app.mar') }}",
            "{{ __('app.mie') }}",
            "{{ __('app.jue') }}",
            "{{ __('app.vie') }}",
            "{{ __('app.sab') }}"
          ],
          monthNames: [
              "{{ __('app.enero') }}",
              "{{ __('app.febrero') }}",
              "{{ __('app.marzo') }}",
              "{{ __('app.abril') }}",
              "{{ __('app.mayo') }}",
              "{{ __('app.junio') }}",
              "{{ __('app.julio') }}",
              "{{ __('app.agosto') }}",
              "{{ __('app.septiembre') }}",
              "{{ __('app.octubre') }}",
              "{{ __('app.noviembre') }}",
              "{{ __('app.diciembre') }}"
          ],
          firstDay: 1
      },
    });
  </script>
@enddatapicker

@section('scripts')
  <script>
    var lang = new Array();
    lang['Potencia Activa kW - Radiación W/m2'] = "{{__('app.Potencia Activa kW - Radiación W/m2')}}";
    lang['Generación kWh Total'] = "{{ __('app.Generación kWh Total') }}";
    lang['kWh - Generación CLP total'] = "{{ __('app.kWh - Generación CLP total') }}";
    lang['Producción y Consumo Mensual de Energía (kWh)'] = "{{__('app.Producción y Consumo Mensual de Energía (kWh)')}}";
    lang['Energía (kWh)'] = "{{ __('app.Energía (kWh)') }}";
    lang['Transferencia Mensual de Energia (CLP)'] = "{{ __('app.Transferencia Mensual de Energia (CLP)') }}";
    lang['Potencia Transferida (CLP)'] = "{{ __('app.Potencia Transferida (CLP)') }}";
    lang['Energía (CLP)'] = "{{ __('app.Energía (CLP)') }}";
    lang['Potencia Transferida (CLP)'] = "{{ __('app.Potencia Transferida (CLP)') }}";
    lang['Producción Total'] = "{{ __('app.Producción Total') }}";
    lang['Transferencia Mensual de Potencia (CLP)'] = "{{ __('app.Transferencia Mensual de Potencia (CLP)') }}";
    lang['Potencia (CLP)'] = "{{ __('app.Potencia (CLP)') }}";
    lang['Servicios Complementarios (CLP)'] = "{{ __('app.Servicios Complementarios (CLP)') }}";
    lang['Servicios Complementarios Total'] = "{{ __('app.Servicios Complementarios Total') }}";
    lang['Servicios Complementarios Mensual (CLP)'] = "{{ __('app.Servicios Complementarios Mensual (CLP)') }}";
    lang['Energía Inyectada (kWh)'] = "{{ __('app.Energía Inyectada (kWh)') }}";
    lang['Energía'] = "{{ __('app.Energía') }}";
    lang['Energía Generada (kWh - CLP)'] = "{{ __('app.Energía Generada (kWh - CLP)') }}";
    lang['Generación CLP'] = "{{ __('app.Generación CLP') }}";
    lang['Generación kWh'] = "{{ __('app.Generación kWh') }}";
	lang['Generación Total'] = "{{ __('app.Generación Total') }}";
	lang['kWh - ConsumoTotal'] = "{{ __('app.kWh - ConsumoTotal') }}";
    lang['año'] = "{{ __('app.año') }}";
    lang['Energía Generada Acumulada (kWh)'] = "{{ __('app.Energía Generada Acumulada (kWh)') }}";
    lang['Total'] = "{{ __('app.Total') }}";
    lang['Transferencia Mensual de Energía (CLP)'] = "{{ __('app.Transferencia Mensual de Energía (CLP)') }}";
    lang['Energía Producida (CLP)'] = "{{ __('app.Energía Producida (CLP)') }}";
    lang['Generación'] = "{{ __('app.Generación') }}";
    lang['kWh - Generacion'] = "{{ __('app.kWh - Generacion') }}";
    lang['kWh - Generacion'] = "{{ __('app.kWh - Generacion') }}";
    lang['Energía Consumida (kWh)'] = "{{ __('app.Energía Consumida (kWh)') }}";
    lang['kWh - Generacion CLP Total'] = "{{ __('app.kWh - Generacion CLP Total') }}";
  </script>
    @parent
    <script>
        var urlEnergiaRange = "{!!route('graficos-comercial.energiaRange')!!}";
        var urlEnergiaMes = "{!!route('graficos-comercial.energiaMes')!!}";
        var urlEnergiaVSMes = "{!!route('graficos-comercial.energiaVSMes')!!}";
        var urlEnergiaAnio = "{!!route('graficos-comercial.energiaAnio')!!}";
        var urlEnergiaVSAnio = "{!!route('graficos-comercial.energiaVSAnio')!!}";

        var urlEnergiaMensual = "{!!route('dashboard.energiaMensual')!!}";
        var urlEnergiaCLPMensual = "{!!route('dashboard.energiaCLPMensual')!!}";
        var urlPotenciaCLPMensual = "{!!route('dashboard.potenciaCLPMensual')!!}";
        var urlServCompCLPMensual = "{!!route('dashboard.servCompCLPMensual')!!}";
    </script>
    <script src="{{ asset('plugins/highcharts/highcharts.js') }}"></script>
    <script src="{{ asset('plugins/highcharts/exporting.js') }}"></script>
    <script src="{{ asset('plugins/highcharts/export-data.js') }}"></script>
    <script src="{{ asset('plugins/highcharts/moment.min.js') }}"></script>
    <script src="{{ asset('plugins/highcharts/moment-timezone-with-data-2012-2022.min.js') }}"></script>        
    <script src="{{ asset('js/web-comercial.js') }}"></script>    
@endsection 