@extends('layouts.app')

@content
    @slot('breadcrumbItems')
        <li class="breadcrumb-item active" aria-current="page">Energía</li>
    @endslot
    @slot('title')
        <h1 class="display-6 text-uppercase">Energía</h1>
    @endslot

    <form id="csv" method="post" action="{!!route('carga-datos.empresaCSV')!!}" encytpe="multipart/form-data">
        @csrf
        <input id="fileCSV" name="fileCSV" type="file" class="file"
            data-preview-file-type="text"
            data-browse-on-zone-click="true"
            data-allowed-file-extensions='["csv"]'
            required>

        <button type="submit" class="btn btn-primary-hover rounded-0 text-uppercase" id="guardar">
            Guardar
        </button>
    </form>

    <div class="row">
        <div class="col-12 mb-4">
            {{$dataTable->table(['id' => 'datatable'])}}
        </div>
    </div>
@endcontent

@section('scripts')
    @parent
    @jqueryConfirm
    @datatables
    <script>
        $(function() {
            {{$dataTable->generateScripts()}}
        })
    </script>
    
    <script>
        $("form").submit(function(event) {
            event.preventDefault();
            var data = new FormData(this);
            $.ajax({
                method: 'POST',
                url: "{!!route('carga-datos.empresaCSV')!!}",
                data: data,
                contentType: false,
                cache: false,
                processData:false,
                beforeSend: function(){
                    $("#guardar").attr("disabled","disabled");
                    $('#csv').css("opacity",".5");
                },
                success: function(data){
                    if(data.resp == 1){
                        alertify.success(data.mssg);
                    }else{
                       alertify.error(data.mssg);
                    }
                    $('form').trigger('reset'); 
                    $("#guardar").attr("disabled",false);
                    $('#csv').css("opacity","1");
                },
                error: function(xhr,status,error){
                    alertify.error("Disculpe, ocurrio un error");
                    $("#guardar").attr("disabled",false);
                    $('#csv').css("opacity","1");
                }
            });
        });

        $("#fileCSV").fileinput({
            language: "es",
            overwriteInitial: false,
            browseClass: "btn btn-primary d-none",
            showCaption: false,
            showRemove: false,
            showUpload: false
        });
    </script>
@endsection