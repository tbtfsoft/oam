@extends('layouts.app')

@content
  @slot('breadcrumbItems')
      <li class="breadcrumb-item active" aria-current="page">{{__('app.facturacion.recibidas')}}</li>
  @endslot
  @slot('title')
      <h1 class="display-6 text-uppercase">{{__('app.facturacion.recibidas')}}</h1>
  @endslot

  <form id="formFilter" class="form-validator">

    <div class="form-row mb-3">
        @php
            $selecEmpresa = Auth::user()->empresas->first();
            $selecEmpresa = $selecEmpresa ? $selecEmpresa->id : null
        @endphp

        @SelectCompanys(['class'=>"col-6 col-sm-6 col-companys", 'selected' => Input::get('empresa_xls') ? Input::get('empresa_xls') : $selecEmpresa])

        @SelectMonths(['class'=>"col-6 col-sm-6 col-months", "selected" => isset($month) ? $month :  null ])

        @SelectYears(['class'=>"col-6 col-sm-6 col-years", "selected" => isset($year) ? $year : null ])

        @include('layouts.components.selects.modules',['type' => 'select', 'modules' => [
                  [
                    'label' => __('app.Todos'),
                    'value' => 0,
                  ],
                  [
                    'label' => __('app.Energía'),
                    'value' => 1,
                  ],
                  [
                    'label' => __('app.Potencia'),
                    'value' => 2,
                  ]
                ],
        'class'=>"col-6 col-md-3 col-lg-2"])

        <div class="col-12 col-md-4 col-lg-4 col-xl-2">
          <div class="form-group">
            <label class="d-none d-sm-inline-block w-100"></label>
            <div class="btn-group w-100" role="group" aria-label="Button group with nested dropdown">
              <button type="button" class="btn btn--padding btn-primary-hover" id="filtrar">
                <i class="fa fa-search" aria-hidden="true"></i> {{__('app.filtrar')}}
              </button>
            </div>
          </div>
        </div>

      </div>
  </form>

  <div class="form-row">
    <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4" style="padding-bottom: 25px;">
      <div class="list bg-warning shadow-sm rounded overflow-hidden">
        <div class="list-item">
          <div class="list-thumb bg-warning-active rounded-circle shadow-sm p-2 h3 mx-auto mb-0" style="color: #FFFFFF;">
            <i class="fa fa-hand-holding-usd"></i>
          </div>
          <div class="list-body text-right"  style='font-family: "Courier New"'>
            <span class="list-title text-white" style='font-weight: bold;'>$ {{ number_format($data['total'],0,',','.') }}</span>
            <span class="list-content text-white">
            Total Facturas de recibidas
            </span>
          </div>
        </div>
      </div>
    </div>

  <div class="container-fluid">
    <div class="row">
      <div class="col-12 table-responsive">
        <table class="table table-hover table-sm">
          <thead>
            <tr>
              <th class="h6 font-weight-bold">{{__('app.Fecha de recepción SII') }}</th>
              <th class="h6 font-weight-bold">{{__('validation.attributes.rut') }}</th>
              <th class="h6 font-weight-bold">{{__('validation.attributes.razon_social') }}</th>
              <th class="h6 font-weight-bold">{{__('validation.attributes.tipo') }}</th>
              <th class="h6 font-weight-bold">{{__('validation.attributes.folio') }}</th>
              <th class="h6 font-weight-bold">{{__('validation.attributes.fecha_emision')}}</th>
              <th class="h6 font-weight-bold">{{__('validation.attributes.periodo_cen')}}</th>
              <th class="h6 font-weight-bold">{{__('validation.attributes.monto_total')}}</th>
              <th class="h6 font-weight-bold">{{__('validation.attributes.pago_recibido')}}</th>
            </tr>
          </thead>
          <tbody>
            @forelse ($data['collects'] as $key => $row)
              <tr class="">
                <td scope="row">{{$row->fecha_aceptacion_ts}}</td>
                <td scope="row">{{strtoupper(AppHelpers::rut($row->id_empresa_acreedora))}}</td>
                <td scope="row">{{strtoupper(AppHelpers::razon_social($row->id_empresa_acreedora))}}</td>
                <td scope="row">{{AppHelpers::tipo_factura($row->id_tipo_dte)}}</td>
                <td scope="row">{{$row->folio}}</td>
                <td scope="row">{{ date('d/m/Y', strtotime($row->fecha_emision)) }}</td>
                <td scope="row">{{$row->periodo}}</td>
                <td scope="row">$ {{ number_format($row->monto_bruto,0,',','.') }}</td>
                <td scope="row" class="align-center">
                  <h3 class="align-center" style="opacity: .8">
                    <span class="badge badge-{{   $row->pago ? 'primary' : 'danger' }} font-weight-normal w-100">
                      {{  $row->pago ? 'SI' : 'NO' }}
                    </span>
                  </h3>
                </td>
              </tr>
            @empty
              <tr>
                <td colspan="9">
                  <div class="alert alert-light text-dark font-weight-bold rounded text-center py3" role="alert">
                    {{__('app.SIN REGISTROS OBTENIDOS')}}
                  </div>
                </td>
              </tr>
            @endforelse
          </tbody>
        </table>
      </div>
      <div class="col-12 text-center max-auto">
        {{ $data['collects']->links() }}
      </div>
    </div>
  </div>
@endcontent

@section('scripts')
  @parent
  <script src="{{ asset('plugins/js-form-validator-2.1/js-form-validator.js') }}"></script>
  <script>
    let formData = serialize(document.querySelector('#formFilter'));
    Array.prototype.forEach.call(document.getElementsByClassName("page-link"), function(element, index) {
      element.setAttribute("href", element.getAttribute("href") + '&' + formData);
    });

    document.getElementById("filtrar").addEventListener("click", function() {
      if(document.querySelector('.form-validator')) {
        document.body.classList.add("loading");
        window.location = window.location.pathname + '?' + $('form.form-validator').serialize();
      }
    });
  </script>
@endsection
