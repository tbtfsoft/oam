@extends('layouts.app')

@content
  @slot('breadcrumbItems')
      <li class="breadcrumb-item active text-uppercase" aria-current="page">{{__('app.renname xml')}}</li>
  @endslot
  @slot('title')
      <h1 class="display-6 text-uppercase">{{__('app.renname xml')}}</h1>
  @endslot

  <form id="rename-xml--execute" method="post" action="{!!route('rename-xml.execute')!!}" class="form-validator"  encytpe="multipart/form-data">
    @csrf
	  @fileinput
      <div class="form-group">
        <input id="fileXML" name="filesXML[]" type="file" class="file" multiple data-allowed-file-extensions='["xml"]' required  data-browse-on-zone-click="true">
      </div>
      @slot('script_file_input')
        <script>
          $("#fileXML").fileinput({
            language: "{{ session()->has('locale') ? session()->get('locale') : 'es' }}",
            showPreview: false,
            overwriteInitial: false,
            browseClass: "btn btn-primary",
            previewFileIcon: '<i class="fas fa-file"></i>',
            showCaption: true,
            uploadAsync: false,
            showRemove: true,
            showUpload: false,
            previewFileIconSettings: {
              'docx': '<i class="fas fa-file-word text-primary"></i>',
              'xlsx': '<i class="fas fa-file-excel text-success"></i>',
              'pptx': '<i class="fas fa-file-powerpoint text-danger"></i>',
              'jpg': '<i class="fas fa-file-image text-warning"></i>',
              'pdf': '<i class="fas fa-file-pdf text-danger"></i>',
              'zip': '<i class="fas fa-file-archive text-muted"></i>',
            }
          });
        </script>
      @endslot
	  @endfileinput
    <button type="submit" class="btn btn-primary-hover text-uppercase" id="guardar">
      {{__('app.procesar')}}
    </button>

    <a href="{{ route('rename-xml.getFiles') }}" class="btn btn-secondary text-uppercase">{{__('app.descargar')}}</a>
	</form>
@endcontent

@section('styles')
@endsection

@section('scripts')
  @parent
  <script src="{{ asset('plugins/js-form-validator-2.1/js-form-validator.js') }}"></script>
  <script>
    var f = document.querySelector('.form-validator'),
        form = $('form#rename-xml--execute');

    if(f){
      var submitForm = function(err,res) {
        if(res) {
          let data = new FormData(form[0]);
          $.ajax({
            method: 'POST',
            url: form.attr('action'),
            data: data,
            processData: false,
            contentType: false,
            dataType: "json",
            beforeSend: function(){
              form.addClass('loading');
            },
            success: function(data){
              if (data.message) {
                Swal.fire({
                  text: data.message,
                  icon: "success"
                })
              }
              form.trigger('reset').removeClass('loading');
              window.location = '{{ route('rename-xml.getFiles') }}';
            },
            error: function(xhr,status,error){
              form.removeClass('loading').addClass('shake');
              Swal.fire(xhr.responseJSON.message, {
                icon: "error",
              });
            }
          });
        }else {
          form.addClass('shake');
          setTimeout(function() {
             form.removeClass("shake");
          }, 820)
        }
        return false;
      }

      f.setAttribute('novalidate', true);

      new Validator(f,function (err, res) {
          submitForm(err, res);
      }, {
          locale: "{{ session()->has('locale') ? session()->get('locale') : 'es' }}"
      });
    }
  </script>
@endsection