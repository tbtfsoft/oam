@extends('layouts.app')

@php 
    $mes_esp = '';
    $mes_ant = '';
    $dia = date('d');
    $mes = date('m');
    $anio = date('Y');
    $anio_ant = date('Y');
    $hora = date('H:i');
    if ($mes == '01') {
      $mes_esp = 'Enero';
      $mes_ant = 'Diciembre';
      $anio_ant = $anio_ant - 1;
    }elseif ($mes == '02') {
      $mes_esp = 'Febrero';
      $mes_ant = 'Enero';
    }elseif ($mes == '03') {
      $mes_esp = 'Marzo';
      $mes_ant = 'Febrero';
    }elseif ($mes == '04') {
      $mes_esp = 'Abril';
      $mes_ant = 'Marzo';
    }elseif ($mes == '05') {
      $mes_esp = 'Mayo';
      $mes_ant = 'Abril';
    }elseif ($mes == '06') {
      $mes_esp = 'Junio';
      $mes_ant = 'Mayo';
    }elseif ($mes == '07') {
      $mes_esp = 'Julio';
      $mes_ant = 'Junio';
    }elseif ($mes == '08') {
      $mes_esp = 'Agosto';
      $mes_ant = 'Julio';
    }elseif ($mes == '09') {
      $mes_esp = 'Septiembre';
      $mes_ant = 'Agosto';
    }elseif ($mes == '10') {
      $mes_esp = 'Octubre';
      $mes_ant = 'Septiembre';
    }elseif ($mes == '11') {
      $mes_esp = 'Noviembre';
      $mes_ant = 'Octubre';
    }elseif ($mes == '12') {
      $mes_esp = 'Diciembre';
      $mes_ant = 'Noviembre';
    }
  @endphp

@content
    @slot('breadcrumbItems')
        <li class="breadcrumb-item active" aria-current="page">PRMTE / STATUS</li>
    @endslot
    @slot('title')
        <div class="row align-items-center">
            <div class="col">
                <h1 class="display-6 text-uppercase my-0 pt-md-3">PRMTE</h1>
            </div>
            <div class="col-12 col-md-6">
                <div class="row justify-content-end">
                    @if(Auth::user()->hasRole(['super-admin']))
                        @SelectCompanys(['class'=>"col-12 col-sm-6", 'selected' => 414 ])
                    @else
                        @SelectCompanys(['class'=>"col-12 col-sm-6", 'selected' => Auth::user()->empresas->first()->id ])
                    @endif
                </div>
            </div>
        </div>
    @endslot
    <?php date_default_timezone_set('America/Santiago');?>
    
    <div class="row">
        <div class="col-12">
            <div class="title-grafic">
                <div style="font-weight: bold;"> {{__('app.Status actual')}} ({{$dia}}/{{$mes}}/{{$anio}} {{$hora}}) </div>
                <button type="button" class="btn btn-secondary rounded-circle opacity-5 btn-sm" data-toggle="tooltip" data-placement="top" title="{{__('app.Data obtained via PRMTE')}}">
                    <i class="fas fa-info-circle"></i>
                </button>
            </div>
        </div>

      <div class="col-6 col-sm-6 col-md-6 col-lg-6 mb-6">
            <div class="list bg-success shadow-sm rounded overflow-hidden">
                <div class="list-item">
                    <div class="list-thumb bg-success-active rounded-circle shadow-sm p-2 h3 mx-auto mb-0" style="color: #FFFFFF;">
                        <i class="fa fa-database"></i>
                    </div>
                    <div class="list-body text-right"  style='font-family: "Courier New"'>
                        <span id="registros_span" class="list-title text-white" style='font-weight: bold;'></span>
                        <span class="list-content text-white">{{__('app.Registros Recibidos') }}</span>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-6 col-md-6 col-lg-6 mb-6">
            <div class="list bg-success shadow-sm rounded overflow-hidden">
                <div class="list-item">
                    <div class="list-thumb bg-success-active rounded-circle shadow-sm p-2 h3 mx-auto mb-0" style="color: #FFFFFF;">
                        <i class="fa fa-wifi"></i>
                    </div>
                    <div class="list-body text-right"  style='font-family: "Courier New"'>
                        <span id="disponibilidad_span" class="list-title text-white" style='font-weight: bold;'></span>
                        <span class="list-content text-white">{{__('app.Disponibilidad') }}</span>

                    </div>
                </div>
            </div>
        </div>        

        <div class="col-12 col-md-12 my-12">
            <div class="shadow rounded bg-white">
                <div id="potenciaDiaria" class="p-2"></div>
            </div>
        </div>               
    </div>

    <div class="row mt-5">

    <input type="hidden" id="fecha_actual" name="fecha_actual" value="{{date('d/m/Y')}}" />    
    @endcontent

    @section('scripts')
        @parent
        <script>
            var urlPotenciaDiaria = "{!!route('prmte.potenciaDiariaPRMTE')!!}";
        </script>
        <script src="{{ asset('plugins/highcharts/highcharts.js') }}"></script>
        <script src="{{ asset('plugins/highcharts/exporting.js') }}"></script>
        <script src="{{ asset('plugins/highcharts/export-data.js') }}"></script>
        <script src="{{ asset('plugins/highcharts/moment.min.js') }}"></script>
        <script src="{{ asset('plugins/highcharts/moment-timezone-with-data-2012-2022.min.js') }}"></script>        
        <script src="{{ asset('js/prmte.js') }}"></script>
    @endsection
    @section('styles')
    <style>
        .shadow { -webkit-transition: .5s all; -o-transition: .5s all; transition: .5s all; }
        .shadow:hover { box-shadow: 0 .125rem .25rem rgba(51,51,51,.075)!important; }
    </style>
@endsection
