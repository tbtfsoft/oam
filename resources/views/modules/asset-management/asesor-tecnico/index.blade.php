@extends('layouts.app')

@content
  @slot('breadcrumbItems')
      <li class="breadcrumb-item active" aria-current="page">ASESOR TECNICO</li>
  @endslot
  @slot('title')
      <h1 class="display-6 text-uppercase">ASESOR TECNICO</h1>
  @endslot
@endcontent

@section('styles')
@endsection

@section('scripts')