
<p>Estimado(a), {{$data['empresa']->nombre_xls}}</p>
<p>Aquí lo correspondiente a las  {{$data['empresa']->nombre_xls}}</p>
<p>
    Transferencia económicas  {{ $data['mes'] }} {{$data['anio']}} - Cuadros de Pago "{{$data['empresa']->nombre_xls}}"
</p>
<p>
    <b>Enegía:</b>  {{ $data['energiaCenCosto']}} - Nro facturas a generar ({{$data['energiaCenCount']}}) <br>
    @if ($data['potenciaCenCount'] > 0)
        <b>Potencia:</b>  {{$data['potenciaCenCosto']}} - Nro facturas a generar ({{$data['potenciaCenCount']}}) <br>
    @endif
</p>
<p>
Total a facturar (CLP): <b>{{ $data['costoTotal'] }}</b><br>
Total nro. facturas a generar: <b> {{ $data['potenciaCenCount'] + $data['potenciaCenCount'] }}</b>
</p>

<br>
<p>
    Nota: Adjunto archivos de integración para poder facturar en su sistema ERP. <br>
    Quedo atenta a sus comentarios, Saludos cordiales
</p>
