@extends('layouts.app')

@content
  @slot('breadcrumbItems')
        <li class="breadcrumb-item active text-uppercase" aria-current="page">{{__('app.cargar-data')}}</li>
  @endslot
  @slot('title')
      <h1 class="display-6 text-uppercase">
        {{__('app.cargar-data.facturas')}}
      </h1>
  @endslot
  
  <form id="form-emitidas" class="form-validator">
    @csrf
    <div class="form-row mb-3">

      @SelectMonths(['class'=>"col-6 col-sm-6 col-months"])

      @SelectYears(['class'=>"col-6 col-sm-6 col-years"])

    </div>

    @fileinput
      <div class="form-group">
        <input id="files" name="files[]" type="file" class="file" multiple data-allowed-file-extensions='["xml"]' required  data-browse-on-zone-click="true">
      </div>
      @slot('script_file_input')
        <script>
          $("#files").fileinput({
            language: "{{ session()->has('locale') ? session()->get('locale') : 'es' }}",
            showPreview: false,
            overwriteInitial: false,
            browseClass: "btn btn-primary",
            previewFileIcon: '<i class="fas fa-file"></i>',
            showCaption: true,
            uploadAsync: false,
            showRemove: true,
            showUpload: false,
            previewFileIconSettings: {
              'docx': '<i class="fas fa-file-word text-primary"></i>',
              'xlsx': '<i class="fas fa-file-excel text-success"></i>',
              'pptx': '<i class="fas fa-file-powerpoint text-danger"></i>',
              'jpg': '<i class="fas fa-file-image text-warning"></i>',
              'pdf': '<i class="fas fa-file-pdf text-danger"></i>',
              'zip': '<i class="fas fa-file-archive text-muted"></i>',
            }
          });
        </script>
      @endslot
    @endfileinput
   
    <button type="submit" class="btn btn-primary-hover rounded-0 text-uppercase pt-2 mt-2" id="guardar">
      {{ __('app.guardar') }}
    </button>
  </form>
@endcontent

@section('scripts')
  @parent
  <style>
    .file-drop-zone {
      overflow-y: scroll;
      height: 300px;
      max-height: 300px;
    }
  </style>
  <script src="{{ asset('plugins/js-form-validator-2.1/js-form-validator.js') }}"></script>
  <script>
    var f = document.querySelector('.form-validator'),
        form = $('form#form-emitidas');

    if(f){
      var submitForm = function(err,res) {
        if(res) {
          let data = new FormData(form[0]);
          $.ajax({
              method: 'POST',
              url: "{{ route('cargar-data.facturas-emitidas.ajaxCSV') }}",
              data: data,
              processData: false,
              contentType: false,
              dataType: "json",
              beforeSend: function(){
                form.addClass('loading');
              },
              success: function(data) {
                form.removeClass('loading');
                if (data.catch && data.catch.length > 0) {
                  let error = '<div class="text-left"><ul class="list-unstyled text-center">';
                  for (var i = data.catch.length - 1; i >= 0; i--) {
                    error  = error + '<li> <b>' + data.catch[i].file + '</b> -  ' + data.catch[i].error + '</li>'
                  }
                  error = error + '</ul></div>';
                  Swal.fire({
                      title: data.message,
                      type: 'info',
                      width: '80%',
                      background: '#fff url(/images/trees.png)',
                      html: error
                    })
                }else {
                  Swal.fire({
                    text: data.message,
                    icon: "success"
                  })
                  form.trigger('reset');
                }
              },
              error: function(xhr,status,error){
                  form.removeClass('loading');
                  let errorMessage = xhr.responseJSON.message ?  xhr.responseJSON.message : error
                  Swal.fire(errorMessage, {
                      icon: "error",
                  });
              }
          });
        }else {
          form.addClass('shake');
          setTimeout(function() {
             form.removeClass("shake");
          }, 820)
        }
        return false;
      }

      f.setAttribute('novalidate', true);

      new Validator(f,function (err, res) {
        submitForm(err, res);
      }, {
        locale: "{{ session()->has('locale') ? session()->get('locale') : 'es' }}"
      });
    }
  </script>
@endsection