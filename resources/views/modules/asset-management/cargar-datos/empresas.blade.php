@extends('layouts.app')

@content
    @slot('breadcrumbItems')
        <li class="breadcrumb-item active text-uppercase" aria-current="page">{{__('app.cargar-data')}}</li>
    @endslot
    @slot('title')
        <h1 class="display-6 text-uppercase">Empresas</h1>
    @endslot

    <form id="csv" method="post" action="{!!route('cargar-data.empresaCSV')!!}" class="form-validator"  encytpe="multipart/form-data">
        @csrf
        
        

        @fileinput
            <div class="form-group">
                <input id="fileCSV" name="fileCSV" type="file" class="file"
                    data-rule="required"
                    data-preview-file-type="text"
                    data-browse-on-zone-click="true"
                    data-allowed-file-extensions='["csv"]'
                    required>
            </div>
        @endfileinput

        <button type="submit" class="btn btn-primary-hover rounded-0 text-uppercase pt-2 mt-2" id="guardar">
            {{ __('app.guardar') }}
        </button>
        <a href="{{ route('empresas.create') }}" title="{{ __('Empresas') }}" class="btn btn-primary rounded-0 text-uppercase mt-2">
            <span>{{ __('app.agregar') }}</span>
        </a>
    </form>

    <div class="row mt2">
        <div class="col-12 mb-4">
            <hr>
            {{$dataTable->table(['id' => 'datatable'])}}
        </div>
    </div>
@endcontent

@section('scripts')
    @parent
    @jqueryConfirm
    @datatables
    <script src="{{ asset('plugins/js-form-validator-2.1/js-form-validator.css') }}"></script>
    <script src="{{ asset('plugins/js-form-validator-2.1/js-form-validator.js') }}"></script>
    <script>
        $(function() { {{$dataTable->generateScripts()}} })

        var f = document.querySelector('.form-validator'),
            form = $('form#csv');

        if(f){
            var submitForm = function(err,res) {
                if(res) {
                    let data = new FormData(form[0]);
                    $.ajax({
                        method: 'POST',
                        url: form.attr('action'),
                        data: data,
                        processData: false,
                        contentType: false,
                        dataType: "json",
                        beforeSend: function(){
                            form.addClass('loading');
                        },
                        success: function(data){
                            Swal.fire({
                              text: data.message,
                              icon: "success"
                            })
                            form.trigger('reset').removeClass('loading');
                        },
                        error: function(xhr,status,error){
                            form.removeClass('loading').addClass('shake');
                            Swal.fire(xhr.responseJSON.message, {
                                icon: "error",
                            });
                        }
                    });
                }else {
                    form.addClass('shake');
                    setTimeout(function() {
                       form.removeClass("shake");
                    }, 820)
                }
                return false;
            }

            f.setAttribute('novalidate', true);

            new Validator(f, function (err, res) {
                submitForm(err, res);
            }, {
                locale: '{{ App::getLocale() }}'
            });
        }
    </script>
@endsection