@extends('layouts.app')

@content
    @slot('breadcrumbItems')
        <li class="breadcrumb-item text-uppercase active" aria-current="page">
            {{ __('app.cargar-data') }}
        </li>
    @endslot

    @slot('title')
        <h1 class="display-6 text-uppercase">
            {{ __('app.cargar-data.template-email.index') }}
        </h1>
    @endslot

    <form method="POST" class="form-validator" action="{!!route('cargar-data.template-email.filter')!!}">
        @csrf
        <div class="form-row mb-3">
            @SelectCompanys(['class'=>"col-6 col-sm-6 col-md-4 col-lg-4 col-xl-3"])

            @SelectTypeDocument(['class'=>"col-6 col-sm-6 col-document"])

            @SelectMonths(['class'=>"col-6 col-sm-6 col-md-4 col-lg-4 col-xl-2 col-months"])

            @SelectYears(['class'=>"col-6 col-sm-6 col-md-4 col-lg-4 col-xl-2 col-years"])

            <div class="col-12 col-md-4 col-lg-4 col-xl-2">
              <div class="form-group">
                <label class="d-none d-sm-inline-block w-100"></label>
                <div class="btn-group w-100" role="group" aria-label="Button group with nested dropdown">
                  <button type="submit" class="btn btn--padding btn-primary-hover" id="filtrar">
                    <i class="fa fa-search" aria-hidden="true"></i> {{__('app.filtrar')}}
                  </button>
                </div>
              </div>
            </div>
        </div>

    </form>

    <div id="response"></div>
    @isset ($html)
        <textarea id="summernote" name="editordata">{!! $html !!}</textarea>
        <form class="my-2" id="summernote_form">
            <label for="summernote_subject" class="w-100 mb-0 mt-2">
                Asusto:
            </label>
            <input type="text" class="form-control" id="summernote_subject" name="summernote_subject" value="{{ 'transferencia economicas de '. $empresa->nombre_xls }}">

            <label for="summernote_emails" class="w-100 mb-0 mt-2">
                Agregue los email separados por coma (,).
            </label>
            <div class="d-flex">
                <input type="text" id="summernote_emails" name="summernote_emails" class="form-control" placeholder="email@correo.com, email2@correo.com, email3@correo.com">
                <button onclick="submitForm()" type="button" class="btn btn-sm btn-danger px-3">
                    Enviar
                </button>
            </div>
        </form>
    @endisset
@endcontent

@section('scripts')
  @parent
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
    <script src="{{ asset('plugins/js-form-validator-2.1/js-form-validator.js') }}"></script>

    <script>
        $('#summernote').summernote({
            tabsize: 2,
            height: 300,
            lang: 'es-ES'
        });

        var f = document.querySelector('.form-validator')
        if(f) {
            f.setAttribute('novalidate', true);
            new Validator(f,function (err, res) {
                return res;
            }, {
                locale: '{{ App::getLocale() }}'
            });
        }

        var submitForm = function() {
            const form = $('form#summernote_form')
            let data = {
                html: $('#summernote').summernote('code'),
                emails: document.getElementById("summernote_emails").value,
                subject: document.getElementById("summernote_subject").value
            }
            if (data.emails) {
                form.addClass('loading');
                $.ajax({
                    method: 'POST',
                    url: "{{ route('cargar-data.template-email.send') }}",
                    dataType: "json",
                    data,
                    beforeSend: function() {
                        form.addClass('loading');
                    },
                    success: function(data) {
                        form.removeClass('loading');
                        Swal.fire(data.message, {
                            icon: "success",
                        });
                    },
                    error: function(xhr,status,error){
                        form.removeClass('loading');
                        Swal.fire(xhr.responseJSON.message, {
                            icon: "error",
                        });
                    }
                });
            } else {
                Swal.fire('Debe agregar por lo menos un correo electrónico', {
                    icon: "error",
                });
            }
        }
    </script>
@endsection
