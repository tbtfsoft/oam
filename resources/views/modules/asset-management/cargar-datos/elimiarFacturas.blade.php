@extends('layouts.app')

@content
    @slot('breadcrumbItems')
        <li class="breadcrumb-item active text-uppercase" aria-current="page">{{__('app.cargar-data.facturas.eliminar')}}</li>
    @endslot
    @slot('title')
        <h1 class="display-6 text-uppercase">{{__('app.cargar-data.facturas.eliminar')}}</h1>
    @endslot
    <form class="form-validator" method="POST" action="{{ route('cargar-data.facturas.eliminar.post') }}">
      @csrf
      <div class="form-row mb-3">
        @SelectCompanys(['class'=>"col-6 col-sm-6 col-companys"])

        @SelectMonths(['class'=>"col-6 col-sm-6 col-months"])

        @SelectYears(['class'=>"col-6 col-sm-6 col-years"])

        <div class="col-12 col-md-4 col-lg-4 col-xl-2">
          <div class="form-group">
            <label class="d-none d-sm-inline-block w-100"></label>
            <div class="btn-group w-100" role="group" aria-label="Button group with nested dropdown">
              <button type="submit" class="btn btn--padding btn-primary-hover" id="filtrar">
                <i class="fa fa-search" aria-hidden="true"></i> {{__('app.filtrar')}}
              </button>
            </div>
          </div>
        </div>
      </div>
    </form>
@endcontent

@section('scripts')
  @parent
  <script src="{{ asset('plugins/js-form-validator-2.1/js-form-validator.js') }}"></script>
  <script>
      var f = document.querySelector('.form-validator')
      if(f) {
        f.setAttribute('novalidate', true);
        new Validator(f,function (err, res) {
          return res;
        }, {
          locale: '{{ App::getLocale() }}'
        });
      }
  </script>
@endsection