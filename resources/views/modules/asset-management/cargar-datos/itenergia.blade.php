@extends('layouts.app')

@content
    @slot('breadcrumbItems')
        <li class="breadcrumb-item text-uppercase active" aria-current="page">{{ __('app.cargar-data') }}</li>
    @endslot
    @slot('title')
        <h1 class="display-6 text-uppercase">{{ __('app.cargar-data.it-energia') }}</h1>
    @endslot

    <form id="csv" method="post" encytpe="multipart/form-data" class="form-validator">
        @csrf
        <div class="form-row mb-3">
            @SelectTypeDocument(['class'=>"col-6 col-sm-6 col-md-4 col-lg-3"])

            @SelectMonths(['class'=>"col-6 col-sm-6 col-md-4 col-lg-4 col-xl-2 col-months"])

            @SelectYears(['class'=>"col-6 col-sm-6 col-md-4 col-lg-4 col-xl-2 col-years"])
        </div>

        

        <div id="response"></div>

        @fileinput
            <div class="form-group">
                <input id="fileCSV" name="fileCSV" type="file" class="file"
                    data-rule="required"
                    data-preview-file-type="text"
                    data-browse-on-zone-click="true"
                    data-allowed-file-extensions='["csv"]'
                    required>
            </div>
        @endfileinput


        <button type="submit" class="btn btn-primary-hover rounded-0 text-uppercase mt-2 pt-2" id="guardar">
            {{ __('app.guardar') }}
        </button>
    </form>
@endcontent

@section('scripts')
    @parent
    <script src="{{ asset('plugins/js-form-validator-2.1/js-form-validator.js') }}"></script>
    <script>
        var f = document.querySelector('.form-validator'),
            form = $('form#csv'),
            urlCSV = "{!! route('cargar-data.energiaCSV') !!}",
            urlExistData = "{!! route('cargar-data.energiaCSV.isdata',['xx','yy','zz'])!!}";

        function setData(form) {
            let data = new FormData(form[0]);
            $.ajax({
                method: 'POST',
                url: urlCSV,
                data: data,
                processData: false,
                contentType: false,
                dataType: "json",
                beforeSend: function(){
                    form.addClass('loading');
                },
                success: function(data){
                    if (data.catch.length > 0) {
                        let error = '<p>{{ __("app.No sé pudo procesar estas empresas") }}:<p>' +
                                    '<div class="text-left"><ul>';
                        for (var i = data.catch.length - 1; i >= 0; i--) {
                            error = error + '<li>'+data.catch[i]+'</li>'
                        }
                        error = error + '</ul></div>';
                        Swal.fire({
                            title: data.message,
                            type: 'info',
                            width: '80%',
                            background: '#fff url(/images/trees.png)',
                            html: error
                          })
                    }else {
                        Swal.fire(data.message, {
                            icon: "success",
                        });
                    }
                    form.trigger('reset').removeClass('loading');
                },
                error: function(xhr,status,error){
                    form.removeClass('loading');
                    Swal.fire(xhr.responseJSON.message, {
                        icon: "error",
                    });
                }
            });
        }

    if(f){
        var submitForm = function(err,res) {
            if(res) {
                let url  = urlExistData .replace("xx", $("select[name*='mes']").val())
                            .replace("yy", $("select[name*='anio']").val())
                            .replace("zz", $("select[name*='tipo_doc_cen']").val())
                $.ajax({
                    method: 'GET',
                    url: url,
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    beforeSend: function(){
                        form.addClass('loading');
                    },
                    success: function(data){
                        if(!data.is_data) {
                            setData(form)
                        }
                        else {
                            form.removeClass('loading');
                            Swal.fire({
                              title: data.title,
                              text: data.text,
                              icon: "warning",
                              buttons: true,
                              dangerMode: true,
                            })
                            .then((willDelete) => {
                              if (willDelete) {
                                form.addClass('loading');
                                setData(form)
                              }
                            });
                        }
                    },
                    error: function(xhr,status,error){
                        form.removeClass('loading');
                        Swal.fire(xhr.responseJSON.message, {
                            icon: "error",
                        });
                    }
                });
            }else {
                form.addClass('shake');
                setTimeout(function() {
                   form.removeClass("shake");
                }, 820)
            }
            return false;
        }

        f.setAttribute('novalidate', true);

        new Validator(f,function (err, res) {
            submitForm(err, res);
        }, {
            locale: '{{ App::getLocale() }}'
        });
    }
    </script>
@endsection