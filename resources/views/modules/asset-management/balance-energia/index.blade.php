@extends('layouts.app')

@content
    @slot('breadcrumbItems')
        <li class="breadcrumb-item active text-uppercase" aria-current="page">{{__('app.cargar-data') }}</li>
    @endslot
    @slot('title')
        <h1 class="display-6 text-uppercase">{{ __('app.cargar-data.it-energia') }}</h1>
    @endslot

    <form id="csv" method="post" encytpe="multipart/form-data" class="form-validator">
        @csrf
        <div class="form-row mb-3">            
            @SelectCompanys(['class'=>"col-6 col-sm-6 col-md-4 col-lg-4 col-xl-3"])

            @SelectMonths(['class'=>"col-6 col-sm-6 col-md-4 col-lg-4 col-xl-2"])

            @SelectYears(['class'=>"col-6 col-sm-6 col-md-4 col-lg-4 col-xl-2"])
        </div>

        <div id="response"></div>

        <button type="submit" class="btn btn-primary-hover rounded-0 text-uppercase pt-2 mt-2" id="guardar">
            {{__('app.ver balance de energía')}}
        </button>
    </form>
@endcontent

@section('scripts')
    @parent
    <script src="{{ asset('plugins/js-form-validator-2.1/js-form-validator.js') }}"></script>
    <script>
        var f = document.querySelector('.form-validator'),
            form = $('form#csv'),
            formData;

            formData = new FormData(form[0]);

        if(f){
            var submitForm = function(err,res) {
                if(res) {
                    formData = new FormData(form[0]);
                    let empresa = document.getElementById('empresa_xls');
                    formData.append("empresa", empresa.options[empresa.selectedIndex].innerHTML);
                    $.ajax({
                        method: 'POST',
                        url: "{!!route('balance-energia.check')!!}",
                        data: formData,
                        processData: false,
                        contentType: false,
                        dataType: "json",
                        beforeSend: function(){
                            form.addClass('loading');
                        },
                        success: function(data){
                            $('#response').html(data.response);
                            form.trigger('reset').removeClass('loading');
                        },
                        error: function(xhr,status,error){
                            form.removeClass('loading');
                            Swal.fire(xhr.responseJSON.message, {
                                timer: 3000,
                                icon: "error",
                            });
                        }
                    });
                }else {
                    form.addClass('shake');
                    setTimeout(function() {
                       form.removeClass("shake");
                    }, 820)
                }
                return false;
            }

            f.setAttribute('novalidate', true);

            new Validator(f,function (err, res) {
                submitForm(err, res);
            });

            $.ajax({
            method: 'POST',
            url: "{!!route('balance-energia.check')!!}",
            data: formData,
            processData: false,
            contentType: false,
            dataType: "json",
            beforeSend: function(){
                form.addClass('loading');
            },
            success: function(data){
                $('#response').html(data.response);
                form.trigger('reset').removeClass('loading');
            },
            error: function(xhr,status,error){
                form.removeClass('loading');
                Swal.fire(xhr.responseJSON.message, {
                    timer: 3000,
                    icon: "error",
                });
            }
            });
        }


    </script>
@endsection