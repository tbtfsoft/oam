@php
  $energiaOUT = abs(floatval($data->energiaPFV['kwh_out'])) - abs(floatval($data->energiaCEN['kwh_out']));
  $energiaIN = abs(floatval($data->energiaPFV['kwh_in'])) - abs(floatval($data->energiaCEN['kwh_in']));
  $symbol = 'kWh';
@endphp


<div class="jumbotron py-3 bg-light px-4" id="warning-energia">
  <div class="row">
    <div class="col-12">
      <div class="table-responsive">
        <table class="table table-striped mb-0">
          <thead>
            <tr>
              <th>{{__('app.Data obtenida')}}</th>
              <th>{{__('app.Energía inyectada')}}</th>
              <th>{{__('app.Energía consumida')}}</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{{ __('app.Medidor') }}</td>
              <td><b>{{ $data->energiaPFV['kwh_in'] }} {{ $symbol }}</b></td>
              <td><b>{{ $data->energiaPFV['kwh_out'] }} {{ $symbol }}</b></td>
            </tr>
            <tr>
              <td>CEN</td>
              <td><b>{{ $data->energiaCEN['kwh_in'] }} {{ $symbol }}</b></td>
              <td><b>{{ $data->energiaCEN['kwh_out'] }} {{ $symbol }}</b></td>
            </tr>
            <tr>
              <td><b>{{__('app.DELTA')}}</b></td>
              <td><b>{{ $energiaIN }} {{ $symbol }}</b></td>
              <td><b>{{ $energiaOUT  }} {{ $symbol }}</b></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="col-12">
      <hr>
    </div>
    <div class="col-6">
      @php
        $validationIN = $energiaIN > 1 || $energiaIN < -1;
      @endphp
      <div class="jumbotron py-3 px-4 {{ $validationIN ? 'bg-danger' : 'bg-success' }}">
        <div class="row">
          <div class="col-12">
            <h3 class="text-uppercase">{{ __('app.Energía inyectada') }}</h3>
            <div class="text-white">
              @if ($validationIN)
                <p class="mb-0"><b>{{ __('app.Error en el balance de energía.') }}</b></p>
              @else
                <p class="mb-0"><b>{{ __('app.Balance de energía exitoso.') }}</b></p>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-6">
      @php
        $validationOUT = $energiaOUT > 1 || $energiaOUT < -1;
      @endphp
      <div class="jumbotron py-3 px-4 {{ $validationOUT ? 'bg-danger' : 'bg-success' }}">
        <div class="row">
          <div class="col-12">
            <h3 class="text-uppercase">{{ __('app.Energía consumida') }}</h3>
            <div class="text-white">
              @if ($validationOUT)
                <p class="mb-0"><b>{{__('app.Error en el balance de energía.')}}</b></p>
              @else
                <p class="mb-0"><b>{{__('app.Balance de energía exitoso.')}}</b></p>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>