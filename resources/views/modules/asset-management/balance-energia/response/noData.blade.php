<div class="jumbotron bg-{{ isset($type) ? $type : 'danger' }} py-3 px-4" id="warning-energia">
  <h4 class="my-0 text-uppercase">
		{{ isset($title) ? $title : _('app.No poseemos data') }}
  </h4>
  <p class="my-0">
  	{{__('app.Para la empresa :empresa en el período :periodo en nuestra base de datos de oEnergy.',
  			['empresa' => $empresa, 'periodo' => $periodo])
  	}}
  	<br> {{ __('app.Si desea continuar presione') }} '<b>{{ _('app.generar balance') }}</b>' {{ _('app.para procesar la data de CEN.') }}
  </p>
  <hr  class="my-1">
  <a class="btn btn-dark btn-sm text-white text-uppercase" onclick="generarBalance()">
  	{{ _('app.generar balance') }}
  </a>
</div>