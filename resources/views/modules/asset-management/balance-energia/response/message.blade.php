<div class="jumbotron bg-{{$type}} {{ $type == 'danger' ? 'text-white' : '' }} py-3 px-4" id="warning-energia">
	@if ( isset($title) && !is_null($title))
  	<h4 class="my-0">
			{{$title}}
  	</h4>
  	<hr class="my-1">
  @endif
  <p class="my-0">
  	@isset ($message)
  		{!! $message !!}
  	@endisset
  </p>
</div>