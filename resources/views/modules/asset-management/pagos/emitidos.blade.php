@extends('layouts.app')

@content
    @slot('breadcrumbItems')
        <li class="breadcrumb-item active text-uppercase" aria-current="page">{{__('app.pagos.emitidos')}}</li>
    @endslot
    @slot('title')
        <h1 class="display-6 text-uppercase">{{__('app.pagos.emitidos')}}</h1>
    @endslot
    <form id="formFilter" class="form-validator">
      <div class="form-row mb-3">
        @SelectCompanys(['class'=>"col-6 col-sm-6 col-companys"])

        @SelectMonths(['class'=>"col-6 col-sm-6 col-months"])

        @SelectYears(['class'=>"col-6 col-sm-6 col-years"])

        <div class="col-12 col-md-4 col-lg-4 col-xl-2">
          <div class="form-group">
            <label class="d-none d-sm-inline-block w-100"></label>
            <div class="btn-group w-100" role="group" aria-label="Button group with nested dropdown">
              <button type="button" class="btn btn--padding btn-primary btn-primary-hover text-uppercase" id="filtrar">
                <i class="fa fa-search" aria-hidden="true"></i> {{__('app.filtrar')}}
              </button>
            </div>
          </div>
        </div>
      </div>

      <div class="container-fluid">
        <div class="row">
          <div class="col-12 table-responsive">
            <table class="table table-hover table-sm">
              <thead>
                <tr class="">
                  <th class="h6 font-weight-bold">{{__('validation.attributes.razon_social')}}</th>
                  <th class="h6 font-weight-bold">{{__('validation.attributes.fecha_factura')}}</th>
                  <th class="h6 font-weight-bold">{{__('validation.attributes.fecha_pago')}}</th>
                  <th class="h6 font-weight-bold">{{__('validation.attributes.folio_factura')}}</th>
                  <th class="h6 font-weight-bold">{{__('validation.attributes.monto_total')}}</th>
                  <th class="h6 font-weight-bold"  style="width: 70px">{{__('validation.attributes.emitido')}}</th>
                </tr>
              </thead>
              <tbody>
              @forelse ($data['collects'] as $key => $row)
                <tr>
                  <td scope="row">
                    <p class="mb-0">{{strtoupper(AppHelpers::rut($row->id_empresa_acreedora))}}</p>
                    <small class="text-truncate">
                      {{strtoupper(AppHelpers::razon_social($row->id_empresa_acreedora))}}
                    </small>
                  </td>
                  <td scope="row">{{ date('d/m/Y', strtotime($row->fecha_emision) )}}</td>
                  <td scope="row" id="td-pago-fecha-{{$row->id}}">{{ $row->pago_fecha ? date('d/m/Y', strtotime($row->pago_fecha)) : '' }}</td>
                  <td scope="row">{{$row->folio}}</td>
                  <td scope="row">$ {{ number_format($row->monto_bruto,0,',','.') }}</td>
                  <td class="action-td td-{{$row->id}} {{ $row->pago ? 'active' : '' }}">
                    <button class="btn btn-primary btn-sm w-100" type="button" onclick="cancelarFactura({{$row->id}})">
                      {{__('app.si')}}
                    </button>
                    <button class="btn btn-dark btn-sm w-100" style="opacity: .5" type="button" onclick="updateFactura({{$row->id}})">
                      {{__('app.no')}}
                    </button>
                  </td>
                </tr>
              @empty
                <tr>
                  <td colspan="7">
                    <div class="alert alert-light text-dark font-weight-bold rounded text-center py3" role="alert">
                      {{__('app.SIN REGISTROS OBTENIDOS')}}
                    </div>
                  </td>
                </tr>
              @endforelse
            </tbody>
            </table>
          </div>
          <div class="col-12 text-center max-auto" id="filters-collects">
            {{ $data['collects']->links() }}
          </div>
        </div>
      </div>
    </form>
@endcontent
@section('scripts')
  @parent
  <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css"/>
  <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
  <script src="https://unpkg.com/gijgo@1.9.13/js/messages/messages.es-es.js" type="text/javascript"></script>

  <script>
    let formData = serialize(document.querySelector('#formFilter'));
    console.log(document.getElementsByClassName("page-link"));
    Array.prototype.forEach.call(document.getElementsByClassName("page-link"), function(element, index) {
      element.setAttribute("href", element.getAttribute("href") + '&' + formData);
    });

    document.getElementById("filtrar").addEventListener("click", function() {
      if(document.querySelector('.form-validator')) {
        document.body.classList.add("loading");
        window.location = window.location.pathname + '?' + $('form.form-validator').serialize();
      }
    });

    function cancelarFactura(factura_id) {
      Swal.fire({
        title: "{{__('app.esta seguro?')}}",
        text: "{{__('app.Se eliminara la fecha de pago de esta factura!')}}",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "{{__('app.si, eliminar!')}}"
      }).then((result) => {
        if (result.value) {
          $.ajax({
            method: 'POST',
            url: "{{ route('pagos.cancel') }}",
            data: {
              factura_id: factura_id
            },
            dataType: "json",
            success: function(data) {
              Swal.fire(
                "{{__('app.se restableció')}}",
                "{{__('app.La factura no se encuentra pagada')}}",
                "success"
              )
              $('.td-'+factura_id).toggleClass('active');
              $('#td-pago-fecha-' + factura_id).html()
            },
            error: function(xhr,status,error){
              Swal.fire(xhr.responseJSON.message, {
                icon: "error",
              });
            }
          });
        }
      })
    }
    function updateFactura(factura_id) {
      Swal.mixin({
        html: `
        <form class="idform">
        <div class="row">
          <div class="form-group col-12 col-sm-6">
            <label for="pago_fecha">{{__('validation.attributes.fecha_pago')}}</label>
            <input name="pago_fecha" id="pago_fecha" type='text' class="form-control" placeholder="{{__('validation.attributes.fecha_emision')}}" data-rule="required">
          </div>
          <div class="form-group col-12 col-sm-6">
            <label for="codigo" class="align-left">{{__('app.codigo de pago')}}</label>
            <input name="codigo" id="codigo" type='text' class="form-control" placeholder="{{__('app.codigo de pago')}}" required>
          </div>
        </div>
        </form>`,
        confirmButtonText: "{{__('app.siguiente')}}",
        showCancelButton: true,
        onOpen: function() {
          $('#pago_fecha').datepicker({
            uiLibrary: 'bootstrap4',
            locale: 'es-es',
            weekStartDay: 1
          });
        },
        preConfirm: () => {
          if (!document.getElementById('pago_fecha').value) {
            Swal.showValidationMessage("{{__('app.complete los campos requeridos')}}")
          }
        }
      }).queue([{title: "{{__('app.factura')}}"}
      ]).then((result) => {
        if (result.value) {
          $.ajax({
            method: 'POST',
            url: "{{ route('pagos.updated') }}",
            data: {
              pago_fecha: document.getElementById('pago_fecha').value,
              pago_codigo: document.getElementById('codigo').value,
              factura_id: factura_id
            },
            dataType: "json",
            success: function(data) {
              Swal.fire({
                type: 'success',
                title: data.message
              })
              $('.td-'+factura_id).toggleClass('active');
              $('#td-pago-fecha-' + factura_id).html(data.factura.pago_fecha)
            },
            error: function(xhr,status,error){
              Swal.fire(xhr.responseJSON.message, {
                icon: "error",
              });
            }
          });
        }
      })
    }
  </script>
  <style>
    .action-td > * { display: none; }
    .action-td:not(.active) button:last-child,
    .action-td.active button:first-child { display: block; }
  </style>
@endsection