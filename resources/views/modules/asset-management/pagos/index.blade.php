@extends('layouts.app')

@content
  @slot('breadcrumbItems')
    <li class="breadcrumb-item active" aria-current="page">Pagos</li>
  @endslot
  @slot('title')
    <h1 class="display-6 text-uppercase">Pagos</h1>
  @endslot

  <form id="csv" method="post" encytpe="multipart/form-data" class="form-validator">
    @csrf
    <div class="form-row mb-3">            
      @SelectCompanys(['class'=>"col-6 col-sm-6 col-md-4 col-lg-4 col-xl-3"])

      @SelectTypeDocument(['class'=>"col-6 col-sm-6 col-document"])

      @SelectMonths(['class'=>"col-6 col-sm-6 col-md-4 col-lg-4 col-xl-2 col-months"])

      @SelectYears(['class'=>"col-6 col-sm-6 col-md-4 col-lg-4 col-xl-2 col-years"])
    </div>
    
    @fileinput
      <div class="form-group">
        <input id="fileCSV" name="fileCSV" type="file" class="file form-control"
            data-rule="required"
            data-preview-file-type="text"
            data-browse-on-zone-click="true"
            data-allowed-file-extensions='["csv"]'>
      </div>
    @endfileinput

    <button type="submit" class="btn btn-primary rounded-0 text-uppercase pt-2 mt-2" id="guardar">
      {{ __('app.guardar') }}
    </button>
  </form>
@endcontent

@section('scripts')
  @parent
  <script src="{{ asset('plugins/js-form-validator-2.1/js-form-validator.js') }}"></script>
  <script>
    var f = document.querySelector('.form-validator'),
        form = $('form#csv');

    if(f){
      var submitForm = function(err,res) {
        if(res) {
          let data = new FormData(form[0]);
          $.ajax({
            method: 'POST',
            url: "{{ route('carga-datos.pagos.ajaxCSV') }}",
            data: data,
            processData: false,
            contentType: false,
            dataType: "json",
            beforeSend: function(){
              form.addClass('loading');
            },
            success: function(data){
              Swal.fire({
                text: data.message,
                icon: "success"
              })
              form.trigger('reset').removeClass('loading');
            },
            error: function(xhr,status,error){
              form.removeClass('loading');
              Swal.fire(xhr.responseJSON.message, {
                  icon: "error",
              });
            }
          });
        }else {
          form.addClass('shake');
          setTimeout(function() {
             form.removeClass("shake");
          }, 820)
        }
        return false;
      }

      f.setAttribute('novalidate', true);

      new Validator(f,function (err, res) {
          submitForm(err, res);
      }, {
        locale: "{{ session()->has('locale') ? session()->get('locale') : 'es' }}"
      });
    }
  </script>
@endsection