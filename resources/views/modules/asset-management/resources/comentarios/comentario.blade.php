<samll class="block">
  @lang('app.publicado por', ['name' => $comentario->user->name, 'date' => $comentario->created_at])
</samll>
<div class="block content h5 border-bottom pb-1 mb-1">
  {!! $comentario->comentario !!}
  @if (Auth::user()->hasRole(['super-admin', 'backoffice']))
    <button class="btn btn-danger btn-sm text-uppercase" onclick="eliminarComentario({{ $comentario->id }})">
      {{__('app.eliminar')}}
    </button>
    <button class="btn btn-light btn-sm text-uppercase" onclick="actualizarComentario({{ $comentario->id }})">
      {{__('app.actualizar')}}
    </button>
  @endif
</div>
<div class="block">
  @forelse ($comentario->empresas as $e)
    @php
      $comentario_empresa = $e->empresa
      @endphp
    <span class="badge badge-dark">
      {{ $comentario_empresa->label ? $comentario_empresa->label : $comentario_empresa->nombre_xls }}
    </span>
  @empty
    <span class="badge badge-dark">{{__('app.general')</span>
  @endforelse
</div>