<form class="idform">
  <div class="row">
    <div class="col-12"> 
      <div class="form-group">
        <select class="tail-select w-100" name="empresas_comentario[]" id="empresas_comentario" required multiple>
            @foreach ($empresas as $element)
              @php
                if (isset($element->empresa)) {
                  $empresa = $element->empresa;
                } else {
                  $empresa = $element;
                }
              @endphp
              <option value="" data-description="Todas" {{ !count($comentario->empresas_ids)? 'selected' : null }}>
                General
              </option>
              <option value="{{ $empresa->id }}" data-description="RUT: {{ $empresa->rut }}"
                  {{ in_array($empresa->id, $comentario->empresas_ids) ? 'selected' : '' }}>
                  {{ $empresa->label ? $empresa->label : $empresa->nombre_xls }}
              </option>
            @endforeach
        </select>
        <div class="tail-move-container-comentario"></div>
        {!! Form::textarea('comentario', $comentario->comentario, ['id' => 'comentario', 'class' => 'form-control', 'rows' => 4, 'cols' => 54, 'placeholder' => 'Escriba algo...', 'data-rule' => 'comentario|minlength-5']) !!}
      </div>
    </div>
  </div>
</form>