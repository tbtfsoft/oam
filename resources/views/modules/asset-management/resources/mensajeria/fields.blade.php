@inject('modules', 'App\Injections\PermissionTrait')

<div class="row">
  {!! Form::hidden('id_user', Auth::id(), null) !!}
  <div class="col-6">
    <div class="form-group">
      @php
          $empresas = Auth::user()->hasRole(['backoffice|super-admin'])
                      ? App\Models\AssetManagement\Empresa::orderBy('order', 'asc')->get()
                      : Auth::user()->empresas;

          $selected = isset($model) ? $model->empresas_ids : [];
      @endphp
      <div class="{{ $class ?? null }}">
          <div class="form-group">
              {!! Form::label('empresas[]', __('app.empresas')) !!}
              <select class="tail-select w-100" name="empresas[]" id="empresas" multiple data-rule='required'>
                  <option value="-1" data-description="{{__('app.general')}}" {{ !isset($model) || count($model->empresas_ids) == 0 ? 'selected' : null }}>
                    {{__('app.general')}}
                  </option>
                  @foreach ($empresas as $element)
                      <option value="{{ $element->id }}" data-description="RUT: {{ $element->rut }}"
                              {{ in_array($element->id, $selected) ? 'selected' : '' }}>
                          {{ $element->label ? $element->label : $element->nombre_xls }}
                      </option>
                  @endforeach
              </select>
          </div>
      </div> 
      {{-- {!! Form::select('empresas[]', $modules->empresas(), isset($model) ? $model->empresas_ids : null, ['class' => $errors->has('id_empresa') ? 'tail-select error' : 'tail-select', 'required' => true, 'data-rule' => 'required', 'multiple']) !!} --}}
      @if ($errors->has('empresas'))
        <div class="error" data-type="validator-error">
          {{ $errors->first('empresas') }}
        </div>
      @endif
    </div>
  </div>

  <div class="col-12 col-sm-6 col-lg-2">
    <div class="form-group">
      {!! Form::label('fecha', __('validation.attributes.fecha')."*") !!}
      {!! Form::text('fecha',null, ['class' => $errors->has('fecha') ? 'form-control error' : 'form-control', 'placeholder' => 'Fecha','required' => true, 'data-rule' => 'required']) !!}
      @if ($errors->has('fecha'))
        <div class="error" data-type="validator-error">{{ $errors->first('fecha') }}</div>
      @endif
    </div>
  </div>

  <div class="col-12">
    <div class="tail-move-container"></div>
  </div>

  <div class="col-12">
    <div class="form-group">
      {!! Form::label('titulo', __('validation.attributes.titulo')."*") !!}
      {!! Form::text('titulo',null, ['class' => $errors->has('titulo') ? 'form-control error' : 'form-control', 'placeholder' => __('validation.attributes.titulo'),'required' => true, 'data-rule' => 'required|minlength-3']) !!}
      @if ($errors->has('titulo'))
        <div class="error" data-type="validator-error">{{ $errors->first('titulo') }}</div>
      @endif
    </div>
  </div>

  <div class="col-12"> 
    <div class="form-group">
      {!! Form::label('descripcion', __('validation.attributes.descripcion')."*") !!}
      <textarea name="descripcion" id="descripcion" style="height: 0; opacity: 0; width: 0; border: none">
        {!! isset($model) ? $model->descripcion : null !!}
      </textarea>
      <div id="descripcion2">
        {!! isset($model) ? $model->descripcion : null !!}
      </div>

      @if ($errors->has('descripcion'))
        <div class="error" data-type="validator-error">{{ $errors->first('descripcion') }}</div>
      @endif
    </div>
  </div>

  <div class="col-12"> 
    @fileinput
      <div class="form-group">
        <input id="files" name="files[]" type="file" multiple class="file"
            data-preview-file-type="text"
            data-browse-on-zone-click="true"
            required>
      </div>
      @slot('script_file_input')
        <script>
          $("#files").fileinput({
            language: "es",
            theme: "fas",
            showPreview: true,
            overwriteInitial: false,
            browseClass: "btn btn-primary",
            previewFileIcon: '<i class="fas fa-file"></i>',
            showCaption: true,
            uploadAsync: false,
            showRemove: true,
            showUpload: false,
            @isset ($model)
              initialPreview: [
                @foreach ($model->files as $file)
                  "<img class='file-preview-image kv-preview-data' src='{{ $file['url'] }}'>",
                @endforeach
              ],
              initialPreviewConfig: [
                @foreach ($model->files as $file)
                  {caption: "{{ $file['basename'] }}", url: "{{ $file['delete'] }}", filetype: "{{ $file['type'] }}"},
                @endforeach
              ],
            @endisset
            previewFileIconSettings: {
              'docx': '<i class="fas fa-file-word text-primary"></i>',
              'xlsx': '<i class="fas fa-file-excel text-success"></i>',
              'pptx': '<i class="fas fa-file-powerpoint text-danger"></i>',
              'jpg': '<i class="fas fa-file-image text-warning"></i>',
              'pdf': '<i class="fas fa-file-pdf text-danger"></i>',
              'zip': '<i class="fas fa-file-archive text-muted"></i>',
            }
          });
          $("#files").on("filepredelete", function(jqXHR) {
              var abort = true;
              if (confirm("Are you sure you want to delete this image?")) {
                  abort = false;
              }
              return abort; // you can also send any data/object that you can receive on `filecustomerror` event
          });
        </script>
      @endslot
    @endfileinput
  </div>

  @isset ($model)
    <div class="col-12"> 
      <div class="form-group">
        {!! Form::label('comentario', __('app.comentario')."*") !!}
        <select class="tail-select w-100" name="empresas_comentario[]" id="empresas_comentario" data-rule="empresas_comentario" multiple>
        </select>
        <div class="tail-move-container-comentario"></div>
        {!! Form::textarea('comentario', null, ['id' => 'comentario', 'class' => 'form-control', 'rows' => 4, 'cols' => 54, 'placeholder' => __('app.escriba algo'), 'data-rule' => 'comentario|minlength-5']) !!}
        @if ($errors->has('comentario'))
          <div class="error" data-type="validator-error">{{ $errors->first('comentario') }}</div>
        @endif
      </div>
    </div>
  @endisset
  <div class="col-12 mt-3">
    {!! link_to_route($prefix_router.'.index', Lang::get('crud.back'),[], ['class'=>'btn btn-outline-danger btn-sm text-uppercase load-link']); !!}
    {!! Form::submit(Lang::get('crud.save'),['class' => 'btn btn-primary btn-sm text-uppercase']) !!}
  </div>
</div>

@datapicker
	<script>
	  $('input[name="fecha"]').daterangepicker({
	    singleDatePicker: true,
	    showDropdowns: true,
	    minYear: 1901,
	    maxYear: "{{ date('Y') }}",
	    locale: {
				format: 'DD-MM-YYYY',
        applyLabel: "{{ __('app.aplicar') }}",
        cancelLabel: "{{ __('app.cancelar') }}",
        fromLabel: "{{ __('app.de') }}",
        toLabel: "{{ __('app.hasta') }}",
        customRangeLabel: "Custom",
        daysOfWeek: [
          "{{ __('app.dom') }}",
          "{{ __('app.lun') }}",
          "{{ __('app.mar') }}",
          "{{ __('app.mie') }}",
          "{{ __('app.jue') }}",
          "{{ __('app.vie') }}",
          "{{ __('app.sab') }}"
        ],
        monthNames: [
            "{{ __('app.enero') }}",
            "{{ __('app.febrero') }}",
            "{{ __('app.marzo') }}",
            "{{ __('app.abril') }}",
            "{{ __('app.mayo') }}",
            "{{ __('app.junio') }}",
            "{{ __('app.julio') }}",
            "{{ __('app.agosto') }}",
            "{{ __('app.septiembre') }}",
            "{{ __('app.octubre') }}",
            "{{ __('app.noviembre') }}",
            "{{ __('app.diciembre') }}"
        ],
        firstDay: 1
			}
	  });
	</script>
  <style>
.ql-editor{
    min-height:200px;
}
  </style>
@enddatapicker

@section('scripts')
  @parent
  <script src="{{ asset('plugins/js-form-validator-2.1/js-form-validator.js') }}"></script>
  <!-- Main Quill library -->
  <link href="{{ asset('/plugins/quill/quill.snow.css') }}" rel="stylesheet">
  <script src="{{ asset('/plugins/quill/quill.min.js') }}"></script>

<script>
  var quill = new Quill('#descripcion2', {
    theme: 'snow'
  });
  quill.on('text-change', function(delta, oldDelta, source) {
    document.getElementById("descripcion").value = quill.root.innerHTML
  });
</script>
  <script>
    var da = tail.select("#empresas", {
      search: true,
      descriptions: true,
      locale: "es",
      hideSelected: true,
      hideDisabled: true,
      placeholder: "{{ __('app.seleccione las empresas') }}",
      multiShowCount: false,
      multiContainer: ".tail-move-container"
    }).on("change", function(item, state){
      // console.log({item, item.key})
    });
    
    var daComentario = tail.select("#empresas_comentario", {
      search: true,
      descriptions: true,
      locale: "es",
      hideSelected: true,
      hideDisabled: true,
      placeholder: "{{ __('app.seleccione las empresas') }}",
      multiShowCount: false,
      multiContainer: ".tail-move-container-comentario"
    });
    
    var f = document.querySelector('.form-validator');
    if(f){
      f.setAttribute('novalidate', true);
      var options = {
        rules: {
          comentario: function (value) {
            if ($('#empresas_comentario').val().length) {
              return true;
            }
            return false;
          },
          empresas_comentario: function (value)  {
            if ($('#comentario').val().length) {
              return true;
            }
            return false;
          }
        },
        messages: {
          es: {
            comentario: {
              incorrect: "{{ __('app.seleccione por lo menos una empresa') }}"
            },
            empresas_comentario: {
              incorrect: "{{ __('app.debe escribir un comentario') }}"
            },
          }
        },
        locale: "{{ session()->has('locale') ? session()->get('locale') : 'es' }}"
      };
      new Validator(f, function (err, res) {
        return res;
      },options);

      @isset ($model)
        $('#empresas').change(function() {
          let empresas_ids = $(this).val()
          if (!empresas_ids.length) {
            daComentario.disable();
          } else {
            daComentario.enable();
          }
          getEmpresasSelect(empresas_ids)
        })

        function getEmpresasSelect (empresas_ids) {
          $.ajax({
            method: 'GET',
            url: "{{ route('generales.empresas.gerById') }}",
            data: {
              empresas_ids,
              correspondencia_id: {{ $model->id }},
              user_id: {{ Auth::id() }}
            },
            dataType: "json",
            success: function(data) {
              setEmpresasSelect(data.empresas)
            },
            error: function(xhr,status,error){
              Swal.fire(xhr.responseJSON.message, {
                icon: "error",
              });
            }
          });
        }

        function setEmpresasSelect (empresas, ini = false) {
          let items = []
          if (empresas.length) {
            empresas.forEach(function(empresa) {
              if (ini) {
                items.push({
                  'key': empresa.empresa.id,
                  'value': empresa.empresa.label ? empresa.empresa.label : empresa.empresa.nombre_xls,
                  'description': empresa.empresa.rut
                })
              } else {
                items.push({
                  'key': empresa.id,
                  'value': empresa.label ? empresa.label : empresa.nombre_xls,
                  'description': empresa.rut
                })
              }
            })
            daComentario.config('items', items)
          } else {
            getEmpresasSelect(items)
          }
        }

        setEmpresasSelect(@json($model->empresas), true)
      @endisset
    }
  </script>
@endsection