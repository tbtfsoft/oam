@extends('layouts.app')

@content
  @slot('breadcrumbItems')
    <li class="breadcrumb-item text-uppercase" aria-current="page">
      <a href="{{ route($prefix_router.'.index') }}" class="load-link">
        {{ AppHelpers::trans_choice($class_basename,1) }}
      </a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">
      <span class="text-lowercase text-uppercase">
        @lang('crud.new')
      </span>
    </li>
  @endslot

  @slot('title')
    <h1 class="display-6 text-uppercase">{{ AppHelpers::trans_choice($class_basename,1) }} | @lang('crud.new')</h1>
  @endslot

  <!-- Small Stats Blocks -->
  <div class="row">
    <div class="col-12 mb-4">
      {!! Form::open(['route' => $prefix_router.'.store', 'class' => 'form-validator', 'files' =>true]) !!} 

      @include('modules.asset-management.resources.mensajeria.fields',['create' => true])

      {!! Form::close() !!}
    </div>
  </div>
@endcontent