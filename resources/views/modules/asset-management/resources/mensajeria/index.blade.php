@extends('layouts.app')

@content
  @slot('breadcrumbItems')
    <li class="breadcrumb-item active" aria-current="page">
      <span class="text-uppercase">
        {{ AppHelpers::trans_choice($class_basename, 1) }}
      </span>
    </li>
  @endslot

  @slot('title')
    <div class="row align-items-center">
      <div class="col">
        <h1 class="display-6 text-uppercase">
          {{ AppHelpers::trans_choice($class_basename,1) }}
        </h1>
      </div>
      @if (Auth::user()->hasRole(['super-admin', 'backoffice']))
        <div class="col-auto">
          <a href="{{ route($prefix_router.'.create') }}" class="btn btn-danger btn-sm load-link">
            @lang('crud.new')
          </a>
        </div>
      @endif
    </div>
  @endslot

  <div class="row">
    <div class="col-12 mb-4">
      {{$dataTable->table(['id' => 'datatable'])}}
    </div>
  </div>
  <style>
    .div-row {
      display: grid;
      grid-gap: 15px;
      grid-template-columns: 1fr auto;
      align-items: flex-end;
    }

    .div-row .date {
      display: grid;
      font-size: 12px;
      line-height: 1;
      font-weight: 700
    }
    .div-row .content {
      display: grid;
      grid-template-columns: 1fr;
    }
    .dataTable thead {
      display: none;
    }
  </style>
@endcontent

@datapicker
  <script>
    $('input[name="fecha"]').daterangepicker({
      singleDatePicker: true,
      showDropdowns: true,
      minYear: 1901,
      maxYear: "{{ date('Y') }}",
      locale: {
        format: 'Y-m-d H:i:s'
      }
    });
  </script>
@enddatapicker

@section('scripts')
  @parent
  @jqueryConfirm
  @datatables
  <script>
    $(function() {
      {{$dataTable->generateScripts()}}
    })
  </script>
@endsection