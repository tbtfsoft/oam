@inject('Services', 'App\Injections\CorrespondenciaTrait')

@php
	$statusModel = $model->getStatusModel(Auth::user()->id);
	$status = $Services->getStatus($statusModel ? $statusModel->status : 0)
@endphp
<div class="div-row">
	<div class="content">
		<div class="date">
			{{ $fecha }}
		</div>
		<div>
			<div class="badge d-inline-block {{ $status->class }}"> {{ $status->label }} </div>
			<div class="d-inline-block" style="font-size: 12px; line-height: 1">
				{{ __('app.setBy', ['name' => $model->user->name]) }}
			</div>
		</div>
		<a href="{{ route($prefix_router.'.show',$id) }}" class="h4 text-truncate mb-0">
			{{ $titulo }}
		</a>
	</div>
	<div class="content">
		@if (Auth::user()->hasRole(['super-admin', 'backoffice']))
			@isset ($prefix_router)
				<div class="w-100 text-right">
					<a href="{{ route($prefix_router.'.edit',$id) }}" class="btn btn-sm btn-light load-link" onclick="document.body.classList.add('loading')">
						<i class="fa fa-edit"></i>
						{{__('app.editar')}}
					</a>

			    <form id="form-destroy_{{$id}}" action="{{ route($prefix_router.'.destroy',$id) }}" method="POST" onsubmit="return confirmar(this,event);" class="d-inline">
			    	@csrf
			    	@method('DELETE')

						<button class="btn btn-sm btn-light btn-confirm load-link" type="submit" data-toggle="confirmation" data-singleton="true">
							<i class="fa fa-trash"></i>
							{{__('app.eliminar')}}
						</button>
			    </form>
				</div>
			@endisset
		@endif
	</div>
</div>