@extends('layouts.app')

@inject('Services', 'App\Injections\CorrespondenciaTrait')

@content
  @slot('breadcrumbItems')
    <li class="breadcrumb-item text-uppercase" aria-current="page">
      <a href="{{ route($prefix_router.'.index') }}">
        {{ AppHelpers::trans_choice($class_basename,1) }}
      </a>
    </li>
  @endslot

  @slot('title')
    <div class="row align-items-center">
      <div class="col">
        <small class="d-inline-block my-0 text-muted" style="font-size: .7rem">
          @lang('app.publicado por', ['name' => $model->user->name, 'date' => $model->fecha])
        </small>
        <h3 class="display-6 text-uppercase mb-0">
          {{ $model->titulo }}
        </h3>
        @php
          $status = $model->getStatusModel(Auth::user()->id);
          $statusIndo = $Services->getStatus($status->status);
        @endphp
        <small class="badge {{ $statusIndo->class }}">
          {{ $statusIndo->label }}
        </small>
        {{ __('app.setBy', ['name' => $status->user->name]) }}
      </div>
      <div class="col-auto pr-0">
        @if (!$model->isClose())
          @if ((Auth::user()->hasRole(['client-admin']) && $status->status < 1))
            @php
              $nextStatus = $Services->nextStatus($status->$status)
            @endphp
            <form action="{{ route($prefix_router.'.change.status', $model->id) }}" method="POST" id="changeStatus">
              @csrf
              <input type="button" class="btn {{ $nextStatus->class }} btn-sm text-uppercase" value="{{ $nextStatus->action }}" onclick="return changeStatus(this);">
            </form>
          @elseif (Auth::user()->hasRole(['super-admin', 'backoffice']))
            @php
              $nextStatus = $Services->nextStatus(1)
            @endphp
            <form action="{{ route($prefix_router.'.change.status', $model->id) }}" method="POST" id="changeStatus">
              @csrf
              <input type="button" class="btn {{ $nextStatus->class }} btn-sm text-uppercase" value="{{ $nextStatus->action }}" onclick="return changeStatus(this);">
            </form>
          @endif
        @endif
      </div>
      @if (Auth::user()->hasRole(['super-admin', 'backoffice']))
        <div class="col-auto">
          <a href="{{ route($prefix_router.'.edit', $model->id) }}" class="btn btn-light btn-sm load-link text-uppercase">
            @lang('crud.edit')
          </a>
        </div>
        <div class="col-auto pl-0">
          <a href="{{ route($prefix_router.'.create') }}" class="btn btn-danger btn-sm load-link text-uppercase">
            @lang('crud.new')
          </a>
        </div>
      @endif
    </div>
  @endslot

  <div class="row">
    <div class="col-12 mb-2 py-2">
      <div class="alert alert-light text-dark rounded" role="alert">
        {!! $model->descripcion !!}
      </div>
    </div>
  
    @if (count($model->empresas))
      <div class="col-12 mb-2 pb-2">
        <div class="h7 text-muted text-uppercase border-bottom pb-1 mb-1 comentario-section_title">
          <span>{{__('app.empresas')}}</span> <div class="badge badge-danger">{{ count($model->empresas) }}</div>
        </div>
        <div class="row mx-0 pt-1">
          @foreach ($model->empresas as $e)
            <div class="col-auto pr-2 pl-0">
              <div class="alert alert-light text-dark rounded mx-1" role="alert">
                <small class="d-block">{{ $e->empresa->rut }}</small>
                <div class="h6 mb-0">
                  {{ $e->empresa->label ? $e->empresa->label : $e->empresa->nombre_xls }}
                </div>
              </div>
            </div>    
          @endforeach
        </div>
      </div>
    @endif

    @if (count($comentarios))
      <div class="col-12 mb-2 pb-2"  class="list-comentario">
        <div class="h7 text-muted text-uppercase border-bottom pb-1 mb-1 comentario-section_title">
          {{__('app.comentarios')}}
          <div class="badge badge-danger" id="list-comentario--count">{{ count($comentarios) }}</div>
        </div>
        
        @foreach ($comentarios as $comentario)
          <div class="alert alert-light text-dark rounded comentario" id="comentario_{{ $comentario->id }}" role="alert">
            @include('modules.asset-management.resources.comentarios.comentario',['comentario' => $comentario])
          </div>
        @endforeach
          
        <div id="list-comentario--empty" class="alert alert-dark rounded {{ !count($model->comentarios) ? null : 'd-none' }}" role="alert"  style="opacity: .3">
          <div class="block h6 mb-0 text-uppercase">
            {{__('app.sin comentarios')}}
          </div>
        </div>
      </div>
    @endif
    
    @if (count($model->files))
      <div class="col-12 mb-2 pb-2">
        <div class="h7 text-muted text-uppercase border-bottom pb-1 mb-1 comentario-section_title">
          {{__('app.archivos')}}
          <div class="badge badge-danger">
            {{ count($model->files) }}
          </div>
        </div>
        @forelse ($model->files as $file)
          <div class="alert alert-light text-dark rounded" role="alert">
            <a href="{{ asset($file['url']) }}" target="_blank">{{ $file['basename'] }}</a>
          </div>
        @empty
          <div class="alert alert-dark rounded" role="alert"  style="opacity: .3">
            <div class="block h6 mb-0 text-uppercase">
              {{__('app.sin archivos')}}
            </div>
          </div>
        @endforelse
      </div>
    @endif

    @if (Auth::user()->hasRole(['super-admin', 'backoffice']))
      <div class="col-12 mb-2 pb-2"  class="list-historial">
        <div class="h7 text-muted text-uppercase border-bottom pb-1 mb-1 historial-section_title">
          {{__('app.historial')}}
        </div>
      
        @foreach ($model->status()->get() as $s)
          <div class="alert alert-light text-dark rounded my-1" role="alert">
            @php
                $st = $Services->getStatus($s->status);
            @endphp
            <small class="badge {{ $st->class }}">
              {{ $st->label }}
            </small>
            @lang('app.setBy', ['name'=> $s->user->name]) - <small class="text-muted">{{ $s->created_at}}</small>
          </div>
        @endforeach
      </div>
    @endif
  </div>
@endcontent

@section('scripts')
  @parent
  <script src="https://rawgit.com/jackmoore/autosize/master/dist/autosize.min.js"></script>
  <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css"/>
  <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
  <script src="https://unpkg.com/gijgo@1.9.13/js/messages/messages.es-es.js" type="text/javascript"></script>

  <script>
    autosize(document.getElementById("descripcion"));

    function eliminarComentario (id) {
      Swal.fire({
        title: "{{__('app.esta seguro?')}}",
        text: "{{__('app.de eliminar el comentario') }}",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "{{ __('app.si, eliminar!') }}"
      }).then((result) => {
        if (result.value) {
          $.ajax({
            method: 'POST',
            url: "{{ route('comentarios.delete') }}",
            data: {
              comentario_id: id,
              modulo_id: {{ $model->id }}
            },
            dataType: "json",
            success: function(data) {
              let elementCount = document.getElementById('list-comentario--count')
              let nComentarios = parseInt(elementCount.innerHTML) - 1
              elementCount.innerHTML = nComentarios
              if (!nComentarios) {
                $('#list-comentario--empty').removeClass('d-none')
              } 
              $('#comentario_' + id).remove()
              Swal.fire(data.message, '', 'success')
            },
            error: function(xhr, status, error) {
              Swal.fire(xhr.responseJSON.message ? xhr.responseJSON.message : error, {
                icon: "error",
              });
            }
          });
        }
      })
    }

    function actualizarComentario(id) {
     $.ajax({
        method: 'POST',
        url: "{{ route('comentarios.edit') }}",
        data: {
          comentario_id: id,
          user_id: {{ Auth::id() }}
        },
        dataType: "json",
        success: function(data) {
          Swal.mixin({
            html: data.html,
            confirmButtonText: "{{__('app.aceptar') }}",
            showCancelButton: true,
            onOpen: function() {
              var daComentario = tail.select("#empresas_comentario", {
                search: true,
                descriptions: true,
                locale: "es",
                hideSelected: true,
                hideDisabled: true,
                placeholder: "{{__('app.seleccione las empresas') }}",
                multiShowCount: false,
                multiContainer: ".tail-move-container-comentario"
              });
            },
            preConfirm: () => {
              if (!$('#empresas_comentario').val().length || !document.getElementById('comentario').value ) {
                Swal.showValidationMessage("{{__('app.complete los campos requeridos')}}")
              }
            }
          }).queue([{title: "{{__('app.editar comentario')}}"}
          ]).then((result) => {
            if (result.value) {
              $.ajax({
                method: 'POST',
                url: "{{ route('comentarios.update') }}",
                data: {
                  empresas_comentario: $('#empresas_comentario').val(),
                  comentario: document.getElementById('comentario').value,
                  comentario_id: id
                },
                dataType: "json",
                success: function(data) {
                  $('#comentario_'+id).html(data.html)
                  Swal.fire({
                    type: 'success',
                    title: data.message
                  })
                },
                error: function(xhr,status,error){
                  console.error({error})
                  Swal.fire({
                    title: 'Error!',
                    text: xhr.responseJSON.message ? xhr.responseJSON.message : "{{__('app.ocurrio algo inesperado, intente mas tarde.')}}",
                    icon: 'error'
                  })
                }
              });
            }
          })
        },
        error: function(xhr, status, error) {
          console.error({error})
          Swal.fire({
            title: 'Error!',
            text: xhr.responseJSON.message ? xhr.responseJSON.message : "{{__('app.ocurrio algo inesperado, intente mas tarde.')}}",
            icon: 'error'
          })
        }
      });
    }


    function changeStatus(button) {
      Swal.fire({
        title: "{{__('app.esta seguro?')}}",
        text: "",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "{{__('app.si')}}"
      }).then((result) => {
        if (result.value) {
          $(button).parent().submit();
          return true;
        }
        return false;
      })
    }
  </script>
@endsection