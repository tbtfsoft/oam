@extends('layouts.app')
@php
  setlocale(LC_MONETARY, 'en_US.UTF-8');
@endphp
@content
  @slot('breadcrumbItems')
      <li class="breadcrumb-item active text-uppercase" aria-current="page">{{ __('app.cuadros-pago.matriz-acredor') }}</li>
  @endslot
  @slot('title')
      <h1 class="display-6 text-uppercase">{{ __('app.cuadros-pago.matriz-acredor') }}</h1>
  @endslot
  
  <form id="formFilter" class="form-validator">
    <div class="form-row mb-3">
    @include('layouts.components.selects.modules')
      @if(Auth::user()->hasRole(['super-admin']))
          @SelectCompanys(['class'=>"col-6 col-sm-6 col-companys", 'selected' => 414 ])
      @else
          @SelectCompanys(['class'=>"col-6 col-sm-6 col-companys", 'selected' => Auth::user()->empresas->first()->id ])
      @endif

      @SelectTypeDocument(['class'=>"col-6 col-sm-6 col-document", "selected" => 2])

      @SelectMonths(['class'=>"col-6 col-sm-6 col-months", "selected" => isset($month) ? $month :  null ])

      @SelectYears(['class'=>"col-6 col-sm-6 col-years", "selected" => date("Y")])

      <div class="col-6 col-sm-6 col-md-4 col-lg-4 col-xl-2">
        <div class="form-group">
          <label class="d-none d-sm-inline-block w-100"></label>
          <div class="btn-group w-100" role="group" aria-label="Button group with nested dropdown">
            <button type="button" class="btn btn--padding btn-primary-hover" id="filtrar">
              <i class="fa fa-search" aria-hidden="true"></i> {{__('app.filtrar')}}
            </button>
          </div>
        </div>
      </div>
      
    </div>

    <div class="form-row">
    <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4" style="padding-bottom: 25px;">
      <div class="list bg-success shadow-sm rounded overflow-hidden">
        <div class="list-item">
          <div class="list-thumb bg-success-active rounded-circle shadow-sm p-2 h3 mx-auto mb-0" style="color: #FFFFFF;">
            <i class="fa fa-hand-holding-usd"></i>
          </div>
          <div class="list-body text-right"  style='font-family: "Courier New"'>
            <span class="list-title text-white" style='font-weight: bold;'>$ {{ number_format($data['total'],0,',','.') }}</span>
            <span class="list-content text-white">
            Total instrucciones de pago
            </span>
          </div>
        </div>
      </div>
    </div>
      
    </div>
    <div class="container-fluid">
      <div class="row">

        <div class="col-12 table-responsive">
            <table class="table w-100">
              <thead>
                <tr>
                  <th>#</th>
                  <th class="text-uppercase">{{__('app.deudor')}}</th>
                  <th class="text-uppercase">{{__('app.acreedor')}}</th>
                  <th class="text-uppercase">{{__('app.monto')}}</th>
                  <th class="text-uppercase">{{__('app.documento')}}</th>
                  <th class="text-uppercase">{{__('app.periodo')}}</th>
                </tr>
              </thead>
              <tbody>
                @forelse ($data['collects'] as $key => $row)
                  <tr>
                    <td scope="row">{{ $key+1 }}</td>
                    <td>
                      {{ array_key_exists($row->id_empresa_deudora, $data['empresas']) ?
                         $data['empresas'][$row->id_empresa_deudora] : 'N/D' }}
                    </td>
                    <td>
                      {{ array_key_exists($row->id_empresa_acreedora, $data['empresas']) ? 
                         $data['empresas'][$row->id_empresa_acreedora] : 'N/D' }}
                    </td>
                    <td>$ {{ number_format($row->costo,0,',','.') }}</td>
                    <td>{{ $data['typesDocuments'][$row->id_tipo_doc_cen] }}</td>
                    <td>{{ $row->periodo }}</td>
                  </tr>
                @empty
                  <tr>
                    <td colspan="6">
                      <div class="alert alert-light text-dark font-weight-bold rounded text-center py3" role="alert">
                        {{__('app.SIN REGISTROS OBTENIDOS')}}
                      </div>
                    </td>
                  </tr>
                @endforelse
              </tbody>
            </table>
        </div>

        <div class="col-12 text-center max-auto">
          {{ $data['collects']->links() }}
        </div>
      </div>
    </div>
  </form>
@endcontent

@section('scripts')
  @parent
  <script src="{{ asset('plugins/js-form-validator-2.1/js-form-validator.js') }}"></script>
  <script>
    let formData = serialize(document.querySelector('#formFilter'));
    Array.prototype.forEach.call(document.getElementsByClassName("page-link"), function(element, index) {
      element.setAttribute("href", window.location.pathname + element.getAttribute("href") + '&' + formData);
    });

    document.getElementById("filtrar").addEventListener("click", function() {
      if(document.querySelector('.form-validator')) {
        document.body.classList.add("loading");
        window.location = window.location.pathname + '?' + $('form.form-validator').serialize();
      }
    });
  </script>
@endsection