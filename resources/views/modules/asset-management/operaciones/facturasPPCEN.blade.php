@extends('layouts.app')

@content
  @slot('breadcrumbItems')
    <li class="breadcrumb-item active text-uppercase" aria-current="page">{{__('app.operaciones.facturasPPCEN')}}</li>
  @endslot
  @slot('title')
  <div class="row align-items-center">
    <div class="col">
        <h1 class="display-6 text-uppercase my-0 pt-md-3">{{__('app.operaciones.facturasPPCEN')}}</h1>
    </div>
  </div>      
  @endslot

  <?php date_default_timezone_set('America/Santiago');?>

  <!-- Var -->
  @php 
    $mes = date('m');
    $anio = date('Y');
  @endphp
  

  <div class="">
    <form class="row" action="{!!route('operaciones.generarPlanillaDTEPPCEN')!!}" method="GET" target="_blank">
    @SelectCompanys(['class'=>"col-6 col-sm-6 col-companys"])

    @SelectMonths(['class'=>"col-6 col-sm-6 col-months"])

    @SelectYears(['class'=>"col-6 col-sm-6 col-years"])

    @include('layouts.components.selects.modules',['type' => 'select', 'modules' => [
              [
                'label' => __('app.Todos'),
                'value' => 0,
              ],
              [
                'label' => __('app.Energía'),
                'value' => 1,
              ],
              [
                'label' => __('app.Potencia'),
                'value' => 2,
              ]
            ],
    'class'=>"col-6 col-md-3 col-lg-2"])
    <div class="col-12 col-md-2">   
      <div class="form-group">
        <label class="w-100"></label>
        <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
          <button class="btn btn--padding btn-primary-hover text-uppercase" type="summit">
            {{__('app.generar')}}
          </button>
        </div>
      </div>
    </div> 
    </form>  
  </div>
  
@endcontent

@section('scripts')
    @parent
    <script>
        var urlReporte = "{!!route('reportes.generarReporteElectrico')!!}";
    </script>      
    <script src="{{ asset('js/reportes-electrico.js') }}"></script>    
@endsection