<div class="row">
	<div class="col-12">
		<div class="table-responsive">
      <table class="table table-striped mb-0">
        <thead>
          <tr>
            <th>EMPRESA</th>
            <th>TIPO DE DOCUMENTO</th>
            <th>MÓDULO</th>
            <th>PERIODO</th>
            <th>DESCARGAR</th>
          </tr>
        </thead>
        <tbody>
					@forelse ($files as $file)
						@php
							$fileExplote = explode('_',explode('/',$file)[1]);
							$id_empresa = explode('_',explode('/',$file)[0])[0];
							$countExplode = count($fileExplote) - 3;
							$fileExploteAux = array_slice($fileExplote, $countExplode);
							$empresa = implode('_',array_slice($fileExplote, 0, $countExplode ));
							$periodo = explode('.',$fileExploteAux[2])[0];
						@endphp
	          <tr>
							<td>{{ $empresa }}</td>
							<td>{{ $fileExploteAux[0] }}</td>
							<td>{{ $fileExploteAux[1] }}</td>
							<td>{{ $periodo }}</td>
          		<td>
          			<a href="{{ route('operaciones.ERP.download.file',['id_empresa'=>$id_empresa,'empresa'=>$empresa,'tipo_documento'=>$fileExploteAux[0],'module' => $fileExploteAux[1], 'periodo' => $periodo]) }}">DESCARGAR</a>
          		</td>
						</th>
					@empty
						<tr>
							<td colspan="5" class="text-center">
								SIN ARCHIVOS
							</td>
						</tr>
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>