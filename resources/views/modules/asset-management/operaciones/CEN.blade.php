@extends('layouts.app')

@content
  @slot('breadcrumbItems')
      <li class="breadcrumb-item active" aria-current="page">{{__('app.Emisión masiva de documentos a CEN')}}</li>
  @endslot
  @slot('title')
      <h1 class="display-6 text-uppercase">{{__('app.Emisión masiva de documentos a CEN')}}</h1>
  @endslot

  <form id="formCEN" class="form-validator" method="POST" action="{{ route('operaciones.CEN.export') }}">
  	@csrf
    <div class="form-row mb-3">
      @SelectCompanys(['class'=>"col-6 col-sm-6 col-companys"])

      @if (!Auth::user()->hasRole(['client','client-admin']))
        @SelectTypeDocument(['class'=>"col-6 col-sm-6 col-document"])

        @SelectMonths(['class'=>"col-6 col-sm-6 col-months"])

        @SelectYears(['class'=>"col-6 col-sm-6 col-years"])

        <div class="col-6 col-sm-6 col-md-4 col-lg-4 col-xl-2">
          <div class="form-group">
            <label class="w-100"></label>
            <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
              <button type="submit" class="btn btn--padding btn-primary-hover" id="filtrar">
                {{__('app.generar')}}
              </button>
            </div>
          </div>
        </div>
      @endif

      @include('layouts.components.selects.modules')
    </div>
  </form>
@endcontent

@section('scripts')
  @parent
  <script src="{{ asset('plugins/js-form-validator-2.1/js-form-validator.js') }}"></script>
  <script>
    var f = document.querySelector('.form-validator'),
        form = $('form#csv');

    if(f){
      var submitForm = function(err,res) {
        if(res) {
        	form.addClass('loading');
        	document.getElementById("formCEN").submit();
        	return false;
      	}
      }

      f.setAttribute('novalidate', true);

      new Validator(f,function (err, res) {
          submitForm(err, res);
      }, {
          locale: "{{ session()->has('locale') ? session()->get('locale') : 'es' }}"
      });
    }
  </script>
@endsection
