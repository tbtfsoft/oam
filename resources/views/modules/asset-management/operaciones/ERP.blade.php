@extends('layouts.app')

@content
    @slot('breadcrumbItems')
        <li class="breadcrumb-item active text-uppercase" aria-current="page">ERP</li>
    @endslot
    @slot('title')
        <h1 class="display-6 text-uppercase">{{__('app.Emisión masiva de documentos a ERP')}}</h1>
    @endslot

  <form id="formERP" class="form-validator" method="POST" action="{{ route('operaciones.ERP.export') }}">
  	@csrf
    <div class="form-row mb-3">
      @SelectCompanys(['class'=>"col-6 col-sm-6 col-companys"])

      @if (!Auth::user()->hasRole(['client','client-admin']))

        @SelectTypeDocument(['class'=>"col-6 col-sm-6 col-document"])

        @SelectMonths(['class'=>"col-6 col-sm-6 col-months"])

        @SelectYears(['class'=>"col-6 col-sm-6 col-years"])

        @include('layouts.components.selects.modules',['type' => 'select', 'modules' => [
                  [
                    'label' => __('app.Energía'),
                    'value' => 1,
                  ],
                  [
                    'label' => __('app.Potencia'),
                    'value' => 2,
                  ],
                  [
                    'label' => __('app.Servicios complementarios'),
                    'value' => 3,
                  ]
                ],
                'class'=>"col-6 col-md-3 col-lg-2"])
      
        <div class="col-6 col-sm-6 col-md-4 col-lg-4 col-xl-2">
          <div class="form-group">
            <label class="w-100"></label>
            <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
              <button type="submit" class="btn btn--padding btn-primary-hover text-uppercase" id="filtrar">
                {{__('app.generar')}}
              </button>
            </div>
          </div>
        </div>
      @endif
    </div>

    <div class="row">
      <div id="response" class="col-12"></div>
    </div>
  </form>
@endcontent

@section('scripts')
  @parent
  <script src="{{ asset('plugins/js-form-validator-2.1/js-form-validator.js') }}"></script>
  <script>
    var f = document.querySelector('.form-validator'),
        form = $('form#formERP'),
        routerGetDocuments = "{{ route('operaciones.ERP.getDocumentsDirectory','IDXZXX') }}";

    var getDocuments = function(empresa_xls) {
      $.ajax({
        method: 'GET',
        url: routerGetDocuments.replace("IDXZXX", empresa_xls),
        beforeSend: function(){
          form.addClass('loading');
        },
        success: function(data){
          $('#response').html(data.response);
          form.removeClass('loading');
        },
        error: function(xhr,status,error){
          form.removeClass('loading');
          Swal.fire(xhr.responseJSON.message, {
              icon: "error",
          });
        }
      });
    }

    $('#empresa_xls').change(function() {
      let empresa_xls = $('#empresa_xls').val()
      if (empresa_xls) {   
        getDocuments(empresa_xls);
      }
    })

    if(f){
      var submitForm = function(err,res) {
        if(res) {
          let data = new FormData(form[0]);
          $.ajax({
              method: 'POST',
              url: form.attr('action'),
              data: data,
              processData: false,
              contentType: false,
              dataType: "json",
              beforeSend: function(){
                form.addClass('loading');
              },
              success: function(data){
                form.removeClass('loading');
                Swal.fire(data.message);
                // window.location.href = data.url;
                getDocuments($('#empresa_xls').val());
              },
              error: function(xhr,status,error){
                form.removeClass('loading');
                Swal.fire(xhr.responseJSON.message, {
                  icon: "error"
                });
              }
          });
        	return false;
      	}
      }

      f.setAttribute('novalidate', true);

      new Validator(f,function (err, res) {
          submitForm(err, res);
      }, {
          locale: "{{ session()->has('locale') ? session()->get('locale') : 'es' }}"
      });
    }
  </script>
@endsection
