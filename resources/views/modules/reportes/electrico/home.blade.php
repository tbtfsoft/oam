@extends('layouts.app')

@content
  @slot('breadcrumbItems')
    <li class="breadcrumb-item active text-uppercase" aria-current="page">{{ __('app.reportes.electrico') }}</li>
  @endslot
  @slot('title')
  <div class="row align-items-center">
    <div class="col">
        <h1 class="display-6 text-uppercase my-0 pt-md-3">{{ __('app.reportes.electrico') }}</h1>
    </div>
  </div>      
  @endslot

  <?php date_default_timezone_set('America/Santiago');?>

  <!-- Var -->
  @php 
    $mes = date('m');
    $anio = date('Y');
  @endphp
  

  <div class="">
    <form class="row" action="{!!route('reportes.generarReporteElectrico')!!}" method="GET" target="_blank">
    @if(Auth::user()->hasRole(['super-admin']))
        @SelectCompanys(['class'=>"col-12 col-md-3", 'selected' => 0 ])
    @else
        @SelectCompanys(['class'=>"col-12 col-md-3", 'selected' => 0 ])
    @endif
    
    <div class="col-12 col-md-3">   
      <div class="form-group">
        <label for="dates">{{__('app.reporte')}}</label>
        <select id ="id_reporte" name="id_reporte" class="form-control">
          <option value="1" disabled>{{__('app.Medidor') }}</option>
          <option value="2" disabled>{{__('app.Inversores') }}</option>
          <option value="5" disabled>{{__('app.Combiner Box') }}</option>
          <option value="3" disabled>{{__('app.Trackers') }}</option>
          <option value="4" disabled>{{__('app.Calidad') }}</option>
        </select>
      </div>  
    </div>
    <div class="col-12 col-md-3">   
      <div class="form-group">
        <label for="dates">{{__('app.rango de fechas')}}</label>
        <input type="text" id="dateRangePicker" name="dates" class="form-control mb-2" />
      </div>  
    </div> 
    <div class="col-12 col-md-2">   
      <div class="form-group">
        <label class="w-100"></label>
        <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
          <button class="btn btn--padding btn-primary-hover text-uppercase" type="summit">
            {{__('app.Generar')}}
          </button>
        </div>
      </div>
    </div> 
    </form>  
  </div>
  
@endcontent

@datapicker
  <script>
    startDate = "{{ date('d/m/Y')}}"+" 00:00";
    endtDate = "{{ date('d/m/Y')}}"+" 23:59";
  </script>

  <script>
    /*
     Documentacion màs detallada: https://www.daterangepicker.com/ 
    */    
    $('#dateRangePicker').daterangepicker({
      timePicker: true,
      timePicker24Hour: true,
      showDropdowns: true,
      minYear: 2010, // El año mminimo.
      startDate: startDate, // Obtiene la fecha actual
      endDate: endtDate, // El fin de fecha es un dia adicional a la fecha actual.
      autoclose: false,
      locale: {
        format: 'DD/M/Y HH:mm',
          separator: " - ",
          applyLabel: "{{ __('app.aplicar') }}",
          cancelLabel: "{{ __('app.cancelar') }}",
          fromLabel: "{{ __('app.de') }}",
          toLabel: "{{ __('app.hasta') }}",
          customRangeLabel: "Custom",
          daysOfWeek: [
            "{{ __('app.dom') }}",
            "{{ __('app.lun') }}",
            "{{ __('app.mar') }}",
            "{{ __('app.mie') }}",
            "{{ __('app.jue') }}",
            "{{ __('app.vie') }}",
            "{{ __('app.sab') }}"
          ],
          monthNames: [
              "{{ __('app.enero') }}",
              "{{ __('app.febrero') }}",
              "{{ __('app.marzo') }}",
              "{{ __('app.abril') }}",
              "{{ __('app.mayo') }}",
              "{{ __('app.junio') }}",
              "{{ __('app.julio') }}",
              "{{ __('app.agosto') }}",
              "{{ __('app.septiembre') }}",
              "{{ __('app.octubre') }}",
              "{{ __('app.noviembre') }}",
              "{{ __('app.diciembre') }}"
          ],
          firstDay: 1
      },
    });
  </script>
@enddatapicker

@section('scripts')
    @parent
    <script>
        var urlReporte = "{!!route('reportes.generarReporteElectrico')!!}";
    </script>      
    <script src="{{ asset('js/reportes-electrico.js') }}"></script>    
@endsection