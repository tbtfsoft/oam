<ul class="nav nav-pills border-bottom border-primary" id="pills-tab" role="tablist">
  <li class="nav-item">
    <a class="nav-link px-4 py-2 rounded-0 active" id="pills-empresas-tab" data-toggle="pill" href="#pills-empresas" role="tab" aria-controls="pills-empresas" aria-selected="true">
      Empresas
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link px-4 py-2 rounded-0" id="pills-bancos-tab" data-toggle="pill" href="#pills-bancos" role="tab" aria-controls="pills-bancos" aria-selected="false">
      Bancos
    </a>
  </li>
</ul>
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-empresas" role="tabpanel" aria-labelledby="pills-empresas-tab">
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                  <th>ID</th>
                  <th>NOMBRE</th>
                  <th style="width: 140px">RUT</th>
                  <th>DIRECCION</th>
                  <th>BANK</th>
                  <th>CREADO</th>
              </tr>
            </thead>
            <tbody>
            @foreach ($empresas as $empresa)
                <tr>
                    <td>{{$empresa->id}}</td>
                    <td>
                      {{$empresa->name}}
                      <small class="d-block text-muted mb-0">{{mb_strtoupper($empresa->business_name, 'UTF-8')}}</small>
                    </td>
                    <td>{{$empresa->rut . '-' . $empresa->verification_code }}</td>
                    <td>{{$empresa->commercial_address}}</td>
                    <td>
                      {{$empresa->bank_account}}
                      <small class="d-block text-muted mb-0">{{$empresa->bank}}</small>
                    </td>
                    <td>
                      <div class="badge badge-light" title="{{ $empresa->created_at }}">
                        <div class="date-year h4 my-0 py-0">{{ $empresa->created_at->format('Y') }}</div>
                        <div class="date-days h6 my-0 py-0">{{ $empresa->created_at->format('d M') }}</div>
                      </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
  </div>
  <div class="tab-pane fade" id="pills-bancos" role="tabpanel" aria-labelledby="pills-bancos-tab">
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                  <th>ID</th>
                  <th>CODE</th>
                  <th>NOMBRE</th>
                  <th>TIPO</th>
                  <th>SBIF</th>
              </tr>
            </thead>
            <tbody>
            @foreach ($bancos as $banco)
                <tr>
                    <td>{{$banco->id}}</td>
                    <td>{{$banco->code}}</td>
                    <td>{{$banco->name}}</td>
                    <td>{{$banco->type}}</td>
                    <td>{{$banco->sbif}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
  </div>
</div>