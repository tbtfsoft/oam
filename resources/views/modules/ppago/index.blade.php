@extends('layouts.app')

@content
    @slot('breadcrumbItems')
        <li class="breadcrumb-item active" aria-current="page">Empresas PPCEN</li>
    @endslot
    @slot('title')
        <div class="d-flex">
            <div class="col align-items-center">
                <h1 class="display-6 text-uppercase">Empresas PPCEN</h1>
            </div>
            <div class="col text-right">
                <a href="{{ route('ppagos.export') }}" class="btn btn--padding btn-primary btn-ppexportar" id="exportar">
                  Exportar
                </a>
                <button type="button" class="btn btn--padding btn-danger btn-ppprocesar" id="procesar">
                  Procesar
                </button>
            </div>
        </div>
    @endslot

    <form id="response" class="shadow rounded bg-white">
        

        @include('modules.ppago.response.table',['empresas' => $empresas, 'bancos' => $bancos])
    </form>
@endcontent

@section('scripts')
    @parent
    <script>
        var response = $('#response');

        $('.btn-ppprocesar').on('click', function() {
            Swal.fire({
              title: 'Desea continuar?',
              text: 'Se eliminara la data ya existente',
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {
               response.addClass('loading');
                setData()
              }
            });
        })


        function setData() {
            $.ajax({
                method: 'GET',
                url: "{!!route('ppagos.process')!!}",
                processData: false,
                contentType: false,
                dataType: "json",
                beforeSend: function(){
                   response.addClass('loading');
                },
                success: function(data){
                   response.html(data.response);
                    Swal.fire({
                      text: data.message,
                      icon: "success"
                    })
                   response.removeClass('loading');
                },
                error: function(xhr,status,error){
                   response.removeClass('loading');
                    Swal.fire(xhr.responseJSON.message, {
                        timer: 3000,
                        icon: "error",
                    });
                }
            });
        }
    </script>
@endsection