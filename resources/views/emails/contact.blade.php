@component('mail::message')
The body of your message.
@component('mail::button', ['url' => ''])
Button Text
@endcomponent
{{ $details['email'] }}
Thanks,<br>
{{ config('app.name') }}
@slot('footer')
    @component('mail::footer')
        © {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.') S
    @endcomponent
@endslot
@endcomponent
