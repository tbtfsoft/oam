@extends('layouts.app')

<?php date_default_timezone_set('America/Santiago');?>

@php 
    $mes_esp = '';
    $mes_ant = '';
    $mes = date('m');
    $anio = date('Y');
    $anio_ant = date('Y');
    if ($mes == '01') {
      $mes_esp = __('app.enero');
      $mes_ant = __('app.diciembre');
      $anio_ant = $anio_ant - 1;
    }elseif ($mes == '02') {
      $mes_esp = __('app.febrero');
      $mes_ant = __('app.enero');
    }elseif ($mes == '03') {
      $mes_esp = __('app.marzo');
      $mes_ant = __('app.febrero');
    }elseif ($mes == '04') {
      $mes_esp = __('app.abril');
      $mes_ant = __('app.marzo');
    }elseif ($mes == '05') {
      $mes_esp = __('app.mayo');
      $mes_ant = __('app.abril');
    }elseif ($mes == '06') {
      $mes_esp = __('app.junio');
      $mes_ant = __('app.mayo');
    }elseif ($mes == '07') {
      $mes_esp = __('app.julio');
      $mes_ant = __('app.junio');
    }elseif ($mes == '08') {
      $mes_esp = __('app.agosto');
      $mes_ant = __('app.julio');
    }elseif ($mes == '09') {
      $mes_esp = __('app.septiembre');
      $mes_ant = __('app.agosto');
    }elseif ($mes == '10') {
      $mes_esp = __('app.octubre');
      $mes_ant = __('app.septiembre');
    }elseif ($mes == '11') {
      $mes_esp = __('app.noviembre');
      $mes_ant = __('app.octubre');
    }elseif ($mes == '12') {
      $mes_esp = __('app.diciembre');
      $mes_ant = __('app.noviembre');
    }
  @endphp

@content
    @slot('title')
        <div class="row align-items-center">
            <div class="col">
                <h1 class="display-6 text-uppercase my-0 pt-md-3">{{ __('app.Dashboard') }}</h1>
            </div>
            <div class="col-12 col-md-6">
                <div class="row justify-content-end">
                    @if(Auth::user()->hasRole(['super-admin']))
                        @SelectCompanys(['class'=>"col-12 col-sm-6", 'selected' => 414 ])
                    @else
                        @SelectCompanys(['class'=>"col-12 col-sm-6", 'selected' => Auth::user()->empresas->first()->id ])
                    @endif
                </div>
            </div>
        </div>
    @endslot
    
    <div class="row">
        <input type="hidden" value="{{$anio}}" id="anio">
        <div class="col-12">
            <div class="title-grafic">
                <div style="font-weight: bold;"> {{ __('app.Mes actual') }} ({{$mes_esp}} {{$anio}})</div>
                <button type="button" class="btn btn-secondary rounded-circle opacity-5 btn-sm" data-toggle="tooltip" data-placement="top" title="{{ __('app.Calculo basados en valores instantáneos') }}">
                    <i class="fas fa-info-circle"></i>
                </button>
            </div>
        </div>
        <div class="col-12 col-md-6 my-2">
            <div class="shadow rounded bg-white">
                <div id="potenciaDiaria" class="p-2"></div>
            </div>
        </div>
        <div class="col-12 col-md-6 my-2">
            <div class="shadow rounded bg-white">
                <div id="energiaDiaria" class="p-2"></div>
            </div>
        </div>  
        <div class="col-12 col-md-12 my-2">
            <div class="shadow rounded bg-white">
                <div id="energiaMesual" class="p-2"></div>
            </div>
        </div>                
    </div>

    <div class="row mt-5">
        <div class="col-12">
            <div class="title-grafic">
                <div style="font-weight: bold;">
                    {{ __('app.Mes en proceso') }} ({{$mes_ant}} {{$anio_ant}})
                </div>
                <button type="button" class="btn btn-secondary rounded-circle opacity-5 btn-sm" data-toggle="tooltip" data-placement="top" title="{{ __('app.Calculo basados en valores instantáneos') }}">
                    <i class="fas fa-info-circle"></i>
                </button>
            </div>
        </div>
        <div class="col-12 col-md-12 my-2">
            <div class="shadow rounded bg-white">
                <div id="energiaMesualProceso" class="p-2"></div>
            </div>
        </div>                
    </div>

    <div class="row mt-5">
        <div class="col-12 mt-lg-4">
            <div class="title-grafic">
                <div style="font-weight: bold;"> {{ __('app.Año en proceso - Mes cerrado') }} ({{$anio}})</div>
                <button type="button" class="btn btn-secondary rounded-circle opacity-5 btn-sm" data-toggle="tooltip" data-placement="top" title="{{ __('app.Calculos basados en valores acumulativos') }}">
                    <i class="fas fa-info-circle"></i>
                </button>
            </div>
        </div>
        <div class="col-12 col-md-6 my-2">
            <div class="shadow rounded bg-white">
                <div id="produccionEnergiMensual" class="p-2"></div>
            </div>
        </div>
        <div class="col-12 col-md-6 my-2">
            <div class="shadow rounded bg-white">
                <div id="produccionEnergiaCLPMensual" class="p-2"></div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-12 col-md-6 my-2">
            <div class="shadow rounded bg-white">
                <div id="produccionPotenciaCLPMensual" class="p-2"></div>
            </div>
        </div>    
        <div class="col-12 col-md-6 my-2">
            <div class="shadow rounded bg-white">
                <div id="servCompCLPMensual" class="p-2"></div>
                </div>
            </div>               
        </div>
    </div>

    <input type="hidden" id="fecha_actual" name="fecha_actual" value="{{date('d/m/Y')}}" />    
    @endcontent

    @section('scripts')
        @parent
        <script>
            var urlEnergiaMensual = "{!!route('dashboard.energiaMensual')!!}";
            var urlEnergiaCLPMensual = "{!!route('dashboard.energiaCLPMensual')!!}";
            var urlPotenciaCLPMensual = "{!!route('dashboard.potenciaCLPMensual')!!}";
            var urlServCompCLPMensual = "{!!route('dashboard.servCompCLPMensual')!!}";
            var urlPotenciaDiaria = "{!!route('dashboard.potenciaDiaria')!!}";
            var urlEnergiaDiaria = "{!!route('dashboard.energiaDiaria')!!}";
            var urlEnergiaMensual2 = "{!!route('dashboard.energiaMensual2')!!}";
            var urlEnergiaMensual2Ant = "{!!route('dashboard.energiaMensual2Ant')!!}";
        </script>
        <script src="{{ asset('plugins/highcharts/highcharts.js') }}"></script>
        <script src="{{ asset('plugins/highcharts/exporting.js') }}"></script>
        <script src="{{ asset('plugins/highcharts/export-data.js') }}"></script>
        <script src="{{ asset('plugins/highcharts/moment.min.js') }}"></script>
        <script src="{{ asset('plugins/highcharts/moment-timezone-with-data-2012-2022.min.js') }}"></script>        
        <script src="{{ asset('js/dashboard.js') }}"></script>
    @endsection
    @section('styles')
        <style>
            .shadow { -webkit-transition: .5s all; -o-transition: .5s all; transition: .5s all; }
            .shadow:hover { box-shadow: 0 .125rem .25rem rgba(51,51,51,.075)!important; }
        </style>
        <script>
            var lang = new Array();
            lang['Potencia Activa kW - Radiación W/m2'] = "{{__('app.Potencia Activa kW - Radiación W/m2')}}";
            lang['Generación kWh Total'] = "{{ __('app.Generación kWh Total') }}";
            lang['kWh - Generación CLP total'] = "{{ __('app.kWh - Generación CLP total') }}";
            lang['Producción y Consumo Mensual de Energía (kWh)'] = "{{__('app.Producción y Consumo Mensual de Energía (kWh)')}}";
            lang['Energía (kWh)'] = "{{ __('app.Energía (kWh)') }}";
            lang['Transferencia Mensual de Energia (CLP)'] = "{{ __('app.Transferencia Mensual de Energia (CLP)') }}";
            lang['Potencia Transferida (CLP)'] = "{{ __('app.Potencia Transferida (CLP)') }}";
            lang['Energía (CLP)'] = "{{ __('app.Energía (CLP)') }}";
            lang['Potencia Transferida (CLP)'] = "{{ __('app.Potencia Transferida (CLP)') }}";
            lang['Producción Total'] = "{{ __('app.Producción Total') }}";
            lang['Transferencia Mensual de Potencia (CLP)'] = "{{ __('app.Transferencia Mensual de Potencia (CLP)') }}";
            lang['Potencia (CLP)'] = "{{ __('app.Potencia (CLP)') }}";
            lang['Servicios Complementarios (CLP)'] = "{{ __('app.Servicios Complementarios (CLP)') }}";
            lang['Servicios Complementarios Total'] = "{{ __('app.Servicios Complementarios Total') }}";
            lang['Servicios Complementarios Mensual (CLP)'] = "{{ __('app.Servicios Complementarios Mensual (CLP)') }}";
            lang['Energía Inyectada (kWh)'] = "{{ __('app.Energía Inyectada (kWh)') }}";
            lang['Energía'] = "{{ __('app.Energía') }}";
            lang['Energía Generada (kWh - CLP)'] = "{{ __('app.Energía Generada (kWh - CLP)') }}";
            lang['Generación CLP'] = "{{ __('app.Generación CLP') }}";
            lang['Generación kWh'] = "{{ __('app.Generación kWh') }}";
            lang['año'] = "{{ __('app.año') }}";
        </script>
    @endsection
