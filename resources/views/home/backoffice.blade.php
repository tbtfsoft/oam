@extends('layouts.app')

@content
    @slot('title')
        <div class="row align-items-center">
            <div class="col">
                <h1 class="display-6 text-uppercase my-0 pt-md-3">Dashboard</h1>
            </div>
            <div class="col-12 col-md-6">
                <div class="row">
                    @SelectCompanys(['class'=>"col-6 pr-1"])

                    @SelectYears(['class'=>"col-6 pl-1 col-years"])
                </div>
            </div>
        </div>
    @endslot

    <div class="row">
        <div class="col-12 col-sm-6">
            <div id="produccionEnergiMensual"></div>
        </div>
        <div class="col-12 col-sm-6">
            <div id="produccionEnergiaCLPMensual"></div>
        </div>              
    </div>

    <hr class="my-1">
    
    <div class="row">
        <div class="col-12 col-sm-6">
            <div id="produccionPotenciaCLPMensual"></div>
        </div>    
        <div class="col-12 col-sm-6">
            <div id="servCompCLPMensual"></div>
        </div>               
    </div>    
@endcontent

@section('scripts')
    @parent
    <script>
        var urlEnergiaMensual = "{!!route('dashboard.energiaMensual')!!}";
        var urlEnergiaCLPMensual = "{!!route('dashboard.energiaCLPMensual')!!}";
        var urlPotenciaCLPMensual = "{!!route('dashboard.potenciaCLPMensual')!!}";
        var urlServCompCLPMensual = "{!!route('dashboard.servCompCLPMensual')!!}";
    </script>
    <script src="{{ asset('plugins/highcharts/highcharts.js') }}"></script>
    <script src="{{ asset('plugins/highcharts/exporting.js') }}"></script>
    <script src="{{ asset('plugins/highcharts/export-data.js') }}"></script>
    <script src="{{ asset('js/dashboard.js') }}"></script>
@endsection
