<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableConexiones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::dropIfExists('conexion-empresa');

        Schema::dropIfExists('dispositivo-empresa');

        Schema::dropIfExists('conexiones');

        Schema::create('conexiones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('host')->nullable();
            $table->string('db')->nullable();
            $table->string('nombre_parq')->nullable();
            $table->integer('id_empresa')->nullable();
            $table->integer('id_parq')->nullable();
            $table->integer('id_medidor')->nullable();
            $table->integer('id_radsensor')->nullable();
            $table->timestamps();
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
