<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToEnergiaMedidoresCen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('energia_medidores_cen', function (Blueprint $table) {
            $table->mediumInteger('id_tipo_doc_cen')->after('periodo')->nullable();
            $table->string('dia', 50)->after('hora')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('energia_medidores_cen', function (Blueprint $table) {
            $table->dropColumn('id_tipo_doc_cen');
            $table->dropColumn('dia');
        });
    }
}
