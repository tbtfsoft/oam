<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTables100319 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::dropIfExists('energia_medidores_oe');

        Schema::dropIfExists('energia_medidores_oe_original');

        Schema::dropIfExists('energia_medidores_cen');

        Schema::create('energia_medidores_cen', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->integer('id_empresa')->unsigned();
            $table->foreign('id_empresa')->references('id')->on('empresas');
            
            $table->integer('id_user')->unsigned();
            $table->foreign('id_user')->references('id')->on('users');

            $table->string('periodo');
            $table->mediumInteger('id_tipo_doc_cen')->nullable();
            $table->integer('hora');
            $table->integer('dia')->nullable();
            $table->float('kwh_in');
            $table->float('kwh_out');
            $table->timestamps();
        });

        Schema::dropIfExists('energia_medidores_pfv');

        Schema::create('energia_medidores_pfv', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_empresa')->unsigned();
            $table->foreign('id_empresa')->references('id')->on('empresas');
            
            $table->integer('id_user')->unsigned();
            $table->foreign('id_user')->references('id')->on('users');

            $table->string('periodo');
            $table->float('kwh_in');
            $table->float('kwh_out');
            $table->timestamp('medidor_date')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('energia_medidores_cen');
        Schema::dropIfExists('energia_medidores_pfv');
    }
}
