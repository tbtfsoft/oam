<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateFacturasEmitidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('facturas_emitidas');  

        Schema::dropIfExists('facturas_recibidas');  
        
        Schema::create('facturas_emitidas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('periodo', 50);
            $table->string('id_empresa');
            $table->string('id_empresa_deudora');
            $table->string('id_empresa_acreedora');
            $table->string('id_tipo_dte');
            $table->string('folio');
            $table->string('fecha_emision');
            $table->string('fecha_aceptacion');
            $table->string('fecha_aceptacion_ts');
            $table->string('fecha_vencimiento');
            $table->string('forma_pago');
            $table->float('monto_neto');
            $table->float('monto_bruto');
            $table->string('iva');
            $table->timestamps();
        });
        
        Schema::create('facturas_recibidas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('periodo', 50);
            $table->string('id_empresa');
            $table->string('id_empresa_deudora');
            $table->string('id_empresa_acreedora');
            $table->string('id_tipo_dte');
            $table->string('folio');
            $table->string('fecha_emision');
            $table->string('fecha_aceptacion');
            $table->string('fecha_aceptacion_ts');
            $table->string('fecha_vencimiento');
            $table->string('forma_pago');
            $table->float('monto_neto');
            $table->float('monto_bruto');
            $table->string('iva');
            $table->timestamps();
        });                 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturas_emitidas');  

        Schema::dropIfExists('facturas_recibidas');  
        
        Schema::create('facturas_emitidas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('periodo', 50);
            $table->string('id_tipo_cuadro_pago');
            $table->string('id_empresa_deudora');
            $table->string('id_empresa_acreedora');
            $table->string('id_tipo_dte');
            $table->string('folio');
            $table->string('fecha_emision');
            $table->string('fecha_aceptacion');
            $table->string('fecha_aceptacion_ts');
            $table->string('fecha_vencimiento');
            $table->string('forma_pago');
            $table->string('monto_neto');
            $table->string('monto_bruto');
            $table->string('iva');
            $table->timestamps();
        });
        
        Schema::create('facturas_recibidas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('periodo', 50);
            $table->string('id_tipo_cuadro_pago');
            $table->string('id_empresa_deudora');
            $table->string('id_empresa_acreedora');
            $table->string('id_tipo_dte');
            $table->string('folio');
            $table->string('fecha_emision');
            $table->string('fecha_aceptacion');
            $table->string('fecha_aceptacion_ts');
            $table->string('fecha_vencimiento');
            $table->string('forma_pago');
            $table->string('monto_neto');
            $table->string('monto_bruto');
            $table->string('iva');
            $table->timestamps();
        }); 
    }
}
