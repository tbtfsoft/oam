<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBarrasEmpresasTable10 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('barras_empresas');

        Schema::create('barras_empresas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_barras');
            $table->integer('id_empresa');
            $table->integer('tipo_calculo');
            $table->dateTime('inicial_date');
            $table->dateTime('final_date')->nullable();
            $table->timestamps();
        }); 

        Schema::dropIfExists('modalidad_calculo');

        Schema::create('modalidad_calculo', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->string('tipo_calculo');
            $table->timestamps();
        }); 
        
        Schema::dropIfExists('barras');

        Schema::create('barras', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('barra_xls');
            $table->string('barra_human');                   
            $table->timestamps();
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barras_empresas');

        Schema::create('barras_empresas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_barras');
            $table->integer('id_empresa');
            $table->dateTime('inicial_date');
            $table->dateTime('final_date')->nullable();
            $table->timestamps();
        }); 

        Schema::dropIfExists('modalidad_calculo');

        Schema::create('modalidad_calculo', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->string('tipo_barra');
            $table->timestamps();
        });   
        
        Schema::dropIfExists('barras');

        Schema::create('barras', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('barra_xls');
            $table->string('barra_human');          
            $table->integer('tipo_barra');            
            $table->timestamps();
        });        
    }
}
