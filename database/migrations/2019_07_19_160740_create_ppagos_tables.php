<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePpagosTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pp_empresas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('rut');
            $table->string('verification_code');
            $table->string('business_name');
            $table->string('commercial_business');
            $table->string('dte_reception_email');
            $table->string('bank_account');
            $table->string('bank');
            $table->string('commercial_address')->nullable();
            $table->string('postal_address')->nullable();
            $table->string('manager')->nullable();
            $table->string('payments_contact_first_name')->nullable();
            $table->string('payments_contact_last_name')->nullable();
            $table->string('payments_contact_address')->nullable();
            $table->string('payments_contact_phones')->nullable();
            $table->string('payments_contact_email')->nullable();
            $table->string('bills_contact_first_name')->nullable();
            $table->string('bills_contact_last_name')->nullable();
            $table->string('bills_contact_address')->nullable();
            $table->string('bills_contact_phones')->nullable();
            $table->string('bills_contact_email')->nullable();
            $table->timestamps();
        });

        Schema::create('pp_bancos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('name');
            $table->string('sbif');
            $table->string('type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pp_empresas');
        Schema::dropIfExists('pp_bancos');
    }
}
