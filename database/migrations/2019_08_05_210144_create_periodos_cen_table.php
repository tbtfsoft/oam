<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeriodosCenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('periodos_cen', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('periodo')->nullable();
            $table->string('tipo_doc')->nullable();
            $table->string('tipo_item')->nullable();
            $table->string('glosa')->nullable();
            $table->string('folio')->nullable();
            $table->date('fecha');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('periodos_cen');
    }
}
