<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBarrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('barras');

        Schema::create('barras', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('barra_xls');
            $table->string('barra_human');          
            $table->integer('tipo_barra');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barras');

        Schema::create('barras', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('barra_pnudo');
            $table->string('barra');
            $table->timestamps();
        });
    }
}
