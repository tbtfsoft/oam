<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColummnConexiones03 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('conexiones', function (Blueprint $table) {
            $table->string('db_prmte')->nullable();
            $table->integer('id_parq_prmte')->unsigned()->nullable();
            $table->integer('id_medidor_prmte')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('conexiones', function (Blueprint $table) {
            $table->dropColumn(['db_prmte', 'id_parq_prmte', 'id_medidor_prmte']);
        });
    }
}
