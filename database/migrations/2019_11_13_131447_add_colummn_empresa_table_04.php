<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColummnEmpresaTable04 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empresas', function (Blueprint $table) {
            $table->string('potencia_instalada')->nullable()->default(null)->after('label');
            $table->string('coordenadas_lat')->nullable()->default(null)->after('label');
            $table->string('coordenadas_lng')->nullable()->default(null)->after('label');
            $table->date('fecha_inicio')->nullable()->default(null)->after('label');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empresas', function (Blueprint $table) {
            $table->dropColumn(['potencia_instalada', 'coordenadas_lat', 'coordenadas_lng', 'fecha_inicio']);
        });
    }
}
