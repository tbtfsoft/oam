<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCmgTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('cmg');
        
        Schema::create('cmg', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_empresa')->unsigned();
            $table->foreign('id_empresa')->references('id')->on('empresas');
            
            $table->integer('id_user')->unsigned();
            $table->foreign('id_user')->references('id')->on('users');

            $table->mediumInteger('id_tipo_doc_cen')->after('periodo')->nullable();
            $table->string('hora');
            $table->string('dia', 50)->after('hora')->nullable();
            $table->string('barra');
            $table->string('cmg_mills_kwh');
            $table->string('usd');
            $table->string('cmg_usd_kwh');
            $table->string('periodo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cmg');

        Schema::create('cmg', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_empresa')->unsigned();
            $table->foreign('id_empresa')->references('id')->on('empresas');
            
            $table->integer('id_user')->unsigned();
            $table->foreign('id_user')->references('id')->on('users');

            $table->mediumInteger('id_tipo_doc_cen')->after('periodo')->nullable();
            $table->string('hora');
            $table->string('dia', 50)->after('hora')->nullable();
            $table->string('barra');
            $table->string('cmg_mills_kwh');
            $table->string('usd');
            $table->string('cmg_usd_kwh');
            $table->timestamps();
        });
    }
}
