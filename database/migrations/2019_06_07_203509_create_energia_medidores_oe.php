<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnergiaMedidoresOe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('energia_medidores_oe', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->integer('id_empresa')->unsigned();
            $table->foreign('id_empresa')->references('id')->on('empresas');
            
            $table->integer('id_user')->unsigned();
            $table->foreign('id_user')->references('id')->on('users');

            $table->string('periodo');
            $table->integer('hora');
            $table->string('kwh_in');
            $table->string('kwh_out');
            $table->timestamps();
        });

        Schema::create('energia_medidores_oe_original', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->integer('id_empresa')->unsigned();
            $table->foreign('id_empresa')->references('id')->on('empresas');
            
            $table->integer('id_user')->unsigned();
            $table->foreign('id_user')->references('id')->on('users');

            $table->string('periodo');
            $table->integer('hora');
            $table->string('kwh_in');
            $table->string('kwh_out');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('energia_medidores_oe');
        Schema::dropIfExists('energia_medidores_oe_original');
    }
}
