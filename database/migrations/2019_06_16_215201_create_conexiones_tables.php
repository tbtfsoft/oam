<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateconexionesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conexiones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('db');
            $table->string('tabla');
            $table->string('tipo')->nullable();
            $table->string('slug')->nullable();
            $table->timestamps();
        });

        Schema::create('conexion-empresa', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->integer('empresa_id')->unsigned();
            $table->foreign('empresa_id')->references('id')->on('empresas')->onDelete('cascade');
            
            $table->integer('conexion_id')->unsigned();
            $table->foreign('conexion_id')->references('id')->on('conexiones')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conexion-empresa');
        Schema::dropIfExists('conexiones');
    }
}
