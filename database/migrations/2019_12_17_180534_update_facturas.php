<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateFacturas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('facturas');

        Schema::create('facturas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('periodo', 50);
            $table->integer('id_empresa_deudora')->unsigned()->nullable();
            $table->foreign('id_empresa_deudora')->references('id')->on('empresas')->onDelete('cascade');
            $table->integer('id_empresa_acreedora')->unsigned()->nullable();
            $table->foreign('id_empresa_acreedora')->references('id')->on('empresas')->onDelete('cascade'); 
            $table->string('id_modulo');                      
            $table->string('id_tipo_dte');
            $table->string('folio');
            $table->string('fecha_emision');
            $table->string('fecha_aceptacion');
            $table->string('fecha_aceptacion_ts');
            $table->string('fecha_vencimiento');
            $table->string('forma_pago');
            $table->float('monto_neto');
            $table->float('monto_bruto');
            $table->string('iva');
            $table->string('pago_codigo')->nullable();
            $table->string('pago_fecha')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturas');

        Schema::create('facturas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('periodo', 50);
            $table->integer('id_empresa_deudora')->unsigned()->nullable();
            $table->foreign('id_empresa_deudora')->references('id')->on('empresas')->onDelete('cascade');
            $table->integer('id_empresa_acreedora')->unsigned()->nullable();
            $table->foreign('id_empresa_acreedora')->references('id')->on('empresas')->onDelete('cascade');                        
            $table->string('id_tipo_dte');
            $table->string('folio');
            $table->string('fecha_emision');
            $table->string('fecha_aceptacion');
            $table->string('fecha_aceptacion_ts');
            $table->string('fecha_vencimiento');
            $table->string('forma_pago');
            $table->float('monto_neto');
            $table->float('monto_bruto');
            $table->string('iva');
            $table->string('pago_codigo')->nullable();
            $table->string('pago_fecha')->nullable();
            $table->timestamps();
        });
    }
}
