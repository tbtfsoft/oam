<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCorrespondenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('comentarios_empresas');
        Schema::dropIfExists('correspondencias_empresas');
        Schema::dropIfExists('correspondencias_comentarios');
        Schema::dropIfExists('correspondencias');

        Schema::create('correspondencias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('titulo');
            $table->mediumText('descripcion');
            $table->date('fecha');
            $table->integer('status')->unsigned()->default(0);
            $table->integer('id_user')->unsigned()->nullable();
            $table->foreign('id_user')->references('id')->on('users');
            $table->timestamps();
        });

        Schema::create('modulos_empresas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('modulo');
            $table->integer('id_modulo')->unsigned();
            $table->integer('id_empresa')->unsigned();
            $table->foreign('id_empresa')->references('id')->on('empresas')->onDelete('cascade');
        });

        Schema::create('modulos_comentarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('modulo');
            $table->integer('id_modulo')->unsigned();
            $table->integer('id_user')->unsigned();
            $table->mediumText('comentario');
            $table->timestamps();
        });

        Schema::create('comentarios_empresas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_comentario')->unsigned();
            $table->foreign('id_comentario')->references('id')->on('modulos_comentarios')->onDelete('cascade');
            $table->integer('id_empresa')->unsigned();
            $table->foreign('id_empresa')->references('id')->on('empresas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comentarios_empresas');
        Schema::dropIfExists('modulos_empresas');
        Schema::dropIfExists('modulos_comentarios');
        Schema::dropIfExists('correspondencias');
    }
}
