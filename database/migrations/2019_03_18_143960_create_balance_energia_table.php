<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\AssetManagement\Energia as Model;

class CreateBalanceEnergiaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Model::getTableModel(), function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_empresa_deudora');
            $table->unsignedBigInteger('id_empresa_acreedora');
            $table->unsignedBigInteger('id_tipo_doc_cen')->nullable();
            $table->double('costo');
            $table->string('periodo');
            $table->foreign('id_empresa_deudora')
                ->references('id')
                ->on('empresas')
                ->onDelete('cascade');

            $table->foreign('id_empresa_acreedora')
                ->references('id')
                ->on('empresas')
                ->onDelete('cascade');

            $table->foreign('id_tipo_doc_cen')
                ->references('id')
                ->on('tipo_doc_cen')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Model::getTableModel());
    }
}
