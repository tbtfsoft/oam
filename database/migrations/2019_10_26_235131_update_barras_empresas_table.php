<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBarrasEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('barras_empresas');

        Schema::create('barras_empresas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_barras');
            $table->integer('id_empresa');
            $table->dateTime('inicial_date');
            $table->dateTime('final_date')->nullable();
            $table->timestamps();
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barras_empresas');

        Schema::create('barras_empresas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_barras');
            $table->integer('id_empresa');
            $table->timestamps();
        });        
    }
}
