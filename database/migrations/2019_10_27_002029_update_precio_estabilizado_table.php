<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePrecioEstabilizadoTable29 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('precio_estabilizado'); 
        
        Schema::create('precio_estabilizado', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_barra');
            $table->float('precio_energia');
            $table->float('precio_potencia');
            $table->dateTime('inicial_date');
            $table->dateTime('final_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('precio_estabilizado'); 
        
        Schema::create('precio_estabilizado', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_empresa');
            $table->float('precio_energia');
            $table->float('precio_potencia');
            $table->string('periodo');
            $table->timestamps();
        });
    }
}
