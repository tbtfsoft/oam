<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColummnDb2TablaConexiones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('conexiones', function (Blueprint $table) {
            $table->string('db_2')->nullable()->default(null)->after('label');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('conexiones', function (Blueprint $table) {
            $table->dropColumn(['db_2']);
        });
    }
}
