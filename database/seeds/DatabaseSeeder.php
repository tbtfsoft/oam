<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ModulesSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(TipoDocCenTableSeeder::class);
        $this->call(TipoIntegracionTableSeeder::class);
        $this->call(ConexionesTableSeeder::class);
        $this->call(DispositivosTableSeeder::class);
    }
}
