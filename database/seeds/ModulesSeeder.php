<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class ModulesSeeder extends Seeder
{

    /**
     * @var modules
     */
    protected $modules;

    /**
     * @var roles
     */
    protected $roles;

    /**
     * @var permission
     */
    protected $permission;

    public function __construct()
    {
        $this->modules = config('permission.modules');
    
        $this->roles = config('permission.roles');

        $this->permission = [];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     * @author Carlos Anselmi <carlosanselmi2@gmail.com>
     */
    public function run()
    {
        foreach ($this->modules as $module) {
            $this->CreatePermission($module);
        }

        foreach ($this->roles as $rol) {
            $this->CreateRole($rol);
        }

    }


    /**
     * Funcion recursiva para la creación de los permission
     * @param String $module objeto del modulo
     * @param String $parent name del modulo patre, si es que existe
     * @author Carlos Anselmi <carlosanselmi2@gmail.com>
     */
    public function CreatePermission($module, $parent = null)
    {
        Permission::firstOrCreate([
                'name'          => $module['module'],
                'display_name'  => $module['display_name'],
                'module'        => $parent,
                'description'   => $module['description']
            ]);

        if (!$parent) {
            array_push($this->permission,$module['module']);
        }

        /**
         * Si existe submodules del modulo se crean en una llamada recursiva.
         */
        if(isset($module['modules']) && count($module['modules'])) {
            foreach ($module['modules'] as $key => $subModule) {
                $this->CreatePermission($subModule,$module['module']);
            }
        }
    }

    /**
     * Funcion recursiva para la creación de los Roles
     * @param String $rol Objeto del rol a crear
     * @author Carlos Anselmi <carlosanselmi2@gmail.com>
     */    
    public function CreateRole($rol)
    {
        $role = Role::firstOrCreate([
                'name'          =>  $rol['name'],
                'description'   =>  $rol['description']
            ]);

        if($this->ifRoleSuperAdmin($rol['name']))
            $role->syncPermissions($this->permission);
    }

    public function ifRoleSuperAdmin($role)
    {
        return $role == 'super-admin';
    }
}
