<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class UsersTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     * @author Carlos Anselmi <carlosanselmi2@gmail.com>
     */
    public function run()
    {
        // ADMINISTRADOR
        $admin = factory(User::class)->create([
            'name' => 'Super Administrador',
            'password' => Hash::make('12345'),
            'email' => 'admin@mail.com',
            'rut' => '9.201.619-6',
        ]);

        $admin->assignRole('super-admin');

        $permissions = Permission::all()->where('module')->pluck('name')->toArray();

        $admin->syncPermissions($permissions);

        $clienteAdmin = factory(User::class)->create([
            'rut' => '31.000.000-0',
            'name' => 'Morris',
            'password' => Hash::make('12345'),
            'email' => 'morrissosadiaz@gmail.com',
        ]);

        $clienteAdmin->assignRole('client-admin');

        $cliente = factory(User::class)->create([
            'rut' => '00.000.000-0',
            'name' => 'Carlos Anselmi',
            'password' => Hash::make('12345'),
            'email' => 'carlosanselmi2@gmail.com',
        ]);

        $cliente->assignRole('client');

        $clienteAdmin->childers()->attach($cliente);

        $cliente = factory(User::class)->create([
            'rut' => '31.000.001-9',
            'name' => 'Alfredo Jimenez',
            'password' => Hash::make('12345'),
            'email' => 'carlosanselmi3@hotmail.com',
        ]);

        $cliente->assignRole('client');

        $clienteAdmin->childers()->attach($cliente);
    }
}
