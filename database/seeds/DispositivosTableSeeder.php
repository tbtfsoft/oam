<?php

use Illuminate\Database\Seeder;
use App\Models\Dispositivo;

class DispositivosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    		Dispositivo::create([
    			'id_disp'   => 1001,
                'tipo'      => 'balance-energia'
    		]);
    }
}
