<?php

use Illuminate\Database\Seeder;
use App\Models\AssetManagement\TipoDocCen;

class TipoDocCenTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TipoDocCen::create(['nombre' => 'Preliminar']);
        TipoDocCen::create(['nombre' => 'Definitivo']);
        TipoDocCen::create(['nombre' => 'Reliquidacion']);
    }
}
