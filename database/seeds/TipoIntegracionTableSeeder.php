<?php

use Illuminate\Database\Seeder;
use App\Models\AssetManagement\TipoIntegracion;

class TipoIntegracionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TipoIntegracion::create(['nombre' => 'DTE']);
    }
}
