<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class UpdatePermissions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $permissions = Permission::where('module','!=','Empresa')->get();

      foreach ($permissions as $permission) {
				$flight = Permission::find($permission->id);
				$flight->delete();
      }

      Permission::firstOrCreate([
				'name'          => 'comercializacion',
				'display_name'  => 'Comercialización',
				'module'        => 'Comercialización',
				'description'   => ''
			]);

      Permission::firstOrCreate([
				'name'          => 'scada',
				'display_name'  => 'Scada Web',
				'module'        => 'Scada Web',
				'description'   => ''
			]);

      Permission::firstOrCreate([
				'name'          => 'asesor-tecnico',
				'display_name'  => 'Asesor Técnico',
				'module'        => 'Asesor Técnico',
				'description'   => ''
			]);

      Permission::firstOrCreate([
				'name'          => 'PMRTE',
				'display_name'  => 'PMRTE',
				'module'        => 'PMRTE',
				'description'   => ''
			]);
    }
}
