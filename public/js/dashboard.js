$(document).ready(function(){
    var dt = new Date();
    var year = 1900 + dt.getYear();
    var empresa = $("#empresa_xls").val();    
    
    //Iniciamos el Dashboard
    
    //Se inicia progress bar
    $('.progress').show();
    
    //Variables Electricas y Economicas
    setTimeout(function(){ 
        AjaxPotenciaDiaria();
    }, 500);
    
    setTimeout(function(){ 
        AjaxEnergiaDiaria();
    }, 1000);
    
    setTimeout(function(){ 
        AjaxEnergiaMes();
    }, 1500);

    setTimeout(function(){ 
        AjaxEnergiaMesAnt();
    }, 2000);
    //Balance de Transferencias Economicas CEN (Mes Cerrado)
    setTimeout(function(){ 
        AjaxEnergiaCLPMensual();
    }, 2500);
    
    setTimeout(function(){ 
        AjaxPotenciaCLPMensual();
    }, 3000);
    
    setTimeout(function(){ 
        AjaxServCompCLPMensual();
    }, 3500);
    
    setTimeout(function(){ 
        AjaxEnergiaMensual(); 
    }, 4000);
    
    
    //On change sobre select Empresas
    $("#empresa_xls,#anio").change(function(){
        empresa = $("#empresa_xls").val();
        year = 1900 + dt.getYear();
    
        //Se inicia progress bar
        $('.progress').show();
        //Variables Electricas y Economicas
        setTimeout(function(){ 
            AjaxPotenciaDiaria();
        }, 200);
        
        setTimeout(function(){ 
            AjaxEnergiaDiaria();
        }, 400);
        
        setTimeout(function(){ 
            AjaxEnergiaMes();
        }, 600);

        setTimeout(function(){ 
            AjaxEnergiaMesAnt();
        }, 800);
        //Balance de Transferencias Economicas CEN (Mes Cerrado)
        setTimeout(function(){ 
            AjaxEnergiaCLPMensual();
        }, 1000);
        
        setTimeout(function(){ 
            AjaxPotenciaCLPMensual();
        }, 1200);
        
        setTimeout(function(){ 
            AjaxServCompCLPMensual();
        }, 1400);
        
        setTimeout(function(){ 
            AjaxEnergiaMensual(); 
        }, 1700);
    });
    
    //Variables Electricas y Economicas
    
    function AjaxPotenciaDiaria(){
        $.ajax({
        type: "POST",
        url: urlPotenciaDiaria,
        dataType: "json",
        data: {
            year : year,
            empresa: empresa
        },
        beforeSend: function(){
        
        },
        success: function (data) {
            var options = getOptionsPotenciaDiaria();
            var horas = data.horas;
            var pt = data.pt;
            var ir = data.ir;      
            var pr = data.pr;      
    
            var series = new Array();
    
            var datos = getDataPotenciaDiaria(horas,pt);
                series.push({
                    name: 'kW',
                    data: datos,
                    color: 'blue'
                });
    
            var datos_1 = getDataPotenciaDiaria(horas,ir);
            series.push({
                name: 'W/m2',
                data: datos_1,
                color: 'orange'
            });
    
            if (pr != 9999999) {
                if (data.pr.length > 0) {
                    var datos_2 = getDataPotenciaDiaria(horas,pr);
                    series.push({
                        name: 'PR',
                        data: datos_2,
                        color: 'green'
                    });                
                }                
            }
    
            var subtitulo = data.subtitulo + '<br>'+ $("#fecha_actual").val();
    
            options.subtitle.text = subtitulo;
            options.series = series;
            Highcharts.setOptions({
                time: {
                    timezone: 'America/Santiago'
                }
            });
    
            chart = new Highcharts.Chart(options);
        }
        }); 
    }
    
    function getOptionsPotenciaDiaria(){
        var options = {
            chart: {
                backgroundColor: 'transparent',
                type: 'spline',
                renderTo: 'potenciaDiaria',
                animation: Highcharts.svg, // don't animate in old IE
            },
            title: {
                text: lang['Potencia Activa kW - Radiación W/m2'],
            },
            subtitle:{
                text: '',
            },
            xAxis: {
                type: 'datetime',
                tickPixelInterval: 5
            },
            yAxis: {
                title: {
                    align: 'high',
                    offset: 0,
                    text: 'kW',
                    rotation: 0,
                    y: -15,
                    x: -20
                },
                labels: {
                    formatter: function () {
                        return this.value;
                    }
                }
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                        Highcharts.dateFormat('%H:%M', this.x) + '<br/>' +
                        Highcharts.numberFormat(this.y, 2)+' '+this.series.name;
                }
            },
            plotOptions: {
                spline: {
                    marker: {
                        radius: 0,
                        lineWidth: 0
                    }
                }
            },        
            legend: {
                enabled: true
            },
            exporting: {
                enabled: true
            },
            series: [{
                name: '',
                data: [],
            }]
        }
    
        return options;
    }
    
    function getDataPotenciaDiaria(time,potencia){
    
        var data = [];
    
        if(time.length == potencia.length){
            for (i=0 ; i<potencia.length ; i++){
                data.push({
                    x: time[i],
                    y: potencia[i]
                });
            }
        }
    
        return data;
    }
    
    function AjaxEnergiaDiaria(){
        $.ajax({
        type: "POST",
        url: urlEnergiaDiaria,
        dataType: "json",
        data: {
            empresa: empresa
        },
        beforeSend: function(){
            
        },
        success: function (data) {
            var options = getOptionsEnergiaDiaria();
            var horas = data.horas;
            var generacion = data.generacion;
            var generacionCLP = data.generacionCLP;
            var consumo = data.consumo;
            var generacionTotal = data.generacionTotal;
            var generacionCLPTotal = data.generacionCLPTotal;
            var consumoTotal = data.consumoTotal;  
            var barra = data.barra;
            var modalidad = data.modalidad;
            var precio_energia = data.precio_energia;          
            var x = new Array();
            var series = new Array();
    
            $("#barra_span").html(barra);
            $("#modalidad_span").html(modalidad);
            $("#precio_energia_span").html(' ('+ precio_energia + ' $/kWh)');
    
            x.push({
                categories: horas,
            });
    
            series.push({
                name: lang['Generación kWh'],
                data: generacion,
                type: 'column',
                color: 'green',
                tooltip: {
                    valueSuffix: ' kWh'
                }            
            });                      
    
            series.push({
                name: lang['Generación CLP'],
                data: generacionCLP,
                type: 'spline',
                color: 'orange',
                yAxis: 1,
                tooltip: {
                    valueSuffix: ' $'
                }            
            }); 
    
            var subtitulo = data.subtitulo + '<br>'+ $("#fecha_actual").val() + '<br>'+ '<b>'+ lang['Generación kWh Total'] +': ' + new Intl.NumberFormat().format(generacionTotal) + ' '+ lang['kWh - Generación CLP total'] + ': ' + new Intl.NumberFormat().format(generacionCLPTotal) + ' CLP</b>';
    
            options.subtitle.text = subtitulo;
            options.xAxis = x;
            options.series = series;
            Highcharts.setOptions({
                time: {
                    timezone: 'America/Santiago'
                }
            });
    
            chart = new Highcharts.Chart(options);
    
            
        }
        }); 
    }
    
    function getOptionsEnergiaDiaria(){
        var options = {
            chart: {
                backgroundColor: 'transparent',            
                renderTo: 'energiaDiaria',
                animation: Highcharts.svg, // don't animate in old IE
                zoomType: 'xy'
            },
            title: {
                text: lang['Energía Generada (kWh - CLP)'],
            },
            subtitle:{
                text: '',
            },
            xAxis: {
                categories: [],
                crosshair: true
            },
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value} kWh',
                },
                title: {
                    text: lang['Energía'],
                }
            }, { // Secondary yAxis
                title: {
                    text: 'CLP',
                },
                labels: {
                    format: '{value} $',
                },
                opposite: true
            }],
            legend: {
                enabled: true
            },
            exporting: {
                enabled: true
            },
            series: [{
                name: '',
                data: [],
            }]
        }
    
        return options;
    }
    
    function AjaxEnergiaMes(){
        $.ajax({
        type: "POST",
        url: urlEnergiaMensual2,
        dataType: "json",
        data: {
            empresa: empresa   
        },
        beforeSend: function(){
            
        },
        success: function (data) {
            var options = getOptionsEnergiaMes();
            var horas = data.horas;
            var generacion = data.generacion;
            var generacionCLP = data.generacionCLP;
            var consumo = data.consumo;
            var generacionTotal = data.generacionTotal;
            var generacionCLPTotal = data.generacionCLPTotal;
            var consumoTotal = data.consumoTotal;   
            var barra = data.barra;
            var modalidad = data.modalidad;
            var precio_energia = data.precio_energia; 
            var initial_date = data.initial_date; 
            var final_date = data.final_date;                        
            var x = new Array();
            var series = new Array();
    
            $("#barra_span").html(barra);
            $("#modalidad_span").html(modalidad);
            $("#precio_energia_span").html(' ('+ precio_energia + ' $/kWh)');
    
            x.push({
                categories: horas,
            });
    
            series.push({
                name: lang['Generación kWh'],
                data: generacion,
                type: 'column',
                color: 'green',
                tooltip: {
                    valueSuffix: ' kWh'
                }            
            });                      
    
            series.push({
                name: lang['Generación CLP'],
                data: generacionCLP,
                type: 'spline',
                color: 'orange',
                yAxis: 1,
                tooltip: {
                    valueSuffix: ' $'
                }            
            }); 
    
            var subtitulo = data.subtitulo + '<br>'+ initial_date + ' - ' + final_date + '<br>'+ '<b>'+lang['Generación kWh Total']+': ' + new Intl.NumberFormat().format(generacionTotal) + ' '+lang['kWh - Generación CLP total']+': ' + new Intl.NumberFormat().format(generacionCLPTotal) + ' CLP</b>';
    
            options.subtitle.text = subtitulo;
            options.xAxis = x;
            options.series = series;
            Highcharts.setOptions({
                time: {
                    timezone: 'America/Santiago'
                }
            });
    
            chart = new Highcharts.Chart(options);
    
            
        }
        }); 
    }
    
    function getOptionsEnergiaMes(){
        var options = {
            chart: {
                backgroundColor: 'transparent',            
                renderTo: 'energiaMesual',
                animation: Highcharts.svg, // don't animate in old IE
                zoomType: 'xy'
            },
            title: {
                text: lang['Energía Generada (kWh - CLP)'],
            },
            subtitle:{
                text: '',
            },
            xAxis: {
                categories: [],
                crosshair: true
            },
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value} kWh',
                },
                title: {
                    text: lang['Energía'],
                }
            }, { // Secondary yAxis
                title: {
                    text: 'CLP',
                },
                labels: {
                    format: '{value} $',
                },
                opposite: true
            }],
            legend: {
                enabled: true
            },
            exporting: {
                enabled: true
            },
            series: [{
                name: '',
                data: [],
            }]
        }
    
        return options;
    }
    
    function AjaxEnergiaMesAnt(){
        $.ajax({
        type: "POST",
        url: urlEnergiaMensual2Ant,
        dataType: "json",
        data: {
            empresa: empresa   
        },
        beforeSend: function(){
            
        },
        success: function (data) {
            var options = getOptionsEnergiaMesAnt();
            var horas = data.horas;
            var generacion = data.generacion;
            var generacionCLP = data.generacionCLP;
            var consumo = data.consumo;
            var generacionTotal = data.generacionTotal;
            var generacionCLPTotal = data.generacionCLPTotal;
            var consumoTotal = data.consumoTotal;   
            var barra = data.barra;
            var modalidad = data.modalidad;
            var precio_energia = data.precio_energia; 
            var initial_date = data.initial_date; 
            var final_date = data.final_date;                        
            var x = new Array();
            var series = new Array();
    
            $("#barra_span").html(barra);
            $("#modalidad_span").html(modalidad);
            $("#precio_energia_span").html(' ('+ precio_energia + ' $/kWh)');
    
            x.push({
                categories: horas,
            });
    
            series.push({
                name: lang['Generación kWh'],
                data: generacion,
                type: 'column',
                color: 'green',
                tooltip: {
                    valueSuffix: ' kWh'
                }            
            });                      
    
            series.push({
                name: lang['Generación CLP'],
                data: generacionCLP,
                type: 'spline',
                color: 'orange',
                yAxis: 1,
                tooltip: {
                    valueSuffix: ' $'
                }            
            }); 
    
            var subtitulo = data.subtitulo + '<br>'+ initial_date + ' - ' + final_date + '<br>'+ '<b>'+lang['Generación kWh Total'] + ': ' + new Intl.NumberFormat().format(generacionTotal) + ' '+ lang['kWh - Generación CLP total'] + ': ' + new Intl.NumberFormat().format(generacionCLPTotal) + ' CLP</b>';
    
            options.subtitle.text = subtitulo;
            options.xAxis = x;
            options.series = series;
            Highcharts.setOptions({
                time: {
                    timezone: 'America/Santiago'
                }
            });
    
            chart = new Highcharts.Chart(options);
    
            
        }
        }); 
    }
    
    function getOptionsEnergiaMesAnt(){
        var options = {
            chart: {
                backgroundColor: 'transparent',            
                renderTo: 'energiaMesualProceso',
                animation: Highcharts.svg, // don't animate in old IE
                zoomType: 'xy'
            },
            title: {
                text: lang['Energía Generada (kWh - CLP)'],
            },
            subtitle:{
                text: '',
            },
            xAxis: {
                categories: [],
                crosshair: true
            },
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value} kWh',
                },
                title: {
                    text: lang['Energía'],
                }
            }, { // Secondary yAxis
                title: {
                    text: 'CLP',
                },
                labels: {
                    format: '{value} $',
                },
                opposite: true
            }],
            legend: {
                enabled: true
            },
            exporting: {
                enabled: true
            },
            series: [{
                name: '',
                data: [],
            }]
        }
    
        return options;
    }

    //Balance de Transferencias Economicas CEN (Mes Cerrado)
    
    function AjaxEnergiaMensual(){
        $.ajax({
        type: "POST",
        url: urlEnergiaMensual,
        dataType: "json",
        data: {
            year : year,
            empresa: empresa
        },
        beforeSend: function(){
            $('.progress').show();
        },
        success: function (data) {
        
            var energia_enero = '';
            var energia_febrero = '';
            var energia_abril = '';
            var energia_mayo = '';
            var energia_junio = '';
            var energia_julio = '';
            var energia_agosto = '';
            var energia_septiembre = '';
            var energia_octubre = '';
            var energia_noviembre = '';
            var energia_diciembre = '';      
            var series = new Array();                                                
        
            //Highcharts
        
            //Configuracion Base
            var options = getOptionsEnergiaMensual();  
        
            //Energia Inyectada
            energia_enero = parseFloat(data.energia_enero_in);
            energia_febrero = parseFloat(data.energia_febrero_in);
            energia_marzo = parseFloat(data.energia_marzo_in);
            energia_abril = parseFloat(data.energia_abril_in);
            energia_mayo = parseFloat(data.energia_mayo_in);
            energia_junio = parseFloat(data.energia_junio_in);
            energia_julio = parseFloat(data.energia_julio_in);
            energia_agosto = parseFloat(data.energia_agosto_in);
            energia_septiembre = parseFloat(data.energia_septiembre_in);
            energia_octubre = parseFloat(data.energia_octubre_in);
            energia_noviembre = parseFloat(data.energia_noviembre_in);
            energia_diciembre = parseFloat(data.energia_diciembre_in); 
    
            var energia_in_total = energia_enero + energia_febrero + energia_marzo + energia_abril + energia_mayo + energia_junio + energia_julio + energia_agosto + energia_septiembre + energia_octubre + energia_noviembre +energia_diciembre;
        
            series.push({
                name: lang['Energía Inyectada (kWh)'],
                data: [energia_enero, energia_febrero, energia_marzo, energia_abril, energia_mayo, energia_junio, energia_julio, energia_agosto, energia_septiembre, energia_octubre, energia_noviembre,energia_diciembre],
                color: 'green'
            });
        
            //Energia Consumida
            energia_enero = Math.abs(parseFloat(data.energia_enero_out));
            energia_febrero = Math.abs(parseFloat(data.energia_febrero_out));
            energia_marzo = Math.abs(parseFloat(data.energia_marzo_out));
            energia_abril = Math.abs(parseFloat(data.energia_abril_out));
            energia_mayo = Math.abs(parseFloat(data.energia_mayo_out));
            energia_junio = Math.abs(parseFloat(data.energia_junio_out));
            energia_julio = Math.abs(parseFloat(data.energia_julio_out));
            energia_agosto = Math.abs(parseFloat(data.energia_agosto_out));
            energia_septiembre = Math.abs(parseFloat(data.energia_septiembre_out));
            energia_octubre = Math.abs(parseFloat(data.energia_octubre_out));
            energia_noviembre = Math.abs(parseFloat(data.energia_noviembre_out));
            energia_diciembre = Math.abs(parseFloat(data.energia_diciembre_out));
    
            var energia_out_total = energia_enero + energia_febrero + energia_marzo + energia_abril + energia_mayo + energia_junio + energia_julio + energia_agosto + energia_septiembre + energia_octubre + energia_noviembre +energia_diciembre;
    
            series.push({
                name: 'Energía Consumida (kWh)',
                data: [energia_enero, energia_febrero, energia_marzo, energia_abril, energia_mayo, energia_junio, energia_julio, energia_agosto, energia_septiembre, energia_octubre, energia_noviembre,energia_diciembre],
                color: 'red'
            });         
    
            var subtitulo = data.subtitulo + '<br>' + lang['año'] + ' ' + $("#anio").val() + '<br>'+ '<b>Generación Total: ' + new Intl.NumberFormat().format(energia_in_total) + ' kWh - ConsumoTotal: ' + new Intl.NumberFormat().format(energia_out_total) + ' kWh</b>';
            options.subtitle.text = subtitulo;
            options.series = series; 
            chart = new Highcharts.Chart(options);      
            
            $('.progress').hide();
        }
        }); 
    }
    
    function getOptionsEnergiaMensual(){
        var options = {
            chart: {
            type: 'column',
            style: {
                fontFamily: 'sans-serif'
            },
            renderTo: 'produccionEnergiMensual'
            },
            title: {
                text: lang['Producción y Consumo Mensual de Energía (kWh)']
            },
            subtitle:{
                text: '',
            },        
            xAxis: {
                categories: ['ENE', 'FEB', 'MAR', 'ABR', 'MAY', 'JUN', 'JUL', 'AGO', 'SEP', 'OCT', 'NOV', 'DIC'],
                crosshair: true
            },
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 2000
                    },
                    chartOptions: {
                        xAxis: {
                        }
                    }
                }]
            },
            yAxis: {
                min: 0,
                title: {
                    text: lang['Energía (kWh)']
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y} kWh</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            }    
        }
        return options;
    }
    
    function AjaxEnergiaCLPMensual(){
        $.ajax({
        type: "POST",
        url: urlEnergiaCLPMensual,
        dataType: "json",
        data: {
            year : year,
            empresa: empresa
        },
        beforeSend: function(){
        
        },
        success: function (data) {
        
            var energia_enero = '';
            var energia_febrero = '';
            var energia_abril = '';
            var energia_mayo = '';
            var energia_junio = '';
            var energia_julio = '';
            var energia_agosto = '';
            var energia_septiembre = '';
            var energia_octubre = '';
            var energia_noviembre = '';
            var energia_diciembre = '';      
            var series = new Array();                                                
        
            //Highcharts
        
            //Configuracion Base
            var options = getOptionsEnergiaCLPMensual();  
        
            //Energia Inyectada
            energia_enero = parseFloat(data.energia_enero);
            energia_febrero = parseFloat(data.energia_febrero);
            energia_marzo = parseFloat(data.energia_marzo);
            energia_abril = parseFloat(data.energia_abril);
            energia_mayo = parseFloat(data.energia_mayo);
            energia_junio = parseFloat(data.energia_junio);
            energia_julio = parseFloat(data.energia_julio);
            energia_agosto = parseFloat(data.energia_agosto);
            energia_septiembre = parseFloat(data.energia_septiembre);
            energia_octubre = parseFloat(data.energia_octubre);
            energia_noviembre = parseFloat(data.energia_noviembre);
            energia_diciembre = parseFloat(data.energia_diciembre);
    
            var energia_total = energia_enero + energia_febrero + energia_marzo + energia_abril + energia_mayo + energia_junio + energia_julio + energia_agosto + energia_septiembre + energia_octubre + energia_noviembre +energia_diciembre;
        
            var subtitulo = data.subtitulo + '<br>' + lang['año'] + ' ' + $("#anio ").val() + '<br>'+ '<b>Producción Total: ' + new Intl.NumberFormat().format(energia_total) + ' CLP' + '</b>';
    
            series.push({
                name: 'Energía Producida (CLP)',
                data: [energia_enero, energia_febrero, energia_marzo, energia_abril, energia_mayo, energia_junio, energia_julio, energia_agosto, energia_septiembre, energia_octubre, energia_noviembre,energia_diciembre],
                color: 'green'
            });         
            options.subtitle.text = subtitulo;
            options.series = series; 
            chart = new Highcharts.Chart(options);         
        }
        }); 
    }
    
    function getOptionsEnergiaCLPMensual(){
        var options = {
            chart: {
            type: 'column',
            style: {
                fontFamily: 'sans-serif'
            },
            renderTo: 'produccionEnergiaCLPMensual'
            },
            title: {
                text: lang['Transferencia Mensual de Energia (CLP)']
            },
            subtitle:{
                text: '',
            },
            xAxis: {
                categories: ['ENE', 'FEB', 'MAR', 'ABR', 'MAY', 'JUN', 'JUL', 'AGO', 'SEP', 'OCT', 'NOV', 'DIC'],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: lang['Energía (CLP)']
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>$ {point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            }    
        }
        return options;
    }
    
    function AjaxPotenciaCLPMensual(){
        $.ajax({
        type: "POST",
        url: urlPotenciaCLPMensual,
        dataType: "json",
        data: {
            year : year,
            empresa: empresa
        },
        beforeSend: function(){
        
        },
        success: function (data) {
        
            var energia_enero = '';
            var energia_febrero = '';
            var energia_abril = '';
            var energia_mayo = '';
            var energia_junio = '';
            var energia_julio = '';
            var energia_agosto = '';
            var energia_septiembre = '';
            var energia_octubre = '';
            var energia_noviembre = '';
            var energia_diciembre = '';      
            var series = new Array();                                                
        
            //Highcharts
        
            //Configuracion Base
            var options = getOptionsPotenciaCLPMensual();  
        
            //Energia Inyectada
            energia_enero = parseFloat(data.energia_enero);
            energia_febrero = parseFloat(data.energia_febrero);
            energia_marzo = parseFloat(data.energia_marzo);
            energia_abril = parseFloat(data.energia_abril);
            energia_mayo = parseFloat(data.energia_mayo);
            energia_junio = parseFloat(data.energia_junio);
            energia_julio = parseFloat(data.energia_julio);
            energia_agosto = parseFloat(data.energia_agosto);
            energia_septiembre = parseFloat(data.energia_septiembre);
            energia_octubre = parseFloat(data.energia_octubre);
            energia_noviembre = parseFloat(data.energia_noviembre);
            energia_diciembre = parseFloat(data.energia_diciembre);
    
            var energia_total = energia_enero + energia_febrero + energia_marzo + energia_abril + energia_mayo + energia_junio + energia_julio + energia_agosto + energia_septiembre + energia_octubre + energia_noviembre +energia_diciembre;
        
            var subtitulo = data.subtitulo + '<br>' + lang['año'] + ' ' + $("#anio ").val() + '<br>'+ '<b>'+ lang['Producción Total'] +': ' + new Intl.NumberFormat().format(energia_total) + ' CLP' + '</b>';
        
            series.push({
                name: lang['Potencia Transferida (CLP)'],
                data: [energia_enero, energia_febrero, energia_marzo, energia_abril, energia_mayo, energia_junio, energia_julio, energia_agosto, energia_septiembre, energia_octubre, energia_noviembre,energia_diciembre],
                color: 'green'
            }); 
            options.subtitle.text = subtitulo;        
            options.series = series; 
            chart = new Highcharts.Chart(options);         
        }
        }); 
    }
    
    function getOptionsPotenciaCLPMensual(){
        var options = {
            chart: {
            type: 'column',
            style: {
                fontFamily: 'sans-serif'
            },
            renderTo: 'produccionPotenciaCLPMensual'
            },
            title: {
                text: lang['Transferencia Mensual de Potencia (CLP)']
            },
            subtitle:{
                text: '',
            },
            xAxis: {
                categories: ['ENE', 'FEB', 'MAR', 'ABR', 'MAY', 'JUN', 'JUL', 'AGO', 'SEP', 'OCT', 'NOV', 'DIC'],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: lang['Potencia (CLP)']
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>$ {point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            }    
        }
        return options;
    }
    
    function AjaxServCompCLPMensual(){
        $.ajax({
        type: "POST",
        url: urlServCompCLPMensual,
        dataType: "json",
        data: {
            year : year,
            empresa: empresa
        },
        beforeSend: function(){
        
        },
        success: function (data) {
        
            var energia_enero = '';
            var energia_febrero = '';
            var energia_abril = '';
            var energia_mayo = '';
            var energia_junio = '';
            var energia_julio = '';
            var energia_agosto = '';
            var energia_septiembre = '';
            var energia_octubre = '';
            var energia_noviembre = '';
            var energia_diciembre = '';      
            var series = new Array();                                                
        
            //Highcharts
        
            //Configuracion Base
            var options = getOptionsServCompCLPMensual();  
        
            //Energia Inyectada
            energia_enero = parseFloat(data.energia_enero);
            energia_febrero = parseFloat(data.energia_febrero);
            energia_marzo = parseFloat(data.energia_marzo);
            energia_abril = parseFloat(data.energia_abril);
            energia_mayo = parseFloat(data.energia_mayo);
            energia_junio = parseFloat(data.energia_junio);
            energia_julio = parseFloat(data.energia_julio);
            energia_agosto = parseFloat(data.energia_agosto);
            energia_septiembre = parseFloat(data.energia_septiembre);
            energia_octubre = parseFloat(data.energia_octubre);
            energia_noviembre = parseFloat(data.energia_noviembre);
            energia_diciembre = parseFloat(data.energia_diciembre);
        
            var energia_total = energia_enero + energia_febrero + energia_marzo + energia_abril + energia_mayo + energia_junio + energia_julio + energia_agosto + energia_septiembre + energia_octubre + energia_noviembre +energia_diciembre;
        
            var subtitulo = data.subtitulo + '<br>'+ lang['año'] + ' ' + $("#anio").val() + '<br>'+ '<b>'+lang['Servicios Complementarios Total']+': ' + new Intl.NumberFormat().format(energia_total) + ' CLP' + '</b>';
    
            series.push({
                name: lang['Servicios Complementarios (CLP)'],
                data: [energia_enero, energia_febrero, energia_marzo, energia_abril, energia_mayo, energia_junio, energia_julio, energia_agosto, energia_septiembre, energia_octubre, energia_noviembre,energia_diciembre],
                color: 'red'
            });  
            options.subtitle.text = subtitulo;       
            options.series = series; 
            chart = new Highcharts.Chart(options);         
        }
        }); 
    }
    
    function getOptionsServCompCLPMensual(){
        var options = {
            chart: {
            type: 'column',
            style: {
                fontFamily: 'sans-serif'
            },
            renderTo: 'servCompCLPMensual'
            },
            navigation: {
                buttonOptions: {
                    verticalAlign: 'top',
                    y: 0
                }
            },
            title: {
                text: lang['Servicios Complementarios Mensual (CLP)']
            },
            subtitle:{
                text: '',
            },
            xAxis: {
                categories: ['ENE', 'FEB', 'MAR', 'ABR', 'MAY', 'JUN', 'JUL', 'AGO', 'SEP', 'OCT', 'NOV', 'DIC'],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: lang['Servicios Complementarios (CLP)']
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>$ {point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            }    
        }
        return options;
    }
    
    (function (H) {
        H.wrap(H.Renderer.prototype, 'label', function (proceed, str, x, y, shape, anchorX, anchorY, useHTML) {
            if(/class="fa/.test(str))   useHTML = true;
            // Run original proceed method
            return proceed.apply(this, [].slice.call(arguments, 1));
        });
    }(Highcharts));
});