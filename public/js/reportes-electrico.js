$(document).ready(function(){
    var empresa = $("#empresa_xls").val(); 
    var reporte = $('#id_reporte').val();
    var dateRange = $('#dateRangePicker').val();

        //On change sobre selects
        $("#empresa_xls").change(function(){    
            $('#id_reporte').val(0);         
            if($("#empresa_xls").val() == 332){
                //Piquero
                $('#id_reporte  option[value="1"]').removeAttr('disabled');
                $('#id_reporte  option[value="2"]').removeAttr('disabled');
                $('#id_reporte  option[value="3"]').removeAttr('disabled');
                $('#id_reporte  option[value="4"]').removeAttr('disabled');
                $('#id_reporte  option[value="5"]').removeAttr('disabled');
            }else if($("#empresa_xls").val() == 443){
                //Illalolen
                $('#id_reporte  option[value="1"]').removeAttr('disabled');
                $('#id_reporte  option[value="2"]').removeAttr('disabled');
                $('#id_reporte  option[value="3"]').attr('disabled','disabled');
                $('#id_reporte  option[value="4"]').removeAttr('disabled');
                $('#id_reporte  option[value="5"]').attr('disabled','disabled');
            }else if($("#empresa_xls").val() == 418 || $("#empresa_xls").val() == 419 || $("#empresa_xls").val() == 423){
                //Joaquin Solar Angela Solar Illapel 5
                $('#id_reporte  option[value="1"]').removeAttr('disabled');
                $('#id_reporte  option[value="2"]').attr('disabled','disabled')
                $('#id_reporte  option[value="3"]').attr('disabled','disabled');
                $('#id_reporte  option[value="4"]').attr('disabled','disabled');
                $('#id_reporte  option[value="5"]').attr('disabled','disabled');
            }if($("#empresa_xls").val() == 264 || $("#empresa_xls").val() == 263 || $("#empresa_xls").val() == 262 || $("#empresa_xls").val() == 396 || $("#empresa_xls").val() == 429 || $("#empresa_xls").val() == 406 || $("#empresa_xls").val() == 386){
                //Piquero
                $('#id_reporte  option[value="1"]').removeAttr('disabled');
                $('#id_reporte  option[value="2"]').attr('disabled','disabled')
                $('#id_reporte  option[value="3"]').attr('disabled','disabled');
                $('#id_reporte  option[value="4"]').attr('disabled','disabled');
                $('#id_reporte  option[value="5"]').attr('disabled','disabled');                
            }else{
                //Otros
                $('#id_reporte  option[value="1"]').removeAttr('disabled');
                $('#id_reporte  option[value="2"]').removeAttr('disabled');
                $('#id_reporte  option[value="3"]').removeAttr('disabled');
                $('#id_reporte  option[value="4"]').removeAttr('disabled');
                $('#id_reporte  option[value="5"]').attr('disabled','disabled');
            }
        });

    $("#generar_reporte").click(function(){
        var empresa = $("#empresa_xls").val(); 
        var reporte = $('#id_reporte').val();
        var dateRange = $('#dateRangePicker').val();

        AjaxReporte();
    });

    function AjaxReporte(){
        $.ajax({
        type: "POST",
        url: urlReporte,
        dataType: "json",
        data: {
            empresa: empresa,
            reporte : reporte,
            dateRange : dateRange,            
        },
        beforeSend: function(){
            $('.progress').show();
        },
        success: function (data) {
    
            $('.progress').hide();
        }
        }); 
    }
});