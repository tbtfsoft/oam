$(document).ready(function(){
    var empresa = $("#empresa_xls").val(); 
    var dateRange = $('#dateRangePicker').val();
    var mes = $('#mes').val();
    var anioMes = $('#anio-mes').val();
    var anio = $('#anio').val();
    var dt = new Date();
    var year = 1900 + dt.getYear();
    
    //Iniciamos los Ajax al cargar la pagina
    setTimeout(function(){ 
        AjaxEnergiaRange();
    }, 500);
    
    
    //On change sobre selects
    $("#empresa_xls").change(function(){
        empresa = $("#empresa_xls").val(); 
        dateRange = $('#dateRangePicker').val();
        mes = $('#mes').val();
        anioMes = $('#anio-mes').val();
        anio = $('#anio').val();
        
        setTimeout(function(){ 
            AjaxEnergiaRange();
        }, 500);
    
        setTimeout(function(){ 
            AjaxEnergiaMes();
        }, 1000);
    
        setTimeout(function(){ 
            AjaxEnergiaVSMes();
        }, 1500);
    
        setTimeout(function(){ 
            AjaxEnergiaAnio();
        }, 2000);
    
        setTimeout(function(){ 
            AjaxEnergiaVSAnio()
        }, 2500);
    
        setTimeout(function(){ 
            AjaxEnergiaCLPMensual();
        }, 3000);
    
        setTimeout(function(){ 
            AjaxPotenciaCLPMensual();
        }, 3500);
    
        setTimeout(function(){     
            AjaxServCompCLPMensual();      
        }, 4000);
    
        setTimeout(function(){ 
            AjaxEnergiaMensual(); 
        }, 45000);
    });
    
    //Dia select
    $("#dateRangePicker").change(function(){
        empresa = $("#empresa_xls").val(); 
        dateRange = $('#dateRangePicker').val();
        AjaxEnergiaRange();
    });
    
    //Mes select
    $("#mes,#anio-mes").change(function(){
        empresa = $("#empresa_xls").val(); 
        mes = $('#mes').val();
        anioMes = $('#anio-mes').val();
        setTimeout(function(){ 
            AjaxEnergiaMes();
        }, 500);
    
        setTimeout(function(){ 
            AjaxEnergiaVSMes();
        }, 1000);
    });
    
    //Anio select
    $("#anio").change(function(){
        empresa = $("#empresa_xls").val(); 
        anio = $('#anio').val();
        setTimeout(function(){ 
            AjaxEnergiaAnio();
        }, 500);
    
        setTimeout(function(){ 
            AjaxEnergiaVSAnio();
        }, 1000);
    
        setTimeout(function(){ 
            AjaxEnergiaCLPMensual();
        }, 1500);
        
        setTimeout(function(){ 
            AjaxPotenciaCLPMensual();
        }, 2000);
    
        //Balance de Transferencias Economicas CEN (Mes Cerrado)
        setTimeout(function(){     
            AjaxServCompCLPMensual();      
        }, 2500);
    
        setTimeout(function(){ 
            AjaxEnergiaMensual(); 
        }, 3000);
    });
    
    //On click sobre tabs
    //Dia Tab
    $("#dia-tab").click(function(){
        empresa = $("#empresa_xls").val(); 
        dateRange = $('#dateRangePicker').val();
        AjaxEnergiaRange();
    });
    //Mes Tab
    $("#mesTab-tab").click(function(){
        empresa = $("#empresa_xls").val(); 
        mes = $('#mes').val();
        anioMes = $('#anio-mes').val();
        setTimeout(function(){ 
            AjaxEnergiaMes();
        }, 500);
    
        setTimeout(function(){ 
            AjaxEnergiaVSMes();
        }, 1000);
    });
    
    //Anio Tab
    $("#anioTab-tab").click(function(){
        empresa = $("#empresa_xls").val(); 
        anio = $('#anio').val();
        setTimeout(function(){ 
            AjaxEnergiaAnio();
        }, 500);
    
        setTimeout(function(){ 
            AjaxEnergiaVSAnio();
        }, 1000);
    
        setTimeout(function(){ 
            AjaxEnergiaCLPMensual();
        }, 1500);
        
        setTimeout(function(){ 
            AjaxPotenciaCLPMensual();
        }, 2000);
    
        //Balance de Transferencias Economicas CEN (Mes Cerrado)
        setTimeout(function(){     
            AjaxServCompCLPMensual();      
        }, 2500);
    
        setTimeout(function(){ 
            AjaxEnergiaMensual(); 
        }, 3000);   
    });
    
    function AjaxEnergiaRange(){
        $.ajax({
        type: "POST",
        url: urlEnergiaRange,
        dataType: "json",
        data: {
            dateRange : dateRange,
            empresa: empresa
        },
        beforeSend: function(){
            $('.progress').show();
        },
        success: function (data) {
            var options = getOptionsEnergiaRange();
            var horas = data.horas;
            var generacion = data.generacion;
            var generacionCLP = data.generacionCLP;
            var consumo = data.consumo;
            var generacionTotal = data.generacionTotal;
            var generacionCLPTotal = data.generacionCLPTotal;
            var consumoTotal = data.consumoTotal;  
            var barra = data.barra;
            var modalidad = data.modalidad;
            var precio_energia = data.precio_energia;          
            var x = new Array();
            var series = new Array();
    
            $("#barra_span").html(barra);
            $("#modalidad_span").html(modalidad);
            $("#precio_energia_span").html(' ('+ precio_energia + ' $/kWh)');
    
            x.push({
                categories: horas,
            });
    
            series.push({
                name: lang['Generación kWh'],
                data: generacion,
                type: 'column',
                color: 'green',
                tooltip: {
                    valueSuffix: ' kWh'
                }            
            });                      
    
            series.push({
                name: lang['Generación CLP'],
                data: generacionCLP,
                type: 'spline',
                color: 'orange',
                yAxis: 1,
                tooltip: {
                    valueSuffix: ' $'
                }            
            }); 
    
            var subtitulo = data.subtitulo + '<br>'+ $("#dateRangePicker").val() + '<br>'+ '<b>'+ lang['Generación kWh Total'] +': ' + new Intl.NumberFormat().format(generacionTotal) + ' '+lang['kWh - Generacion CLP Total']+': ' + new Intl.NumberFormat().format(generacionCLPTotal) + ' CLP</b>';
    
            options.subtitle.text = subtitulo;
            options.xAxis = x;
            options.series = series;
            Highcharts.setOptions({
                time: {
                    timezone: 'America/Santiago'
                }
            });
    
            chart = new Highcharts.Chart(options);
    
            $('.progress').hide();
        }
        }); 
    }
    
    function getOptionsEnergiaRange(){
        var options = {
            chart: {
                backgroundColor: 'transparent',            
                renderTo: 'energiaRange',
                animation: Highcharts.svg, // don't animate in old IE
                zoomType: 'xy'
            },
            title: {
                text: lang['Energía Generada (kWh - CLP)'],
            },
            subtitle:{
                text: '',
            },
            xAxis: {
                categories: [],
                crosshair: true
            },
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value} kWh',
                },
                title: {
                    text: lang['Energía'],
                }
            }, { // Secondary yAxis
                title: {
                    text: 'CLP',
                },
                labels: {
                    format: '{value} $',
                },
                opposite: true
            }],
            legend: {
                enabled: true
            },
            exporting: {
                enabled: true
            },
            series: [{
                name: '',
                data: [],
            }]
        }
    
        return options;
    }
    
    function AjaxEnergiaMes(){
        $.ajax({
        type: "POST",
        url: urlEnergiaMes,
        dataType: "json",
        data: {
            empresa: empresa,
            mes : mes,
            anioMes : anioMes,        
        },
        beforeSend: function(){
            $('.progress').show();
        },
        success: function (data) {
            var options = getOptionsEnergiaMes();
            var horas = data.horas;
            var generacion = data.generacion;
            var generacionCLP = data.generacionCLP;
            var consumo = data.consumo;
            var generacionTotal = data.generacionTotal;
            var generacionCLPTotal = data.generacionCLPTotal;
            var consumoTotal = data.consumoTotal;   
            var barra = data.barra;
            var modalidad = data.modalidad;
            var precio_energia = data.precio_energia; 
            var initial_date = data.initial_date; 
            var final_date = data.final_date;                        
            var x = new Array();
            var series = new Array();
    
            $("#barra_span").html(barra);
            $("#modalidad_span").html(modalidad);
            $("#precio_energia_span").html(' ('+ precio_energia + ' $/kWh)');
    
            x.push({
                categories: horas,
            });
    
            series.push({
                name: lang['Generación kWh'],
                data: generacion,
                type: 'column',
                color: 'green',
                tooltip: {
                    valueSuffix: ' kWh'
                }            
            });                      
    
            series.push({
                name: lang['Generación CLP'],
                data: generacionCLP,
                type: 'spline',
                color: 'orange',
                yAxis: 1,
                tooltip: {
                    valueSuffix: ' $'
                }            
            }); 
    
            var subtitulo = data.subtitulo + '<br>'+ initial_date + ' - ' + final_date + '<br>'+ '<b>'+lang['Generación kWh Total']+': ' + new Intl.NumberFormat().format(generacionTotal) + ' '+lang['kWh - Generacion CLP Total']+': ' + new Intl.NumberFormat().format(generacionCLPTotal) + ' CLP</b>';
    
            options.subtitle.text = subtitulo;
            options.xAxis = x;
            options.series = series;
            Highcharts.setOptions({
                time: {
                    timezone: 'America/Santiago'
                }
            });
    
            chart = new Highcharts.Chart(options);
    
            $('.progress').hide();
        }
        }); 
    }
    
    function getOptionsEnergiaMes(){
        var options = {
            chart: {
                backgroundColor: 'transparent',            
                renderTo: 'energiaMes',
                animation: Highcharts.svg, // don't animate in old IE
                zoomType: 'xy'
            },
            title: {
                text: lang['Energía Generada (kWh - CLP)'],
            },
            subtitle:{
                text: '',
            },
            xAxis: {
                categories: [],
                crosshair: true
            },
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value} kWh',
                },
                title: {
                    text: lang['Energía'],
                }
            }, { // Secondary yAxis
                title: {
                    text: 'CLP',
                },
                labels: {
                    format: '{value} $',
                },
                opposite: true
            }],
            legend: {
                enabled: true
            },
            exporting: {
                enabled: true
            },
            series: [{
                name: '',
                data: [],
            }]
        }
    
        return options;
    }
    
    function AjaxEnergiaVSMes(){
        $.ajax({
        type: "POST",
        url: urlEnergiaVSMes,
        dataType: "json",
        data: {
            empresa: empresa,
            mes : mes,
            anioMes : anioMes,        
        },
        beforeSend: function(){
            $('.progress').show();
        },
        success: function (data) {
            var options = getOptionsEnergiaVSMes();
            var horas = data.horas;
            var horasVS1 = data.horasVS1;
            var generacion = data.generacion;
            var generacionTotal = data.generacionTotal;
            var generacionVS1 = data.generacionVS1; 
            var generacionVS1Total = data.generacionVS1Total; 
            var barra = data.barra;
            var modalidad = data.modalidad;
            var precio_energia = data.precio_energia;                   
            var x = new Array();
            var series = new Array();
    
            var year_act = $('#anio-mes').val();
    
            x.push({
                categories: horas,
            });
    
            var serie_1_name =  lang['Generación'] +' ' + year_act
            series.push({
                name: serie_1_name,
                data: generacion,
                color: 'green',
                tooltip: {
                    valueSuffix: ' kWh'
                }            
            });    
            
            var serie_2_name =  lang['Generación'] +' ' + (year_act - 1)
            series.push({
                name: serie_2_name,
                data: generacionVS1,
                color: 'blue',
                tooltip: {
                    valueSuffix: ' kWh'
                }            
            });  
    
            var subtitulo = data.subtitulo + '<br>'+ '<b>'+lang['Generación']+' ' + year_act + ' '+lang['Total']+': ' + new Intl.NumberFormat().format(generacionTotal) + ' '+lang['kWh - Generacion']+' ' + (year_act - 1) + ' '+lang['Total']+': ' + new Intl.NumberFormat().format(generacionVS1Total) + ' kWh</b>';
    
            options.subtitle.text = subtitulo;
            options.xAxis = x;
            options.series = series;
            Highcharts.setOptions({
                time: {
                    timezone: 'America/Santiago'
                }
            });
    
            chart = new Highcharts.Chart(options);
    
            $('.progress').hide();
        }
        }); 
    }
    
    function getOptionsEnergiaVSMes(){
        var options = {
            chart: {
                backgroundColor: 'transparent',
                type: 'spline',
                renderTo: 'energiaVSMes',
                animation: Highcharts.svg, // don't animate in old IE
            },
            title: {
                text: lang['Energía Generada Acumulada (kWh)'],
            },
            subtitle:{
                text: '',
            },
            xAxis: {
                type: 'datetime',
                tickPixelInterval: 5
            },
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value} kWh',
                },
                title: {
                    text: lang['Energía'],
                }
            }],
            plotOptions: {
                spline: {
                    marker: {
                        radius: 0,
                        lineWidth: 0
                    }
                }
            },
            legend: {
                enabled: true
            },
            exporting: {
                enabled: true
            },
            series: [{
                name: '',
                data: [],
            }]
        }
    
        return options;
    }
    
    function AjaxEnergiaAnio(){
        $.ajax({
        type: "POST",
        url: urlEnergiaAnio,
        dataType: "json",
        data: {
            empresa: empresa,
            mes : mes,
            anio : anio,        
        },
        beforeSend: function(){
            $('.progress').show();
        },
        success: function (data) {
            var options = getOptionsEnergiaAnio();
            var horas = data.horas;
            var generacion = data.generacion;
            var generacionCLP = data.generacionCLP;
            var consumo = data.consumo;
            var generacionTotal = data.generacionTotal;
            var generacionCLPTotal = data.generacionCLPTotal;
            var consumoTotal = data.consumoTotal;   
            var barra = data.barra;
            var modalidad = data.modalidad;
            var precio_energia = data.precio_energia; 
            var initial_date = data.initial_date; 
            var final_date = data.final_date;                       
            var x = new Array();
            var series = new Array();
    
            // $("#barra_span").html(barra);
            // $("#modalidad_span").html(modalidad);
            // $("#precio_energia_span").html(' ('+ precio_energia + ' $/kWh)');
    
            x.push({
                categories: horas,
            });
    
            series.push({
                name: lang['Generación kWh'],
                data: generacion,
                type: 'column',
                color: 'green',
                tooltip: {
                    valueSuffix: ' kWh'
                }            
            });                      
    
            series.push({
                name: lang['Generación CLP'],
                data: generacionCLP,
                type: 'spline',
                color: 'orange',
                yAxis: 1,
                tooltip: {
                    valueSuffix: ' $'
                }            
            }); 
    
            var subtitulo = data.subtitulo + '<br>'+ initial_date + ' - ' + final_date + '<br>'+ '<b>'+lang['Generación kWh Total']+': ' + new Intl.NumberFormat().format(generacionTotal) + ' '+lang['kWh - Generación CLP Total']+': ' + new Intl.NumberFormat().format(generacionCLPTotal) + ' CLP</b>';
    
            options.subtitle.text = subtitulo;
            options.xAxis = x;
            options.series = series;
            Highcharts.setOptions({
                time: {
                    timezone: 'America/Santiago'
                }
            });
    
            chart = new Highcharts.Chart(options);
    
            $('.progress').hide();
        }
        }); 
    }
    
    function getOptionsEnergiaAnio(){
        var options = {
            chart: {
                backgroundColor: 'transparent',            
                renderTo: 'energiaAnio',
                animation: Highcharts.svg, // don't animate in old IE
                zoomType: 'xy'
            },
            title: {
                text: lang['Energía Generada (kWh - CLP)'],
            },
            subtitle:{
                text: '',
            },
            xAxis: {
                categories: [],
                crosshair: true
            },
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value} kWh',
                },
                title: {
                    text: lang['Energía'],
                }
            }, { // Secondary yAxis
                title: {
                    text: 'CLP',
                },
                labels: {
                    format: '{value} $',
                },
                opposite: true
            }],
            legend: {
                enabled: true
            },
            exporting: {
                enabled: true
            },
            series: [{
                name: '',
                data: [],
            }]
        }
    
        return options;
    }
    
    function AjaxEnergiaVSAnio(){
        $.ajax({
        type: "POST",
        url: urlEnergiaVSAnio,
        dataType: "json",
        data: {
            empresa: empresa,
            mes : mes,
            anio : anio,               
        },
        beforeSend: function(){
            $('.progress').show();
        },
        success: function (data) {
            var options = getOptionsEnergiaVSAnio();
            var horas = data.horas;
            var horasVS1 = data.horasVS1;
            var generacion = data.generacion;
            var generacionTotal = data.generacionTotal;
            var generacionVS1 = data.generacionVS1; 
            var generacionVS1Total = data.generacionVS1Total; 
            var barra = data.barra;
            var modalidad = data.modalidad;
            var precio_energia = data.precio_energia;                   
            var x = new Array();
            var series = new Array();
    
            var year_act = $('#anio').val();
    
            x.push({
                categories: horas,
            });
    
            var serie_1_name =  lang['Generación']+' ' + year_act
            series.push({
                name: serie_1_name,
                data: generacion,
                color: 'green',
                tooltip: {
                    valueSuffix: ' kWh'
                }            
            });    
            
            var serie_2_name =  lang['Generación']+' ' + (year_act - 1)
            series.push({
                name: serie_2_name,
                data: generacionVS1,
                color: 'blue',
                tooltip: {
                    valueSuffix: ' kWh'
                }            
            });  
    
            var subtitulo = data.subtitulo + '<br>'+ '<b>'+lang['Generación']+' ' + year_act + ' '+lang['Total']+': ' + new Intl.NumberFormat().format(generacionTotal) + ' '+lang['kWh - Generacion']+' ' + (year_act - 1) + ' '+lang['Total']+': ' + new Intl.NumberFormat().format(generacionVS1Total) + ' kWh</b>';
    
            options.subtitle.text = subtitulo;
            options.xAxis = x;
            options.series = series;
            Highcharts.setOptions({
                time: {
                    timezone: 'America/Santiago'
                }
            });
    
            chart = new Highcharts.Chart(options);
    
            $('.progress').hide();
        }
        }); 
    }
    
    function getOptionsEnergiaVSAnio(){
        var options = {
            chart: {
                backgroundColor: 'transparent',
                type: 'spline',
                renderTo: 'energiaVSAnio',
                animation: Highcharts.svg, // don't animate in old IE
            },
            title: {
                text: lang['Energía Generada Acumulada (kWh)'],
            },
            subtitle:{
                text: '',
            },
            xAxis: {
                type: 'datetime',
                tickPixelInterval: 5
            },
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value} kWh',
                },
                title: {
                    text: lang['Energía'],
                }
            }],
            plotOptions: {
                spline: {
                    marker: {
                        radius: 0,
                        lineWidth: 0
                    }
                }
            },
            legend: {
                enabled: true
            },
            exporting: {
                enabled: true
            },
            series: [{
                name: '',
                data: [],
            }]
        }
    
        return options;
    }
    
    //Balance de Transferencias Economicas CEN (Mes Cerrado)
    
    function AjaxEnergiaMensual(){
        $.ajax({
        type: "POST",
        url: urlEnergiaMensual,
        dataType: "json",
        data: {
            year : anio,
            empresa: empresa
        },
        beforeSend: function(){
            $('.progress').show();
        },
        success: function (data) {
        
            var energia_enero = '';
            var energia_febrero = '';
            var energia_abril = '';
            var energia_mayo = '';
            var energia_junio = '';
            var energia_julio = '';
            var energia_agosto = '';
            var energia_septiembre = '';
            var energia_octubre = '';
            var energia_noviembre = '';
            var energia_diciembre = '';      
            var series = new Array();                                                
        
            //Highcharts
        
            //Configuracion Base
            var options = getOptionsEnergiaMensual();  
        
            //Energia Inyectada
            energia_enero = parseFloat(data.energia_enero_in);
            energia_febrero = parseFloat(data.energia_febrero_in);
            energia_marzo = parseFloat(data.energia_marzo_in);
            energia_abril = parseFloat(data.energia_abril_in);
            energia_mayo = parseFloat(data.energia_mayo_in);
            energia_junio = parseFloat(data.energia_junio_in);
            energia_julio = parseFloat(data.energia_julio_in);
            energia_agosto = parseFloat(data.energia_agosto_in);
            energia_septiembre = parseFloat(data.energia_septiembre_in);
            energia_octubre = parseFloat(data.energia_octubre_in);
            energia_noviembre = parseFloat(data.energia_noviembre_in);
            energia_diciembre = parseFloat(data.energia_diciembre_in); 
    
            var energia_in_total = energia_enero + energia_febrero + energia_marzo + energia_abril + energia_mayo + energia_junio + energia_julio + energia_agosto + energia_septiembre + energia_octubre + energia_noviembre +energia_diciembre;
        
            series.push({
                name: lang['Energía Inyectada (kWh)'],
                data: [energia_enero, energia_febrero, energia_marzo, energia_abril, energia_mayo, energia_junio, energia_julio, energia_agosto, energia_septiembre, energia_octubre, energia_noviembre,energia_diciembre],
                color: 'green'
            });
        
            //Energia Consumida
            energia_enero = Math.abs(parseFloat(data.energia_enero_out));
            energia_febrero = Math.abs(parseFloat(data.energia_febrero_out));
            energia_marzo = Math.abs(parseFloat(data.energia_marzo_out));
            energia_abril = Math.abs(parseFloat(data.energia_abril_out));
            energia_mayo = Math.abs(parseFloat(data.energia_mayo_out));
            energia_junio = Math.abs(parseFloat(data.energia_junio_out));
            energia_julio = Math.abs(parseFloat(data.energia_julio_out));
            energia_agosto = Math.abs(parseFloat(data.energia_agosto_out));
            energia_septiembre = Math.abs(parseFloat(data.energia_septiembre_out));
            energia_octubre = Math.abs(parseFloat(data.energia_octubre_out));
            energia_noviembre = Math.abs(parseFloat(data.energia_noviembre_out));
            energia_diciembre = Math.abs(parseFloat(data.energia_diciembre_out));
    
            var energia_out_total = energia_enero + energia_febrero + energia_marzo + energia_abril + energia_mayo + energia_junio + energia_julio + energia_agosto + energia_septiembre + energia_octubre + energia_noviembre +energia_diciembre;
    
            series.push({
                name: lang['Energía Consumida (kWh)'],
                data: [energia_enero, energia_febrero, energia_marzo, energia_abril, energia_mayo, energia_junio, energia_julio, energia_agosto, energia_septiembre, energia_octubre, energia_noviembre,energia_diciembre],
                color: 'red'
            });         
    
            var subtitulo = data.subtitulo + '<br>'+ $("#anio").val() + '<br>'+ '<b>'+lang['Generación Total']+': ' + new Intl.NumberFormat().format(energia_in_total) + ' '+lang['kWh - ConsumoTotal']+': ' + new Intl.NumberFormat().format(energia_out_total) + ' kWh</b>';
            options.subtitle.text = subtitulo;
            options.series = series; 
            chart = new Highcharts.Chart(options);      
            
            $('.progress').hide();
        }
        }); 
    }
    
    function getOptionsEnergiaMensual(){
        var options = {
            chart: {
            type: 'column',
            style: {
                fontFamily: 'sans-serif'
            },
            renderTo: 'produccionEnergiMensual'
            },
            title: {
                text: lang['Producción y Consumo Mensual de Energía (kWh)']
            },
            subtitle:{
                text: '',
            },        
            xAxis: {
                categories: ['ENE', 'FEB', 'MAR', 'ABR', 'MAY', 'JUN', 'JUL', 'AGO', 'SEP', 'OCT', 'NOV', 'DIC'],
                crosshair: true
            },
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 2000
                    },
                    chartOptions: {
                        xAxis: {
                        }
                    }
                }]
            },
            yAxis: {
                min: 0,
                title: {
                    text: lang['Energía (kWh)']
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y} kWh</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            }    
        }
        return options;
    }
    
    function AjaxEnergiaCLPMensual(){
        $.ajax({
        type: "POST",
        url: urlEnergiaCLPMensual,
        dataType: "json",
        data: {
            year : anio,
            empresa: empresa
        },
        beforeSend: function(){
            $('.progress').show();
        },
        success: function (data) {
        
            var energia_enero = '';
            var energia_febrero = '';
            var energia_abril = '';
            var energia_mayo = '';
            var energia_junio = '';
            var energia_julio = '';
            var energia_agosto = '';
            var energia_septiembre = '';
            var energia_octubre = '';
            var energia_noviembre = '';
            var energia_diciembre = '';      
            var series = new Array();                                                
        
            //Highcharts
        
            //Configuracion Base
            var options = getOptionsEnergiaCLPMensual();  
        
            //Energia Inyectada
            energia_enero = parseFloat(data.energia_enero);
            energia_febrero = parseFloat(data.energia_febrero);
            energia_marzo = parseFloat(data.energia_marzo);
            energia_abril = parseFloat(data.energia_abril);
            energia_mayo = parseFloat(data.energia_mayo);
            energia_junio = parseFloat(data.energia_junio);
            energia_julio = parseFloat(data.energia_julio);
            energia_agosto = parseFloat(data.energia_agosto);
            energia_septiembre = parseFloat(data.energia_septiembre);
            energia_octubre = parseFloat(data.energia_octubre);
            energia_noviembre = parseFloat(data.energia_noviembre);
            energia_diciembre = parseFloat(data.energia_diciembre);
    
            var energia_total = energia_enero + energia_febrero + energia_marzo + energia_abril + energia_mayo + energia_junio + energia_julio + energia_agosto + energia_septiembre + energia_octubre + energia_noviembre +energia_diciembre;
        
            var subtitulo = data.subtitulo + '<br>'+ $("#anio ").val() + '<br>'+ '<b>'+lang['Producción Total']+': ' + new Intl.NumberFormat().format(energia_total) + ' CLP' + '</b>';
    
            series.push({
                name: lang['Energía Producida (CLP)'],
                data: [energia_enero, energia_febrero, energia_marzo, energia_abril, energia_mayo, energia_junio, energia_julio, energia_agosto, energia_septiembre, energia_octubre, energia_noviembre,energia_diciembre],
                color: 'green'
            });         
            options.subtitle.text = subtitulo;
            options.series = series; 
            chart = new Highcharts.Chart(options);         
        }
        }); 
    }
    
    function getOptionsEnergiaCLPMensual(){
        var options = {
            chart: {
            type: 'column',
            style: {
                fontFamily: 'sans-serif'
            },
            renderTo: 'produccionEnergiaCLPMensual'
            },
            title: {
                text: lang['Transferencia Mensual de Energía (CLP)']
            },
            subtitle:{
                text: '',
            },
            xAxis: {
                categories: ['ENE', 'FEB', 'MAR', 'ABR', 'MAY', 'JUN', 'JUL', 'AGO', 'SEP', 'OCT', 'NOV', 'DIC'],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: lang['Energía (CLP)']
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>$ {point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            }    
        }
        return options;
    }
    
    function AjaxPotenciaCLPMensual(){
        $.ajax({
        type: "POST",
        url: urlPotenciaCLPMensual,
        dataType: "json",
        data: {
            year : anio,
            empresa: empresa
        },
        beforeSend: function(){
        
        },
        success: function (data) {
        
            var energia_enero = '';
            var energia_febrero = '';
            var energia_abril = '';
            var energia_mayo = '';
            var energia_junio = '';
            var energia_julio = '';
            var energia_agosto = '';
            var energia_septiembre = '';
            var energia_octubre = '';
            var energia_noviembre = '';
            var energia_diciembre = '';      
            var series = new Array();                                                
        
            //Highcharts
        
            //Configuracion Base
            var options = getOptionsPotenciaCLPMensual();  
        
            //Energia Inyectada
            energia_enero = parseFloat(data.energia_enero);
            energia_febrero = parseFloat(data.energia_febrero);
            energia_marzo = parseFloat(data.energia_marzo);
            energia_abril = parseFloat(data.energia_abril);
            energia_mayo = parseFloat(data.energia_mayo);
            energia_junio = parseFloat(data.energia_junio);
            energia_julio = parseFloat(data.energia_julio);
            energia_agosto = parseFloat(data.energia_agosto);
            energia_septiembre = parseFloat(data.energia_septiembre);
            energia_octubre = parseFloat(data.energia_octubre);
            energia_noviembre = parseFloat(data.energia_noviembre);
            energia_diciembre = parseFloat(data.energia_diciembre);
    
            var energia_total = energia_enero + energia_febrero + energia_marzo + energia_abril + energia_mayo + energia_junio + energia_julio + energia_agosto + energia_septiembre + energia_octubre + energia_noviembre +energia_diciembre;
        
            var subtitulo = data.subtitulo + '<br>'+ $("#anio ").val() + '<br>'+ '<b>'+lang['Producción Total']+': ' + new Intl.NumberFormat().format(energia_total) + ' CLP' + '</b>';
        
            series.push({
                name: lang['Potencia Transferida (CLP)'],
                data: [energia_enero, energia_febrero, energia_marzo, energia_abril, energia_mayo, energia_junio, energia_julio, energia_agosto, energia_septiembre, energia_octubre, energia_noviembre,energia_diciembre],
                color: 'green'
            }); 
            options.subtitle.text = subtitulo;        
            options.series = series; 
            chart = new Highcharts.Chart(options);         
        }
        }); 
    }
    
    function getOptionsPotenciaCLPMensual(){
        var options = {
            chart: {
            type: 'column',
            style: {
                fontFamily: 'sans-serif'
            },
            renderTo: 'produccionPotenciaCLPMensual'
            },
            title: {
                text: lang['Transferencia Mensual de Potencia (CLP)']
            },
            subtitle:{
                text: '',
            },
            xAxis: {
                categories: ['ENE', 'FEB', 'MAR', 'ABR', 'MAY', 'JUN', 'JUL', 'AGO', 'SEP', 'OCT', 'NOV', 'DIC'],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: lang['Potencia (CLP)']
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>$ {point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            }    
        }
        return options;
    }
    
    function AjaxServCompCLPMensual(){
        $.ajax({
        type: "POST",
        url: urlServCompCLPMensual,
        dataType: "json",
        data: {
            year : anio,
            empresa: empresa
        },
        beforeSend: function(){
        
        },
        success: function (data) {
        
            var energia_enero = '';
            var energia_febrero = '';
            var energia_abril = '';
            var energia_mayo = '';
            var energia_junio = '';
            var energia_julio = '';
            var energia_agosto = '';
            var energia_septiembre = '';
            var energia_octubre = '';
            var energia_noviembre = '';
            var energia_diciembre = '';      
            var series = new Array();                                                
        
            //Highcharts
        
            //Configuracion Base
            var options = getOptionsServCompCLPMensual();  
        
            //Energia Inyectada
            energia_enero = parseFloat(data.energia_enero);
            energia_febrero = parseFloat(data.energia_febrero);
            energia_marzo = parseFloat(data.energia_marzo);
            energia_abril = parseFloat(data.energia_abril);
            energia_mayo = parseFloat(data.energia_mayo);
            energia_junio = parseFloat(data.energia_junio);
            energia_julio = parseFloat(data.energia_julio);
            energia_agosto = parseFloat(data.energia_agosto);
            energia_septiembre = parseFloat(data.energia_septiembre);
            energia_octubre = parseFloat(data.energia_octubre);
            energia_noviembre = parseFloat(data.energia_noviembre);
            energia_diciembre = parseFloat(data.energia_diciembre);
        
            var energia_total = energia_enero + energia_febrero + energia_marzo + energia_abril + energia_mayo + energia_junio + energia_julio + energia_agosto + energia_septiembre + energia_octubre + energia_noviembre +energia_diciembre;
        
            var subtitulo = data.subtitulo + '<br>'+ $("#anio ").val() + '<br>'+ '<b>'+lang['Servicios Complementarios Total']+': ' + new Intl.NumberFormat().format(energia_total) + ' CLP' + '</b>';
    
            series.push({
                name: lang['Servicios Complementarios (CLP)'],
                data: [energia_enero, energia_febrero, energia_marzo, energia_abril, energia_mayo, energia_junio, energia_julio, energia_agosto, energia_septiembre, energia_octubre, energia_noviembre,energia_diciembre],
                color: 'red'
            });  
            options.subtitle.text = subtitulo;       
            options.series = series; 
            chart = new Highcharts.Chart(options);         
        }
        }); 
    }
    
    function getOptionsServCompCLPMensual(){
        var options = {
            chart: {
            type: 'column',
            style: {
                fontFamily: 'sans-serif'
            },
            renderTo: 'servCompCLPMensual'
            },
            navigation: {
                buttonOptions: {
                    verticalAlign: 'top',
                    y: 0
                }
            },
            title: {
                text: lang['Servicios Complementarios Mensual (CLP)']
            },
            subtitle:{
                text: '',
            },
            xAxis: {
                categories: ['ENE', 'FEB', 'MAR', 'ABR', 'MAY', 'JUN', 'JUL', 'AGO', 'SEP', 'OCT', 'NOV', 'DIC'],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: lang['Servicios Complementarios (CLP)']
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>$ {point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            }    
        }
        return options;
    }
    
    (function (H) {
        H.wrap(H.Renderer.prototype, 'label', function (proceed, str, x, y, shape, anchorX, anchorY, useHTML) {
            if(/class="fa/.test(str))   useHTML = true;
            // Run original proceed method
            return proceed.apply(this, [].slice.call(arguments, 1));
        });
    }(Highcharts));
});