function checkRut(cadena2) {
    var cadena = cadena2; 
    if(cadena.indexOf('-') == -1){
        return false;
    }else{
        var array_rut = cadena.split('-');
        var rut = array_rut[0];
        rut += '';
        rut = rut.replace(/[^0-9]/g, '');
        var verificador = array_rut[1];
        verificador = verificador.toUpperCase();
        var array_rut = new Array();
        var array_multiplica = new Array();

        var valor = 9;

        for(var i=0;i<rut.length;i++){
            array_rut[i] = rut[i];
            array_multiplica[i] = valor;
            valor--;

            valor= (valor<4)?9:valor;
        }

        array_rut = array_rut.reverse();
        var suma = 0;
        for (var i=0;i<array_rut.length;i++){
            suma +=(array_rut[i]*array_multiplica[i]);
        }

        var verificador_real = suma%11;
        verificador_real = (verificador_real==10)?'K':verificador_real;

        if (verificador_real != verificador){
            return false;
        }
        else{
            return true;
        }
    }
}

function number_format(numero, derutmal) {

    numero += ''; // por si pasan un numero en vez de un string
    numero = parseFloat(numero.replace(/[^0-9]/g, '')); // elimino cualquier cosa que no sea numero

    derutmal = derutmal || 0; // por si la variable no fue fue pasada

    // si no es un numero o es igual a cero retorno el mismo cero
    if (isNaN(numero) || numero === 0)
        return parseFloat(0).toFixed(derutmal);

    // si es mayor o menor que cero retorno el valor formateado como numero
    numero = '' + numero.toFixed(derutmal);

    var numero_parts = numero.split('.'),
        regexp = /(\d+)(\d{3})/;

    while (regexp.test(numero_parts[0]))
        numero_parts[0] = numero_parts[0].replace(regexp, '$1' + '.' + '$2');

    return numero_parts.join('.');

}
