$(document).ready(function(){
    var empresa = $("#empresa_xls").val(); 
    var dateRange = $('#dateRangePicker').val();
    var mes = $('#mes').val();
    var anioMes = $('#anio-mes').val();
    var anio = $('#anio').val();
    
    //Iniciamos los Ajax al cargar la pagina
    setTimeout(function(){ 
        AjaxEnergiaRange();
    }, 500);
    
    setTimeout(function(){ 
        AjaxPotenciaRange();
    }, 1000);
    
    setTimeout(function(){ 
        AjaxVoltajesRange();
    }, 1500);
    
    setTimeout(function(){ 
        AjaxInversoresRange();
    }, 2000);
    
    //On change sobre selects
    $("#empresa_xls").change(function(){
        empresa = $("#empresa_xls").val(); 
        dateRange = $('#dateRangePicker').val();
        mes = $('#mes').val();
        anioMes = $('#anio-mes').val();
        anio = $('#anio').val();
        setTimeout(function(){ 
            AjaxEnergiaRange();
        }, 500);
        
        setTimeout(function(){ 
            AjaxPotenciaRange();
        }, 1000);
        
        setTimeout(function(){ 
            AjaxVoltajesRange();
        }, 1500);
        
        setTimeout(function(){ 
            AjaxInversoresRange();
        }, 2000);
    
        setTimeout(function(){ 
            AjaxEnergiaMes();
        }, 2500);
    
        setTimeout(function(){ 
            AjaxEnergiaAnio();
        }, 3000);
    });
    
    //Dia select
    $("#dateRangePicker").change(function(){
        empresa = $("#empresa_xls").val(); 
        dateRange = $('#dateRangePicker').val();
        setTimeout(function(){ 
            AjaxEnergiaRange();
        }, 500);
        
        setTimeout(function(){ 
            AjaxPotenciaRange();
        }, 1000);
        
        setTimeout(function(){ 
            AjaxVoltajesRange();
        }, 1500);
        
        setTimeout(function(){ 
            AjaxInversoresRange();
        }, 2000);
    });
    
    //Mes select
    $("#mes,#anio-mes").change(function(){
        empresa = $("#empresa_xls").val(); 
        mes = $('#mes').val();
        anioMes = $('#anio-mes').val();
        setTimeout(function(){ 
            AjaxEnergiaMes();
        }, 500);
    });
    
    //Anio select
    $("#anio").change(function(){
        empresa = $("#empresa_xls").val(); 
        anio = $('#anio').val();
        setTimeout(function(){ 
            AjaxEnergiaAnio();
        }, 500);
    });
    
    //On click sobre tabs
    //Dia Tab
    $("#dia-tab").click(function(){
        empresa = $("#empresa_xls").val(); 
        dateRange = $('#dateRangePicker').val();
        setTimeout(function(){ 
            AjaxEnergiaRange();
        }, 500);
        
        setTimeout(function(){ 
            AjaxPotenciaRange();
        }, 1000);
        
        setTimeout(function(){ 
            AjaxVoltajesRange();
        }, 1500);
        
        setTimeout(function(){ 
            AjaxInversoresRange();
        }, 2000);
    });
    //Mes Tab
    $("#mesTab-tab").click(function(){
        empresa = $("#empresa_xls").val(); 
        mes = $('#mes').val();
        anioMes = $('#anio-mes').val();
        setTimeout(function(){ 
            AjaxEnergiaMes();
        }, 500);
    });
    
    //Anio Tab
    $("#anioTab-tab").click(function(){
        empresa = $("#empresa_xls").val(); 
        anio = $('#anio').val();
        setTimeout(function(){ 
            AjaxEnergiaAnio();
        }, 500);
    });
    
    
    //Energia Ajax Start
    
    //Energia Range
    function AjaxEnergiaRange(){
        $.ajax({
        type: "POST",
        url: urlEnergiaRange,
        dataType: "json",
        data: {
            dateRange : dateRange,
            empresa: empresa
        },
        beforeSend: function(){
            $('.progress').show();
        },
        success: function (data) {
            var options = getOptionsEnergiaRange();
            var horas = data.horas;
            var generacion = data.generacion;
            var generacionCLP = data.generacionCLP;
            var consumo = data.consumo;
            var generacionTotal = data.generacionTotal;
            var generacionCLPTotal = data.generacionCLPTotal;
            var consumoTotal = data.consumoTotal;  
            var barra = data.barra;
            var modalidad = data.modalidad;
            var precio_energia = data.precio_energia;          
            var x = new Array();
            var series = new Array();
    
            $("#barra_span").html(barra);
            $("#modalidad_span").html(modalidad);
            $("#precio_energia_span").html(' ('+ precio_energia + ' $/kWh)');
    
            x.push({
                categories: horas,
            });
    
            series.push({
                name: lang['Generación kWh'],
                data: generacion,
                type: 'column',
                color: 'green',
                tooltip: {
                    valueSuffix: ' kWh'
                }            
            });
            
            series.push({
                name: lang['Consumo kWh'],
                data: consumo,
                type: 'column',
                color: 'red',
                tooltip: {
                    valueSuffix: ' kWh'
                }            
            });
    
            var subtitulo = data.subtitulo + '<br>'+ $("#dateRangePicker").val() + '<br>'+ '<b>'+lang['Generación Total']+': ' + new Intl.NumberFormat().format(generacionTotal) + ' kWh - ' + lang['Consumo Total'] +': ' + new Intl.NumberFormat().format(consumoTotal) + ' kWh</b>';
    
            options.subtitle.text = subtitulo;
            options.xAxis = x;
            options.series = series;
            Highcharts.setOptions({
                time: {
                    timezone: 'America/Santiago'
                }
            });
    
            chart = new Highcharts.Chart(options);
    
            $('.progress').hide();
        }
        }); 
    }
    
    function getOptionsEnergiaRange(){
        var options = {
            chart: {
                backgroundColor: 'transparent',            
                renderTo: 'energiaRange',
                animation: Highcharts.svg, // don't animate in old IE
                zoomType: 'xy'
            },
            title: {
                text: lang['Energía Generada y Consumida (kWh)'],
            },
            subtitle:{
                text: '',
            },
            xAxis: {
                categories: [],
                crosshair: true
            },
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value} kWh',
                },
                title: {
                    text: lang['Energía'],
                }
            }, { // Secondary yAxis
                title: {
                    text: '',
                },
                labels: {
                    format: '{value} $',
                },
                opposite: true
            }],
            legend: {
                enabled: true
            },
            exporting: {
                enabled: true
            },
            series: [{
                name: '',
                data: [],
            }]
        }
    
        return options;
    }
    
    //Energia Mes
    function AjaxEnergiaMes(){
        $.ajax({
        type: "POST",
        url: urlEnergiaMes,
        dataType: "json",
        data: {
            empresa: empresa,
            mes : mes,
            anioMes : anioMes,        
        },
        beforeSend: function(){
            $('.progress').show();
        },
        success: function (data) {
            var options = getOptionsEnergiaMes();
            var horas = data.horas;
            var generacion = data.generacion;
            var generacionCLP = data.generacionCLP;
            var consumo = data.consumo;
            var generacionTotal = data.generacionTotal;
            var generacionCLPTotal = data.generacionCLPTotal;
            var consumoTotal = data.consumoTotal;   
            var barra = data.barra;
            var modalidad = data.modalidad;
            var precio_energia = data.precio_energia; 
            var initial_date = data.initial_date; 
            var final_date = data.final_date;                        
            var x = new Array();
            var series = new Array();
    
            $("#barra_span").html(barra);
            $("#modalidad_span").html(modalidad);
            $("#precio_energia_span").html(' ('+ precio_energia + ' $/kWh)');
    
            x.push({
                categories: horas,
            });
    
            series.push({
                name: lang['Generación kWh'],
                data: generacion,
                type: 'column',
                color: 'green',
                tooltip: {
                    valueSuffix: ' kWh'
                }            
            }); 
            
            series.push({
                name: lang['Consumo kWh'],
                data: consumo,
                type: 'column',
                color: 'red',
                tooltip: {
                    valueSuffix: ' kWh'
                }            
            });
    
            var subtitulo = data.subtitulo + '<br>'+ initial_date + ' - ' + final_date + '<br>'+ '<b>'+lang['Generación Total']+': ' + new Intl.NumberFormat().format(generacionTotal) + ' kWh - ' + lang['Consumo Total']+': ' + new Intl.NumberFormat().format(consumoTotal) + ' kWh</b>';
    
            options.subtitle.text = subtitulo;
            options.xAxis = x;
            options.series = series;
            Highcharts.setOptions({
                time: {
                    timezone: 'America/Santiago'
                }
            });
    
            chart = new Highcharts.Chart(options);
    
            $('.progress').hide();
        }
        }); 
    }
    
    function getOptionsEnergiaMes(){
        var options = {
            chart: {
                backgroundColor: 'transparent',            
                renderTo: 'energiaMes',
                animation: Highcharts.svg, // don't animate in old IE
                zoomType: 'xy'
            },
            title: {
                text: lang['Energía Generada (kWh)'],
            },
            subtitle:{
                text: '',
            },
            xAxis: {
                categories: [],
                crosshair: true
            },
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value} kWh',
                },
                title: {
                    text: lang['Energía'],
                }
            }, { // Secondary yAxis
                title: {
                    text: '',
                },
                labels: {
                    format: '{value} $',
                },
                opposite: true
            }],
            legend: {
                enabled: true
            },
            exporting: {
                enabled: true
            },
            series: [{
                name: '',
                data: [],
            }]
        }
    
        return options;
    }
    
    //Energia Anio
    function AjaxEnergiaAnio(){
        $.ajax({
        type: "POST",
        url: urlEnergiaAnio,
        dataType: "json",
        data: {
            empresa: empresa,
            mes : mes,
            anio : anio,        
        },
        beforeSend: function(){
            $('.progress').show();
        },
        success: function (data) {
            var options = getOptionsEnergiaAnio();
            var horas = data.horas;
            var generacion = data.generacion;
            var generacionCLP = data.generacionCLP;
            var consumo = data.consumo;
            var generacionTotal = data.generacionTotal;
            var generacionCLPTotal = data.generacionCLPTotal;
            var consumoTotal = data.consumoTotal;   
            var barra = data.barra;
            var modalidad = data.modalidad;
            var precio_energia = data.precio_energia; 
            var initial_date = data.initial_date; 
            var final_date = data.final_date;                       
            var x = new Array();
            var series = new Array();
    
            // $("#barra_span").html(barra);
            // $("#modalidad_span").html(modalidad);
            // $("#precio_energia_span").html(' ('+ precio_energia + ' $/kWh)');
    
            x.push({
                categories: horas,
            });
    
            series.push({
                name: lang['Generación kWh'],
                data: generacion,
                type: 'column',
                color: 'green',
                tooltip: {
                    valueSuffix: ' kWh'
                }            
            });   
            
            series.push({
                name: lang['Consumo kWh'],
                data: consumo,
                type: 'column',
                color: 'red',
                tooltip: {
                    valueSuffix: ' kWh'
                }            
            });
    
            var subtitulo = data.subtitulo + '<br>'+ initial_date + ' - ' + final_date + '<br>'+ '<b>'+lang['Generación Total']+': ' + new Intl.NumberFormat().format(generacionTotal) + ' kWh - ' + lang['Consumo Total'] +': ' + new Intl.NumberFormat().format(consumoTotal) + ' kWh</b>';
    
            options.subtitle.text = subtitulo;
            options.xAxis = x;
            options.series = series;
            Highcharts.setOptions({
                time: {
                    timezone: 'America/Santiago'
                }
            });
    
            chart = new Highcharts.Chart(options);
    
            $('.progress').hide();
        }
        }); 
    }
    
    function getOptionsEnergiaAnio(){
        var options = {
            chart: {
                backgroundColor: 'transparent',            
                renderTo: 'energiaAnio',
                animation: Highcharts.svg, // don't animate in old IE
                zoomType: 'xy'
            },
            title: {
                text: lang['Energía Generada y Consumida (kWh)'],
            },
            subtitle:{
                text: '',
            },
            xAxis: {
                categories: [],
                crosshair: true
            },
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value} kWh',
                },
                title: {
                    text: lang['Energía'],
                }
            }, { // Secondary yAxis
                title: {
                    text: '',
                },
                labels: {
                    format: '{value} $',
                },
                opposite: true
            }],
            legend: {
                enabled: true
            },
            exporting: {
                enabled: true
            },
            series: [{
                name: '',
                data: [],
            }]
        }
    
        return options;
    }
    
    //Energia Ajax End
    
    //Potencia Ajax Start
    function AjaxPotenciaRange(){
        $.ajax({
        type: "POST",
        url: urlPotenciaRange,
        dataType: "json",
        data: {
            dateRange : dateRange,
            empresa: empresa
        },
        beforeSend: function(){
            $('.progress').show();
        },
        success: function (data) {
            var options = getOptionsPotenciaRange();
            var horas = data.horas;
            var pt = data.pt;
            var ir = data.ir;      
            var pr = data.pr;    
    
            var series = new Array();
    
            var datos = getDataPotenciaRange(horas,pt);
                series.push({
                    name: 'kW',
                    data: datos,
                    color: 'blue'
                });
    
            var datos_1 = getDataPotenciaRange(horas,ir);
            series.push({
                name: 'W/m2',
                data: datos_1,
                color: 'orange'
            });
    
            if (pr != 9999999) {
                if (data.pr.length > 0) {
                    var datos_2 = getDataPotenciaRange(horas,pr);
                    series.push({
                        name: 'PR',
                        data: datos_2,
                        color: 'green'
                    });                
                }                
            }
    
            var subtitulo = data.subtitulo + '<br>'+ $("#dateRangePicker").val();
    
            options.subtitle.text = subtitulo;
            options.series = series;
            Highcharts.setOptions({
                time: {
                    timezone: 'America/Santiago'
                }
            });
    
            chart = new Highcharts.Chart(options);
    
            $('.progress').hide();
        }
        }); 
    }
    
    function getOptionsPotenciaRange(){
        var options = {
            chart: {
                backgroundColor: 'transparent',
                type: 'spline',
                renderTo: 'potenciaRange',
                animation: Highcharts.svg, // don't animate in old IE
            },
            title: {
                text: lang['Potencia Activa kW - Radiación W/m2'],
            },
            subtitle:{
                text: '',
            },
            xAxis: {
                type: 'datetime',
                tickPixelInterval: 5
            },
            yAxis: {
                title: {
                    align: 'high',
                    offset: 0,
                    text: 'kW',
                    rotation: 0,
                    y: -15,
                    x: -20
                },
                labels: {
                    formatter: function () {
                        return this.value;
                    }
                }
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                        Highcharts.dateFormat('%H:%M', this.x) + '<br/>' +
                        Highcharts.numberFormat(this.y, 2)+' '+this.series.name;
                }
            },
            plotOptions: {
                spline: {
                    marker: {
                        radius: 0,
                        lineWidth: 0
                    }
                }
            },        
            legend: {
                enabled: true
            },
            exporting: {
                enabled: true
            },
            series: [{
                name: '',
                data: [],
            }]
        }
    
        return options;
    }
    
    function getDataPotenciaRange(time,potencia){
    
        var data = [];
    
        if(time.length == potencia.length){
            for (i=0 ; i<potencia.length ; i++){
                data.push({
                    x: time[i],
                    y: potencia[i]
                });
            }
        }
    
        return data;
    }
    
    //Potencia Ajax End
    
    //Voltajes Ajax Start
    function AjaxVoltajesRange(){
        $.ajax({
        type: "POST",
        url: urlVoltajesRange,
        dataType: "json",
        data: {
            dateRange : dateRange,
            empresa: empresa
        },
        beforeSend: function(){
            $('.progress').show();
        },
        success: function (data) {
            var options = getOptionsVoltajesRange();
            var horas = data.horas;
            var v1 = data.v1;
            var v2 = data.v2;
            var v3 = data.v3;
            var lst = data.lst;
            var lit = data.lit;
            var pfv = data.pfv;            
    
            var series = new Array();
    
            var datos = getDataVoltajesRange(horas,v1);
                series.push({
                    name: lang['Voltaje AB'],
                    data: datos,
                    color: 'blue'
                });
    
            var datos_1 = getDataVoltajesRange(horas,v2);
            series.push({
                name: lang['Voltaje AB'],
                data: datos_1,
                color: 'red'
            });
            
            var datos_2 = getDataVoltajesRange(horas,v3);
            series.push({
                name: lang['Voltaje CA'],
                data: datos_2,
                color: 'black'
            });
    
            var datos_3 = getDataVoltajesRange(horas,pfv);
            series.push({
                name: 'VN',
                data: datos_3,
                color: 'green'
            }); 
            
            var datos_4 = getDataVoltajesRange(horas,lst);
            series.push({
                name: 'LST',
                data: datos_4,
                color: 'orange'
            }); 
            
            var datos_5 = getDataVoltajesRange(horas,lit);
            series.push({
                name: 'LIT',
                data: datos_5,
                color: 'orange'
            });             
    
            var subtitulo = data.subtitulo + '<br>'+ $("#dateRangePicker").val();
    
            options.subtitle.text = subtitulo;
            options.series = series;
            Highcharts.setOptions({
                time: {
                    timezone: 'America/Santiago'
                }
            });
    
            chart = new Highcharts.Chart(options);
    
            $('.progress').hide();
        }
        }); 
    }
    
    function getOptionsVoltajesRange(){
        var options = {
            chart: {
                backgroundColor: 'transparent',
                type: 'spline',
                renderTo: 'voltajesRange',
                animation: Highcharts.svg, // don't animate in old IE
            },
            title: {
                text: lang['Voltaje de Línea'],
            },
            subtitle:{
                text: '',
            },
            xAxis: {
                type: 'datetime',
                tickPixelInterval: 5
            },
            yAxis: {
                title: {
                    align: 'high',
                    offset: 0,
                    text: 'kW',
                    rotation: 0,
                    y: -15,
                    x: -20
                },
                labels: {
                    formatter: function () {
                        return this.value;
                    }
                }
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                        Highcharts.dateFormat('%H:%M', this.x) + '<br/>' +
                        Highcharts.numberFormat(this.y, 2)+' '+this.series.name;
                }
            },
            plotOptions: {
                spline: {
                    marker: {
                        radius: 0,
                        lineWidth: 0
                    }
                }
            },        
            legend: {
                enabled: true
            },
            exporting: {
                enabled: true
            },
            series: [{
                name: '',
                data: [],
            }]
        }
    
        return options;
    }
    
    function getDataVoltajesRange(time,potencia){
    
        var data = [];
    
        if(time.length == potencia.length){
            for (i=0 ; i<potencia.length ; i++){
                data.push({
                    x: time[i],
                    y: potencia[i]
                });
            }
        }
    
        return data;
    }
    
    //Voltajes Ajax End
    
    
    //Inversores Ajax Start
    function AjaxInversoresRange(){
        $.ajax({
        type: "POST",
        url: urlInversoresRange,
        dataType: "json",
        data: {
            dateRange : dateRange,
            empresa: empresa
        },
        beforeSend: function(){
            $('.progress').show();
        },
        success: function (data) {
    
            var options = getOptionsInversoresRange();
    
            var pfv = data.pfv;
            var inversores = data.inversor;
            var potencia = data.potencia;
            var horas = data.horas;  
            var inversores_count = data.inversores_count;   
            
            if(pfv == 11){
                var titulo = lang['Potencia activa total Combiner Box'];
                var dispositivo = lang['Combiner box'] + ' ';
            }else{
                var titulo = lang['Potencia activa total Inversores'];
                var dispositivo = lang['Inversor'] + ' ';
            }
    
            var series = new Array();
    
            for (let index_1 = 1; index_1 <= inversores_count; index_1++) {
                if (typeof inversores[index_1] !== 'undefined') {
                    var inversor_nombre = dispositivo+inversores[index_1][0];
                    var datos = getDataInversoresRange(horas[index_1],potencia[index_1]);
                    series.push({
                        name: inversor_nombre,
                        data: datos
                    });
                } 
            }    
    
            var subtitulo = data.subtitulo + '<br>'+ $("#dateRangePicker").val();
    
            options.title.text = titulo;
            options.subtitle.text = subtitulo;
            options.series = series;
            Highcharts.setOptions({
                time: {
                    timezone: 'America/Santiago'
                }
            });
    
            chart = new Highcharts.Chart(options);
    
            $('.progress').hide();
        }
        }); 
    }
    
    function getOptionsInversoresRange(){
        var options = {
            chart: {
                backgroundColor: 'transparent',
                type: 'spline',
                renderTo: 'inversoresRange',
                animation: Highcharts.svg, // don't animate in old IE
                marginRight: 10,
                height: 500,
            },
            title: {
                text: '',
            },
            subtitle:{
                text: '',
            },
            xAxis: {
                type: 'datetime',
                tickPixelInterval: 5
            },
            yAxis: {
                title: {
                    align: 'high',
                    offset: 0,
                    text: 'kW',
                    rotation: 0,
                    y: -15,
                    x: -20
                },
                labels: {
                    formatter: function () {
                        return this.value;
                    }
                }
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                        Highcharts.dateFormat('%H:%M', this.x) + '<br/>' +
                        Highcharts.numberFormat(this.y, 2)+' kW';
                }
            },
            plotOptions: {
                spline: {
                    marker: {
                        radius: 0,
                        lineWidth: 0
                    }
                }
            },
            legend: {
                enabled: true
            },
            exporting: {
                enabled: true
            },
            series: [{
                name: '',
                data: [],
            }]
        }
    
        return options;
    }
    
    function getDataInversoresRange(time,potencia){
    
        var data = [];
    
        if(time.length == potencia.length){
            for (i=0 ; i<potencia.length ; i++){
                data.push({
                    x: time[i],
                    y: potencia[i]
                });
            }
        }
    
        return data;
    }
    
    //Inversores Ajax End
    
    (function (H) {
        H.wrap(H.Renderer.prototype, 'label', function (proceed, str, x, y, shape, anchorX, anchorY, useHTML) {
            if(/class="fa/.test(str))   useHTML = true;
            // Run original proceed method
            return proceed.apply(this, [].slice.call(arguments, 1));
        });
    }(Highcharts));
});