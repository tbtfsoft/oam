empresa_hija = $("#empresa_xls").val();
if (empresa_hija) {
    AjaxEmpresasPadre();
}
//On change sobre select Empresas
$("#empresa_xls").change(function(){
    empresa_hija = $("#empresa_xls").val();
    AjaxEmpresasPadre();
});

function AjaxEmpresasPadre(){
    $.ajax({
        type: "POST",
        url: urlEmpresasPadre,
        dataType: "json",
        data: { empresa_hija },
        beforeSend: function(){ },
        success: function (data) {
            $("#empresa-menu").html(data.response);
            $(".navbarDropdownUser:not(.nav-link)").html(data.sidebar);
        }
    });
}