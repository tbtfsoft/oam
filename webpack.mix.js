const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js(['resources/js/app.js'], 'public/js/app.js')
	.scripts([
		'./node_modules/tail.select/langs/tail.select-es.js',
		'./node_modules/tail.select/js/tail.select.min.js'
	], 'public/js/resouces.js')
   	.sass('resources/sass/app.scss', 'public/css')
	.styles([
		'./node_modules/tail.select/css/tail.select-bootstrap4.css',
		'public/css/app.css'
	], 'public/css/app.css')
   .options({
        processCssUrls: false,
    }).sourceMaps().version();


mix.browserSync({
    proxy: "http://oenergy.oam/"
});