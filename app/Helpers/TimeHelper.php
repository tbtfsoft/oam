<?php

namespace App\Helpers;

use DateTime;
use DateTimeZone;

class TimeHelper
{
	
	public static function getDateTime ($timestamp) {
		$objTime = new DateTime($timestamp);
		return $datetime = $objTime->format('U');
	}  	

	public static function getPeriodoRange($timestamp){
		$datetime = explode(" ", $timestamp);
		$date = explode("/", $datetime[1]);
		$mes = $date[1];
		$mes =  $mes < 10 ?  '0'. $mes : $mes;
		$periodo = $mes.'/'.$date[2];		

		return $periodo;	
	}

	public static function formatDateRange($timestamp){
		$objTime = new DateTime(str_replace('/', "-", $timestamp));		
		return $datetime = date('Y-m-d H:i:s', $objTime->format('U'));	
	}
	
	public static function formatDateHuman($timestamp){
		$objTime = new DateTime($timestamp);		
		return $datetime = date('d/m/Y', $objTime->format('U'));	
	}
	
	public static function formatDateHumanAnio($timestamp){
		$objTime = new DateTime($timestamp);		
		return $datetime = date('m/Y', $objTime->format('U'));	
	}	
}