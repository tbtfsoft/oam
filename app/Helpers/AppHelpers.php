<?php


class AppHelpers
{

	public static function lang($name, $base = true) {
		if ($base)
			return Lang::get('app.'.$name);
		return Lang::get($name);
	}

	public static function trans_choice($name, $plurar, $base = true) {
		if ($base)
			return trans_choice('app.'.$name,$plurar);
		return trans_choice($name,$plurar);
	}

	public static function razon_social ($id) {
		$rut =  \App\Models\AssetManagement\Empresa::where('id', $id)->first()->rut;
		$rut = str_replace('.', "", $rut);
		$explot_rut = explode("-", $rut);
		$empresa = \App\Models\PPagoCen\PPEmpresa::where('rut', $explot_rut[0])->where('verification_code', $explot_rut[1])->first();
		if ($empresa) {
			return mb_strtoupper($empresa->business_name, 'UTF-8');
		}
		return $rut;
	}

	public static function rut ($id) {
		$rut =  \App\Models\AssetManagement\Empresa::where('id', $id)->first()->rut;
		return $rut;
	}	

	public static function tipo_factura ($id) {
		switch ($id) {
			case '33':
				return 'Factura eléctronica';
				break;
			
			default:
					return $id;
				break;
		}
	}

	/**
	 * Al pasarle una fecha, se obtinen la diferencia entra la fecha y la fecha actual.
	 * @param  String $ini Fecha inicial
	 * @return String      Retornara la diferencia de las fechas en texto.
	 */
	public static function edad_pfv ($ini)
	{
		$diffDate = AppHelpers::diffDate($ini);
		$months = $diffDate['months'];
		$years = $diffDate['years'];
		$days = $diffDate['days'];

    if ($months < 1 && $years) {
    	$m = $days > 1 ? "$days Dias." : "$days Dia.";
    } else {
    	$m = null;
    	if ($years > 0) {
    		$m = $years == 1 ? "$years Años." : "$years Año";
    	}
    	if ($months > 0) {
    		$m = $m ? "$m y " : null;
    		$m = $months > 1 ? "$m$months Meses." : "$m$months Mes.";
    	}
    }

		return $m;
	}

	public static function diffDate($date)
	{
    $ini = new DateTime($date);

    $diffDate = $ini->diff(new DateTime(date("Y/m/d")));

    return [
    	'days' 	 => (int)$diffDate->format('%R%a'),
    	'months' => (int)$diffDate->format('%R%m'),
    	'years'  => (int)$diffDate->format('%R%y')
    ];
	}
}