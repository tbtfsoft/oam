<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Permission as Model;
use League\Csv\Reader;
use League\Csv\Writer;
use App\Http\Controllers\Modules\Reportes\Exports\ReportMedidor;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(\Auth::user()->hasRole('super administrador')) {
            // return view('home.super-administrador');
        }
        elseif(\Auth::user()->hasRole('backoffice')) {
            // return view('home.backoffice');
        }
        elseif(\Auth::user()->hasRole('client')) {
            // return view('home.client');
        }
        return view('home');
    }

    public function export2(Request $request) {
        $export = new ReportMedidor($request);
        return $export->export();
    }
}
