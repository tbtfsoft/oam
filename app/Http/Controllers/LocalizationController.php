<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LocalizationController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

   	public function index(Request $request, $locale)
   	{

      	app()->setLocale($locale);

      	session()->put('locale', $locale);
      	
      	$request->session()->put('locale', $locale);

    	return redirect()->back();
    }
}
