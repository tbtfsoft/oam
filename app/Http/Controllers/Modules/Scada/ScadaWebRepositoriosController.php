<?php

namespace App\Http\Controllers\Modules\Scada;

use Illuminate\Http\Request;
use App\Http\Controllers\CRUD\ResourceController;
use \IzyTech\Validator\Exceptions\ValidatorException;
use \IzyTech\Validator\Contracts\ValidatorInterface;
use App\DataTables\MensajeriaDataTable as DataTable;
use App\Models\AssetManagement\InformesOM as model;
use App\Models\AssetManagement\ModulosEmpresa;
use App\Models\AssetManagement\ModulosComentario;
use App\Models\AssetManagement\ModulosStatus;
use App\Models\AssetManagement\ComentariosEmpresa;
use Illuminate\Support\Facades\Auth;
use View;
use Flash;
use Storage;
use Lang;

class ScadaWebRepositoriosController extends ResourceController
{

    protected $user;

    public function __construct(Request $request, Model $model)
    {
          parent::__construct($model, [
              'datatable'  => new DataTable($model, $this->user),
              'prefix_router' =>	'o&m.repositorios',
              'prefix_view' 	=>	'modules.scada.repositorios'
          ]);
    }
      
      /**
       * Store a newly created resource in storage.
       *
       * @param  \Illuminate\Http\Request  $request
       * @return \Illuminate\Http\Response
       */
      public function show($id)
      {
          $model = $this->repository->find($id);
  
          $ids = Auth::user()->empresas->pluck('id'); 
          
          $comentarios = $model->comentarios(function ($query) use ($ids) {
              $query->whereIn('id_empresa', $ids);
          })->get();
          
          return view($this->view('show'))->with('model', $model)->with('comentarios', $comentarios);
      }
      
      /**
       * Store a newly created resource in storage.
       *
       * @param  \Illuminate\Http\Request  $request
       * @return \Illuminate\Http\Response
       */
      public function store(Request $request)
      {
          try {
              $currency = $this->repository->create( $request->input() );
              
              if ($request->hasFile('files')) {
                  foreach ($request->file('files') as $file) {
                      $file->storeAs('public/'.$this->model->getTableModel().'/'.$currency->id, $this->replace_specials_characters($file->getClientOriginalName()));
                  }
              }
  
              if (count($request->empresas) > 0 && ($request->empresas[0] !== null && $request->empresas[0] !== '-1')) {
                  foreach ($request->empresas as $empresa) {
                      if (($request->empresas[0] !== null && $request->empresas[0] !== '-1')) {
                          $currency->empresas()->create([
                              'id_empresa'  => $empresa,
                              'modulo'  => $this->class_basename(),
                              'id_modulo'  => $currency->id
                          ]);
                      }
                  }
              }
              
              $events = ModulosStatus::create([
                'id_user' => Auth::user()->id,
                'modulo'  => $this->model->getTableModel(),
                'id_modulo' => $currency->id
              ]);

              Flash::success(Lang::get('crud.store'));
  
              return redirect(route($this->as('index')));
          }
          catch (ValidatorException $e) {
              Flash::error(Lang::get('crud.error'));
  
              return back()
                              ->withInput($request->input())
                              ->withErrors($e->getMessageBag()->getMessages());
          }
      }
  
      /**
       * Update the specified resource in storage.
       *
       * @param  \Illuminate\Http\Request  $request
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function update(Request $request, $id)
      {
          try {
  
              $currency = $this->repository->update( $request->input(), $id );
              
              if ($request->hasFile('files')) {
                  foreach ($request->file('files') as $file) {
                      $file->storeAs('public/'.$this->model->getTableModel().'/'.$id, $this->replace_specials_characters($file->getClientOriginalName()));
                  }
              }
  
              if ($request->comentario) {
                  $comentario = $currency->comentarios()->create([
                      'id_user'  => Auth::id(),
                      'id_modulo'  => $currency->id,
                      'modulo'  => $this->model->getTableModel(),
                      'comentario'  => $request->comentario
                  ]);
  
                  foreach ($request->empresas_comentario as $empresa) {
                      ComentariosEmpresa::create([
                          'id_modulo'  => $currency->id,
                          'modulo'  => $this->model->getTableModel(),
                          'id_empresa' => $empresa,
                          'id_comentario' => $comentario->id
                      ]);
                  }
              }
  
              $currency->empresas()->delete();
              if (count($request->empresas) > 0 && ($request->empresas[0] !== null && $request->empresas[0] !== '-1')) {
                  foreach ($request->empresas as $empresa) {
                      if (($request->empresas[0] !== null && $request->empresas[0] !== '-1')) {
                          $currency->empresas()->create([
                              'id_empresa'  => $empresa,
                              'modulo'  => $this->model->getTableModel(),
                              'id_modulo'  => $currency->id
                          ]);
                      }
                  }
              }
              Flash::success(Lang::get('crud.update'));
  
              return redirect(route($this->as('index')));
          }
          catch (ValidatorException $e) {
  
                  Flash::error(Lang::get('crud.error'));
  
                  return back()
                                  ->withInput($request->input())
                                  ->withErrors($e->getMessageBag()->getMessages());
          }
      }
  
      /**
       * Remove the specified resource from storage.
       *
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function destroy($id)
      {
  
          $this->repository->find($id)->empresas()->delete();
  
          $this->repository->delete($id);
  
          Storage::deleteDirectory('public/' . $this->model->getTableModel() . '/'. $id);
  
          Flash::warning(Lang::get('crud.destroy'));
  
          return redirect(route($this->as('index')));
      }
  
      public function dataTable()
      {
          return new DataTable($this->model, Auth::user());
      }
  
      /**
       * Retorna el basename del Modelo
       * @return String
       */
      public function class_basename ()
      {
          return 'informes_om';
      }
  
      public function changeStatus(Request $request, $id)
      {
          try {
  
              $currency = Model::findOrFail($id);
  
              if ($currency) {

                  $events = ModulosStatus::create([
                    'id_user' => Auth::user()->id,
                    'modulo'  => $this->model->getTableModel(),
                    'id_modulo' => $currency->id,
                    'status'  => Auth::user()->hasRole(['client-admin']) ? 1 : 2
                  ]);
              
                  Flash::success(Lang::get('app.Se actualizó el estado correctamente'));
  
                  return redirect(route($this->as('show'), $id));
              }
  
              Flash::warning(Lang::get('app.No se encontró el registro'));
  
              return redirect(route($this->as('index')));
          }
          catch (ValidatorException $e) {
  
                  Flash::error(Lang::get('crud.error'));
  
                  return back()
                                  ->withErrors($e->getMessageBag()->getMessages());
          }
      }
  
      /**
       * Retorna la vista por defecto si no existe $v
       * @param  String $v    La vista que se desea buscar. 'index', 'edit', 'show'.. 'etc'.
       * @return string
       */
      protected function view($v)
      {
          $view = $this->prefix_view .'.'. $v;
  
          if(!View::exists($view))
              return 'modules.asset-management.resources.mensajeria.'.$v;
  
          return $view;
      }
      
      public function replace_specials_characters($s) {
          $s = strtolower($s);
          $s = preg_replace("/á|à|â|ã|ª/","a",$s);
          $s = preg_replace("/Á|À|Â|Ã/","A",$s);
          $s = preg_replace("/é|è|ê/","e",$s);
          $s = preg_replace("/É|È|Ê/","E",$s);
          $s = preg_replace("/í|ì|î/","i",$s);
          $s = preg_replace("/Í|Ì|Î/","I",$s);
          $s = preg_replace("/ó|ò|ô|õ|º/","o",$s);
          $s = preg_replace("/Ó|Ò|Ô|Õ/","O",$s);
          $s = preg_replace("/ú|ù|û/","u",$s);
          $s = preg_replace("/Ú|Ù|Û/","U",$s);
          $s = str_replace(" ","_",$s);
          $s = str_replace("ñ","n",$s);
          $s = str_replace("Ñ","N",$s);
          $s = preg_replace('/[^a-zA-Z0-9_.-]/', '', $s);
          return $s;
      }
  
      public function permisssion ($model) {
          if (!$model->isClose()) {
              if ((Auth::user()->hasRole(['client-admin', 'client']) && $model->status < 1) || (Auth::user()->hasRole(['super-admin', 'backoffice']) && $model->status > 0))
                  return true;
          }
          return false;
      }
  }
  