<?php

namespace App\Http\Controllers\Modules\Scada;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ScadaWebComercialController extends Controller
{

	/**
	 * Controlador para la vista principal de WEB COMERCIAL HOME
	 * @return View
	 */
	public function home ()
	{
		return view('modules.scada.web-comercial.home');
	}

}