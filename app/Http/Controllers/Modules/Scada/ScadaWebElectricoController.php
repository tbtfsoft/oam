<?php

namespace App\Http\Controllers\Modules\Scada;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ScadaWebElectricoController extends Controller
{

	/**
	 * Controlador para la vista principal de WEB ELECTRICO HOME
	 * @return View
	 */
	public function home ()
	{
		return view('modules.scada.web-electrico.home');
	}

}