<?php

namespace App\Http\Controllers\Modules\AssetManagement\RenameXML;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;
use Zipper;
use Flash;
use Lang;
use File;

class RenameXMLController extends Controller
{

	/**
     * @var model
     */
    protected $storageDisk = 'renameXML';


	/**
	 * Vista para cargar los archivos XML que seran procesados y renombrados.
	 * @return View
	 * @author Carlos Anselmi <carlosanselmi2@gmail.com>
	 */
	public function index(){
		return view('modules.asset-management.renameXML.index');
	}

	/**
	 * Descarga los archivos XML
	 * @return Zip
	 */
	public function getFiles () {
		$fileName = 'storage\uploads\RenameXML.zip';
    $zip = new \ZipArchive;
		$zip->open('renameXML.zip', \ZipArchive::CREATE);
		if (Storage::disk('file')->exists('renameXML.zip')) {
			Storage::disk('file')->delete('renameXML.zip');
		}
    if ($zip->open(public_path($fileName), \ZipArchive::CREATE)) {
    	foreach (['cen_xml/energia', 'cen_xml/potencia'] as $dir) {
    		$zip->addEmptyDir($dir);
    		if (Storage::disk('file')->exists($dir)) {
	    		foreach (File::files(public_path("storage/uploads/$dir")) as $file) {
	    			$zip->addFile($file, $dir."/".basename($file));
	    		}
    		}
    	}
    	 
    	$zip->close();
    }
    return response()->download($fileName);
	}

	/**
	 * Procesa todo los XML
	 * @param  Request $request
	 * @return Response->json()
	 */
	public function execute(Request $request) {
		if($request->hasfile('filesXML')) {
			$this->clearDirStorage();
			
			foreach($request->file('filesXML') as $file) {
				
				$filename = $file->getClientOriginalName();

				Storage::disk('renameXML')->putFileAs('/', $file, $filename);

				$fileXML = $this->getFileXML($filename,'renameXML');

				$nombreItem = (string)$fileXML->Detalle->NmbItem;

				if($nombreItem == 'GENERACION DE ENERGIA'){
					$this->storageDisk = 'energiaXML';
				}else if($nombreItem == 'GENERACION DE POTENCIA'){
					$this->storageDisk = 'potenciaXML';
				}

				if ($fileXML) {
					$row = $this->getData($fileXML);
					$newNameFIle = $this->newNameFIle($row);
					if (!Storage::disk($this->storageDisk)->exists($newNameFIle)) {
						Storage::disk($this->storageDisk)->putFileAs('/', $file, $newNameFIle);
					}
				}
			}

			return response()->json([
				"message" => Lang::get("app.archivos procesados correctamente")
			], 200);

		} else{
			return response()->json([
				"message" => Lang::get("app.debe subir por lo menos un archivo")
			], 500);
		}
	}

	/**
	 * Limpia todo los archivos de la carpeta de Storage. Asi se procesa los proximos xml.
	 */
	private function clearDirStorage () {
		Storage::disk('file')->deleteDirectory('cen_xml');
	}

	/**
	 * Obtiene el xml mediante el nombre del path del .xml
	 * @param  String $filename Nombre del .xml
	 * @return simplexml 
	 * @author Carlos Anselmi <carlosanselmi2@gmail.com>
	 */
	private function getFileXML ($filename,$disk){
		if (Storage::disk($disk)->exists($filename)){
			$file = Storage::disk($disk)->path($filename);
			//return simplexml_load_file($file)->Documento;
			$simpleXml = simplexml_load_file($file);
			if(isset($simpleXml->DTE)){
				return $simpleXml->DTE->Documento;
			}           
			else {
				if(isset($simpleXml->Documento)){
					return $simpleXml->Documento;
				}else{
					return $simpleXml->SetDTE->DTE->Documento;
				}                
			} 			
		}

		return null;
	}

	/**
	 * Obtiene los datos requeridos del xml
	 * @param  simplexml $xml Archivo a leer
	 * @return Array      Array con los datos requeridos
	 * @author Carlos Anselmi <carlosanselmi2@gmail.com>
	 */
	private function getData ($xml){
		return [
			'TipoDTE'	=> $xml->Encabezado->IdDoc->TipoDTE,
			'folio'	=> $xml->Encabezado->IdDoc->Folio,
			'RUTEmisor'	=> $xml->Encabezado->Emisor->RUTEmisor
		];
	}

	/**
	 * Genera el nuevo nombre del archivo xml con su extension.
	 * @param  Array $arrayLabel Array con los datos a procesar
	 * @return String             'namefile.xml'
	 * @author Carlos Anselmi <carlosanselmi2@gmail.com>
	 */
	private function newNameFIle ($arrayLabel){
		return explode("-", $arrayLabel['RUTEmisor'])[0] . '_' . $arrayLabel['TipoDTE'] . '_' . $arrayLabel['folio'] . '.xml';
	}
}