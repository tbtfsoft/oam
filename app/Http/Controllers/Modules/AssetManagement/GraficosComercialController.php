<?php

namespace App\Http\Controllers\Modules\AssetManagement;

use Illuminate\Http\Request;
use App\Helpers\TimeHelper;
use App\Http\Controllers\Controller;
use App\Models\Conexion;
use App\Models\Barras;
use App\Models\BarrasEmpresas;
use App\Models\ModalidadCalculo;
use App\Models\ModalidadCalculoEmpresas;
use App\Models\FactorReferencia;
use App\Models\PrecioEstabilizado;
use App\Models\AssetManagement\Empresa;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection as Collection;

class GraficosComercialController extends Controller
{
	public function GraficaEnergiaRange(Request $request){
        date_default_timezone_set('America/Santiago');
        $data = Array();
        $empresa_id = $request->empresa;
        $conexion = Conexion::where('id_empresa', $empresa_id)->first();
        $host = $conexion == null ? 0 : $conexion->host;
        $pfv = $conexion == null ? 0 : $conexion->id_parq;
        $medidor = $conexion == null ? 0 : $conexion->id_medidor;
        $radsensor = $conexion == null ? 0 : $conexion->id_radsensor;
        $database_name = $conexion == null ? 0 : $conexion->db;
		$subtitulo = $conexion == null ? 'no existe conexion' : $conexion->nombre_parq;			
		$dateRange = explode("-", $request->dateRange);
        $inicio = TimeHelper::formatDateRange($dateRange[0]);
		$fin = TimeHelper::formatDateRange($dateRange[1]);	
		$periodo = TimeHelper::getPeriodoRange($dateRange[1]);	
		//Modalidad de Calculo
		$modalidad_calculo_id = 1;
		$modalidad = ModalidadCalculoEmpresas::where('id_empresa', $empresa_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first() == null ? $modalidad_calculo_id = 0 : ModalidadCalculoEmpresas::where('id_empresa', $empresa_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first();		
		if($modalidad_calculo_id == 1){
			$modalidad_calculo_id = $modalidad->id_modalidad_calculo;
		}
		$modalidadEmpresaBandera = 1;
		$modalidadEmpresa = ModalidadCalculo::where('id', $modalidad_calculo_id)->first() == null ? $modalidadEmpresaBandera = '' : ModalidadCalculo::where('id', $modalidad_calculo_id)->first();
		if($modalidadEmpresaBandera == 1){
			$modalidadEmpresaBandera = $modalidadEmpresa->nombre;
		}

		//Barras
		$barra_id = 1;
		$barra = BarrasEmpresas::where('id_empresa', $empresa_id)->where('tipo_calculo', $modalidad_calculo_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first() == null ? $barra_id = 0 : BarrasEmpresas::where('id_empresa', $empresa_id)->where('tipo_calculo', $modalidad_calculo_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first();
		if($barra_id == 1){
			$barra_id = $barra->id_barras;
		}
		$barraEmpresaBandera = 1;
		$barraEmpresa = Barras::where('id', $barra_id)->first() == null ? $barraEmpresaBandera = '' : Barras::where('id', $barra_id)->first();
		if($barraEmpresaBandera == 1){
			$barraEmpresaBandera = $barraEmpresa->barra_human;
		}		

		//Definimos la modalidad del calculo Ejemplo PE o CMg
		$precio_energia = 1;	
		if ($modalidad_calculo_id == 1) {			
			$precio = PrecioEstabilizado::where('id_barra', $barra_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first() == null ? $precio_energia = 0 : PrecioEstabilizado::where('id_barra', $barra_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first();
		}else {
			$precio_energia = 0;
		}	
		
		//$factorReferencia = FactorReferencia::where('id_empresa', $empresa_id)->where('id_tipo_doc_cen', '=', 2)->where('periodo', '=', '09/2019')->get()->toArray();

		$data = Array();
        $consumoRaw = Array();
		$inyeccionRaw = Array();
        $consumoRaw2 = Array();
		$inyeccionRaw2 = Array();	
		$inyeccionCLPRaw = Array();	
		$inyeccionCLPRaw2 = Array();	
		$dataRaw = Array();
		$dataRaw2 = Array();
		$horasRaw = Array();	

		if ($precio_energia == 1) {
			$precio_energia = $precio->precio_energia;
		}

        //DB CONNECTION
        if($host == 1){
            $host = config('app.host_1');
            $port = config('app.host_port_1');
            $password = config('app.host_password_1');
        }else if($host == 2){

        }else{
            $host = 0;
            $port = 0;
            $password = 0;            
        }

        if($host != 0){
			//INICIO DE LA CONSULTA
            $strConnect = "host = ".$host." port = ".$port." dbname = '".$database_name."' user = 'postgres' password = ".$password;
			$con = pg_connect($strConnect);
						
			$consulta_cont = 0;
			$fecha_inicial_ciclo = $inicio;
			$fecha_final_ciclo = $fin;

			while(strtotime($fecha_inicial_ciclo) < strtotime($fecha_final_ciclo)){
				
				$end = strtotime('+1 hour', strtotime($fecha_inicial_ciclo));
				$end = date('Y-m-d H:i:s', $end);

				$start = strtotime('-1 hour', strtotime($end));
				$start = date('Y-m-d H:i:s', $start);			
				if(strtotime($start) <= strtotime($fecha_final_ciclo))
				{
					//DB QUERY STRING
					$strQuery = "SELECT (MAX(ener_in) - MIN(ener_in)) AS consumo, (MAX(ener_out) - MIN(ener_out)) AS inyeccion  
					FROM tbl_datos_medidor	
					WHERE id_disp = $medidor
					AND fch_dato BETWEEN '$start' AND '$end'		
					";
				}

				//DB QUERY
				$resRAW = pg_query($con,$strQuery);					

				while($dataFetch = pg_fetch_assoc($resRAW))
				{
					$dataRaw[] = $dataFetch;
				}

				$fecha_expode_0 = $end;
				$fecha_expode_1 = explode(' ', $fecha_expode_0);
				$fecha_expode_2 = explode(':',$fecha_expode_1[1]);
				$fecha_expode_3 = $fecha_expode_2[0].':'.$fecha_expode_2[1];			

				$consumoRaw[] = abs((float)($dataRaw[$consulta_cont]['consumo']));
				$inyeccionRaw[] = abs((float)($dataRaw[$consulta_cont]['inyeccion']));
				$inyeccionCLPRaw[] = (float)number_format((abs(($dataRaw[$consulta_cont]['inyeccion']))) * $precio_energia, 3, '.', '');
				$horasRaw[] = $fecha_expode_3;
				
				$consulta_cont++;
				$fecha_inicial_ciclo = $end;
			}
			//FIN DE LA CONSULTA
			
			//DB QUERY STRING
			$strQuery2 = "SELECT (MAX(ener_in) - MIN(ener_in)) AS consumo, (MAX(ener_out) - MIN(ener_out)) AS inyeccion  
			FROM tbl_datos_medidor	
			WHERE id_disp = $medidor
			AND fch_dato BETWEEN '$inicio' AND '$fin'		
			";

			//DB QUERY
			$resRAW2 = pg_query($con,$strQuery2);		

			while($dataFetch2 = pg_fetch_assoc($resRAW2))
			{
				$dataRaw2[] = $dataFetch2;
			}	
			
			for($i=0; $i < count($dataRaw2) ; $i++){
				$consumoRaw2[] = abs((float)($dataRaw2[$i]['consumo']));
				$inyeccionRaw2[] = abs((float)($dataRaw2[$i]['inyeccion']));
				$inyeccionCLPRaw2[] = (float)number_format(abs(($dataRaw2[$i]['inyeccion'])) * $precio_energia, 0, '.', '');
			}
        }		

        $data = Array(
            'consumo' => $consumoRaw,
			'generacion' => $inyeccionRaw,
			'generacionCLP' => $inyeccionCLPRaw,
            'consumoTotal' => $consumoRaw2,
			'generacionTotal' => $inyeccionRaw2,
			'generacionCLPTotal' => $inyeccionCLPRaw2,			
			'horas' => $horasRaw,
			'subtitulo' => $subtitulo,
			'barra' => $barraEmpresaBandera,
			'modalidad' => $modalidadEmpresaBandera,
			'precio_energia' => number_format($precio_energia, 3, ',', '.'),
        );

        return $data;		
	}
	
	public function GraficaEnergiaMes(Request $request){
        date_default_timezone_set('America/Santiago');
        $data = Array();
        $empresa_id = $request->empresa;
        $conexion = Conexion::where('id_empresa', $empresa_id)->first();
        $host = $conexion == null ? 0 : $conexion->host;
        $pfv = $conexion == null ? 0 : $conexion->id_parq;
        $medidor = $conexion == null ? 0 : $conexion->id_medidor;
        $radsensor = $conexion == null ? 0 : $conexion->id_radsensor;
        $database_name = $conexion == null ? 0 : $conexion->db;
		$subtitulo = $conexion == null ? 'no existe conexion' : $conexion->nombre_parq;			     
		$inicial_date =  $request->anioMes.'-'.$request->mes.'-01';
		$inicio = $inicial_date.' 00:00:00';		
		$fin = date("Y-m-t", strtotime($inicial_date)).' 23:59:59';	
		$inicio_front = TimeHelper::formatDateHuman($inicial_date);
		$fin_front = date("t/m/Y", strtotime($inicial_date));
		//Modalidad de Calculo
		$modalidad_calculo_id = 1;
		$modalidad = ModalidadCalculoEmpresas::where('id_empresa', $empresa_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first() == null ? $modalidad_calculo_id = 0 : ModalidadCalculoEmpresas::where('id_empresa', $empresa_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first();		
		if($modalidad_calculo_id == 1){
			$modalidad_calculo_id = $modalidad->id_modalidad_calculo;
		}
		$modalidadEmpresaBandera = 1;
		$modalidadEmpresa = ModalidadCalculo::where('id', $modalidad_calculo_id)->first() == null ? $modalidadEmpresaBandera = '' : ModalidadCalculo::where('id', $modalidad_calculo_id)->first();
		if($modalidadEmpresaBandera == 1){
			$modalidadEmpresaBandera = $modalidadEmpresa->nombre;
		}

		//Barras
		$barra_id = 1;
		$barra = BarrasEmpresas::where('id_empresa', $empresa_id)->where('tipo_calculo', $modalidad_calculo_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first() == null ? $barra_id = 0 : BarrasEmpresas::where('id_empresa', $empresa_id)->where('tipo_calculo', $modalidad_calculo_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first();
		if($barra_id == 1){
			$barra_id = $barra->id_barras;
		}
		$barraEmpresaBandera = 1;
		$barraEmpresa = Barras::where('id', $barra_id)->first() == null ? $barraEmpresaBandera = '' : Barras::where('id', $barra_id)->first();
		if($barraEmpresaBandera == 1){
			$barraEmpresaBandera = $barraEmpresa->barra_human;
		}		

		//Definimos la modalidad del calculo Ejemplo PE o CMg
		$precio_energia = 1;	
		if ($modalidad_calculo_id == 1) {			
			$precio = PrecioEstabilizado::where('id_barra', $barra_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first() == null ? $precio_energia = 0 : PrecioEstabilizado::where('id_barra', $barra_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first();
		}else {
			$precio_energia = 0;
		}	

		$data = Array();
        $consumoRaw = Array();
		$inyeccionRaw = Array();
        $consumoRaw2 = Array();
		$inyeccionRaw2 = Array();	
		$inyeccionCLPRaw = Array();	
		$inyeccionCLPRaw2 = Array();	
		$dataRaw = Array();
		$dataRaw2 = Array();
		$horasRaw = Array();

		if ($precio_energia == 1) {
			$precio_energia = $precio->precio_energia;
		}

        //DB CONNECTION
        if($host == 1){
            $host = config('app.host_1');
            $port = config('app.host_port_1');
            $password = config('app.host_password_1');
        }else if($host == 2){

        }else{
            $host = 0;
            $port = 0;
            $password = 0;            
        }

        if($host != 0){
			//INICIO DE LA CONSULTA
            $strConnect = "host = ".$host." port = ".$port." dbname = '".$database_name."' user = 'postgres' password = ".$password;
			$con = pg_connect($strConnect);
						
			$consulta_cont = 0;
			$fecha_inicial_ciclo = $inicio;
			$fecha_final_ciclo = $fin;

			while(strtotime($fecha_inicial_ciclo) < strtotime($fecha_final_ciclo)){
				
				$end = strtotime('+1 day', strtotime($fecha_inicial_ciclo));
				$end = date('Y-m-d H:i:s', $end);

				$start = strtotime('-1 day', strtotime($end));
				$start = date('Y-m-d H:i:s', $start);			
				if(strtotime($start) <= strtotime($fecha_final_ciclo))
				{
					//DB QUERY STRING
					$strQuery = "SELECT (MAX(ener_in) - MIN(ener_in)) AS consumo, (MAX(ener_out) - MIN(ener_out)) AS inyeccion  
					FROM tbl_datos_medidor	
					WHERE id_disp = $medidor
					AND fch_dato BETWEEN '$start' AND '$end'		
					";
				}

				//DB QUERY
				$resRAW = pg_query($con,$strQuery);					

				while($dataFetch = pg_fetch_assoc($resRAW))
				{
					$dataRaw[] = $dataFetch;
				}
				
				$fecha_expode_0 = $start;
				$fecha_expode_1 = explode(' ', $fecha_expode_0);
				$fecha_expode_2 = explode(':',$fecha_expode_1[1]);
				$fecha_expode_3 = $fecha_expode_2[0].':'.$fecha_expode_2[1];	

				$consumoRaw[] = abs((float)($dataRaw[$consulta_cont]['consumo']));
				$inyeccionRaw[] = abs((float)($dataRaw[$consulta_cont]['inyeccion']));
				$inyeccionCLPRaw[] = (float)number_format((abs(($dataRaw[$consulta_cont]['inyeccion']))) * $precio_energia, 3, '.', '');	
				$horasRaw[] = TimeHelper::formatDateHuman($fecha_expode_1[0]);
				
				$consulta_cont++;
				$fecha_inicial_ciclo = $end;
			}
			//FIN DE LA CONSULTA
			
			//DB QUERY STRING
			$strQuery2 = "SELECT (MAX(ener_in) - MIN(ener_in)) AS consumo, (MAX(ener_out) - MIN(ener_out)) AS inyeccion  
			FROM tbl_datos_medidor	
			WHERE id_disp = $medidor
			AND fch_dato BETWEEN '$inicio' AND '$fin'		
			";

			//DB QUERY
			$resRAW2 = pg_query($con,$strQuery2);		

			while($dataFetch2 = pg_fetch_assoc($resRAW2))
			{
				$dataRaw2[] = $dataFetch2;
			}	
			
			for($i=0; $i < count($dataRaw2) ; $i++){
				$consumoRaw2[] = abs((float)($dataRaw2[$i]['consumo']));
				$inyeccionRaw2[] = abs((float)($dataRaw2[$i]['inyeccion']));
				$inyeccionCLPRaw2[] = (float)number_format(abs(($dataRaw2[$i]['inyeccion'])) * $precio_energia, 0, '.', '');
			}
        }		

        $data = Array(
            'consumo' => $consumoRaw,
			'generacion' => $inyeccionRaw,
			'generacionCLP' => $inyeccionCLPRaw,
            'consumoTotal' => $consumoRaw2,
			'generacionTotal' => $inyeccionRaw2,
			'generacionCLPTotal' => $inyeccionCLPRaw2,			
			'horas' => $horasRaw,
			'subtitulo' => $subtitulo,
			'initial_date' => $inicio_front,
			'final_date' => $fin_front,
			'barra' => $barraEmpresaBandera,
			'modalidad' => $modalidadEmpresaBandera,
			'precio_energia' => number_format($precio_energia, 3, ',', '.'),
        );

        return $data;		
	}
	
	public function GraficaEnergiaVSMes(Request $request){
        date_default_timezone_set('America/Santiago');
        $data = Array();
        $empresa_id = $request->empresa;
        $conexion = Conexion::where('id_empresa', $empresa_id)->first();
        $host = $conexion == null ? 0 : $conexion->host;
        $pfv = $conexion == null ? 0 : $conexion->id_parq;
        $medidor = $conexion == null ? 0 : $conexion->id_medidor;
        $radsensor = $conexion == null ? 0 : $conexion->id_radsensor;
        $database_name = $conexion == null ? 0 : $conexion->db;
		$subtitulo = $conexion == null ? 'no existe conexion' : $conexion->nombre_parq;			     
		$inicial_date =  $request->anioMes.'-'.$request->mes.'-01';
		$inicio = $inicial_date.' 00:00:00';		
		$fin = date("Y-m-t", strtotime($inicial_date)).' 23:59:59';	
		$inicio_front = TimeHelper::formatDateHuman($inicial_date);
		$fin_front = date("t/m/Y", strtotime($inicial_date));
		//Modalidad de Calculo
		$modalidad_calculo_id = 1;
		$modalidad = ModalidadCalculoEmpresas::where('id_empresa', $empresa_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first() == null ? $modalidad_calculo_id = 0 : ModalidadCalculoEmpresas::where('id_empresa', $empresa_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first();		
		if($modalidad_calculo_id == 1){
			$modalidad_calculo_id = $modalidad->id_modalidad_calculo;
		}
		$modalidadEmpresaBandera = 1;
		$modalidadEmpresa = ModalidadCalculo::where('id', $modalidad_calculo_id)->first() == null ? $modalidadEmpresaBandera = '' : ModalidadCalculo::where('id', $modalidad_calculo_id)->first();
		if($modalidadEmpresaBandera == 1){
			$modalidadEmpresaBandera = $modalidadEmpresa->nombre;
		}

		//Barras
		$barra_id = 1;
		$barra = BarrasEmpresas::where('id_empresa', $empresa_id)->where('tipo_calculo', $modalidad_calculo_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first() == null ? $barra_id = 0 : BarrasEmpresas::where('id_empresa', $empresa_id)->where('tipo_calculo', $modalidad_calculo_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first();
		if($barra_id == 1){
			$barra_id = $barra->id_barras;
		}
		$barraEmpresaBandera = 1;
		$barraEmpresa = Barras::where('id', $barra_id)->first() == null ? $barraEmpresaBandera = '' : Barras::where('id', $barra_id)->first();
		if($barraEmpresaBandera == 1){
			$barraEmpresaBandera = $barraEmpresa->barra_human;
		}		

		//Definimos la modalidad del calculo Ejemplo PE o CMg
		$precio_energia = 1;	
		if ($modalidad_calculo_id == 1) {			
			$precio = PrecioEstabilizado::where('id_barra', $barra_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first() == null ? $precio_energia = 0 : PrecioEstabilizado::where('id_barra', $barra_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first();
		}else {
			$precio_energia = 0;
		}	

		$data = Array();

        $consumoRaw = Array();
		$inyeccionRaw = Array();
        $consumoRaw2 = Array();
		$inyeccionRaw2 = Array();	
		$inyeccionCLPRaw = Array();	
		$inyeccionCLPRaw2 = Array();	
		$dataRaw = Array();
		$dataRaw2 = Array();
		$horasRaw = Array();

		$consumoVSRaw = Array();
		$inyeccionVSRaw = Array();
        $consumoVSRaw2 = Array();
		$inyeccionVSRaw2 = Array();	
		$inyeccionCLPVSRaw = Array();	
		$inyeccionCLPVSRaw2 = Array();	
		$dataVSRaw = Array();
		$dataVSRaw2 = Array();
		$horasVSRaw = Array();

		if ($precio_energia == 1) {
			$precio_energia = $precio->precio_energia;
		}

        //DB CONNECTION
        if($host == 1){
            $host = config('app.host_1');
            $port = config('app.host_port_1');
            $password = config('app.host_password_1');
        }else if($host == 2){

        }else{
            $host = 0;
            $port = 0;
            $password = 0;            
        }

        if($host != 0){
			//INICIO DE LA CONSULTA 1
            $strConnect = "host = ".$host." port = ".$port." dbname = '".$database_name."' user = 'postgres' password = ".$password;
			$con = pg_connect($strConnect);
						
			$consulta_cont = 0;
			$fecha_inicial_ciclo = $inicio;
			$fecha_final_ciclo = $fin;

			while(strtotime($fecha_inicial_ciclo) < strtotime($fecha_final_ciclo)){
				
				$end = strtotime('+1 day', strtotime($fecha_inicial_ciclo));
				$end = date('Y-m-d H:i:s', $end);

				$start = strtotime('-1 day', strtotime($end));
				$start = date('Y-m-d H:i:s', $start);			
				if(strtotime($start) <= strtotime($fecha_final_ciclo))
				{
					//DB QUERY STRING
					$strQuery = "SELECT (MAX(ener_in) - MIN(ener_in)) AS consumo, (MAX(ener_out) - MIN(ener_out)) AS inyeccion  
					FROM tbl_datos_medidor	
					WHERE id_disp = $medidor
					AND fch_dato BETWEEN '$start' AND '$end'		
					";
				}

				//DB QUERY
				$resRAW = pg_query($con,$strQuery);					

				while($dataFetch = pg_fetch_assoc($resRAW))
				{
					$dataRaw[] = $dataFetch;
				}
				
				$fecha_expode_0 = $start;
				$fecha_expode_1 = explode(' ', $fecha_expode_0);
				$fecha_expode_2 = explode(':',$fecha_expode_1[1]);
				$fecha_expode_3 = $fecha_expode_2[0].':'.$fecha_expode_2[1];	

				$consumoRaw[] = abs((float)($dataRaw[$consulta_cont]['consumo']));
				if ($consulta_cont != 0) {
					$inyeccionRaw[] = abs((float)($dataRaw[$consulta_cont]['inyeccion'] + $inyeccionRaw[$consulta_cont - 1]));
				}else {
					$inyeccionRaw[] = abs((float)($dataRaw[$consulta_cont]['inyeccion']));
				}
				$horasRaw[] = TimeHelper::formatDateHuman($fecha_expode_1[0]);
				
				$consulta_cont++;
				$fecha_inicial_ciclo = $end;
			}			
			
			//ACUMULATIVO 1
			$strQuery2 = "SELECT (MAX(ener_in) - MIN(ener_in)) AS consumo, (MAX(ener_out) - MIN(ener_out)) AS inyeccion  
			FROM tbl_datos_medidor	
			WHERE id_disp = $medidor
			AND fch_dato BETWEEN '$inicio' AND '$fin'		
			";

			//DB QUERY
			$resRAW2 = pg_query($con,$strQuery2);		

			while($dataFetch2 = pg_fetch_assoc($resRAW2))
			{
				$dataRaw2[] = $dataFetch2;
			}	
			
			for($i=0; $i < count($dataRaw2) ; $i++){
				$consumoRaw2[] = abs((float)($dataRaw2[$i]['consumo']));
				$inyeccionRaw2[] = abs((float)($dataRaw2[$i]['inyeccion']));
			}

			//FIN DE LA CONSULTA 1

			//INICIO DE LA CONSULTA 2
			$consulta_cont = 0;
			$inicial_date =  ($request->anioMes - 1).'-'.$request->mes.'-01';
			$inicio = $inicial_date.' 00:00:00';		
			$fin = date("Y-m-t", strtotime($inicial_date)).' 23:59:59';	
			$fecha_inicial_ciclo = $inicio;
			$fecha_final_ciclo = $fin;

			while(strtotime($fecha_inicial_ciclo) < strtotime($fecha_final_ciclo)){
				$end = strtotime('+1 day', strtotime($fecha_inicial_ciclo));
				$end = date('Y-m-d H:i:s', $end);

				$start = strtotime('-1 day', strtotime($end));
				$start = date('Y-m-d H:i:s', $start);			
				if(strtotime($start) <= strtotime($fecha_final_ciclo))
				{
					//DB QUERY STRING
					$strQuery = "SELECT (MAX(ener_in) - MIN(ener_in)) AS consumo, (MAX(ener_out) - MIN(ener_out)) AS inyeccion  
					FROM tbl_datos_medidor	
					WHERE id_disp = $medidor
					AND fch_dato BETWEEN '$start' AND '$end'		
					";
				}

				//DB QUERY
				$resRAW = pg_query($con,$strQuery);					

				while($dataFetch = pg_fetch_assoc($resRAW))
				{
					$dataVSRaw[] = $dataFetch;
				}
				
				$fecha_expode_0 = $start;
				$fecha_expode_1 = explode(' ', $fecha_expode_0);
				$fecha_expode_2 = explode(':',$fecha_expode_1[1]);
				$fecha_expode_3 = $fecha_expode_2[0].':'.$fecha_expode_2[1];	

				$consumoVSRaw[] = abs((float)($dataVSRaw[$consulta_cont]['consumo']));
				if ($consulta_cont != 0) {
					$inyeccionVSRaw[] = abs((float)($dataVSRaw[$consulta_cont]['inyeccion'] + $inyeccionVSRaw[$consulta_cont - 1]));
				}else {
					$inyeccionVSRaw[] = abs((float)($dataVSRaw[$consulta_cont]['inyeccion']));
				}
				$horasVSRaw[] = TimeHelper::formatDateHuman($fecha_expode_1[0]);
				
				$consulta_cont++;
				$fecha_inicial_ciclo = $end;
			}			
			
			//ACUMULATIVO 2
			$strQuery2 = "SELECT (MAX(ener_in) - MIN(ener_in)) AS consumo, (MAX(ener_out) - MIN(ener_out)) AS inyeccion  
			FROM tbl_datos_medidor	
			WHERE id_disp = $medidor
			AND fch_dato BETWEEN '$inicio' AND '$fin'		
			";

			//DB QUERY
			$resRAW2 = pg_query($con,$strQuery2);		

			while($dataFetch2 = pg_fetch_assoc($resRAW2))
			{
				$dataVSRaw2[] = $dataFetch2;
			}	
			
			for($i=0; $i < count($dataRaw2) ; $i++){
				$consumoVSRaw2[] = abs((float)($dataVSRaw2[$i]['consumo']));
				$inyeccionVSRaw2[] = abs((float)($dataVSRaw2[$i]['inyeccion']));
			}

			//FIN DE LA CONSULTA 2			
        }		

        $data = Array(
			'generacion' => $inyeccionRaw,
			'generacionTotal' => $inyeccionRaw2,
			'consumo' => $consumoRaw,
			'consumoTotal' => $consumoRaw2,	
			'generacionVS1' => $inyeccionVSRaw,
			'generacionVS1Total' => $inyeccionVSRaw2,
			'consumoVS1' => $consumoVSRaw,
            'consumoVS1Total' => $consumoVSRaw2,			
			'horas' => $horasRaw,
			'horasVS1' => $horasVSRaw,
			'subtitulo' => $subtitulo,
			'initial_date' => $inicio_front,
			'final_date' => $fin_front,
			'barra' => $barraEmpresaBandera,
			'modalidad' => $modalidadEmpresaBandera,
			'precio_energia' => number_format($precio_energia, 3, ',', '.'),
        );

        return $data;		
	}

	public function GraficaEnergiaAnio(Request $request){
        date_default_timezone_set('America/Santiago');
        $data = Array();
        $empresa_id = $request->empresa;
        $conexion = Conexion::where('id_empresa', $empresa_id)->first();
        $host = $conexion == null ? 0 : $conexion->host;
        $pfv = $conexion == null ? 0 : $conexion->id_parq;
        $medidor = $conexion == null ? 0 : $conexion->id_medidor;
        $radsensor = $conexion == null ? 0 : $conexion->id_radsensor;
        $database_name = $conexion == null ? 0 : $conexion->db;
		$subtitulo = $conexion == null ? 'no existe conexion' : $conexion->nombre_parq;			
		$dateRange = explode("-", $request->dateRange);        
		$inicial_date =  $request->anio.'-01-01';
		$inicio = $inicial_date.' 00:00:00';		
		$fin = date("Y-m-t", strtotime($inicial_date)).' 23:59:59';		

		$data = Array();
        $consumoRaw = Array();
		$inyeccionRaw = Array();
        $consumoRaw2 = Array();
		$inyeccionRaw2 = Array();	
		$inyeccionCLPRaw = Array();	
		$inyeccionCLPRaw2 = Array();	
		$dataRaw = Array();
		$dataRaw2 = Array();
		$horasRaw = Array();
		$fecha_inicial_ciclo_2 = '';
		$fecha_final_ciclo_2 = '';

		$modalidadEmpresaBandera = '';
		$barraEmpresaBandera = '';
		$precio_energia = '';	

        //DB CONNECTION
        if($host == 1){
            $host = config('app.host_1');
            $port = config('app.host_port_1');
            $password = config('app.host_password_1');
        }else if($host == 2){

        }else{
            $host = 0;
            $port = 0;
            $password = 0;            
        }

        if($host != 0){
			//INICIO DE LA CONSULTA
            $strConnect = "host = ".$host." port = ".$port." dbname = '".$database_name."' user = 'postgres' password = ".$password;
			$con = pg_connect($strConnect);
						
			$consulta_cont = 0;
			$fecha_inicial_ciclo = $inicio;
			$fecha_final_ciclo = $request->anio.'-12-31 23:59:59';

			$fecha_inicial_ciclo_2 = $inicio;
			$fecha_final_ciclo_2 = $request->anio.'-12-31 23:59:59';

			while(strtotime($fecha_inicial_ciclo) < strtotime($fecha_final_ciclo)){
				
				$end = strtotime('+1 month', strtotime($fecha_inicial_ciclo));
				$end = date('Y-m-d H:i:s', $end);
				$start = strtotime('-1 month', strtotime($end));
				$start = date('Y-m-d H:i:s', $start);			

				if(strtotime($start) <= strtotime($fecha_final_ciclo))
				{
					//DB QUERY STRING
					$strQuery = "SELECT (MAX(ener_in) - MIN(ener_in)) AS consumo, (MAX(ener_out) - MIN(ener_out)) AS inyeccion  
					FROM tbl_datos_medidor	
					WHERE id_disp = $medidor
					AND fch_dato BETWEEN '$start' AND '$end'		
					";

					//Modalidad de Calculo
					$modalidad_calculo_id = 1;
					$modalidad = ModalidadCalculoEmpresas::where('id_empresa', $empresa_id)->where('inicial_date', '<=', $start)->where('final_date', '>=', $end)->first() == null ? $modalidad_calculo_id = 0 : ModalidadCalculoEmpresas::where('id_empresa', $empresa_id)->where('inicial_date', '<=', $start)->where('final_date', '>=', $end)->first();		
					if($modalidad_calculo_id == 1){
						$modalidad_calculo_id = $modalidad->id_modalidad_calculo;
					}
					$modalidadEmpresaBandera = 1;
					$modalidadEmpresa = ModalidadCalculo::where('id', $modalidad_calculo_id)->first() == null ? $modalidadEmpresaBandera = '' : ModalidadCalculo::where('id', $modalidad_calculo_id)->first();
					if($modalidadEmpresaBandera == 1){
						$modalidadEmpresaBandera = $modalidadEmpresa->nombre;
					}

					//Barras
					$barra_id = 1;
					$barra = BarrasEmpresas::where('id_empresa', $empresa_id)->where('tipo_calculo', $modalidad_calculo_id)->where('inicial_date', '<=', $start)->where('final_date', '>=', $end)->first() == null ? $barra_id = 0 : BarrasEmpresas::where('id_empresa', $empresa_id)->where('tipo_calculo', $modalidad_calculo_id)->where('inicial_date', '<=', $start)->where('final_date', '>=', $end)->first();
					if($barra_id == 1){
						$barra_id = $barra->id_barras;
					}
					$barraEmpresaBandera = 1;
					$barraEmpresa = Barras::where('id', $barra_id)->first() == null ? $barraEmpresaBandera = '' : Barras::where('id', $barra_id)->first();
					if($barraEmpresaBandera == 1){
						$barraEmpresaBandera = $barraEmpresa->barra_human;
					}		

					//Definimos la modalidad del calculo Ejemplo PE o CMg
					$precio_energia = 1;	
					if ($modalidad_calculo_id == 1) {			
						$precio = PrecioEstabilizado::where('id_barra', $barra_id)->where('inicial_date', '<=', $start)->where('final_date', '>=', $end)->first() == null ? $precio_energia = 0 : PrecioEstabilizado::where('id_barra', $barra_id)->where('inicial_date', '<=', $start)->where('final_date', '>=', $end)->first();
					}else {
						$precio_energia = 0;
					}	
					
					if ($precio_energia == 1) {
						$precio_energia = $precio->precio_energia;
					}
				}

				//DB QUERY
				$resRAW = pg_query($con,$strQuery);					

				while($dataFetch = pg_fetch_assoc($resRAW))
				{
					$dataRaw[] = $dataFetch;
				}
				
				$fecha_expode_0 = $start;
				$fecha_expode_1 = explode(' ', $fecha_expode_0);
				$fecha_expode_2 = explode(':',$fecha_expode_1[1]);
				$fecha_expode_3 = $fecha_expode_2[0].':'.$fecha_expode_2[1];	

				$consumoRaw[] = abs((float)($dataRaw[$consulta_cont]['consumo']));
				$inyeccionRaw[] = abs((float)($dataRaw[$consulta_cont]['inyeccion']));
				$inyeccionCLPRaw[] = (float)number_format((abs(($dataRaw[$consulta_cont]['inyeccion']))) * $precio_energia, 3, '.', '');	
				$horasRaw[] = TimeHelper::formatDateHumanAnio($fecha_expode_1[0]);
				
				$consulta_cont++;
				$fecha_inicial_ciclo = $end;
			}
			//FIN DE LA CONSULTA
			
			//DB QUERY STRING
			$strQuery2 = "SELECT (MAX(ener_in) - MIN(ener_in)) AS consumo, (MAX(ener_out) - MIN(ener_out)) AS inyeccion  
			FROM tbl_datos_medidor	
			WHERE id_disp = $medidor
			AND fch_dato BETWEEN '$fecha_inicial_ciclo_2' AND '$fecha_final_ciclo_2'		
			";

			//DB QUERY
			$resRAW2 = pg_query($con,$strQuery2);		

			while($dataFetch2 = pg_fetch_assoc($resRAW2))
			{
				$dataRaw2[] = $dataFetch2;
			}	

			for($i=0; $i < count($dataRaw2) ; $i++){
				$consumoRaw2[] = abs((float)($dataRaw2[$i]['consumo']));
				$inyeccionRaw2[] = abs((float)($dataRaw2[$i]['inyeccion']));
				$inyeccionCLPRaw2[] = (float)number_format(abs(($dataRaw2[$i]['inyeccion'])) * $precio_energia, 0, '.', '');
			}
        }		

        $data = Array(
            'consumo' => $consumoRaw,
			'generacion' => $inyeccionRaw,
			'generacionCLP' => $inyeccionCLPRaw,
            'consumoTotal' => $consumoRaw2,
			'generacionTotal' => $inyeccionRaw2,
			'generacionCLPTotal' => array_sum($inyeccionCLPRaw),			
			'horas' => $horasRaw,
			'subtitulo' => $subtitulo,
			'initial_date' => date("d/m/Y", strtotime($fecha_inicial_ciclo_2)),
			'final_date' => date("d/m/Y", strtotime($fecha_final_ciclo_2)),			
			'barra' => $barraEmpresaBandera,
			'modalidad' => $modalidadEmpresaBandera,
			'precio_energia' => number_format($precio_energia, 3, ',', '.'),
        );

        return $data;		
	}
	
	public function GraficaEnergiaVSAnio(Request $request){
        date_default_timezone_set('America/Santiago');
        $data = Array();
        $empresa_id = $request->empresa;
        $conexion = Conexion::where('id_empresa', $empresa_id)->first();
        $host = $conexion == null ? 0 : $conexion->host;
        $pfv = $conexion == null ? 0 : $conexion->id_parq;
        $medidor = $conexion == null ? 0 : $conexion->id_medidor;
        $radsensor = $conexion == null ? 0 : $conexion->id_radsensor;
        $database_name = $conexion == null ? 0 : $conexion->db;
		$subtitulo = $conexion == null ? 'no existe conexion' : $conexion->nombre_parq;			     
		$inicial_date =  $request->anio.'-01-01';
		$inicio = $inicial_date.' 00:00:00';		
		$fin = date("Y-m-t", strtotime($inicial_date)).' 23:59:59';	
		//Modalidad de Calculo
		$modalidad_calculo_id = 1;
		$modalidad = ModalidadCalculoEmpresas::where('id_empresa', $empresa_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first() == null ? $modalidad_calculo_id = 0 : ModalidadCalculoEmpresas::where('id_empresa', $empresa_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first();		
		if($modalidad_calculo_id == 1){
			$modalidad_calculo_id = $modalidad->id_modalidad_calculo;
		}
		$modalidadEmpresaBandera = 1;
		$modalidadEmpresa = ModalidadCalculo::where('id', $modalidad_calculo_id)->first() == null ? $modalidadEmpresaBandera = '' : ModalidadCalculo::where('id', $modalidad_calculo_id)->first();
		if($modalidadEmpresaBandera == 1){
			$modalidadEmpresaBandera = $modalidadEmpresa->nombre;
		}

		//Barras
		$barra_id = 1;
		$barra = BarrasEmpresas::where('id_empresa', $empresa_id)->where('tipo_calculo', $modalidad_calculo_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first() == null ? $barra_id = 0 : BarrasEmpresas::where('id_empresa', $empresa_id)->where('tipo_calculo', $modalidad_calculo_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first();
		if($barra_id == 1){
			$barra_id = $barra->id_barras;
		}
		$barraEmpresaBandera = 1;
		$barraEmpresa = Barras::where('id', $barra_id)->first() == null ? $barraEmpresaBandera = '' : Barras::where('id', $barra_id)->first();
		if($barraEmpresaBandera == 1){
			$barraEmpresaBandera = $barraEmpresa->barra_human;
		}		

		//Definimos la modalidad del calculo Ejemplo PE o CMg
		$precio_energia = 1;	
		if ($modalidad_calculo_id == 1) {			
			$precio = PrecioEstabilizado::where('id_barra', $barra_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first() == null ? $precio_energia = 0 : PrecioEstabilizado::where('id_barra', $barra_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first();
		}else {
			$precio_energia = 0;
		}	

		$data = Array();

        $consumoRaw = Array();
		$inyeccionRaw = Array();
        $consumoRaw2 = Array();
		$inyeccionRaw2 = Array();	
		$inyeccionCLPRaw = Array();	
		$inyeccionCLPRaw2 = Array();	
		$dataRaw = Array();
		$dataRaw2 = Array();
		$horasRaw = Array();

		$consumoVSRaw = Array();
		$inyeccionVSRaw = Array();
        $consumoVSRaw2 = Array();
		$inyeccionVSRaw2 = Array();	
		$inyeccionCLPVSRaw = Array();	
		$inyeccionCLPVSRaw2 = Array();	
		$dataVSRaw = Array();
		$dataVSRaw2 = Array();
		$horasVSRaw = Array();

		if ($precio_energia == 1) {
			$precio_energia = $precio->precio_energia;
		}

        //DB CONNECTION
        if($host == 1){
            $host = config('app.host_1');
            $port = config('app.host_port_1');
            $password = config('app.host_password_1');
        }else if($host == 2){

        }else{
            $host = 0;
            $port = 0;
            $password = 0;            
        }

        if($host != 0){
			//INICIO DE LA CONSULTA 1
            $strConnect = "host = ".$host." port = ".$port." dbname = '".$database_name."' user = 'postgres' password = ".$password;
			$con = pg_connect($strConnect);
						
			$consulta_cont = 0;
			$fecha_inicial_ciclo = $inicio;
			$fecha_final_ciclo = $request->anio.'-12-31 23:59:59';

			$fecha_inicial_ciclo_2 = $inicio;
			$fecha_final_ciclo_2 = $request->anio.'-12-31 23:59:59';

			while(strtotime($fecha_inicial_ciclo) < strtotime($fecha_final_ciclo)){
				
				$end = strtotime('+1 month', strtotime($fecha_inicial_ciclo));
				$end = date('Y-m-d H:i:s', $end);
				$start = strtotime('-1 month', strtotime($end));
				$start = date('Y-m-d H:i:s', $start);

				if(strtotime($start) <= strtotime($fecha_final_ciclo))
				{
					//DB QUERY STRING
					$strQuery = "SELECT (MAX(ener_in) - MIN(ener_in)) AS consumo, (MAX(ener_out) - MIN(ener_out)) AS inyeccion  
					FROM tbl_datos_medidor	
					WHERE id_disp = $medidor
					AND fch_dato BETWEEN '$start' AND '$end'		
					";
				}

				//DB QUERY
				$resRAW = pg_query($con,$strQuery);					

				while($dataFetch = pg_fetch_assoc($resRAW))
				{
					$dataRaw[] = $dataFetch;
				}
				
				$fecha_expode_0 = $start;
				$fecha_expode_1 = explode(' ', $fecha_expode_0);
				$fecha_expode_2 = explode(':',$fecha_expode_1[1]);
				$fecha_expode_3 = $fecha_expode_2[0].':'.$fecha_expode_2[1];	

				$consumoRaw[] = abs((float)($dataRaw[$consulta_cont]['consumo']));
				if ($consulta_cont != 0) {
					$inyeccionRaw[] = abs((float)($dataRaw[$consulta_cont]['inyeccion'] + $inyeccionRaw[$consulta_cont - 1]));
				}else {
					$inyeccionRaw[] = abs((float)($dataRaw[$consulta_cont]['inyeccion']));
				}
				$horasRaw[] = TimeHelper::formatDateHumanAnio($fecha_expode_1[0]);
				
				$consulta_cont++;
				$fecha_inicial_ciclo = $end;
			}			
			
			//ACUMULATIVO 1
			$strQuery2 = "SELECT (MAX(ener_in) - MIN(ener_in)) AS consumo, (MAX(ener_out) - MIN(ener_out)) AS inyeccion  
			FROM tbl_datos_medidor	
			WHERE id_disp = $medidor
			AND fch_dato BETWEEN '$fecha_inicial_ciclo_2' AND '$fecha_final_ciclo_2'		
			";

			//DB QUERY
			$resRAW2 = pg_query($con,$strQuery2);		

			while($dataFetch2 = pg_fetch_assoc($resRAW2))
			{
				$dataRaw2[] = $dataFetch2;
			}	
			
			for($i=0; $i < count($dataRaw2) ; $i++){
				$consumoRaw2[] = abs((float)($dataRaw2[$i]['consumo']));
				$inyeccionRaw2[] = abs((float)($dataRaw2[$i]['inyeccion']));
			}

			//FIN DE LA CONSULTA 1

			//INICIO DE LA CONSULTA 2
			$consulta_cont = 0;
			$inicial_date =  ($request->anio - 1).'-01-01';
			$inicio = $inicial_date.' 00:00:00';		
			$fin = date("Y-m-t", strtotime($inicial_date)).' 23:59:59';	
			$fecha_inicial_ciclo = $inicio;
			$fecha_final_ciclo = ($request->anio - 1).'-12-31 23:59:59';

			$fecha_inicial_ciclo_2 = $inicio;
			$fecha_final_ciclo_2 = ($request->anio - 1).'-12-31 23:59:59';

			while(strtotime($fecha_inicial_ciclo) < strtotime($fecha_final_ciclo)){
				$end = strtotime('+1 month', strtotime($fecha_inicial_ciclo));
				$end = date('Y-m-d H:i:s', $end);
				$start = strtotime('-1 month', strtotime($end));
				$start = date('Y-m-d H:i:s', $start);
						
				if(strtotime($start) <= strtotime($fecha_final_ciclo))
				{
					//DB QUERY STRING
					$strQuery = "SELECT (MAX(ener_in) - MIN(ener_in)) AS consumo, (MAX(ener_out) - MIN(ener_out)) AS inyeccion  
					FROM tbl_datos_medidor	
					WHERE id_disp = $medidor
					AND fch_dato BETWEEN '$start' AND '$end'		
					";
				}

				//DB QUERY
				$resRAW = pg_query($con,$strQuery);					

				while($dataFetch = pg_fetch_assoc($resRAW))
				{
					$dataVSRaw[] = $dataFetch;
				}
				
				$fecha_expode_0 = $start;
				$fecha_expode_1 = explode(' ', $fecha_expode_0);
				$fecha_expode_2 = explode(':',$fecha_expode_1[1]);
				$fecha_expode_3 = $fecha_expode_2[0].':'.$fecha_expode_2[1];	

				$consumoVSRaw[] = abs((float)($dataVSRaw[$consulta_cont]['consumo']));
				if ($consulta_cont != 0) {
					$inyeccionVSRaw[] = abs((float)($dataVSRaw[$consulta_cont]['inyeccion'] + $inyeccionVSRaw[$consulta_cont - 1]));
				}else {
					$inyeccionVSRaw[] = abs((float)($dataVSRaw[$consulta_cont]['inyeccion']));
				}
				$horasVSRaw[] = TimeHelper::formatDateHumanAnio($fecha_expode_1[0]);
				
				$consulta_cont++;
				$fecha_inicial_ciclo = $end;
			}			
			
			//ACUMULATIVO 2
			$strQuery2 = "SELECT (MAX(ener_in) - MIN(ener_in)) AS consumo, (MAX(ener_out) - MIN(ener_out)) AS inyeccion  
			FROM tbl_datos_medidor	
			WHERE id_disp = $medidor
			AND fch_dato BETWEEN '$fecha_inicial_ciclo_2' AND '$fecha_final_ciclo_2'		
			";

			//DB QUERY
			$resRAW2 = pg_query($con,$strQuery2);		

			while($dataFetch2 = pg_fetch_assoc($resRAW2))
			{
				$dataVSRaw2[] = $dataFetch2;
			}	
			
			for($i=0; $i < count($dataRaw2) ; $i++){
				$consumoVSRaw2[] = abs((float)($dataVSRaw2[$i]['consumo']));
				$inyeccionVSRaw2[] = abs((float)($dataVSRaw2[$i]['inyeccion']));
			}

			//FIN DE LA CONSULTA 2			
        }		

        $data = Array(
			'generacion' => $inyeccionRaw,
			'generacionTotal' => $inyeccionRaw2,
			'consumo' => $consumoRaw,
			'consumoTotal' => $consumoRaw2,	
			'generacionVS1' => $inyeccionVSRaw,
			'generacionVS1Total' => $inyeccionVSRaw2,
			'consumoVS1' => $consumoVSRaw,
            'consumoVS1Total' => $consumoVSRaw2,			
			'horas' => $horasRaw,
			'horasVS1' => $horasVSRaw,
			'subtitulo' => $subtitulo,
			'barra' => $barraEmpresaBandera,
			'modalidad' => $modalidadEmpresaBandera,
			'precio_energia' => number_format($precio_energia, 3, ',', '.'),
        );

        return $data;		
	}
}