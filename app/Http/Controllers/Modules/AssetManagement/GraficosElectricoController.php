<?php

namespace App\Http\Controllers\Modules\AssetManagement;

use Illuminate\Http\Request;
use App\Helpers\TimeHelper;
use App\Http\Controllers\Controller;
use App\Models\Conexion;
use App\Models\Barras;
use App\Models\BarrasEmpresas;
use App\Models\ModalidadCalculo;
use App\Models\ModalidadCalculoEmpresas;
use App\Models\PrecioEstabilizado;
use App\Models\AssetManagement\Empresa;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection as Collection;

class GraficosElectricoController extends Controller
{
	public function GraficaPotenciaRange(Request $request){
        date_default_timezone_set('America/Santiago');
        $data = Array();
        $empresa_id = $request->empresa;
        $conexion = Conexion::where('id_empresa', $empresa_id)->first();
        $host = $conexion == null ? 0 : $conexion->host;
        $pfv = $conexion == null ? 0 : $conexion->id_parq;
        $medidor = $conexion == null ? 0 : $conexion->id_medidor;
        $radsensor = $conexion == null ? 0 : $conexion->id_radsensor;
        $database_name = $conexion == null ? 0 : $conexion->db;
		$subtitulo = $conexion == null ? 'no existe conexion' : $conexion->nombre_parq;			
		$dateRange = explode("-", $request->dateRange);
        $inicio = TimeHelper::formatDateRange($dateRange[0]);
		$fin = TimeHelper::formatDateRange($dateRange[1]);	
		$periodo = TimeHelper::getPeriodoRange($dateRange[1]);	
		//Modalidad de Calculo
		$modalidad_calculo_id = 1;
		$modalidad = ModalidadCalculoEmpresas::where('id_empresa', $empresa_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first() == null ? $modalidad_calculo_id = 0 : ModalidadCalculoEmpresas::where('id_empresa', $empresa_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first();		
		if($modalidad_calculo_id == 1){
			$modalidad_calculo_id = $modalidad->id_modalidad_calculo;
		}
		$modalidadEmpresaBandera = 1;
		$modalidadEmpresa = ModalidadCalculo::where('id', $modalidad_calculo_id)->first() == null ? $modalidadEmpresaBandera = '' : ModalidadCalculo::where('id', $modalidad_calculo_id)->first();
		if($modalidadEmpresaBandera == 1){
			$modalidadEmpresaBandera = $modalidadEmpresa->nombre;
		}

		//Barras
		$barra_id = 1;
		$barra = BarrasEmpresas::where('id_empresa', $empresa_id)->where('tipo_calculo', $modalidad_calculo_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first() == null ? $barra_id = 0 : BarrasEmpresas::where('id_empresa', $empresa_id)->where('tipo_calculo', $modalidad_calculo_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first();
		if($barra_id == 1){
			$barra_id = $barra->id_barras;
		}
		$barraEmpresaBandera = 1;
		$barraEmpresa = Barras::where('id', $barra_id)->first() == null ? $barraEmpresaBandera = '' : Barras::where('id', $barra_id)->first();
		if($barraEmpresaBandera == 1){
			$barraEmpresaBandera = $barraEmpresa->barra_human;
		}		

		//Definimos la modalidad del calculo Ejemplo PE o CMg
		$precio_energia = 1;	
		if ($modalidad_calculo_id == 1) {			
			$precio = PrecioEstabilizado::where('id_barra', $barra_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first() == null ? $precio_energia = 0 : PrecioEstabilizado::where('id_barra', $barra_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first();
		}else {
			$precio_energia = 0;
		}		
		
		$data = Array();
        $ptRaw = Array();
		$irRaw = Array();
		$prRaw = Array();
        $dataRaw = Array();
		$horasRaw = Array();	

		if ($precio_energia == 1) {
			$precio_energia = $precio->precio_energia;
		}

        //DB CONNECTION
        if($host == 1){
            $host = config('app.host_1');
            $port = config('app.host_port_1');
            $password = config('app.host_password_1');
        }else if($host == 2){

        }else{
            $host = 0;
            $port = 0;
            $password = 0;            
        }

        if($host != 0){
            //INICIO DE LA CONSULTA
            $strConnect = "host = ".$host." port = ".$port." dbname = '".$database_name."' user = 'postgres' password = ".$password;
            $con = pg_connect($strConnect);
    
			//DB QUERY STRING
			$strQuery = "SELECT pr.fch_dato AS fecha, pr.pp_rad_avg AS pr, 
			abs(med.pot_act_total) AS pt, 
			rad.rad_valor AS ir 
			FROM calcular_prnew($medidor,
			'$inicio', 
			'$fin' ) pr
			LEFT JOIN tbl_datos_medidor med ON med.fch_dato=pr.fch_dato AND med.id_disp=$medidor
			LEFT JOIN tbl_datos_radsensor rad ON rad.fch_dato=pr.fch_dato AND rad.id_disp=$radsensor		
			";	

			//DB QUERY
			$resRAW = pg_query($con,$strQuery);				

			while($dataFetch = pg_fetch_assoc($resRAW))
			{
				$dataRaw[] = $dataFetch;
			}	
			
			for($i=0; $i < count($dataRaw) ; $i++){
				$ptRaw[] = abs((float)($dataRaw[$i]['pt']));
				$irRaw[] = (float)($dataRaw[$i]['ir']);
				$prRaw[] = (float)($dataRaw[$i]['pr']);
				$horasRaw[] = (double)(TimeHelper::getDateTime($dataRaw[$i]['fecha'])*1000);
			}		

			if(count($ptRaw) == 0){
				//DB QUERY STRING
				$strQuery = "SELECT fecha.fecha, rad.rad_valor AS ir, med.pot_act_total AS pt FROM 
				(
				SELECT (dias.dia + horas.hora) fecha FROM
				(
				SELECT dia::date FROM generate_series('$inicio'::date, '$fin'::date, '1 day'::interval) dia
				) dias
				INNER JOIN 
				(
				SELECT dia.hora, 1 AS relac_dias
				FROM
				(
				SELECT horas.serieh + ('''' || date_part('minutes', minutos.seriem) || ' minutes''') ::interval AS hora
				FROM 
				(
				SELECT ('''' || generate_series(0,23) || ' hours''') ::interval  AS serieh
				) AS horas
				INNER JOIN 
				(
				SELECT ('''' || generate_series(0,45,15) || ' minutes''') ::interval AS seriem, 1 AS relac_horas
				) AS minutos
				ON minutos.relac_horas=1
				) dia
				) AS horas ON horas.relac_dias=1
				) fecha 
				LEFT JOIN tbl_datos_radsensor rad ON rad.fch_dato= fecha.fecha AND rad.id_disp=$radsensor
				LEFT JOIN tbl_datos_medidor med ON med.fch_dato= fecha.fecha AND med.id_disp=$medidor
				WHERE fecha.fecha <= CURRENT_TIMESTAMP
				ORDER BY fecha.fecha ASC		
				";

				//DB QUERY
				$resRAW = pg_query($con,$strQuery);				

				while($dataFetch = pg_fetch_assoc($resRAW))
				{
					$dataRaw[] = $dataFetch;
				}	
				
				for($i=0; $i < count($dataRaw) ; $i++){
					$ptRaw[] = abs((float)($dataRaw[$i]['pt']));
					$irRaw[] = (float)($dataRaw[$i]['ir']);
					$prRaw[] = 0;
					$horasRaw[] = (double)(TimeHelper::getDateTime($dataRaw[$i]['fecha'])*1000);
				}			
			}
        }		

        $data = Array(
            'pt' => $ptRaw,
			'ir' => $irRaw,
			'pr' => $prRaw,
            'horas' => $horasRaw,
            'subtitulo' => $subtitulo,
        );

        return $data;		
	}	
	
	public function GraficaVoltajesRange(Request $request){
        date_default_timezone_set('America/Santiago');
        $data = Array();
        $empresa_id = $request->empresa;
        $conexion = Conexion::where('id_empresa', $empresa_id)->first();
        $host = $conexion == null ? 0 : $conexion->host;
        $pfv = $conexion == null ? 0 : $conexion->id_parq;
        $medidor = $conexion == null ? 0 : $conexion->id_medidor;
        $radsensor = $conexion == null ? 0 : $conexion->id_radsensor;
        $database_name = $conexion == null ? 0 : $conexion->db;
		$subtitulo = $conexion == null ? 'no existe conexion' : $conexion->nombre_parq;			
		$dateRange = explode("-", $request->dateRange);
        $inicio = TimeHelper::formatDateRange($dateRange[0]);
		$fin = TimeHelper::formatDateRange($dateRange[1]);	
		$periodo = TimeHelper::getPeriodoRange($dateRange[1]);	
		//Modalidad de Calculo
		$modalidad_calculo_id = 1;
		$modalidad = ModalidadCalculoEmpresas::where('id_empresa', $empresa_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first() == null ? $modalidad_calculo_id = 0 : ModalidadCalculoEmpresas::where('id_empresa', $empresa_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first();		
		if($modalidad_calculo_id == 1){
			$modalidad_calculo_id = $modalidad->id_modalidad_calculo;
		}
		$modalidadEmpresaBandera = 1;
		$modalidadEmpresa = ModalidadCalculo::where('id', $modalidad_calculo_id)->first() == null ? $modalidadEmpresaBandera = '' : ModalidadCalculo::where('id', $modalidad_calculo_id)->first();
		if($modalidadEmpresaBandera == 1){
			$modalidadEmpresaBandera = $modalidadEmpresa->nombre;
		}

		//Barras
		$barra_id = 1;
		$barra = BarrasEmpresas::where('id_empresa', $empresa_id)->where('tipo_calculo', $modalidad_calculo_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first() == null ? $barra_id = 0 : BarrasEmpresas::where('id_empresa', $empresa_id)->where('tipo_calculo', $modalidad_calculo_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first();
		if($barra_id == 1){
			$barra_id = $barra->id_barras;
		}
		$barraEmpresaBandera = 1;
		$barraEmpresa = Barras::where('id', $barra_id)->first() == null ? $barraEmpresaBandera = '' : Barras::where('id', $barra_id)->first();
		if($barraEmpresaBandera == 1){
			$barraEmpresaBandera = $barraEmpresa->barra_human;
		}		

		//Definimos la modalidad del calculo Ejemplo PE o CMg
		$precio_energia = 1;	
		if ($modalidad_calculo_id == 1) {			
			$precio = PrecioEstabilizado::where('id_barra', $barra_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first() == null ? $precio_energia = 0 : PrecioEstabilizado::where('id_barra', $barra_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first();
		}else {
			$precio_energia = 0;
		}		
		
		$data = Array();
        $v1Raw = Array();
		$v2Raw = Array();
		$v3Raw = Array();
		$lst = Array();
		$lit = Array();
		$casa = Array();
        $dataRaw = Array();
		$horasRaw = Array();

		if ($precio_energia == 1) {
			$precio_energia = $precio->precio_energia;
		}

        //DB CONNECTION
        if($host == 1){
            $host = config('app.host_1');
            $port = config('app.host_port_1');
            $password = config('app.host_password_1');
        }else if($host == 2){

        }else{
            $host = 0;
            $port = 0;
            $password = 0;            
        }

        if($host != 0){
			//INICIO DE LA CONSULTA
			$strConnect = "host = ".$host." port = ".$port." dbname = '".$database_name."' user = 'postgres' password = ".$password;
			$con = pg_connect($strConnect);

			//DB QUERY STRING
			$strQuery = "SELECT fecha.fecha, med.vlin_ab AS v1, med.vlin_bc AS v2, med.vlin_ab AS v3  FROM 
			(
			SELECT (dias.dia + horas.hora) fecha FROM
			(
			SELECT dia::date FROM generate_series('$inicio'::date, '$fin'::date, '1 day'::interval) dia
			) dias
			INNER JOIN 
			(
			SELECT dia.hora, 1 AS relac_dias
			FROM
			(
			SELECT horas.serieh + ('''' || date_part('minutes', minutos.seriem) || ' minutes''') ::interval AS hora
			FROM 
			(
			SELECT ('''' || generate_series(0,23) || ' hours''') ::interval  AS serieh
			) AS horas
			INNER JOIN 
			(
			SELECT ('''' || generate_series(0,45,15) || ' minutes''') ::interval AS seriem, 1 AS relac_horas
			) AS minutos
			ON minutos.relac_horas=1
			) dia
			) AS horas ON horas.relac_dias=1
			) fecha 
			LEFT JOIN tbl_datos_medidor med ON med.fch_dato= fecha.fecha AND med.id_disp=$medidor	
			WHERE fecha.fecha <= CURRENT_TIMESTAMP
			ORDER BY fecha.fecha ASC		
			";

			//DB QUERY
			$resRAW = pg_query($con,$strQuery);		

			while($dataFetch = pg_fetch_assoc($resRAW))
			{
				$dataRaw[] = $dataFetch;
			}	
			
			for($i=0; $i < count($dataRaw) ; $i++){
				$v1Raw[] = abs((float)($dataRaw[$i]['v1']));
				$v2Raw[] = abs((float)($dataRaw[$i]['v2']));
				$v3Raw[] = abs((float)($dataRaw[$i]['v3']));
				$horasRaw[] = (double)(TimeHelper::getDateTime($dataRaw[$i]['fecha'])*1000);
				$casa[] = 13200;
				$lit[] = 12342;
				$lst[] = 14058;			
			}
        }		

        $data = Array(
			'v1' => $v1Raw,
			'v2' => $v2Raw,
			'v3' => $v3Raw,		
			'lst' => $lst,
			'lit' => $lit,
			'pfv' => $casa,
			'horas' => $horasRaw,
			'nombre' => 'Medidor',
			'subtitulo' => $subtitulo,
        );

        return $data;		
	}
	
	public function GraficaInversoresRange(Request $request){
        date_default_timezone_set('America/Santiago');
        $data = Array();
        $empresa_id = $request->empresa;
        $conexion = Conexion::where('id_empresa', $empresa_id)->first();
        $host = $conexion == null ? 0 : $conexion->host;
        $pfv = $conexion == null ? 0 : $conexion->id_parq;
        $medidor = $conexion == null ? 0 : $conexion->id_medidor;
        $radsensor = $conexion == null ? 0 : $conexion->id_radsensor;
        $database_name = $conexion == null ? 0 : $conexion->db;
		$subtitulo = $conexion == null ? 'no existe conexion' : $conexion->nombre_parq;			
		$dateRange = explode("-", $request->dateRange);
        $inicio = TimeHelper::formatDateRange($dateRange[0]);
		$fin = TimeHelper::formatDateRange($dateRange[1]);	
		$periodo = TimeHelper::getPeriodoRange($dateRange[1]);	
		//Modalidad de Calculo
		$modalidad_calculo_id = 1;
		$modalidad = ModalidadCalculoEmpresas::where('id_empresa', $empresa_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first() == null ? $modalidad_calculo_id = 0 : ModalidadCalculoEmpresas::where('id_empresa', $empresa_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first();		
		if($modalidad_calculo_id == 1){
			$modalidad_calculo_id = $modalidad->id_modalidad_calculo;
		}
		$modalidadEmpresaBandera = 1;
		$modalidadEmpresa = ModalidadCalculo::where('id', $modalidad_calculo_id)->first() == null ? $modalidadEmpresaBandera = '' : ModalidadCalculo::where('id', $modalidad_calculo_id)->first();
		if($modalidadEmpresaBandera == 1){
			$modalidadEmpresaBandera = $modalidadEmpresa->nombre;
		}

		//Barras
		$barra_id = 1;
		$barra = BarrasEmpresas::where('id_empresa', $empresa_id)->where('tipo_calculo', $modalidad_calculo_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first() == null ? $barra_id = 0 : BarrasEmpresas::where('id_empresa', $empresa_id)->where('tipo_calculo', $modalidad_calculo_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first();
		if($barra_id == 1){
			$barra_id = $barra->id_barras;
		}
		$barraEmpresaBandera = 1;
		$barraEmpresa = Barras::where('id', $barra_id)->first() == null ? $barraEmpresaBandera = '' : Barras::where('id', $barra_id)->first();
		if($barraEmpresaBandera == 1){
			$barraEmpresaBandera = $barraEmpresa->barra_human;
		}		

		//Definimos la modalidad del calculo Ejemplo PE o CMg
		$precio_energia = 1;	
		if ($modalidad_calculo_id == 1) {			
			$precio = PrecioEstabilizado::where('id_barra', $barra_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first() == null ? $precio_energia = 0 : PrecioEstabilizado::where('id_barra', $barra_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first();
		}else {
			$precio_energia = 0;
		}		
		
		$data = Array();
		$inversor = Array();
		$potencia = Array();
        $dataRaw = Array();
		$horasRaw = Array();

		if ($precio_energia == 1) {
			$precio_energia = $precio->precio_energia;
		}

        //DB CONNECTION
        if($host == 1){
            $host = config('app.host_1');
            $port = config('app.host_port_1');
            $password = config('app.host_password_1');
        }else if($host == 2){

        }else{
            $host = 0;
            $port = 0;
            $password = 0;            
        }

        if($host != 0){
			//INICIO DE LA CONSULTA
			$strConnect = "host = ".$host." port = ".$port." dbname = '".$database_name."' user = 'postgres' password = ".$password;
			$con = pg_connect($strConnect);

			//CERNICALO
			if($pfv == 4){
				if($medidor == 301){
					//CGE
					//DB QUERY STRING
					$strQuery = "SELECT inv.fch_dato AS fecha, disp.id_local AS inversor, inv.id_disp, inv.pot_act_total AS potencia, est.nom_estatus AS inversor_status, inv.cte_string1, inv.cte_string2, inv.cte_string3, inv.cte_string4, inv.cte_string5, inv.cte_string6, inv.cte_string7, inv.cte_string8
					FROM   tbl_datos_inversor inv
					INNER JOIN tbl_dispositivos disp ON disp.id_disp=inv.id_disp
					INNER JOIN tbl_estatus_inversores est ON est.id_estatus=inv.id_estatus
					WHERE  inv.id_disp BETWEEN 307 AND 346
					AND
					inv.fch_dato between '$inicio'  AND 
				'$fin' 
					ORDER  BY inv.fch_dato, inv.id_disp ASC
					";				
				}else if($medidor == 391) {
					//COPELEC
					//DB QUERY STRING
					$strQuery = "SELECT inv.fch_dato AS fecha, disp.id_local AS inversor, inv.id_disp, inv.pot_act_total AS potencia, est.nom_estatus AS inversor_status, inv.cte_string1, inv.cte_string2, inv.cte_string3, inv.cte_string4, inv.cte_string5, inv.cte_string6, inv.cte_string7, inv.cte_string8
					FROM   tbl_datos_inversor inv
					INNER JOIN tbl_dispositivos disp ON disp.id_disp=inv.id_disp
					INNER JOIN tbl_estatus_inversores est ON est.id_estatus=inv.id_estatus
					WHERE  inv.id_disp BETWEEN 348 AND 390
					AND
					inv.fch_dato between '$inicio'  AND 
				'$fin' 
					ORDER  BY inv.fch_dato, inv.id_disp ASC
					";				
				}			
			}elseif($pfv == 11){
				//PIQUERO
				//DB QUERY STRING
				$strQuery = "SELECT datos.fch_dato AS fecha, datos.id_local AS inversor, round(((datos.volt_dc * datos.cte_total) / 1000)::numeric,2)  AS potencia, datos.cte_dc_in1, datos.cte_dc_in2, datos.cte_dc_in3, datos.cte_dc_in4, datos.cte_dc_in5, datos.cte_dc_in6, datos.cte_dc_in7, datos.cte_dc_in8, datos.cte_dc_in9, datos.cte_dc_in10, datos.cte_dc_in11, datos.cte_dc_in12, datos.cte_dc_in13, datos.cte_dc_in14, datos.cte_dc_in15, datos.cte_dc_in16, datos.cte_dc_in17, datos.cte_dc_in18, datos.cte_dc_in19, datos.cte_dc_in20, datos.cte_dc_in21, datos.cte_dc_in22, datos.cte_dc_in23, datos.cte_dc_in24 
				FROM 
				(
				SELECT cbx.fch_dato, disp.id_local, volt_dc, cte_dc_in1, cte_dc_in2, cte_dc_in3, cte_dc_in4, cte_dc_in5, cte_dc_in6, cte_dc_in7, cte_dc_in8, cte_dc_in9, cte_dc_in10, cte_dc_in11, cte_dc_in12, cte_dc_in13, cte_dc_in14, cte_dc_in15, cte_dc_in16, cte_dc_in17, cte_dc_in18, cte_dc_in19, cte_dc_in20, cte_dc_in21, cte_dc_in22, cte_dc_in23, cte_dc_in24,
				(cte_dc_in1 + cte_dc_in2 + cte_dc_in3 + cte_dc_in4 + cte_dc_in5 + cte_dc_in6 + cte_dc_in7 + cte_dc_in8 +
				cte_dc_in9 + cte_dc_in10 + cte_dc_in11 + cte_dc_in12 + cte_dc_in13 + cte_dc_in14 + cte_dc_in15 + cte_dc_in16 +
				cte_dc_in17 + cte_dc_in18 + cte_dc_in19 + cte_dc_in20 + cte_dc_in21 + cte_dc_in22 + cte_dc_in23 + cte_dc_in24) cte_total
				FROM tbl_datos_combinerbox_sma cbx
				INNER JOIN tbl_dispositivos disp ON disp.id_disp=cbx.id_disp
				WHERE cbx.id_parq=$pfv AND disp.id_tipo_disp=9 AND
				cbx.fch_dato between '$inicio' AND 
				'$fin' 
				ORDER  BY cbx.fch_dato, disp.id_local
				) datos";	
			}elseif($pfv == 13){
				//SANTA LAURA
				//DB QUERY STRING
				$strQuery = "SELECT inv.fch_dato AS fecha, disp.id_local AS inversor, inv.id_disp, inv.pot_act_total AS potencia, est.nom_estatus AS inversor_status, inv.cte_string1, inv.cte_string2, inv.cte_string3, inv.cte_string4, inv.cte_string5, inv.cte_string6, inv.cte_string7,  inv.cte_string8, inv.cte_string9, inv.cte_string10, inv.cte_string11, inv.cte_string12
				FROM   tbl_datos_inversor_huawei100 inv
				INNER JOIN tbl_dispositivos disp ON disp.id_disp=inv.id_disp
				INNER JOIN tbl_estatus_inversores est ON est.id_estatus=inv.id_estatus
				WHERE  inv.id_disp = ANY 
				(SELECT id_disp FROM tbl_dispositivos WHERE id_parq=$pfv AND id_tipo_disp=5) AND
				inv.fch_dato between '$inicio'  AND '$fin' 
				ORDER  BY inv.fch_dato, inv.id_disp ASC	
				";	
			}elseif($pfv == 15){
				//LECHUZAS
				//DB QUERY STRING
				$strQuery = "SELECT inv.fch_dato AS fecha, disp.id_local AS inversor, inv.id_disp, inv.pot_act_total AS potencia, est.nom_estatus AS inversor_status, inv.cte_string1, inv.cte_string2, inv.cte_string3, inv.cte_string4, inv.cte_string5, inv.cte_string6, inv.cte_string7,  inv.cte_string8, inv.cte_string9, inv.cte_string10, inv.cte_string11, inv.cte_string12
				FROM   tbl_datos_inversor_huawei100 inv
				INNER JOIN tbl_dispositivos disp ON disp.id_disp=inv.id_disp
				INNER JOIN tbl_estatus_inversores est ON est.id_estatus=inv.id_estatus
				WHERE  inv.id_disp = ANY 
				(SELECT id_disp FROM tbl_dispositivos WHERE id_parq=$pfv AND id_tipo_disp=5) AND
				inv.fch_dato between '$inicio'  AND '$fin' 
				ORDER  BY inv.fch_dato, inv.id_disp ASC	
				";	
			}elseif($pfv == 16){
				//ILLALOLEN
				//DB QUERY STRING
				$strQuery = "SELECT  
				CASE disp.id_local WHEN 11 THEN 1 WHEN 12 THEN 2  WHEN 13 THEN 3  WHEN 14 THEN 4  WHEN 15 THEN 5   
				WHEN 16 THEN 6 WHEN 21 THEN 7  WHEN 22 THEN 8  WHEN 23 THEN  9  WHEN 24 THEN 10 
				WHEN 25 THEN 11 WHEN 26 THEN 12  WHEN 27 THEN 13  WHEN 31 THEN 14  WHEN 32 THEN 15 
				WHEN 33 THEN 16 WHEN 34 THEN 17  WHEN 35 THEN 18  WHEN 36 THEN  19  WHEN 37 THEN 20 
				WHEN 41 THEN 21 WHEN 42 THEN 22  WHEN 43 THEN 23  WHEN 44 THEN 24  WHEN 45 THEN 25 
				WHEN 46 THEN 26 WHEN 51 THEN 27  WHEN 52 THEN 28  WHEN 53 THEN  29  WHEN 54 THEN 30 
				WHEN 55 THEN 31 WHEN 56 THEN 32  WHEN 57 THEN 33  WHEN 61 THEN  34  WHEN 62 THEN 35
				WHEN 63 THEN 36 WHEN 64 THEN 37  WHEN 65 THEN 38  WHEN 66 THEN  39  WHEN 67 THEN 40 
				END inversor,
				inv.fch_dato AS fecha, disp.id_local AS inversor_2, inv.id_disp, inv.pot_act_total AS potencia
				FROM   tbl_datos_inversor_stp60 inv
				INNER JOIN tbl_dispositivos disp ON disp.id_disp=inv.id_disp
				WHERE  inv.id_disp = ANY 
				(SELECT id_disp FROM tbl_dispositivos WHERE id_parq=16 AND id_tipo_disp=5) AND
				inv.fch_dato between '$inicio'  AND '$fin'
				ORDER  BY inv.fch_dato, disp.id_local ASC
				";	
			}else{
				//ALL OTHER PFV
				//DB QUERY STRING
				$strQuery = "SELECT inv.fch_dato AS fecha, disp.id_local AS inversor, inv.id_disp, inv.pot_act_total AS potencia, est.nom_estatus AS inversor_status, inv.cte_string1, inv.cte_string2, inv.cte_string3, inv.cte_string4, inv.cte_string5, inv.cte_string6, inv.cte_string7, inv.cte_string8
				FROM   tbl_datos_inversor inv
				INNER JOIN tbl_dispositivos disp ON disp.id_disp=inv.id_disp
				INNER JOIN tbl_estatus_inversores est ON est.id_estatus=inv.id_estatus
				WHERE  inv.id_disp = ANY 
				(SELECT id_disp FROM tbl_dispositivos WHERE id_parq=$pfv AND id_tipo_disp=5) AND
				inv.fch_dato between '$inicio'  AND '$fin' 
				ORDER  BY inv.fch_dato, inv.id_disp ASC	
				";
			}


		//DB QUERY
		$resRAW = pg_query($con,$strQuery);		

        while($dataFetch = pg_fetch_assoc($resRAW))
        {
            $dataRaw[] = $dataFetch;
		}	

		$count_data_menus = (count($dataRaw) - 1);

		//START FOR
		for($i=0; $i < count($dataRaw) ; $i++){
			$inversor = $dataRaw[$i]['inversor'];

			$inversor_array[$inversor][] = $inversor;
			$potencia_array[$inversor][] = abs((float)($dataRaw[$i]['potencia']));
			$horasRaw_array[$inversor][] = (double)(TimeHelper::getDateTime($dataRaw[$i]['fecha'])*1000);	
			$fecha_array[$inversor][] = $dataRaw[$i]['fecha'];	

			
			if($pfv == 11){
				//PIQUERO
				$invCte1_array[$inversor][] = $dataRaw[$i]['cte_dc_in1'];
				$invCte2_array[$inversor][] = $dataRaw[$i]['cte_dc_in2'];
				$invCte3_array[$inversor][] = $dataRaw[$i]['cte_dc_in3'];
				$invCte4_array[$inversor][] = $dataRaw[$i]['cte_dc_in4'];
				$invCte5_array[$inversor][] = $dataRaw[$i]['cte_dc_in5'];
				$invCte6_array[$inversor][] = $dataRaw[$i]['cte_dc_in6'];
				$invCte7_array[$inversor][] = $dataRaw[$i]['cte_dc_in7'];
				$invCte8_array[$inversor][] = $dataRaw[$i]['cte_dc_in8'];
				$invCte9_array[$inversor][] = $dataRaw[$i]['cte_dc_in9'];
				$invCte10_array[$inversor][] = $dataRaw[$i]['cte_dc_in10'];
				$invCte11_array[$inversor][] = $dataRaw[$i]['cte_dc_in11'];
				$invCte12_array[$inversor][] = $dataRaw[$i]['cte_dc_in12'];
				$invCte13_array[$inversor][] = $dataRaw[$i]['cte_dc_in13'];
				$invCte14_array[$inversor][] = $dataRaw[$i]['cte_dc_in14'];
				$invCte15_array[$inversor][] = $dataRaw[$i]['cte_dc_in15'];
				$invCte16_array[$inversor][] = $dataRaw[$i]['cte_dc_in16'];
				$invCte17_array[$inversor][] = $dataRaw[$i]['cte_dc_in17'];
				$invCte18_array[$inversor][] = $dataRaw[$i]['cte_dc_in18'];
				$invCte19_array[$inversor][] = $dataRaw[$i]['cte_dc_in19'];
				$invCte20_array[$inversor][] = $dataRaw[$i]['cte_dc_in20'];
				$invCte21_array[$inversor][] = $dataRaw[$i]['cte_dc_in21'];
				$invCte22_array[$inversor][] = $dataRaw[$i]['cte_dc_in22'];
				$invCte23_array[$inversor][] = $dataRaw[$i]['cte_dc_in23'];
				$invCte24_array[$inversor][] = $dataRaw[$i]['cte_dc_in24'];
			}elseif($pfv == 13){
				//SANTA LAURA
				$status_array[$inversor][] = $dataRaw[$i]['inversor_status'];

				$invCte1_array[$inversor][] = $dataRaw[$i]['cte_string1'];
				$invCte2_array[$inversor][] = $dataRaw[$i]['cte_string2'];
				$invCte3_array[$inversor][] = $dataRaw[$i]['cte_string3'];
				$invCte4_array[$inversor][] = $dataRaw[$i]['cte_string4'];
				$invCte5_array[$inversor][] = $dataRaw[$i]['cte_string5'];
				$invCte6_array[$inversor][] = $dataRaw[$i]['cte_string6'];
				$invCte7_array[$inversor][] = $dataRaw[$i]['cte_string7'];
				$invCte8_array[$inversor][] = $dataRaw[$i]['cte_string8'];
				$invCte9_array[$inversor][] = $dataRaw[$i]['cte_string9'];
				$invCte10_array[$inversor][] = $dataRaw[$i]['cte_string10'];
				$invCte11_array[$inversor][] = $dataRaw[$i]['cte_string11'];
				$invCte12_array[$inversor][] = $dataRaw[$i]['cte_string12'];
			}elseif($pfv == 16){
				//ILLALOLEN
				$inversor_array_2[$inversor][] = $dataRaw[$i]['inversor_2'];
			}else{
				//ALL AD CAPITAL PFV
				$status_array[$inversor][] = $dataRaw[$i]['inversor_status'];

				$invCte1_array[$inversor][] = $dataRaw[$i]['cte_string1'];
				$invCte2_array[$inversor][] = $dataRaw[$i]['cte_string2'];
				$invCte3_array[$inversor][] = $dataRaw[$i]['cte_string3'];
				$invCte4_array[$inversor][] = $dataRaw[$i]['cte_string4'];
				$invCte5_array[$inversor][] = $dataRaw[$i]['cte_string5'];
				$invCte6_array[$inversor][] = $dataRaw[$i]['cte_string6'];
				$invCte7_array[$inversor][] = $dataRaw[$i]['cte_string7'];
				$invCte8_array[$inversor][] = $dataRaw[$i]['cte_string8'];
			}

		}
		//END FOR	

		$inversores_count = count($inversor_array);
		$inversores_min = min($inversor_array);
		$inversores_max = max($inversor_array);

		if($pfv == 11){
			//PIQUERO
			for ($i=1; $i <= $inversores_count ; $i++) { 
				if(isset($inversor_array[$i])){
					$inversor_final_array[] = end($inversor_array[$i]);
					$fecha_final_array[] = end($fecha_array[$i]);
					$potencia_final_array[] = end($potencia_array[$i]);
					$invCte1_final_array[] = end($invCte1_array[$i]);
					$invCte2_final_array[] = end($invCte2_array[$i]);
					$invCte3_final_array[] = end($invCte3_array[$i]);
					$invCte4_final_array[] = end($invCte4_array[$i]);
					$invCte5_final_array[] = end($invCte5_array[$i]);
					$invCte6_final_array[] = end($invCte6_array[$i]);
					$invCte7_final_array[] = end($invCte7_array[$i]);
					$invCte8_final_array[] = end($invCte8_array[$i]);
					$invCte9_final_array[] = end($invCte9_array[$i]);
					$invCte10_final_array[] = end($invCte10_array[$i]);
					$invCte11_final_array[] = end($invCte11_array[$i]);
					$invCte12_final_array[] = end($invCte12_array[$i]);
					$invCte13_final_array[] = end($invCte13_array[$i]);
					$invCte14_final_array[] = end($invCte14_array[$i]);
					$invCte15_final_array[] = end($invCte15_array[$i]);
					$invCte16_final_array[] = end($invCte16_array[$i]);
					$invCte17_final_array[] = end($invCte17_array[$i]);
					$invCte18_final_array[] = end($invCte18_array[$i]);
					$invCte19_final_array[] = end($invCte19_array[$i]);
					$invCte20_final_array[] = end($invCte20_array[$i]);
					$invCte21_final_array[] = end($invCte21_array[$i]);
					$invCte22_final_array[] = end($invCte22_array[$i]);
					$invCte23_final_array[] = end($invCte23_array[$i]);
					$invCte24_final_array[] = end($invCte24_array[$i]);
				}
			}

			$data = Array(	
				'inversor' => $inversor_array,
				'potencia' => $potencia_array,		
				'horas' => $horasRaw_array,	
				'inversor_final_array' => $inversor_final_array,
				'fecha_final_array' => $fecha_final_array,				
				'potencia_final_array' => $potencia_final_array,
				'invCte1_final_array' => $invCte1_final_array,	
				'invCte2_final_array' => $invCte2_final_array,
				'invCte3_final_array' => $invCte3_final_array,
				'invCte4_final_array' => $invCte4_final_array,
				'invCte5_final_array' => $invCte5_final_array,
				'invCte6_final_array' => $invCte6_final_array,
				'invCte7_final_array' => $invCte7_final_array,
				'invCte8_final_array' => $invCte8_final_array,
				'invCte9_final_array' => $invCte9_final_array,
				'invCte10_final_array' => $invCte10_final_array,
				'invCte11_final_array' => $invCte11_final_array,
				'invCte12_final_array' => $invCte12_final_array,
				'invCte13_final_array' => $invCte13_final_array,
				'invCte14_final_array' => $invCte14_final_array,
				'invCte15_final_array' => $invCte15_final_array,
				'invCte16_final_array' => $invCte16_final_array,
				'invCte17_final_array' => $invCte17_final_array,
				'invCte18_final_array' => $invCte18_final_array,
				'invCte19_final_array' => $invCte19_final_array,
				'invCte20_final_array' => $invCte20_final_array,
				'invCte21_final_array' => $invCte21_final_array,
				'invCte22_final_array' => $invCte22_final_array,
				'invCte23_final_array' => $invCte23_final_array,
				'invCte24_final_array' => $invCte24_final_array,
				'invCte8_array' => $invCte8_array,	
				'inversores_count' => $inversores_count,
				'pfv' => $pfv,
				'subtitulo' => $subtitulo,
				'nombre' => 'Inversores',			
			);
		}elseif($pfv == 13){
			//SANTA LAURA
			for ($i=1; $i <= $inversores_count ; $i++) { 
				if(isset($inversor_array[$i])){
					$inversor_final_array[] = end($inversor_array[$i]);
					$fecha_final_array[] = end($fecha_array[$i]);
					$status_final_array[] = end($status_array[$i]);
					$potencia_final_array[] = end($potencia_array[$i]);
					$invCte1_final_array[] = end($invCte1_array[$i]);
					$invCte2_final_array[] = end($invCte2_array[$i]);
					$invCte3_final_array[] = end($invCte3_array[$i]);
					$invCte4_final_array[] = end($invCte4_array[$i]);
					$invCte5_final_array[] = end($invCte5_array[$i]);
					$invCte6_final_array[] = end($invCte6_array[$i]);
					$invCte7_final_array[] = end($invCte7_array[$i]);
					$invCte8_final_array[] = end($invCte8_array[$i]);
					$invCte9_final_array[] = end($invCte9_array[$i]);
					$invCte10_final_array[] = end($invCte10_array[$i]);
					$invCte11_final_array[] = end($invCte11_array[$i]);
					$invCte12_final_array[] = end($invCte12_array[$i]);
				}
			}

			$data = Array(	
				'inversor' => $inversor_array,
				'potencia' => $potencia_array,		
				'horas' => $horasRaw_array,	
				'inversor_final_array' => $inversor_final_array,
				'fecha_final_array' => $fecha_final_array,		
				'status_final_array' => $status_final_array,			
				'potencia_final_array' => $potencia_final_array,
				'invCte1_final_array' => $invCte1_final_array,	
				'invCte2_final_array' => $invCte2_final_array,
				'invCte3_final_array' => $invCte3_final_array,
				'invCte4_final_array' => $invCte4_final_array,
				'invCte5_final_array' => $invCte5_final_array,
				'invCte6_final_array' => $invCte6_final_array,
				'invCte7_final_array' => $invCte7_final_array,
				'invCte8_final_array' => $invCte8_final_array,
				'invCte9_final_array' => $invCte9_final_array,
				'invCte10_final_array' => $invCte10_final_array,
				'invCte11_final_array' => $invCte11_final_array,
				'invCte12_final_array' => $invCte12_final_array,
				'invCte8_array' => $invCte8_array,	
				'inversores_count' => $inversores_count,
				'pfv' => $pfv,
				'subtitulo' => $subtitulo,
				'nombre' => 'Inversores',			
			);
		}elseif($pfv == 16){
			//ILLALOLEN
			for ($i=1; $i <= $inversores_count ; $i++) { 
				$inversor_final_array[] = end($inversor_array[$i]);
				$fecha_final_array[] = end($fecha_array[$i]);
				$potencia_final_array[] = end($potencia_array[$i]);
			}

			$data = Array(	
				'inversor' => $inversor_array_2,
				'potencia' => $potencia_array,		
				'horas' => $horasRaw_array,	
				'inversor_final_array' => $inversor_final_array,
				'fecha_final_array' => $fecha_final_array,				
				'potencia_final_array' => $potencia_final_array,	
				'inversores_count' => $inversores_count,
				'pfv' => $pfv,
				'subtitulo' => $subtitulo,
				'nombre' => 'Inversores',			
			);
		}else{	
			//ALL AD CAPITAL PFV		
			for ($i=1; $i <= $inversores_count ; $i++) { 
				if(isset($inversor_array[$i])){
					$inversor_final_array[] = end($inversor_array[$i]);
					$fecha_final_array[] = end($fecha_array[$i]);
					$status_final_array[] = end($status_array[$i]);
					$potencia_final_array[] = end($potencia_array[$i]);
					$invCte1_final_array[] = end($invCte1_array[$i]);
					$invCte2_final_array[] = end($invCte2_array[$i]);
					$invCte3_final_array[] = end($invCte3_array[$i]);
					$invCte4_final_array[] = end($invCte4_array[$i]);
					$invCte5_final_array[] = end($invCte5_array[$i]);
					$invCte6_final_array[] = end($invCte6_array[$i]);
					$invCte7_final_array[] = end($invCte7_array[$i]);
					$invCte8_final_array[] = end($invCte8_array[$i]);
				}
			}

			$data = Array(	
				'inversor' => $inversor_array,
				'potencia' => $potencia_array,		
				'horas' => $horasRaw_array,	
				'inversor_final_array' => $inversor_final_array,
				'fecha_final_array' => $fecha_final_array,		
				'status_final_array' => $status_final_array,			
				'potencia_final_array' => $potencia_final_array,
				'invCte1_final_array' => $invCte1_final_array,	
				'invCte2_final_array' => $invCte2_final_array,
				'invCte3_final_array' => $invCte3_final_array,
				'invCte4_final_array' => $invCte4_final_array,
				'invCte5_final_array' => $invCte5_final_array,
				'invCte6_final_array' => $invCte6_final_array,
				'invCte7_final_array' => $invCte7_final_array,
				'invCte8_final_array' => $invCte8_final_array,
				'invCte8_array' => $invCte8_array,	
				'inversores_count' => $inversores_count,
				'pfv' => $pfv,
				'subtitulo' => $subtitulo,
				'nombre' => 'Inversores',			
			);								
		}		
	}
        return $data;		
	}
}