<?php

namespace App\Http\Controllers\Modules\AssetManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AssetManagement\Energia;
use App\Models\AssetManagement\Empresa;
use App\Models\AssetManagement\EnergiaMedidoresCEN as EnegiaCEN;
use App\Models\AssetManagement\EnergiaMedidoresPFV as EnergiaPFV;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection as Collection;
use Lang;

class BalanceEnergiaController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('modules.asset-management.balance-energia.index');
    }

    /**
     * Verifica si existe data en ENERGIA_MEDIDORES_OE (OAM)
     * @param  Request $request [description]
     * @return JSON
     * @author Carlos Anselmi <Carlosanselmi2@gmail.com>
     */
    public function AjaxCheckPerido(Request $request)
    {
        $request->periodo = $this->getPeriodo($request->mes,$request->anio);

        // Verifica que exista data en CEN
        $energiaCENCheck = $this->checkEnergia(new EnegiaCEN(), $request->empresa_xls, $request->periodo);
        if(empty($energiaCENCheck)) {
            return response()->json(['response' => view('modules.asset-management.balance-energia.response.message')
                        ->with('message', lang::get('app.La data de Energía Medidores CEN aun no está disponible para el periodo seleccionado'))
                        ->with('type', 'warning')
                        ->render()
                    ], 200);
        }

        // Verifica que exista data en PFV
        $energiaPFVCheck = $this->checkEnergia(new EnergiaPFV(), $request->empresa_xls, $request->periodo);
        if(empty($energiaPFVCheck)) {
            return response()->json(['response' => view('modules.asset-management.balance-energia.response.message')
                        ->with('message', lang::get('app.La data de Energía Medidores PFV aun no está disponible para el periodo seleccionado'))
                        ->with('type', 'warning')
                        ->render()
                    ], 200);
        }

        $energiaCEN = $this->balanceEnergia(new EnegiaCEN(), $request->empresa_xls, $request->periodo);
        $energiaPFV = $this->balanceEnergia(new EnergiaPFV(), $request->empresa_xls, $request->periodo);

        return $this->messageData(['energiaPFV' => $energiaPFV, 'energiaCEN' => $energiaCEN]);
    }

    /**
     * Verifica si en el modelo (energiaCEN o EnergiaPFV) existe data para poder procesar.
     * @param Model $model           Modelo el cual se verificara la existencia de data.
     * @param Number $empresa_id     ID de la empressa a buscar
     * @param String $periodo        periodo MM/YY
     * @return void
     */
    public function checkEnergia($model, $empresa_id, $perido)
    {
        return $model::where('id_empresa',$empresa_id)->where('periodo',$perido)->first();
    }

    public function balanceEnergia($model, $empresa_id, $perido)
    {
        $data = Array();
        $balanceEnergia = $model::where('id_empresa',$empresa_id)->where('periodo',$perido)->get()->toArray();
        $kwh_in = round(Collection::make($balanceEnergia)->sum('kwh_in'));
        $kwh_out = round(Collection::make($balanceEnergia)->sum('kwh_out'));
        
        $data = Array(
            'kwh_in' => $kwh_in,
            'kwh_out' => $kwh_out    
        );

        return $data;
    }

    /**
     * Genera el balance de energia del periodo
     * @return JSON
     * @author Carlos Anselmi <Carlosanselmi2@gmail.com>
     */
    public function messageData($data)
    {
        return response()->json([
            'response' => view('modules.asset-management.balance-energia.response.data')
                            ->with('data', (object)$data)
                            ->render()
        ],200);
    }

    /**
     * Retorna el periodo formateado. $month/$year (MM/YY)
     * @param  String $month  Mes
     * @param  String $year Año
     * @return String       Mes formateado
     * @author Carlos Anselmi <Carlos Anselmi>
     */
    private function getPeriodo($month, $year)
    {
        return $month . '/' . $year;
    }
}
