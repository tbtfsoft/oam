<?php

namespace App\Http\Controllers\Modules\AssetManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AssetManagement\Empresa;
use App\Models\AssetManagement\EmpresaIntegracion;
use App\Models\AssetManagement\TipoIntegracion;
use Spatie\Permission\Models\Permission;
use App\DataTables\GenericDataTable as DataTable;
use Lang;

class EmpresaController extends Controller
{

    /**
     * @var dataTable
     */
    protected $dataTable;

    public function __construct(Empresa $empresa)
    {
        $this->dataTable  = $this->dataTable($empresa);
    }

    /**
     * Cargar empresas
     * @return View
     */
    public function index(){
        return $this->dataTable->render('modules.asset-management.cargar-datos.empresas');
    }

    public function postAjaxGetIntegracion(Request $request){
        $empresa_id = $request->empresa;
        $data = Array();
        $empresa_integracion = EmpresaIntegracion::where('empresa_id',$empresa_id)
                                    ->whereNull('deleted_at')
                                    ->get();
        foreach($empresa_integracion as $value):
            $integracion = TipoIntegracion::find($value->tipo_integracion_id);
            $data[] = ['id'=> $integracion->id,'nombre'=>$integracion->nombre];
        endforeach;
        return response()->json($data);
    }

    public function postAjaxSaveCsv(Request $request){

        if (!$request->hasFile('fileCSV')) {
            return response()->json([
                "message" => Lang::get("app.No se adjunto el archivo. Intente de nuevo")
            ],500);
        }

        /*OBTENEMOS EL CAMPO TYPE FILE DEFINIDO EN EL FORMULARIO*/
        $file = $request->file('fileCSV');

        /* OBTENEMOS EL NOMBRE DEL ARCHIVO */
        $nombre = $file->getClientOriginalName();

        $parts = explode(".",$nombre);

        if($parts[1] != "csv"){

            if (!$request->hasFile('fileCSV')) {
                return response()->json([
                    "message" => lang::get("app.Archivo no valido")
                ],500);
            }
        }

        \Storage::disk('public')->put($nombre,  \File::get($file));

        $path = storage_path('app/public/uploads/'.$nombre);

        $data_con = \file_get_contents($path);
        $data_line = file($path);
        $array = Array();
        $insert = Array();

        foreach($data_line as $value):
            $datos = explode(";", utf8_encode(trim($value)));
            array_push($array,$datos);
        endforeach;


        // $ruts = Array();

        /* RECORREMOS LA MATRIZ */
        foreach($array as $value):
            // if(!in_array($value[1],$ruts)){

                // array_push($ruts,$value[1]);

                $insert = Array(
                    "nombre_xls" => strtoupper($value[0]),
                    "rut" => $value[1]
                );

                $empresa = Empresa::create($insert);

                EmpresaIntegracion::create(['empresa_id' => $empresa->id,'tipo_integracion_id' => 1]);

                Permission::firstOrCreate([
                    'name'          => strtoupper($value[0]),
                    'display_name'  => strtoupper($value[0]),
                    'module'        => 'Empresa',
                    'description'   => $value[1]
                ]);
            // }

        endforeach;

        return response()->json([
            "message" => lang::get("app.Archivo importado exitosamente")
        ],200);
    }


    public function dataTable()
    {
        return new DataTable(new Empresa(), \Auth::user());
    }
}
