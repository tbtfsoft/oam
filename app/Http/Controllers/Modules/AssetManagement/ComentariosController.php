<?php

namespace App\Http\Controllers\Modules\AssetManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AssetManagement\ModulosComentario;
use App\Models\AssetManagement\ComentariosEmpresa;
use Illuminate\Http\Response;
use App\User;
use Lang;

class ComentariosController extends Controller
{

	/**
	 * Elimina un comentario.
	 * 
	 * @param  Request $request
	 * @return Response::json
	 */
	public function destroy (Request $request)
	{
		$comentario = ModulosComentario::find($request->comentario_id);

		if (!$comentario) {
			return response()->json(['messages' => lang::get('app.No se encontró el comentario')], 404);
		}

		$comentario->delete();
	
		return response()->json(['message' => lang::get('app.El comentario se eliminó correctamente')], 200);
	}

	/**
	 * Edita un comentario.
	 *
	 * @return  Request  $request
	 */
	public function edit (Request $request)
	{
		$comentario = ModulosComentario::find($request->comentario_id);

		if (!$comentario) {
			return response()->json(['message' => lang::get('app.No se encontró el comentario')], 404);
		}

		$empresas = $comentario->getEmpresasParent('correspondencia');

		if (!count($empresas)) {
			$user = User::find($request->user_id);
			if ($user) {
				$empresas = $user->empresas;
			}
		}
		
		return response()->json([
			'html' => view('modules.asset-management.resources.comentarios.edit',
							compact(['empresas', 'comentario'])
						)->render()
		], 200);
	}

	/**
	 * Actualiza el comentario
	 * 
	 * @param  Request $request
	 * @return Response::json()
	 */
	public function update (Request $request)
	{
		$comentario = ModulosComentario::find($request->comentario_id);

		$comentario->empresas()->delete();

		foreach ($request->empresas_comentario as $empresa) {
			if ($empresa) {
				$coment = ComentariosEmpresa::create([
					'id_modulo'     => $comentario->id_modulo,
					'modulo'        => $comentario->modulo,
					'id_empresa'    => $empresa,
					'id_comentario' => $request->comentario_id
				]);
			}
		}

		$comentario->comentario = $request->comentario;

		$comentario->save();

		return response()->json([
			'message' => lang::get('app.El comentario se actualizó correctamente'),
			'html' => view('modules.asset-management.resources.comentarios.comentario',
							compact(['comentario'])
						)->render(),
			'request'  => $comentario
		]);
	}
}
