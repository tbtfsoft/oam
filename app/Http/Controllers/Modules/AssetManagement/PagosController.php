<?php

namespace App\Http\Controllers\Modules\AssetManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AssetManagement\Empresa;
use App\Models\AssetManagement\Facturas as Model;
use Illuminate\Support\Collection as Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use DB;
use Flash;
use Lang;

class PagosController extends Controller
{

  /**
  * [Controlador de la vista de pagos index]
  * @return View
  */
  public function index () {
		return view('modules.asset-management.pagos.index');
  }

  /**
  * [Controlador de la vista de pagos recibidos]
  * @return View
  */
  public function recibidos (Request $request) {
		return view('modules.asset-management.pagos.recibidos')->with('data', $this->getData($request,'id_empresa_acreedora'));
  }


  /**
  * [Controlador de la vista de pagos emitidos]
  * @return View
  */
  public function emitidos (Request $request) {
		return view('modules.asset-management.pagos.emitidos')->with('data', $this->getData($request,'id_empresa_deudora'));
  }

  /**
   * Obtiene el paginado de los pagos.
   * @param  Request $request
   * @param  String  $tipo_de_factura para filtrar el tipo de factura, emitidas o recibidas
   * @return Array
   */
  public function getData(Request $request, $tipo_de_factura)
  {
    $queryModel = Model::query();

    if(!is_null($request->empresa_xls)) {
      $queryModel->where($tipo_de_factura, $request->empresa_xls);
    }

    if(!is_null($request->mes) && !is_null($request->anio)) {
      $queryModel->where('periodo', $this->getPeriodo($request->mes,$request->anio));
    }

    $queryModel->orderBy('pago_fecha', 'ASC');

    return [
      'collects' => $queryModel->paginate(20)
    ];
  }

  /**
   * Ajax para procesar el CSV
   * @param  Request $request
   * @return Json
   */
  public function ajaxUpdatePago(Request $request)
  {
    $model = Model::where('id', $request->factura_id)->first();
    
    $model->pago_fecha = $request->pago_fecha;

    $model->pago_codigo = $request->pago_codigo;

    $model->save();

    return response()->json([
      "message"   => Lang::get("app.Factura procesada"),
      "factura"  => $model
    ],200);
  }

  /**
   * Cancelar la factura.
   * @param  Request $request
   * @return Json
   */
  public function cancelarFactura (Request $request)
  {
    $model = Model::where('id', $request->factura_id)->first();
    
    $model->pago_fecha = null;

    $model->pago_codigo = null;

    $model->save();

    return response()->json([
      "message"   => Lang::get("app.Factura procesada"),
      "factura"  => $model
    ],200);
  }

  /**
   * Retorna el periodo formateado. $month/$year (MM/YY)
   * @param  String $month  Mes
   * @param  String $year Año
   * @return String       Mes formateado
   * @author Carlos Anselmi <Carlos Anselmi>
   */
  private function getPeriodo($month,$year)
  {
    return $month . '/' . $year;
  }
}