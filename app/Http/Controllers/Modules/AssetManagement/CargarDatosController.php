<?php

namespace App\Http\Controllers\Modules\AssetManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AssetManagement\EnergiaMedidoresCEN;

class CargarDatosController extends Controller
{

	public function empresas()
	{
		return view('modules.asset-management.cargar-datos.empresas');
	}

	public function itPotencia()
	{
		return view('modules.asset-management.cargar-datos.itpotencia');
	}

    public function energiaMedidoresAjaxCSV(Request $request)
    {

        if (!$request->hasFile('fileCSV')) {
            return response()->json([
                "message" => "No se adjunto el archivo. Intente de nuevo."
            ],500);
        }

        //obtenemos el campo file definido en el formulario
        $file = $request->file('fileCSV');
 
        //obtenemos el nombre del archivo
        $name = $file->getClientOriginalName();
 
        //indicamos que queremos guardar un nuevo archivo en el disco local
        if(\Storage::disk('file')->put($name,\File::get($file))) {

            $handle = fopen(\Storage::disk('file')->path($name), "r");

            while ($line = fgetcsv($handle, 1000, ";")) {
                if(is_numeric($line[0])) {
                    EnergiaMedidoresCEN::firstOrCreate([
                        'id_empresa'    =>  $request->input('empresa_xls'),
                        'id_user'       =>  \Auth::id(),
                        'periodo'       =>  $request->input('mes').'/'.$request->input('anio'),
                        'hora'          =>  $line[0],
                        'kwh_out'       =>  $line[1],
                        'kwh_in'        =>  $line[2]
                    ]);
                } else {
                    if($line[0] == 'Total [kWh]') {
                        EnergiaMedidoresCEN::firstOrCreate([
                            'id_empresa'    =>  $request->input('empresa_xls'),
                            'id_user'       =>  \Auth::id(),
                            'periodo'       =>  $request->input('mes').'/'.$request->input('anio'),
                            'hora'          =>  9000,
                            'kwh_out'       =>  $line[1],
                            'kwh_in'        =>  $line[2]
                        ]);
                    }
                }
            }

            fclose($handle);

            \Storage::disk('file')->delete($name);
        }

        return response()->json([
            "message" => "Archivo importado exitosamente"
        ],200);
    }
}