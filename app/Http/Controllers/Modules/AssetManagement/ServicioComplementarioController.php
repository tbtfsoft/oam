<?php

namespace App\Http\Controllers\Modules\AssetManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\MessagesResponseTrait;
use App\Models\AssetManagement\ServicioComplementario;
use App\Models\AssetManagement\Empresa;
use Storage;
use DB;

class ServicioComplementarioController extends Controller
{

    /**
     * @var catch
     */
    protected $catch;

    /**
     * __construct
     * @author Carlos Anselmi <carlosanselmi2@gmail.com>
     */
    public function __construct()
    {
        $this->catch  = [];
    }

    /**
     * Vista de serivicos complementarios
     * @return View
     * @author Carlos Anselmi <carlosanselmi2@gmail.com>
     */
    public function index()
    {
        return view('modules.asset-management.cargar-datos.servicios-complementarios');
    }

    /**
     * Ajax de archivo CSV donde se procesara y almacenara en el Modelo Energía en la tabla balance_energia
     * @param  Request $request
     * @return Json
     * @author Carlos Anselmi <carlosanselmi2@gmail.com>
     */
    public function servicioComplementarioAjaxCSV(Request $request)
    {

        if (!$request->hasFile('fileCSV')) {
            return MessagesResponseTrait::simple([
                'title'   => '404 FILE CSV',
                'message' => 'Debe ingrasar el archivo CSV para poder procesar la data.',
                'type'    => 'warning'
            ],500);
        }

        $periodo = $this->getPeriodo($request->mes,$request->anio);

        $tipo_doc_cen_id = $request->input('tipo_doc_cen');

        ServicioComplementario::where("periodo",$periodo)->where("id_tipo_doc_cen",$tipo_doc_cen_id)->delete();
        
        //obtenemos el nombre del archivo
        $file = $request->file('fileCSV');
        $name = $file->getClientOriginalName();
        //indicamos que queremos guardar un nuevo archivo en el disco local
        if(Storage::disk('file')->put($name,\File::get($file))) {

            $handle = fopen(\Storage::disk('file')->path($name), "r");

            $receptores = fgetcsv($handle, null, ";");

            unset($receptores[0]);

            $receptoresID = $this->getReceptores(array_map("utf8_encode", $receptores));

            $idsEmpresas = $this->getIdsEmpresas();

            while ($rowEmisora = fgetcsv($handle, null, ";")) {
                $rowEmisora = array_map("utf8_encode", $rowEmisora);
                if (array_key_exists(strtoupper($rowEmisora[0]), $idsEmpresas)) {
                    $empresaEmisora = $idsEmpresas[strtoupper($rowEmisora[0])];
                    unset($rowEmisora[0]);
                    foreach($rowEmisora as $key => $data):
                        if(!empty($receptoresID[$key])) {
                            ServicioComplementario::create([
                                "id_empresa_deudora" => $empresaEmisora,
                                "id_empresa_acreedora" => $receptoresID[$key],
                                "periodo" => $periodo,
                                "id_tipo_doc_cen" => $tipo_doc_cen_id,
                                "costo" => str_replace([".",","],["","."],$data)                                
                            ]);
                        } else {
                            $messageCatch = "No existe la empresa ACREEDORA [".strtoupper($receptores[$key])."]";
                            array_push($this->catch, $messageCatch);
                        }
                    endforeach;
                } else {
                    $messageCatch = "*** No existe la empresa DEUDORA [".strtoupper($rowEmisora[0])."] ***";
                    if(!in_array($messageCatch, $this->catch)) {
                        array_push($this->catch, $messageCatch);
                    }
                }
            }

            fclose($handle);

            Storage::disk('file')->delete($name);

            return MessagesResponseTrait::simple([
                'message'  => 'Se proceso correctamente',
                'type'     => 'success',
                'catch'    => $this->catch
            ],200);
        }
    }

    /**
     * Retorna el periodo formateado. $month/$year (MM/YY)
     * @param  String $month  Mes
     * @param  String $year Año
     * @return String       Mes formateado
     * @author Carlos Anselmi <Carlos Anselmi>
     */
    private function getPeriodo ($month,$year)
    {
        return $month . '/' . $year;
    }

    /**
     * Retorna los ID de los Receptores del primer row del csv
     * @return Array       $receptores
     * @author Carlos Anselmi <Carlos Anselmi>
     */
    private function getReceptores ($receptores)
    {
        $receptoresID = [];

        foreach($receptores as $key => $receptor):
            $empresaReceptora = Empresa::where('nombre_xls', strtoupper($receptor))->get()->first();
            if (!empty($empresaReceptora))
                $receptoresID[$key] = $empresaReceptora->id;
            else {
                $receptoresID[$key] = null;
                array_push($this->catch,"La empresa receptora [".strtoupper($receptor)."] no esta registrada en el sistema.");
            }
        endforeach;

        return $receptoresID;
    }

    public function getIdsEmpresas () {

        $empresas = Empresa::all();

        $nombresEmpresas = [];

        foreach ($empresas as $empresa) {
            $nombresEmpresas[$empresa->nombre_xls] = $empresa->id;
        }

        return $nombresEmpresas;
    }


    public function existData (Request $request,$month, $year, $id_tipo_doc_cen)
    {
        $periodo = $this->getPeriodo($month,$year);

        $energia = ServicioComplementario::where("periodo",$periodo)->where("id_tipo_doc_cen",$id_tipo_doc_cen)->first();

        if ($energia) {
            return response()->json([
                'is_data' => true,
                'title' => 'Desea continuar?',
                'text' => 'Existe data para el periodo '.$periodo.' se eliminara data de este periodo para procesar su archivo'
            ],200);
        }

        return response()->json(['is_data' => false],200);
    }
}