<?php

namespace App\Http\Controllers\Modules\AssetManagement;

use App\Http\Controllers\Controller;
use App\Models\AssetManagement\Facturas as Model;
use App\Models\AssetManagement\Empresa;
use Illuminate\Http\Request;
use Illuminate\Support\Collection as Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Yajra\DataTables\Html\Builder;
use DataTables;
use Storage;
use DB;
use Lang;
use Flash;

class FacturacionController extends Controller
{


  /**
   * [Controlador de la vista de carga da data]
   * @return View
   */
  public function index() {
    return view('modules.asset-management.cargar-datos.facturas');
  }

  /**
   * [Controlador de la vista de emitidas]
   * @return View
   */
  public function emitidas (Request $request) {
      return view('modules.asset-management.facturacion.emitidas')->with('data', $this->getData($request,'id_empresa_acreedora'));
  }

  /**
   * [Controlador de la vista de recibidas]
   * @return View
   */
  public function recibidas (Request $request) {
    return view('modules.asset-management.facturacion.recibidas')->with('data', $this->getData($request,'id_empresa_deudora'));
  }

  /**
   * Obtiene el paginado de los pagos.
   * @param  Request $request
   * @param  String  $tipo_de_factura para filtrar el tipo de factura, emitidas o recibidas
   * @return Array
   */
  public function getData(Request $request, $tipo_de_factura) {
    $queryModel = Model::query();

    if(!is_null($request->empresa_xls)) {
      $queryModel->where($tipo_de_factura, $request->empresa_xls);
    }

    if(!is_null($request->mes) && !is_null($request->anio)) {
      $queryModel->where('periodo', $this->getPeriodo($request->mes,$request->anio));
    }

    if(!is_null($request->module)) {
      if ($request->module != 0) {
        $queryModel->where('id_modulo', $request->module);
      }
    }

    $queryModel->orderBy('pago_fecha', 'ASC');
    
    return [
      'total' => $queryModel->sum('monto_bruto'),
      'collects' => $queryModel->paginate(20)
    ];
  }

  /**
  * Ajax para procesar el CSV
  * @param  Request $request
  * @return Json
  */
  public function ajaxCSV(Request $request) {
    $catch = [];
    $files_no_procesados = [];
    $idsEmpresas = $this->getIdsEmpresas();

    if($request->hasfile('files')) {
        $files = $request->file('files');
        foreach($files as $file) {
          $fileXML = $this->getFileXML($file);            
          if ($fileXML) {
            $row = $this->getRow($fileXML, $request);
            $empresa_deudora = strval($row['id_empresa_deudora']);
            $empresa_acreedora = strval($row['id_empresa_acreedora']);
            if(array_key_exists( $empresa_deudora, $idsEmpresas) && array_key_exists($empresa_acreedora, $idsEmpresas)) {
              Model::firstOrCreate(
                [
                  'id_empresa_deudora' => $idsEmpresas[$empresa_deudora],
                  'id_empresa_acreedora' => $idsEmpresas[$empresa_acreedora],
                  'folio' => $row['folio']
                ],
                $row
              );
            } else {
              array_push($files_no_procesados, ['file' => $file->getClientOriginalName(), 'error' => Lang::get('app.no se encontro la empresa correspondiente')]);
            }
          } else {
            array_push($files_no_procesados, ['file' => $file->getClientOriginalName(), 'error' => Lang::get('app.error al leer el xml')]);
          }
        }

        if (count($files_no_procesados)){
          return response()->json(["message" => Lang('app.archivos no procesados') . '('.count($files_no_procesados).')' , 'catch' => $files_no_procesados], 200);
        }else{
          return response()->json(["message" => Lang::get('app.archivos procesados correctamente')], 200);
        }
          
    } else {
      return response()->json(["message" => Lang::get('app.debe subir por lo menos un archivo')], 500);
    }
  }

  /**
   * Obtiene el xml mediante el nombre del path del .xml
   * @param  String $filename Nombre del .xml
   * @return simplexml 
   * @author Carlos Anselmi <carlosanselmi2@gmail.com>
   */
  private function getFileXML ($file) {
      try {
          //return simplexml_load_file($file)->DTE->Documento;
          $simpleXml = simplexml_load_file($file);
          if(isset($simpleXml->DTE)){
            return $simpleXml->DTE->Documento;
          }           
          else {
            if(isset($simpleXml->Documento)){
              return $simpleXml->Documento;
            }else{
              return $simpleXml->SetDTE->DTE->Documento;
            }                
          }          
      } catch (Exception $e) {
          return null;
      }
  }

  /**
   * Obtiene los datos requeridos del xml
   * @param  simplexml $xml Archivo a leer
   * @return Array      Array con los datos requeridos
   * @author Carlos Anselmi <carlosanselmi2@gmail.com>
   */
  private function getRow ($xml, $request) {

    $modulo = 0;
    $nombreItem = (string)$xml->Detalle->NmbItem;

    if($nombreItem == 'GENERACION DE ENERGIA'){
      $modulo = 1;
    }else if($nombreItem == 'GENERACION DE POTENCIA'){
      $modulo = 2;
    }else {
      $modulo = 3;
    }
    

    return [
      'periodo' => $this->getPeriodo($request->input('mes'), $request->input('anio')),
      'id_empresa_deudora' => $xml->Encabezado->Receptor->RUTRecep,
      'id_empresa_acreedora' => $xml->Encabezado->Emisor->RUTEmisor,
      'id_modulo' => $modulo,
      'id_tipo_dte' => $xml->Encabezado->IdDoc->TipoDTE,
      'folio' => $xml->Encabezado->IdDoc->Folio,
      'fecha_emision' => $xml->Encabezado->IdDoc->FchEmis,
      'fecha_aceptacion' => $xml->TED->DD->CAF->DA->FA,
      'fecha_aceptacion_ts' => $xml->TED->DD->TSTED,
      'fecha_vencimiento' => $xml->Encabezado->IdDoc->FchVenc,
      'forma_pago' => $xml->Encabezado->IdDoc->FmaPago,
      'monto_neto' => $xml->Encabezado->Totales->MntNeto,
      'monto_bruto' => $xml->Encabezado->Totales->MntTotal,
      'iva' => $xml->Encabezado->Totales->IVA
    ];
  }

  public function getIdsEmpresas () {

    $empresas = Empresa::all();

    $nombresEmpresas = [];

    foreach ($empresas as $empresa) {
        $rut = str_replace('.', "", $empresa->rut);
        $nombresEmpresas[strtoupper($rut)] = $empresa->id;
    }

    return $nombresEmpresas;
}

  /**
  * Retorna el periodo formateado. $month/$year (MM/YY)
  * @param  String $month  Mes
  * @param  String $year Año
  * @return String       Mes formateado
  * @author Carlos Anselmi <Carlos Anselmi>
  */
  private function getPeriodo($month, $year) {
      return $month . '/' . $year;
  }

  public function emitPagos(Request $request) {
    foreach ($request->pagos as $key => $pago) {
      $explode_pago = explode("_", $pago);
      $factura = Model::where('id', $explode_pago[0])->first();
      $factura->pago = $explode_pago[1] === '1'; 
      $factura->save();
    }

    Flash::success(Lang::get('app.actualización de las facturas correctamente'));

    return back();
  }

  /**
   * Vista para eliminar las facturas mediante un filtrado
   * @param  Request $request
   * @return Void
   */
  public function eliminar (Request $request) {
    return view('modules.asset-management.cargar-datos.elimiarFacturas');
  }

  /**
   * Funcion para eliminar las facturas
   * @param  Request $request
   * @return Void
   */
  public function eliminarPost (Request $request) {

    $queryModel = Model::query();

    $model = Model::where('id_empresa_deudora', $request->empresa_xls)
                  ->where('periodo', $this->getPeriodo($request->mes,$request->anio))->delete();

    Flash::success(lang::get('app.facturas eliminadas correctamente'));

    return back();
  }
}
