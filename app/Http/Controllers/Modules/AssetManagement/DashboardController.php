<?php

namespace App\Http\Controllers\Modules\AssetManagement;

use Illuminate\Http\Request;
use App\Helpers\TimeHelper;
use App\Http\Controllers\Controller;
use App\Models\Conexion;
use App\Models\Barras;
use App\Models\BarrasEmpresas;
use App\Models\ModalidadCalculo;
use App\Models\ModalidadCalculoEmpresas;
use App\Models\PrecioEstabilizado;
use App\Models\AssetManagement\Empresa;
use App\Models\AssetManagement\EnergiaMedidoresCEN as EnegiaCEN;
use App\Models\AssetManagement\EnergiaMedidoresPFV as EnergiaPFV;
use App\Models\AssetManagement\Energia as BalanceEconomicoEnergiaCEN;
use App\Models\AssetManagement\Potencia as BalanceEconomicoPotenciaCEN;
use App\Models\AssetManagement\ServicioComplementario as BalanceEconomicoSSCCCEN;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection as Collection;

class DashboardController extends Controller
{

    protected $tbl;

    protected $id_disp;

    protected $db;

    protected $table;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Busca la data para las graficas
     * @param  Request $request [description]
     * @return JSON
     * @author Morris Sosa <morris.sosa@oenergy.cl>
     */

    public function GraficaEnergiaMensual(Request $request){
        $data = Array();
        $empresa_id = $request->empresa;
        $conexion = Conexion::where('id_empresa', $empresa_id)->first();
		$subtitulo = $conexion == null ? 'no existe conexion' : $conexion->nombre_parq;	      
        $year = $request->year;
        $enero = '01'.'/'.$year;
        $febrero = '02'.'/'.$year;
        $marzo = '03'.'/'.$year;
        $abril = '04'.'/'.$year;
        $mayo = '05'.'/'.$year;
        $junio = '06'.'/'.$year;
        $julio = '07'.'/'.$year;
        $agosto = '08'.'/'.$year;
        $septiembre = '09'.'/'.$year;
        $octubre = '10'.'/'.$year;
        $noviembre = '11'.'/'.$year;
        $diciembre = '12'.'/'.$year;        

        $energia_enero = (EnergiaPFV::where('id_empresa',$request->empresa)->where('periodo',$enero)->get()->toArray() == null) ? 0 : EnergiaPFV::where('id_empresa',$request->empresa)->where('periodo',$enero)->get()->toArray();
        $energia_febrero = (EnergiaPFV::where('id_empresa',$request->empresa)->where('periodo',$febrero)->get()->toArray() == null) ? 0 : EnergiaPFV::where('id_empresa',$request->empresa)->where('periodo',$febrero)->get()->toArray();
        $energia_marzo = (EnergiaPFV::where('id_empresa',$request->empresa)->where('periodo',$marzo)->get()->toArray() == null) ? 0 : EnergiaPFV::where('id_empresa',$request->empresa)->where('periodo',$marzo)->get()->toArray();
        $energia_abril = (EnergiaPFV::where('id_empresa',$request->empresa)->where('periodo',$abril)->get()->toArray() == null) ? 0 : EnergiaPFV::where('id_empresa',$request->empresa)->where('periodo',$abril)->get()->toArray();
        $energia_mayo = (EnergiaPFV::where('id_empresa',$request->empresa)->where('periodo',$mayo)->get()->toArray() == null) ? 0 : EnergiaPFV::where('id_empresa',$request->empresa)->where('periodo',$mayo)->get()->toArray();
        $energia_junio = (EnergiaPFV::where('id_empresa',$request->empresa)->where('periodo',$junio)->get()->toArray() == null) ? 0 : EnergiaPFV::where('id_empresa',$request->empresa)->where('periodo',$junio)->get()->toArray();
        $energia_julio = (EnergiaPFV::where('id_empresa',$request->empresa)->where('periodo',$julio)->get()->toArray() == null) ? 0 : EnergiaPFV::where('id_empresa',$request->empresa)->where('periodo',$julio)->get()->toArray();
        $energia_agosto = (EnergiaPFV::where('id_empresa',$request->empresa)->where('periodo',$agosto)->get()->toArray() == null) ? 0 : EnergiaPFV::where('id_empresa',$request->empresa)->where('periodo',$agosto)->get()->toArray();
        $energia_septiembre = (EnergiaPFV::where('id_empresa',$request->empresa)->where('periodo',$septiembre)->get()->toArray() == null) ? 0 : EnergiaPFV::where('id_empresa',$request->empresa)->where('periodo',$septiembre)->get()->toArray();
        $energia_octubre = (EnergiaPFV::where('id_empresa',$request->empresa)->where('periodo',$octubre)->get()->toArray() == null) ? 0 : EnergiaPFV::where('id_empresa',$request->empresa)->where('periodo',$octubre)->get()->toArray();
        $energia_noviembre = (EnergiaPFV::where('id_empresa',$request->empresa)->where('periodo',$noviembre)->get()->toArray() == null) ? 0 : EnergiaPFV::where('id_empresa',$request->empresa)->where('periodo',$noviembre)->get()->toArray();
        $energia_diciembre = (EnergiaPFV::where('id_empresa',$request->empresa)->where('periodo',$diciembre)->get()->toArray() == null) ? 0 :    EnergiaPFV::where('id_empresa',$request->empresa)->where('periodo',$diciembre)->get()->toArray();     

        $energia_scada = $this->GraficaEnergiaAnio($empresa_id,$year);

        //Enero
        if ($energia_enero == 0) {
            if (date('m').'/'.$year == $energia_scada['mesAnio'][0]) {
                $energia_enero_in = 0;
                $energia_enero_out = 0;
            }else {
                $energia_enero_in = $energia_scada['generacionTotal'][0];
                $energia_enero_out = $energia_scada['consumoTotal'][0];
            }
        }else {
            $energia_enero_in = round(Collection::make($energia_enero)->sum('kwh_in'));
            $energia_enero_out = round(Collection::make($energia_enero)->sum('kwh_out'));
        }
        //Febrero
        if ($energia_febrero == 0) {
            if (date('m').'/'.$year == $energia_scada['mesAnio'][1]) {
                $energia_febrero_in = 0;
                $energia_febrero_out = 0;
            }else {
                $energia_febrero_in = $energia_scada['generacionTotal'][1];
                $energia_febrero_out = $energia_scada['consumoTotal'][1];
            }
        }else {
            $energia_febrero_in = round(Collection::make($energia_febrero)->sum('kwh_in'));
            $energia_febrero_out = round(Collection::make($energia_febrero)->sum('kwh_out'));
        }
        //Marzo
        if ($energia_marzo == 0) {
            if (date('m').'/'.$year == $energia_scada['mesAnio'][2]) {
                $energia_marzo_in = 0;
                $energia_marzo_out = 0;
            }else {
                $energia_marzo_in = $energia_scada['generacionTotal'][2];
                $energia_marzo_out = $energia_scada['consumoTotal'][2];
            }
        }else {
            $energia_marzo_in = round(Collection::make($energia_marzo)->sum('kwh_in'));
            $energia_marzo_out = round(Collection::make($energia_marzo)->sum('kwh_out'));
        }
        //Abril
        if ($energia_abril == 0) {
            if (date('m').'/'.$year == $energia_scada['mesAnio'][3]) {
                $energia_abril_in = 0;
                $energia_abril_out = 0;
            }else {
                $energia_abril_in = $energia_scada['generacionTotal'][3];
                $energia_abril_out = $energia_scada['consumoTotal'][3];
            }
        }else {
            $energia_abril_in = round(Collection::make($energia_abril)->sum('kwh_in'));
            $energia_abril_out = round(Collection::make($energia_abril)->sum('kwh_out'));
        }
        //Mayo
        if ($energia_mayo == 0) {
            if (date('m').'/'.$year == $energia_scada['mesAnio'][4]) {
                $energia_mayo_in = 0;
                $energia_mayo_out = 0;
            }else {
                $energia_mayo_in = $energia_scada['generacionTotal'][4];
                $energia_mayo_out = $energia_scada['consumoTotal'][4];
            }
        }else {
            $energia_mayo_in = round(Collection::make($energia_mayo)->sum('kwh_in'));
            $energia_mayo_out = round(Collection::make($energia_mayo)->sum('kwh_out'));
        }
        //Junio
        if ($energia_junio == 0) {
            if (date('m').'/'.$year == $energia_scada['mesAnio'][5]) {
                $energia_junio_in = 0;
                $energia_junio_out = 0;
            }else {
                $energia_junio_in = $energia_scada['generacionTotal'][5];
                $energia_junio_out = $energia_scada['consumoTotal'][5];
            }
        }else {
            $energia_junio_in = round(Collection::make($energia_junio)->sum('kwh_in'));
            $energia_junio_out = round(Collection::make($energia_junio)->sum('kwh_out'));
        }
        //Julio
        if ($energia_julio == 0) {
            if (date('m').'/'.$year == $energia_scada['mesAnio'][6]) {
                $energia_julio_in = 0;
                $energia_julio_out = 0;
            }else {
                $energia_julio_in = $energia_scada['generacionTotal'][6];
                $energia_julio_out = $energia_scada['consumoTotal'][6];
            }
        }else {
            $energia_julio_in = round(Collection::make($energia_julio)->sum('kwh_in'));
            $energia_julio_out = round(Collection::make($energia_julio)->sum('kwh_out'));
        }

        if ($energia_agosto == 0) {
            if (date('m').'/'.$year == $energia_scada['mesAnio'][7]) {
                $energia_agosto_in = 0;
                $energia_agosto_out = 0;
            }else {
                $energia_agosto_in = $energia_scada['generacionTotal'][7];
                $energia_agosto_out = $energia_scada['consumoTotal'][7];
            }
        }else {
            $energia_agosto_in = round(Collection::make($energia_agosto)->sum('kwh_in'));
            $energia_agosto_out = round(Collection::make($energia_agosto)->sum('kwh_out'));
        }
        //Septiembre
        if ($energia_septiembre == 0) {
            if (date('m').'/'.$year == $energia_scada['mesAnio'][8]) {
                $energia_septiembre_in = 0;
                $energia_septiembre_out = 0;
            }else {
                $energia_septiembre_in = $energia_scada['generacionTotal'][8];
                $energia_septiembre_out = $energia_scada['consumoTotal'][8];
            }
        }else {
            $energia_septiembre_in = round(Collection::make($energia_septiembre)->sum('kwh_in'));
            $energia_septiembre_out = round(Collection::make($energia_septiembre)->sum('kwh_out'));
        }
        //Octubre
        if ($energia_octubre == 0) {
            if (date('m').'/'.$year == $energia_scada['mesAnio'][9]) {
                $energia_octubre_in = 0;
                $energia_octubre_out = 0;
            }else {
                $energia_octubre_in = $energia_scada['generacionTotal'][9];
                $energia_octubre_out = $energia_scada['consumoTotal'][9];
            }
        }else {
            $energia_octubre_in = round(Collection::make($energia_octubre)->sum('kwh_in'));
            $energia_octubre_out = round(Collection::make($energia_octubre)->sum('kwh_out'));
        }
        //Noviembre
        if ($energia_noviembre == 0) {
            if (date('m').'/'.$year == $energia_scada['mesAnio'][10]) {
                $energia_noviembre_in = 0;
                $energia_noviembre_out = 0;
            }else {
                $energia_noviembre_in = $energia_scada['generacionTotal'][10];
                $energia_noviembre_out = $energia_scada['consumoTotal'][10];
            }
        }else {
            $energia_noviembre_in = round(Collection::make($energia_noviembre)->sum('kwh_in'));
            $energia_noviembre_out = round(Collection::make($energia_noviembre)->sum('kwh_out'));
        }
        //Diciembre
        if ($energia_diciembre == 0) {
            if (date('m').'/'.$year == $energia_scada['mesAnio'][11]) {
                $energia_diciembre_in = 0;
                $energia_diciembre_out = 0;
            }else {
                $energia_diciembre_in = $energia_scada['generacionTotal'][11];
                $energia_diciembre_out = $energia_scada['consumoTotal'][11];
            }
        }else {
            $energia_diciembre_in = round(Collection::make($energia_diciembre)->sum('kwh_in'));
            $energia_diciembre_out = round(Collection::make($energia_diciembre)->sum('kwh_out'));
        }

        $data = Array(
            'energia_enero_in' => $energia_enero_in,
            'energia_febrero_in' => $energia_febrero_in,
            'energia_marzo_in' => $energia_marzo_in,
            'energia_abril_in' => $energia_abril_in,
            'energia_mayo_in' => $energia_mayo_in,
            'energia_junio_in' => $energia_junio_in,
            'energia_julio_in' => $energia_julio_in,
            'energia_agosto_in' => $energia_agosto_in,
            'energia_septiembre_in' => $energia_septiembre_in,
            'energia_octubre_in' => $energia_octubre_in,
            'energia_noviembre_in' => $energia_noviembre_in,
            'energia_diciembre_in' => $energia_diciembre_in,			
            'energia_enero_out' => $energia_enero_out,
            'energia_febrero_out' => $energia_febrero_out,
            'energia_marzo_out' => $energia_marzo_out,
            'energia_abril_out' => $energia_abril_out,
            'energia_mayo_out' => $energia_mayo_out,
            'energia_junio_out' => $energia_junio_out,
            'energia_julio_out' => $energia_julio_out,
            'energia_agosto_out' => $energia_agosto_out,
            'energia_septiembre_out' => $energia_septiembre_out,
            'energia_octubre_out' => $energia_octubre_out,
            'energia_noviembre_out' => $energia_noviembre_out,
            'energia_diciembre_out' => $energia_diciembre_out, 
            'subtitulo' => $subtitulo,         
        );

        return $data;
    }


	public function GraficaEnergiaAnio($empresa,$anio){
        date_default_timezone_set('America/Santiago');
        $data = Array();
        $empresa_id = $empresa;
        $conexion = Conexion::where('id_empresa', $empresa_id)->first();
        $host = $conexion == null ? 0 : $conexion->host;
        $pfv = $conexion == null ? 0 : $conexion->id_parq;
        $medidor = $conexion == null ? 0 : $conexion->id_medidor;
        $radsensor = $conexion == null ? 0 : $conexion->id_radsensor;
        $database_name = $conexion == null ? 0 : $conexion->db;
		$subtitulo = $conexion == null ? 'no existe conexion' : $conexion->nombre_parq;			      
		$inicial_date =  $anio.'-01-01';
		$inicio = $inicial_date.' 00:00:00';		
		$fin = date("Y-m-t", strtotime($inicial_date)).' 23:59:59';		

        $data = Array();
        
        $inicio_prueba = Array();
        $fin_prueba = Array();
        $data = Array();
        $consumoRaw = Array();
		$inyeccionRaw = Array();
        $consumoRaw2 = Array();
		$inyeccionRaw2 = Array();	
		$inyeccionCLPRaw = Array();	
		$inyeccionCLPRaw2 = Array();	
		$dataRaw = Array();
		$dataRaw2 = Array();
		$horasRaw = Array();
		$fecha_inicial_ciclo_2 = '';
		$fecha_final_ciclo_2 = '';

		$modalidadEmpresaBandera = '';
		$barraEmpresaBandera = '';
		$precio_energia = '';	

        //DB CONNECTION
        if($host == 1){
            $host = config('app.host_1');
            $port = config('app.host_port_1');
            $password = config('app.host_password_1');
        }else if($host == 2){

        }else{
            $host = 0;
            $port = 0;
            $password = 0;            
        }

        if($host != 0){
			//INICIO DE LA CONSULTA
            $strConnect = "host = ".$host." port = ".$port." dbname = '".$database_name."' user = 'postgres' password = ".$password;
			$con = pg_connect($strConnect);
						
			$consulta_cont = 0;
			$fecha_inicial_ciclo = $inicio;
			$fecha_final_ciclo = $anio.'-12-31 23:59:59';

			$fecha_inicial_ciclo_2 = $inicio;
			$fecha_final_ciclo_2 = $anio.'-12-31 23:59:59';

			while(strtotime($fecha_inicial_ciclo) < strtotime($fecha_final_ciclo)){
				
				$end = strtotime('+1 month', strtotime($fecha_inicial_ciclo));
				$end = date('Y-m-d H:i:s', $end);
				$start = strtotime('-1 month', strtotime($end));
				$start = date('Y-m-d H:i:s', $start);			

				if(strtotime($start) <= strtotime($fecha_final_ciclo))
				{
					//DB QUERY STRING
					$strQuery = "SELECT (MAX(ener_in) - MIN(ener_in)) AS consumo, (MAX(ener_out) - MIN(ener_out)) AS inyeccion  
					FROM tbl_datos_medidor	
					WHERE id_disp = $medidor
					AND fch_dato BETWEEN '$start' AND '$end'		
					";

					//Modalidad de Calculo
					$modalidad_calculo_id = 1;
					$modalidad = ModalidadCalculoEmpresas::where('id_empresa', $empresa_id)->where('inicial_date', '<=', $start)->where('final_date', '>=', $end)->first() == null ? $modalidad_calculo_id = 0 : ModalidadCalculoEmpresas::where('id_empresa', $empresa_id)->where('inicial_date', '<=', $start)->where('final_date', '>=', $end)->first();		
					if($modalidad_calculo_id == 1){
						$modalidad_calculo_id = $modalidad->id_modalidad_calculo;
					}
					$modalidadEmpresaBandera = 1;
					$modalidadEmpresa = ModalidadCalculo::where('id', $modalidad_calculo_id)->first() == null ? $modalidadEmpresaBandera = '' : ModalidadCalculo::where('id', $modalidad_calculo_id)->first();
					if($modalidadEmpresaBandera == 1){
						$modalidadEmpresaBandera = $modalidadEmpresa->nombre;
					}

					//Barras
					$barra_id = 1;
					$barra = BarrasEmpresas::where('id_empresa', $empresa_id)->where('tipo_calculo', $modalidad_calculo_id)->where('inicial_date', '<=', $start)->where('final_date', '>=', $end)->first() == null ? $barra_id = 0 : BarrasEmpresas::where('id_empresa', $empresa_id)->where('tipo_calculo', $modalidad_calculo_id)->where('inicial_date', '<=', $start)->where('final_date', '>=', $end)->first();
					if($barra_id == 1){
						$barra_id = $barra->id_barras;
					}
					$barraEmpresaBandera = 1;
					$barraEmpresa = Barras::where('id', $barra_id)->first() == null ? $barraEmpresaBandera = '' : Barras::where('id', $barra_id)->first();
					if($barraEmpresaBandera == 1){
						$barraEmpresaBandera = $barraEmpresa->barra_human;
					}		

					//Definimos la modalidad del calculo Ejemplo PE o CMg
					$precio_energia = 1;	
					if ($modalidad_calculo_id == 1) {			
						$precio = PrecioEstabilizado::where('id_barra', $barra_id)->where('inicial_date', '<=', $start)->where('final_date', '>=', $end)->first() == null ? $precio_energia = 0 : PrecioEstabilizado::where('id_barra', $barra_id)->where('inicial_date', '<=', $start)->where('final_date', '>=', $end)->first();
					}else {
						$precio_energia = 0;
					}	
					
					if ($precio_energia == 1) {
						$precio_energia = $precio->precio_energia;
					}
				}

				//DB QUERY
				$resRAW = pg_query($con,$strQuery);					

				while($dataFetch = pg_fetch_assoc($resRAW))
				{
					$dataRaw[] = $dataFetch;
				}                
                
				$fecha_expode_0 = $start;
				$fecha_expode_1 = explode(' ', $fecha_expode_0);
				$fecha_expode_2 = explode(':',$fecha_expode_1[1]);
				$fecha_expode_3 = $fecha_expode_2[0].':'.$fecha_expode_2[1];	

				$consumoRaw[] = abs((float)($dataRaw[$consulta_cont]['consumo']));
				$inyeccionRaw[] = abs((float)($dataRaw[$consulta_cont]['inyeccion']));
				$inyeccionCLPRaw[] = (float)number_format((abs(($dataRaw[$consulta_cont]['inyeccion']))) * $precio_energia, 3, '.', '');	
				$horasRaw[] = TimeHelper::formatDateHumanAnio($fecha_expode_1[0]);
				
				$consulta_cont++;
                $fecha_inicial_ciclo = $end;
			}
			//FIN DE LA CONSULTA
        }	

        $data = Array(
            'consumoTotal' => $consumoRaw,
			'generacionTotal' => $inyeccionRaw,		
            'mesAnio' => $horasRaw
        );

        return $data;		
	}    

    public function GraficaEnergiaCLPMensual(Request $request){
        $data = Array();
        $empresa_id = $request->empresa;
        $conexion = Conexion::where('id_empresa', $empresa_id)->first();
		$subtitulo = $conexion == null ? 'no existe conexion' : $conexion->nombre_parq;	
        $year = $request->year;
        $enero = '01'.'/'.$year;
        $febrero = '02'.'/'.$year;
        $marzo = '03'.'/'.$year;
        $abril = '04'.'/'.$year;
        $mayo = '05'.'/'.$year;
        $junio = '06'.'/'.$year;
        $julio = '07'.'/'.$year;
        $agosto = '08'.'/'.$year;
        $septiembre = '09'.'/'.$year;
        $octubre = '10'.'/'.$year;
        $noviembre = '11'.'/'.$year;
        $diciembre = '12'.'/'.$year;        

        $energia_enero = (BalanceEconomicoEnergiaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$enero)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoEnergiaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$enero)->where('id_tipo_doc_cen',2)->get()->toArray();        
        $energia_febrero = (BalanceEconomicoEnergiaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$febrero)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoEnergiaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$febrero)->where('id_tipo_doc_cen',2)->get()->toArray();
        $energia_marzo = (BalanceEconomicoEnergiaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$marzo)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoEnergiaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$marzo)->where('id_tipo_doc_cen',2)->get()->toArray();
        $energia_abril = (BalanceEconomicoEnergiaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$abril)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoEnergiaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$abril)->where('id_tipo_doc_cen',2)->get()->toArray();
        $energia_abril_r = (BalanceEconomicoEnergiaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$abril)->where('id_tipo_doc_cen',3)->get()->toArray() == null) ? 0 : BalanceEconomicoEnergiaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$abril)->where('id_tipo_doc_cen',3)->get()->toArray();
        $energia_mayo = (BalanceEconomicoEnergiaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$mayo)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoEnergiaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$mayo)->where('id_tipo_doc_cen',2)->get()->toArray();
        $energia_junio = (BalanceEconomicoEnergiaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$junio)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoEnergiaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$junio)->where('id_tipo_doc_cen',2)->get()->toArray();
        $energia_julio = (BalanceEconomicoEnergiaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$julio)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoEnergiaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$julio)->where('id_tipo_doc_cen',2)->get()->toArray();
        $energia_agosto = (BalanceEconomicoEnergiaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$agosto)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoEnergiaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$agosto)->where('id_tipo_doc_cen',2)->get()->toArray();
        $energia_septiembre = (BalanceEconomicoEnergiaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$septiembre)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoEnergiaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$septiembre)->where('id_tipo_doc_cen',2)->get()->toArray();
        $energia_octubre = (BalanceEconomicoEnergiaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$octubre)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoEnergiaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$octubre)->where('id_tipo_doc_cen',2)->get()->toArray();
        $energia_noviembre = (BalanceEconomicoEnergiaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$noviembre)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoEnergiaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$noviembre)->where('id_tipo_doc_cen',2)->get()->toArray();
        $energia_diciembre = (BalanceEconomicoEnergiaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$diciembre)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoEnergiaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$diciembre)->where('id_tipo_doc_cen',2)->get()->toArray();     

        $data = Array(
            'energia_enero' => Collection::make($energia_enero)->sum('costo'),
            'energia_febrero' => Collection::make($energia_febrero)->sum('costo'),
            'energia_marzo' => Collection::make($energia_marzo)->sum('costo'),
            'energia_abril' => (Collection::make($energia_abril)->sum('costo') + Collection::make($energia_abril_r)->sum('costo')),
            'energia_mayo' => Collection::make($energia_mayo)->sum('costo'),
            'energia_junio' => Collection::make($energia_junio)->sum('costo'),
            'energia_julio' => Collection::make($energia_julio)->sum('costo'),
            'energia_agosto' => Collection::make($energia_agosto)->sum('costo'),
            'energia_septiembre' => Collection::make($energia_septiembre)->sum('costo'),
            'energia_octubre' => Collection::make($energia_octubre)->sum('costo'),
            'energia_noviembre' => Collection::make($energia_noviembre)->sum('costo'),
            'energia_diciembre' => Collection::make($energia_diciembre)->sum('costo'),  
            'subtitulo' => $subtitulo,                 
        );

        return $data;
    }
    
    public function GraficaPotenciaCLPMensual(Request $request){
        $data = Array();
        $empresa_id = $request->empresa;
        $conexion = Conexion::where('id_empresa', $empresa_id)->first();
		$subtitulo = $conexion == null ? 'no existe conexion' : $conexion->nombre_parq;	
        $year = $request->year;
        $enero = '01'.'/'.$year;
        $febrero = '02'.'/'.$year;
        $marzo = '03'.'/'.$year;
        $abril = '04'.'/'.$year;
        $mayo = '05'.'/'.$year;
        $junio = '06'.'/'.$year;
        $julio = '07'.'/'.$year;
        $agosto = '08'.'/'.$year;
        $septiembre = '09'.'/'.$year;
        $octubre = '10'.'/'.$year;
        $noviembre = '11'.'/'.$year;
        $diciembre = '12'.'/'.$year;        

        $energia_enero = (BalanceEconomicoPotenciaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$enero)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoPotenciaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$enero)->where('id_tipo_doc_cen',2)->get()->toArray();        
        $energia_febrero = (BalanceEconomicoPotenciaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$febrero)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoPotenciaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$febrero)->where('id_tipo_doc_cen',2)->get()->toArray();
        $energia_marzo = (BalanceEconomicoPotenciaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$marzo)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoPotenciaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$marzo)->where('id_tipo_doc_cen',2)->get()->toArray();
        $energia_abril = (BalanceEconomicoPotenciaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$abril)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoPotenciaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$abril)->where('id_tipo_doc_cen',2)->get()->toArray();
        $energia_mayo = (BalanceEconomicoPotenciaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$mayo)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoPotenciaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$mayo)->where('id_tipo_doc_cen',2)->get()->toArray();
        $energia_junio = (BalanceEconomicoPotenciaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$junio)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoPotenciaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$junio)->where('id_tipo_doc_cen',2)->get()->toArray();
        $energia_julio = (BalanceEconomicoPotenciaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$julio)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoPotenciaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$julio)->where('id_tipo_doc_cen',2)->get()->toArray();
        $energia_agosto = (BalanceEconomicoPotenciaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$agosto)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoPotenciaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$agosto)->where('id_tipo_doc_cen',2)->get()->toArray();
        $energia_septiembre = (BalanceEconomicoPotenciaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$septiembre)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoPotenciaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$septiembre)->where('id_tipo_doc_cen',2)->get()->toArray();
        $energia_octubre = (BalanceEconomicoPotenciaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$octubre)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoPotenciaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$octubre)->where('id_tipo_doc_cen',2)->get()->toArray();
        $energia_noviembre = (BalanceEconomicoPotenciaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$noviembre)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoPotenciaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$noviembre)->where('id_tipo_doc_cen',2)->get()->toArray();
        $energia_diciembre = (BalanceEconomicoPotenciaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$diciembre)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoPotenciaCEN::where('id_empresa_acreedora',$request->empresa)->where('periodo',$diciembre)->where('id_tipo_doc_cen',2)->get()->toArray();     

        $data = Array(
            'energia_enero' => Collection::make($energia_enero)->sum('costo'),
            'energia_febrero' => Collection::make($energia_febrero)->sum('costo'),
            'energia_marzo' => Collection::make($energia_marzo)->sum('costo'),
            'energia_abril' => Collection::make($energia_abril)->sum('costo'),
            'energia_mayo' => Collection::make($energia_mayo)->sum('costo'),
            'energia_junio' => Collection::make($energia_junio)->sum('costo'),
            'energia_julio' => Collection::make($energia_julio)->sum('costo'),
            'energia_agosto' => Collection::make($energia_agosto)->sum('costo'),
            'energia_septiembre' => Collection::make($energia_septiembre)->sum('costo'),
            'energia_octubre' => Collection::make($energia_octubre)->sum('costo'),
            'energia_noviembre' => Collection::make($energia_noviembre)->sum('costo'),
            'energia_diciembre' => Collection::make($energia_diciembre)->sum('costo'),   
            'subtitulo' => $subtitulo,                
        );

        return $data;
    }  
    
    public function GraficaServCompCLPMensual(Request $request){
        $data = Array();
        $empresa_id = $request->empresa;
        $conexion = Conexion::where('id_empresa', $empresa_id)->first();
		$subtitulo = $conexion == null ? 'no existe conexion' : $conexion->nombre_parq;	
        $year = $request->year;
        $enero = '01'.'/'.$year;
        $febrero = '02'.'/'.$year;
        $marzo = '03'.'/'.$year;
        $abril = '04'.'/'.$year;
        $mayo = '05'.'/'.$year;
        $junio = '06'.'/'.$year;
        $julio = '07'.'/'.$year;
        $agosto = '08'.'/'.$year;
        $septiembre = '09'.'/'.$year;
        $octubre = '10'.'/'.$year;
        $noviembre = '11'.'/'.$year;
        $diciembre = '12'.'/'.$year;        

        $energia_enero = (BalanceEconomicoSSCCCEN::where('id_empresa_deudora',$request->empresa)->where('periodo',$enero)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoSSCCCEN::where('id_empresa_deudora',$request->empresa)->where('periodo',$enero)->where('id_tipo_doc_cen',2)->get()->toArray();        
        $energia_febrero = (BalanceEconomicoSSCCCEN::where('id_empresa_deudora',$request->empresa)->where('periodo',$febrero)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoSSCCCEN::where('id_empresa_deudora',$request->empresa)->where('periodo',$febrero)->where('id_tipo_doc_cen',2)->get()->toArray();
        $energia_marzo = (BalanceEconomicoSSCCCEN::where('id_empresa_deudora',$request->empresa)->where('periodo',$marzo)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoSSCCCEN::where('id_empresa_deudora',$request->empresa)->where('periodo',$marzo)->where('id_tipo_doc_cen',2)->get()->toArray();
        $energia_abril = (BalanceEconomicoSSCCCEN::where('id_empresa_deudora',$request->empresa)->where('periodo',$abril)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoSSCCCEN::where('id_empresa_deudora',$request->empresa)->where('periodo',$abril)->where('id_tipo_doc_cen',2)->get()->toArray();
        $energia_mayo = (BalanceEconomicoSSCCCEN::where('id_empresa_deudora',$request->empresa)->where('periodo',$mayo)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoSSCCCEN::where('id_empresa_deudora',$request->empresa)->where('periodo',$mayo)->where('id_tipo_doc_cen',2)->get()->toArray();
        $energia_junio = (BalanceEconomicoSSCCCEN::where('id_empresa_deudora',$request->empresa)->where('periodo',$junio)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoSSCCCEN::where('id_empresa_deudora',$request->empresa)->where('periodo',$junio)->where('id_tipo_doc_cen',2)->get()->toArray();
        $energia_julio = (BalanceEconomicoSSCCCEN::where('id_empresa_deudora',$request->empresa)->where('periodo',$julio)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoSSCCCEN::where('id_empresa_deudora',$request->empresa)->where('periodo',$julio)->where('id_tipo_doc_cen',2)->get()->toArray();
        $energia_agosto = (BalanceEconomicoSSCCCEN::where('id_empresa_deudora',$request->empresa)->where('periodo',$agosto)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoSSCCCEN::where('id_empresa_deudora',$request->empresa)->where('periodo',$agosto)->where('id_tipo_doc_cen',2)->get()->toArray();
        $energia_septiembre = (BalanceEconomicoSSCCCEN::where('id_empresa_deudora',$request->empresa)->where('periodo',$septiembre)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoSSCCCEN::where('id_empresa_deudora',$request->empresa)->where('periodo',$septiembre)->where('id_tipo_doc_cen',2)->get()->toArray();
        $energia_octubre = (BalanceEconomicoSSCCCEN::where('id_empresa_deudora',$request->empresa)->where('periodo',$octubre)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoSSCCCEN::where('id_empresa_deudora',$request->empresa)->where('periodo',$octubre)->where('id_tipo_doc_cen',2)->get()->toArray();
        $energia_noviembre = (BalanceEconomicoSSCCCEN::where('id_empresa_deudora',$request->empresa)->where('periodo',$noviembre)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoSSCCCEN::where('id_empresa_deudora',$request->empresa)->where('periodo',$noviembre)->where('id_tipo_doc_cen',2)->get()->toArray();
        $energia_diciembre = (BalanceEconomicoSSCCCEN::where('id_empresa_deudora',$request->empresa)->where('periodo',$diciembre)->where('id_tipo_doc_cen',2)->get()->toArray() == null) ? 0 : BalanceEconomicoSSCCCEN::where('id_empresa_deudora',$request->empresa)->where('periodo',$diciembre)->where('id_tipo_doc_cen',2)->get()->toArray();     

        $data = Array(
            'energia_enero' => Collection::make($energia_enero)->sum('costo'),
            'energia_febrero' => Collection::make($energia_febrero)->sum('costo'),
            'energia_marzo' => Collection::make($energia_marzo)->sum('costo'),
            'energia_abril' => Collection::make($energia_abril)->sum('costo'),
            'energia_mayo' => Collection::make($energia_mayo)->sum('costo'),
            'energia_junio' => Collection::make($energia_junio)->sum('costo'),
            'energia_julio' => Collection::make($energia_julio)->sum('costo'),
            'energia_agosto' => Collection::make($energia_agosto)->sum('costo'),
            'energia_septiembre' => Collection::make($energia_septiembre)->sum('costo'),
            'energia_octubre' => Collection::make($energia_octubre)->sum('costo'),
            'energia_noviembre' => Collection::make($energia_noviembre)->sum('costo'),
            'energia_diciembre' => Collection::make($energia_diciembre)->sum('costo'),   
            'subtitulo' => $subtitulo,                
        );

        return $data;
    } 
    
    public function GraficaPotenciaDiaria(Request $request){
        date_default_timezone_set('America/Santiago');
        $data = Array();
        $empresa_id = $request->empresa;
        $conexion = Conexion::where('id_empresa', $empresa_id)->first();
        $host = $conexion == null ? 0 : $conexion->host;
        $pfv = $conexion == null ? 0 : $conexion->id_parq;
        $medidor = $conexion == null ? 0 : $conexion->id_medidor;
        $radsensor = $conexion == null ? 0 : $conexion->id_radsensor;
        $database_name = $conexion == null ? 0 : $conexion->db;
        $subtitulo = $conexion == null ? 'no existe conexion' : $conexion->nombre_parq;						

        $inicio = date('Y-m-d').' 00:00:00';
        $fin = date('Y-m-d').' 23:59:59';

		$data = Array();
        $ptRaw = Array();
		$irRaw = Array();
		$prRaw = Array();
        $dataRaw = Array();
		$horasRaw = Array();
		
        //DB CONNECTION
        if($host == 1){
            $host = config('app.host_1');
            $port = config('app.host_port_1');
            $password = config('app.host_password_1');
        }else if($host == 2){

        }else{
            $host = 0;
            $port = 0;
            $password = 0;            
        }

        if($host != 0){
            //INICIO DE LA CONSULTA
            $strConnect = "host = ".$host." port = ".$port." dbname = '".$database_name."' user = 'postgres' password = ".$password;
            $con = pg_connect($strConnect);
    
			//DB QUERY STRING
			$strQuery = "SELECT pr.fch_dato AS fecha, pr.pp_rad_avg AS pr, 
			abs(med.pot_act_total) AS pt, 
			rad.rad_valor AS ir 
			FROM calcular_prnew($medidor,
			'$inicio', 
			'$fin' ) pr
			LEFT JOIN tbl_datos_medidor med ON med.fch_dato=pr.fch_dato AND med.id_disp=$medidor
			LEFT JOIN tbl_datos_radsensor rad ON rad.fch_dato=pr.fch_dato AND rad.id_disp=$radsensor		
			";	

			//DB QUERY
			$resRAW = pg_query($con,$strQuery);				

			while($dataFetch = pg_fetch_assoc($resRAW))
			{
				$dataRaw[] = $dataFetch;
			}	
			
			for($i=0; $i < count($dataRaw) ; $i++){
				$ptRaw[] = abs((float)($dataRaw[$i]['pt']));
				$irRaw[] = (float)($dataRaw[$i]['ir']);
				$prRaw[] = (float)($dataRaw[$i]['pr']);
				$horasRaw[] = (double)(TimeHelper::getDateTime($dataRaw[$i]['fecha'])*1000);
			}		

			if(count($ptRaw) == 0){
				//DB QUERY STRING
				$strQuery = "SELECT fecha.fecha, rad.rad_valor AS ir, med.pot_act_total AS pt FROM 
				(
				SELECT (dias.dia + horas.hora) fecha FROM
				(
				SELECT dia::date FROM generate_series('$inicio'::date, '$fin'::date, '1 day'::interval) dia
				) dias
				INNER JOIN 
				(
				SELECT dia.hora, 1 AS relac_dias
				FROM
				(
				SELECT horas.serieh + ('''' || date_part('minutes', minutos.seriem) || ' minutes''') ::interval AS hora
				FROM 
				(
				SELECT ('''' || generate_series(0,23) || ' hours''') ::interval  AS serieh
				) AS horas
				INNER JOIN 
				(
				SELECT ('''' || generate_series(0,45,15) || ' minutes''') ::interval AS seriem, 1 AS relac_horas
				) AS minutos
				ON minutos.relac_horas=1
				) dia
				) AS horas ON horas.relac_dias=1
				) fecha 
				LEFT JOIN tbl_datos_radsensor rad ON rad.fch_dato= fecha.fecha AND rad.id_disp=$radsensor
				LEFT JOIN tbl_datos_medidor med ON med.fch_dato= fecha.fecha AND med.id_disp=$medidor
				WHERE fecha.fecha <= CURRENT_TIMESTAMP
				ORDER BY fecha.fecha ASC		
				";

				//DB QUERY
				$resRAW = pg_query($con,$strQuery);				

				while($dataFetch = pg_fetch_assoc($resRAW))
				{
					$dataRaw[] = $dataFetch;
				}	
				
				for($i=0; $i < count($dataRaw) ; $i++){
					$ptRaw[] = abs((float)($dataRaw[$i]['pt']));
					$irRaw[] = (float)($dataRaw[$i]['ir']);
					$prRaw[] = 0;
					$horasRaw[] = (double)(TimeHelper::getDateTime($dataRaw[$i]['fecha'])*1000);
				}			
			}
        }		

        $data = Array(
            'pt' => $ptRaw,
			'ir' => $irRaw,
			'pr' => $prRaw,
            'horas' => $horasRaw,
            'subtitulo' => $subtitulo,
        );

        return $data;
    }
    
    public function GraficaEnergiaDiaria(Request $request){
        date_default_timezone_set('America/Santiago');
        $data = Array();
        $empresa_id = $request->empresa;
        $conexion = Conexion::where('id_empresa', $empresa_id)->first();
        $host = $conexion == null ? 0 : $conexion->host;
        $pfv = $conexion == null ? 0 : $conexion->id_parq;
        $medidor = $conexion == null ? 0 : $conexion->id_medidor;
        $radsensor = $conexion == null ? 0 : $conexion->id_radsensor;
        $database_name = $conexion == null ? 0 : $conexion->db;
		$subtitulo = $conexion == null ? 'no existe conexion' : $conexion->nombre_parq;			
        $inicio = date('Y-m-d').' 00:00:00';
        $fin = date('Y-m-d').' 23:59:59';
		//Modalidad de Calculo
		$modalidad_calculo_id = 1;
		$modalidad = ModalidadCalculoEmpresas::where('id_empresa', $empresa_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first() == null ? $modalidad_calculo_id = 0 : ModalidadCalculoEmpresas::where('id_empresa', $empresa_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first();		
		if($modalidad_calculo_id == 1){
			$modalidad_calculo_id = $modalidad->id_modalidad_calculo;
		}
		$modalidadEmpresaBandera = 1;
		$modalidadEmpresa = ModalidadCalculo::where('id', $modalidad_calculo_id)->first() == null ? $modalidadEmpresaBandera = '' : ModalidadCalculo::where('id', $modalidad_calculo_id)->first();
		if($modalidadEmpresaBandera == 1){
			$modalidadEmpresaBandera = $modalidadEmpresa->nombre;
		}

		//Barras
		$barra_id = 1;
		$barra = BarrasEmpresas::where('id_empresa', $empresa_id)->where('tipo_calculo', $modalidad_calculo_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first() == null ? $barra_id = 0 : BarrasEmpresas::where('id_empresa', $empresa_id)->where('tipo_calculo', $modalidad_calculo_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first();
		if($barra_id == 1){
			$barra_id = $barra->id_barras;
		}
		$barraEmpresaBandera = 1;
		$barraEmpresa = Barras::where('id', $barra_id)->first() == null ? $barraEmpresaBandera = '' : Barras::where('id', $barra_id)->first();
		if($barraEmpresaBandera == 1){
			$barraEmpresaBandera = $barraEmpresa->barra_human;
		}		

		//Definimos la modalidad del calculo Ejemplo PE o CMg
		$precio_energia = 1;	
		if ($modalidad_calculo_id == 1) {			
			$precio = PrecioEstabilizado::where('id_barra', $barra_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first() == null ? $precio_energia = 0 : PrecioEstabilizado::where('id_barra', $barra_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first();
		}else {
			$precio_energia = 0;
		}		
		
		$data = Array();
        $consumoRaw = Array();
		$inyeccionRaw = Array();
        $consumoRaw2 = Array();
		$inyeccionRaw2 = Array();	
		$inyeccionCLPRaw = Array();	
		$inyeccionCLPRaw2 = Array();	
		$dataRaw = Array();
		$dataRaw2 = Array();
		$horasRaw = Array();	

		if ($precio_energia == 1) {
			$precio_energia = $precio->precio_energia;
		}

        //DB CONNECTION
        if($host == 1){
            $host = config('app.host_1');
            $port = config('app.host_port_1');
            $password = config('app.host_password_1');
        }else if($host == 2){

        }else{
            $host = 0;
            $port = 0;
            $password = 0;            
        }

        if($host != 0){
			//INICIO DE LA CONSULTA
            $strConnect = "host = ".$host." port = ".$port." dbname = '".$database_name."' user = 'postgres' password = ".$password;
			$con = pg_connect($strConnect);
						
			$consulta_cont = 0;
			$fecha_inicial_ciclo = $inicio;
			$fecha_final_ciclo = $fin;

			while(strtotime($fecha_inicial_ciclo) < strtotime($fecha_final_ciclo)){
				
				$end = strtotime('+1 hour', strtotime($fecha_inicial_ciclo));
				$end = date('Y-m-d H:i:s', $end);

				$start = strtotime('-1 hour', strtotime($end));
				$start = date('Y-m-d H:i:s', $start);			
				if(strtotime($end) <= strtotime($fecha_final_ciclo))
				{
					//DB QUERY STRING
					$strQuery = "SELECT (MAX(ener_in) - MIN(ener_in)) AS consumo, (MAX(ener_out) - MIN(ener_out)) AS inyeccion  
					FROM tbl_datos_medidor	
					WHERE id_disp = $medidor
					AND fch_dato BETWEEN '$start' AND '$end'		
					";
				}

				//DB QUERY
				$resRAW = pg_query($con,$strQuery);					

				while($dataFetch = pg_fetch_assoc($resRAW))
				{
					$dataRaw[] = $dataFetch;
				}

				$fecha_expode_0 = $end;
				$fecha_expode_1 = explode(' ', $fecha_expode_0);
				$fecha_expode_2 = explode(':',$fecha_expode_1[1]);
				$fecha_expode_3 = $fecha_expode_2[0].':'.$fecha_expode_2[1];			

				$consumoRaw[] = abs((float)($dataRaw[$consulta_cont]['consumo']));
				$inyeccionRaw[] = abs((float)($dataRaw[$consulta_cont]['inyeccion']));
				$inyeccionCLPRaw[] = (float)number_format((abs(($dataRaw[$consulta_cont]['inyeccion']))) * $precio_energia, 3, '.', '');
				$horasRaw[] = $fecha_expode_3;
				
				$consulta_cont++;
				$fecha_inicial_ciclo = $end;
			}
			//FIN DE LA CONSULTA
			
			//DB QUERY STRING
			$strQuery2 = "SELECT (MAX(ener_in) - MIN(ener_in)) AS consumo, (MAX(ener_out) - MIN(ener_out)) AS inyeccion  
			FROM tbl_datos_medidor	
			WHERE id_disp = $medidor
			AND fch_dato BETWEEN '$inicio' AND '$fin'		
			";

			//DB QUERY
			$resRAW2 = pg_query($con,$strQuery2);		

			while($dataFetch2 = pg_fetch_assoc($resRAW2))
			{
				$dataRaw2[] = $dataFetch2;
			}	
			
			for($i=0; $i < count($dataRaw2) ; $i++){
				$consumoRaw2[] = abs((float)($dataRaw2[$i]['consumo']));
				$inyeccionRaw2[] = abs((float)($dataRaw2[$i]['inyeccion']));
				$inyeccionCLPRaw2[] = (float)number_format(abs(($dataRaw2[$i]['inyeccion'])) * $precio_energia, 0, '.', '');
			}
        }		

        $data = Array(
            'consumo' => $consumoRaw,
			'generacion' => $inyeccionRaw,
			'generacionCLP' => $inyeccionCLPRaw,
            'consumoTotal' => $consumoRaw2,
			'generacionTotal' => $inyeccionRaw2,
			'generacionCLPTotal' => $inyeccionCLPRaw2,			
			'horas' => $horasRaw,
			'subtitulo' => $subtitulo,
			'barra' => $barraEmpresaBandera,
			'modalidad' => $modalidadEmpresaBandera,
			'precio_energia' => $precio_energia,
        );

        return $data;		
    }
    
    public function GraficaEnergiaMes(Request $request){
        date_default_timezone_set('America/Santiago');
        $data = Array();
        $empresa_id = $request->empresa;
        $conexion = Conexion::where('id_empresa', $empresa_id)->first();
        $host = $conexion == null ? 0 : $conexion->host;
        $pfv = $conexion == null ? 0 : $conexion->id_parq;
        $medidor = $conexion == null ? 0 : $conexion->id_medidor;
        $radsensor = $conexion == null ? 0 : $conexion->id_radsensor;
        $database_name = $conexion == null ? 0 : $conexion->db;
		$subtitulo = $conexion == null ? 'no existe conexion' : $conexion->nombre_parq;			     
		$inicial_date =  date("Y").'-'.date("m").'-01';
		$inicio = $inicial_date.' 00:00:00';		
		$fin = date("Y-m-t", strtotime($inicial_date)).' 23:59:59';	
		$inicio_front = TimeHelper::formatDateHuman($inicial_date);
		$fin_front = date("t/m/Y", strtotime($inicial_date));
		//Modalidad de Calculo
		$modalidad_calculo_id = 1;
		$modalidad = ModalidadCalculoEmpresas::where('id_empresa', $empresa_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first() == null ? $modalidad_calculo_id = 0 : ModalidadCalculoEmpresas::where('id_empresa', $empresa_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first();		
		if($modalidad_calculo_id == 1){
			$modalidad_calculo_id = $modalidad->id_modalidad_calculo;
		}
		$modalidadEmpresaBandera = 1;
		$modalidadEmpresa = ModalidadCalculo::where('id', $modalidad_calculo_id)->first() == null ? $modalidadEmpresaBandera = '' : ModalidadCalculo::where('id', $modalidad_calculo_id)->first();
		if($modalidadEmpresaBandera == 1){
			$modalidadEmpresaBandera = $modalidadEmpresa->nombre;
		}

		//Barras
		$barra_id = 1;
		$barra = BarrasEmpresas::where('id_empresa', $empresa_id)->where('tipo_calculo', $modalidad_calculo_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first() == null ? $barra_id = 0 : BarrasEmpresas::where('id_empresa', $empresa_id)->where('tipo_calculo', $modalidad_calculo_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first();
		if($barra_id == 1){
			$barra_id = $barra->id_barras;
		}
		$barraEmpresaBandera = 1;
		$barraEmpresa = Barras::where('id', $barra_id)->first() == null ? $barraEmpresaBandera = '' : Barras::where('id', $barra_id)->first();
		if($barraEmpresaBandera == 1){
			$barraEmpresaBandera = $barraEmpresa->barra_human;
		}		

		//Definimos la modalidad del calculo Ejemplo PE o CMg
		$precio_energia = 1;	
		if ($modalidad_calculo_id == 1) {			
			$precio = PrecioEstabilizado::where('id_barra', $barra_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first() == null ? $precio_energia = 0 : PrecioEstabilizado::where('id_barra', $barra_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first();
		}else {
			$precio_energia = 0;
		}	

		$data = Array();
        $consumoRaw = Array();
		$inyeccionRaw = Array();
        $consumoRaw2 = Array();
		$inyeccionRaw2 = Array();	
		$inyeccionCLPRaw = Array();	
		$inyeccionCLPRaw2 = Array();	
		$dataRaw = Array();
		$dataRaw2 = Array();
		$horasRaw = Array();

		if ($precio_energia == 1) {
			$precio_energia = $precio->precio_energia;
		}

        //DB CONNECTION
        if($host == 1){
            $host = config('app.host_1');
            $port = config('app.host_port_1');
            $password = config('app.host_password_1');
        }else if($host == 2){

        }else{
            $host = 0;
            $port = 0;
            $password = 0;            
        }

        if($host != 0){
			//INICIO DE LA CONSULTA
            $strConnect = "host = ".$host." port = ".$port." dbname = '".$database_name."' user = 'postgres' password = ".$password;
			$con = pg_connect($strConnect);
						
			$consulta_cont = 0;
			$fecha_inicial_ciclo = $inicio;
			$fecha_final_ciclo = $fin;

			while(strtotime($fecha_inicial_ciclo) < strtotime($fecha_final_ciclo)){
				
				$end = strtotime('+1 day', strtotime($fecha_inicial_ciclo));
				$end = date('Y-m-d H:i:s', $end);

				$start = strtotime('-1 day', strtotime($end));
				$start = date('Y-m-d H:i:s', $start);			
				if(strtotime($start) <= strtotime($fecha_final_ciclo))
				{
					//DB QUERY STRING
					$strQuery = "SELECT (MAX(ener_in) - MIN(ener_in)) AS consumo, (MAX(ener_out) - MIN(ener_out)) AS inyeccion  
					FROM tbl_datos_medidor	
					WHERE id_disp = $medidor
					AND fch_dato BETWEEN '$start' AND '$end'		
					";
				}

				//DB QUERY
				$resRAW = pg_query($con,$strQuery);					

				while($dataFetch = pg_fetch_assoc($resRAW))
				{
					$dataRaw[] = $dataFetch;
				}
				
				$fecha_expode_0 = $start;
				$fecha_expode_1 = explode(' ', $fecha_expode_0);
				$fecha_expode_2 = explode(':',$fecha_expode_1[1]);
				$fecha_expode_3 = $fecha_expode_2[0].':'.$fecha_expode_2[1];	

				$consumoRaw[] = abs((float)($dataRaw[$consulta_cont]['consumo']));
				$inyeccionRaw[] = abs((float)($dataRaw[$consulta_cont]['inyeccion']));
				$inyeccionCLPRaw[] = (float)number_format((abs(($dataRaw[$consulta_cont]['inyeccion']))) * $precio_energia, 3, '.', '');	
				$horasRaw[] = TimeHelper::formatDateHuman($fecha_expode_1[0]);
				
				$consulta_cont++;
				$fecha_inicial_ciclo = $end;
			}
			//FIN DE LA CONSULTA
			
			//DB QUERY STRING
			$strQuery2 = "SELECT (MAX(ener_in) - MIN(ener_in)) AS consumo, (MAX(ener_out) - MIN(ener_out)) AS inyeccion  
			FROM tbl_datos_medidor	
			WHERE id_disp = $medidor
			AND fch_dato BETWEEN '$inicio' AND '$fin'		
			";

			//DB QUERY
			$resRAW2 = pg_query($con,$strQuery2);		

			while($dataFetch2 = pg_fetch_assoc($resRAW2))
			{
				$dataRaw2[] = $dataFetch2;
			}	
			
			for($i=0; $i < count($dataRaw2) ; $i++){
				$consumoRaw2[] = abs((float)($dataRaw2[$i]['consumo']));
				$inyeccionRaw2[] = abs((float)($dataRaw2[$i]['inyeccion']));
				$inyeccionCLPRaw2[] = (float)number_format(abs(($dataRaw2[$i]['inyeccion'])) * $precio_energia, 0, '.', '');
			}
        }		

        $data = Array(
            'consumo' => $consumoRaw,
			'generacion' => $inyeccionRaw,
			'generacionCLP' => $inyeccionCLPRaw,
            'consumoTotal' => $consumoRaw2,
			'generacionTotal' => $inyeccionRaw2,
			'generacionCLPTotal' => $inyeccionCLPRaw2,			
			'horas' => $horasRaw,
			'subtitulo' => $subtitulo,
			'initial_date' => $inicio_front,
			'final_date' => $fin_front,
			'barra' => $barraEmpresaBandera,
			'modalidad' => $modalidadEmpresaBandera,
			'precio_energia' => $precio_energia,
        );

        return $data;		
    }
    
    public function GraficaEnergiaMesAnt(Request $request){
        date_default_timezone_set('America/Santiago');
        $data = Array();
        $empresa_id = $request->empresa;
        $conexion = Conexion::where('id_empresa', $empresa_id)->first();
        $host = $conexion == null ? 0 : $conexion->host;
        $pfv = $conexion == null ? 0 : $conexion->id_parq;
        $medidor = $conexion == null ? 0 : $conexion->id_medidor;
        $radsensor = $conexion == null ? 0 : $conexion->id_radsensor;
        $database_name = $conexion == null ? 0 : $conexion->db;
		$subtitulo = $conexion == null ? 'no existe conexion' : $conexion->nombre_parq;			     
        $inicial_date =  date("Y").'-'.date("m").'-01';
        $inicial_date_ant = strtotime('-1 month', strtotime($inicial_date));
        $inicial_date_ant_f = date('Y-m-d', $inicial_date_ant);		
		$inicio = $inicial_date_ant_f.' 00:00:00';		
		$fin = date("Y-m-t", strtotime($inicial_date_ant_f)).' 23:59:59';	
		$inicio_front = TimeHelper::formatDateHuman($inicial_date_ant_f);
		$fin_front = date("t/m/Y", strtotime($inicial_date_ant_f));
		//Modalidad de Calculo
		$modalidad_calculo_id = 1;
		$modalidad = ModalidadCalculoEmpresas::where('id_empresa', $empresa_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first() == null ? $modalidad_calculo_id = 0 : ModalidadCalculoEmpresas::where('id_empresa', $empresa_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first();		
		if($modalidad_calculo_id == 1){
			$modalidad_calculo_id = $modalidad->id_modalidad_calculo;
		}
		$modalidadEmpresaBandera = 1;
		$modalidadEmpresa = ModalidadCalculo::where('id', $modalidad_calculo_id)->first() == null ? $modalidadEmpresaBandera = '' : ModalidadCalculo::where('id', $modalidad_calculo_id)->first();
		if($modalidadEmpresaBandera == 1){
			$modalidadEmpresaBandera = $modalidadEmpresa->nombre;
		}

		//Barras
		$barra_id = 1;
		$barra = BarrasEmpresas::where('id_empresa', $empresa_id)->where('tipo_calculo', $modalidad_calculo_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first() == null ? $barra_id = 0 : BarrasEmpresas::where('id_empresa', $empresa_id)->where('tipo_calculo', $modalidad_calculo_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first();
		if($barra_id == 1){
			$barra_id = $barra->id_barras;
		}
		$barraEmpresaBandera = 1;
		$barraEmpresa = Barras::where('id', $barra_id)->first() == null ? $barraEmpresaBandera = '' : Barras::where('id', $barra_id)->first();
		if($barraEmpresaBandera == 1){
			$barraEmpresaBandera = $barraEmpresa->barra_human;
		}		

		//Definimos la modalidad del calculo Ejemplo PE o CMg
		$precio_energia = 1;	
		if ($modalidad_calculo_id == 1) {			
			$precio = PrecioEstabilizado::where('id_barra', $barra_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first() == null ? $precio_energia = 0 : PrecioEstabilizado::where('id_barra', $barra_id)->where('inicial_date', '<=', $inicio)->where('final_date', '>=', $fin)->first();
		}else {
			$precio_energia = 0;
		}	

		$data = Array();
        $consumoRaw = Array();
		$inyeccionRaw = Array();
        $consumoRaw2 = Array();
		$inyeccionRaw2 = Array();	
		$inyeccionCLPRaw = Array();	
		$inyeccionCLPRaw2 = Array();	
		$dataRaw = Array();
		$dataRaw2 = Array();
		$horasRaw = Array();

		if ($precio_energia == 1) {
			$precio_energia = $precio->precio_energia;
		}

        //DB CONNECTION
        if($host == 1){
            $host = config('app.host_1');
            $port = config('app.host_port_1');
            $password = config('app.host_password_1');
        }else if($host == 2){

        }else{
            $host = 0;
            $port = 0;
            $password = 0;            
        }

        if($host != 0){
			//INICIO DE LA CONSULTA
            $strConnect = "host = ".$host." port = ".$port." dbname = '".$database_name."' user = 'postgres' password = ".$password;
			$con = pg_connect($strConnect);
						
			$consulta_cont = 0;
			$fecha_inicial_ciclo = $inicio;
			$fecha_final_ciclo = $fin;

			while(strtotime($fecha_inicial_ciclo) < strtotime($fecha_final_ciclo)){
				
				$end = strtotime('+1 day', strtotime($fecha_inicial_ciclo));
				$end = date('Y-m-d H:i:s', $end);

				$start = strtotime('-1 day', strtotime($end));
				$start = date('Y-m-d H:i:s', $start);			
				if(strtotime($start) <= strtotime($fecha_final_ciclo))
				{
					//DB QUERY STRING
					$strQuery = "SELECT (MAX(ener_in) - MIN(ener_in)) AS consumo, (MAX(ener_out) - MIN(ener_out)) AS inyeccion  
					FROM tbl_datos_medidor	
					WHERE id_disp = $medidor
					AND fch_dato BETWEEN '$start' AND '$end'		
					";
				}

				//DB QUERY
				$resRAW = pg_query($con,$strQuery);					

				while($dataFetch = pg_fetch_assoc($resRAW))
				{
					$dataRaw[] = $dataFetch;
				}
				
				$fecha_expode_0 = $start;
				$fecha_expode_1 = explode(' ', $fecha_expode_0);
				$fecha_expode_2 = explode(':',$fecha_expode_1[1]);
				$fecha_expode_3 = $fecha_expode_2[0].':'.$fecha_expode_2[1];	

				$consumoRaw[] = abs((float)($dataRaw[$consulta_cont]['consumo']));
				$inyeccionRaw[] = abs((float)($dataRaw[$consulta_cont]['inyeccion']));
				$inyeccionCLPRaw[] = (float)number_format((abs(($dataRaw[$consulta_cont]['inyeccion']))) * $precio_energia, 3, '.', '');	
				$horasRaw[] = TimeHelper::formatDateHuman($fecha_expode_1[0]);
				
				$consulta_cont++;
				$fecha_inicial_ciclo = $end;
			}
			//FIN DE LA CONSULTA
			
			//DB QUERY STRING
			$strQuery2 = "SELECT (MAX(ener_in) - MIN(ener_in)) AS consumo, (MAX(ener_out) - MIN(ener_out)) AS inyeccion  
			FROM tbl_datos_medidor	
			WHERE id_disp = $medidor
			AND fch_dato BETWEEN '$inicio' AND '$fin'		
			";

			//DB QUERY
			$resRAW2 = pg_query($con,$strQuery2);		

			while($dataFetch2 = pg_fetch_assoc($resRAW2))
			{
				$dataRaw2[] = $dataFetch2;
			}	
			
			for($i=0; $i < count($dataRaw2) ; $i++){
				$consumoRaw2[] = abs((float)($dataRaw2[$i]['consumo']));
				$inyeccionRaw2[] = abs((float)($dataRaw2[$i]['inyeccion']));
				$inyeccionCLPRaw2[] = (float)number_format(abs(($dataRaw2[$i]['inyeccion'])) * $precio_energia, 0, '.', '');
			}
        }		

        $data = Array(
            'consumo' => $consumoRaw,
			'generacion' => $inyeccionRaw,
			'generacionCLP' => $inyeccionCLPRaw,
            'consumoTotal' => $consumoRaw2,
			'generacionTotal' => $inyeccionRaw2,
			'generacionCLPTotal' => $inyeccionCLPRaw2,			
			'horas' => $horasRaw,
			'subtitulo' => $subtitulo,
			'initial_date' => $inicio_front,
			'final_date' => $fin_front,
			'barra' => $barraEmpresaBandera,
			'modalidad' => $modalidadEmpresaBandera,
			'precio_energia' => $precio_energia,
        );

        return $data;		
	}
}
