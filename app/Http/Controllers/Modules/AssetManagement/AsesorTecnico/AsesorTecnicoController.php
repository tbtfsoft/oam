<?php

namespace App\Http\Controllers\Modules\AssetManagement\AsesorTecnico;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AsesorTecnicoController  extends Controller
{
	public function index () {
		return view('modules.asset-management.asesor-tecnico.index');
	}	
}