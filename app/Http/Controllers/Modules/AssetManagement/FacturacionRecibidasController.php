<?php

namespace App\Http\Controllers\Modules\AssetManagement;

use App\Http\Controllers\Controller;
use App\Models\AssetManagement\FacturasRecibidas as Model;
use Illuminate\Http\Request;
use Illuminate\Support\Collection as Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Storage;
use DB;
use Lang;

class FacturacionRecibidasController extends Controller
{

    /**
    * [Controlador de la vista de emitidas]
    * @return View
    */
    public function index() {
        return view('modules.asset-management.cargar-datos.facturas-recibidas');
    }

    /**
     * [Controlador de la vista de recibidas]
     * @return View
     */    
    public function recibidas (Request $request) {
        return view('modules.asset-management.facturacion.recibidas')->with('data', $this->getData($request));
    }

    /**
     * [getData description]
     * @param  Request $request    [description]
     * @param  String  $cuadroPago [description]
     * @return [type]              [description]
     */
    public function getData (Request $request)
    {
        $where = isset($request->empresa_xls) && $request->empresa_xls ? " WHERE id_empresa = '$request->empresa_xls'" : null;
        if ($where) {
          $where = $request->mes && $request->anio ? $where . " AND periodo = '$request->mes/$request->anio'" : $where;
        } else {
          $where = $request->mes && $request->anio ? $where . " WHERE periodo = '$request->mes/$request->anio'" : $where;
        }

        $data = DB::select("SELECT * FROM " . Model::getTableModel() . $where);

        $page = $request->page ? $request->page : 1;

        $size = $request->size ? $request->size : 20;

        $collect = collect($data);

        $paginationData = new LengthAwarePaginator($collect->forPage($page, $size), $collect->count(),$size,$page);

        return [
            'collects' => $paginationData
        ];
    }


    /**
    * Ajax para procesar el CSV
    * @param  Request $request
    * @return Json
    */
    public function ajaxCSV(Request $request)
    {        
        $catch = [];

        if($request->hasfile('files')) {
            $files = $request->file('files');
            foreach($files as $file) {
              $fileXML = $this->getFileXML($file);            
              if ($fileXML) {
                $row = $this->getRow($fileXML, $request);
                Model::firstOrCreate(
                  [
                    'id_empresa_deudora' => $row['id_empresa_deudora'],
                    'id_empresa_acreedora' => $row['id_empresa_acreedora'],
                    'folio' => $row['folio']
                  ],
                  $row
                );
              }
            }
            return response()->json(["message" => Lang::get("app.archivos procesados correctamente")], 200);
        } else {
          return response()->json(["message" => Lang::get("app.debe subir por lo menos un archivo")], 500);
        }
    }

    /**
     * Obtiene el xml mediante el nombre del path del .xml
     * @param  String $filename Nombre del .xml
     * @return simplexml 
     * @author Carlos Anselmi <carlosanselmi2@gmail.com>
     */
    private function getFileXML ($file)
    {
        try {
            return simplexml_load_file($file)->Documento;
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Obtiene los datos requeridos del xml
     * @param  simplexml $xml Archivo a leer
     * @return Array      Array con los datos requeridos
     * @author Carlos Anselmi <carlosanselmi2@gmail.com>
     */
    private function getRow ($xml, $request)
    {
        return [
            'periodo' => $this->getPeriodo($request->input('mes'), $request->input('anio')),
            'id_empresa'        =>  $request->input('empresa_xls'),
            'id_empresa_deudora' => $xml->Encabezado->Receptor->RUTRecep,
            'id_empresa_acreedora' => $xml->Encabezado->Emisor->RUTEmisor,
            'id_tipo_dte' => $xml->Encabezado->IdDoc->TipoDTE,
            'folio' => $xml->Encabezado->IdDoc->Folio,
            'fecha_emision' => $xml->Encabezado->IdDoc->FchEmis,
            'fecha_aceptacion' => $xml->TED->DD->CAF->DA->FA,
            'fecha_aceptacion_ts' => $xml->TED->DD->TSTED,
            'fecha_vencimiento' => $xml->Encabezado->IdDoc->FchVenc,
            'forma_pago' => $xml->Encabezado->IdDoc->FmaPago,
            'monto_neto' => $xml->Encabezado->Totales->MntNeto,
            'monto_bruto' => $xml->Encabezado->Totales->MntTotal,
            'iva' => $xml->Encabezado->Totales->IVA
        ];
    }

    /**
    * Retorna el periodo formateado. $month/$year (MM/YY)
    * @param  String $month  Mes
    * @param  String $year Año
    * @return String       Mes formateado
    * @author Carlos Anselmi <Carlos Anselmi>
    */
    private function getPeriodo($month, $year)
    {
        return $month . '/' . $year;
    }
}
