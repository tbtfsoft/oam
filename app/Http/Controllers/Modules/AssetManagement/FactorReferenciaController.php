<?php

namespace App\Http\Controllers\Modules\AssetManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\MessagesResponseTrait;
use App\Models\FactorReferencia;
use Storage;
use DB;
use Lang;

class FactorReferenciaController extends Controller
{

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
    public function index()
    {
        return view('modules.asset-management.cargar-datos.factor-referencia');
    }

  /**
   * Ajax para procesar el CSV
   * @param  Request $request
   * @return Json
   */
  public function ajaxCSV(Request $request)
  {
    if (!$request->hasFile('fileCSV')) {
      return response()->json([
        "message" => "No se adjunto el archivo. Intente de nuevo."
      ],500);
    }

    //obtenemos el campo file definido en el formulario
    $file = $request->file('fileCSV');

    //obtenemos el nombre del archivo
    $name = $file->getClientOriginalName();

    //indicamos que queremos guardar un nuevo archivo en el disco local
    if(Storage::disk('file')->put($name,\File::get($file))) {

      $handle = fopen(\Storage::disk('file')->path($name), "r");

      $count = 0;

      while ($row = fgetcsv($handle, 1000, ";")) {
        $this->recovery($row, $request, $count = $count + 1);
      }

      fclose($handle);

      Storage::disk('file')->delete($name);
    }

    return response()->json([
        "message"   => lang::get("app.Archivo importado exitosamente"),
        "registro"  => $count - 1
    ],200);
  }

  /**
   * Añade un nuevo registro a la tabla de Energia Medidores CEN
   * @param  Array   $row     Los datos de energia
   * @param  Request $request
   * @param  Integer  $count   Contador del count
   * @return Void
   */
  private function recovery(Array $row, Request $request, $count)
  {
    if($count != 1) {
      FactorReferencia::firstOrCreate([
        'id_empresa'        =>  $request->input('empresa_xls'),
        'id_user'           =>  $request->user()->id,
        'id_tipo_doc_cen'   =>  $request->input('tipo_doc_cen'),
        'hora'          =>  $row[0],
        'dia'           =>  $row[1],
        'fr_generado'   =>  floatval(str_replace(",", ".", $row[2])),
        'fr_consumido'  =>  floatval(str_replace(",", ".", $row[3])),
        'periodo'       =>  $this->getPeriodo($request->input('mes'), $request->input('anio'))
      ]);
    }
  }
        
  /**
  * Retorna el periodo formateado. $month/$year (MM/YY)
  * @param  String $month  Mes
  * @param  String $year Año
  * @return String       Mes formateado
  * @author Carlos Anselmi <Carlos Anselmi>
  */
  private function getPeriodo($month, $year)
  {
      return $month . '/' . $year;
  }
}