<?php

namespace App\Http\Controllers\Modules\AssetManagement;

use App\Models\AssetManagement\EnergiaMedidoresOEoriginal as EnegiaOE;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Storage;
use Lang;

class EnergiaMedidoresControllerOE extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
	public function energiaMedidores()
	{
		return view('modules.asset-management.cargar-datos.energia-medidores-oe');
	}

  /**
   * Ajax para procesar el CSV
   * @param  Request $request
   * @return Json
   */
  public function ajaxCSV(Request $request)
  {
    if (!$request->hasFile('fileCSV')) {
      return response()->json([
        "message" => lang::get("app.No se adjunto el archivo. Intentelo de nuevo")
      ],500);
    }

    $request->periodo = $this->getPeriodo($request->input('mes'),$request->input('anio'));

    //obtenemos el campo file definido en el formulario
    $file = $request->file('fileCSV');

    //obtenemos el nombre del archivo
    $name = $file->getClientOriginalName();

    //indicamos que queremos guardar un nuevo archivo en el disco local
    if(Storage::disk('file')->put($name,\File::get($file))) {

      $handle = fopen(\Storage::disk('file')->path($name), "r");

      $count = -1;

      while ($row = fgetcsv($handle, 1000, ";")) {
        $this->recovery($row,$request,$count = $count+1);
      }

      fclose($handle);

      Storage::disk('file')->delete($name);
    }

    return response()->json([
        "message"   => lang::get("app.Archivo importado exitosamente"),
        "registro"  => $count - 1
    ],200);
  }

  /**
   * Añade un nuevo registro a la tabla de Energia Medidores OE
   * @param  Array   $row     Los datos de energia
   * @param  Request $request
   * @param  Integer  $count   Contador del count
   * @return Void
   */
  private function recovery(Array $row, Request $request, $count)
  {
    if($count != 1) {
      if($count == 0)
        $row[0] =  9000;
      
      $row[1] = !empty($row[1]) ? $row[1] : 0;

      $row[2] = !empty($row[2]) ? $row[2] : 0;

      EnegiaOE::firstOrCreate([
        'id_empresa' =>  $request->input('empresa_xls'),
        'id_user'    =>  $request->user()->id,
        'periodo'    =>  $request->input('mes').'/'.$request->input('anio'),
        'hora'       =>  $row[0],
        'kwh_out'    =>  $row[1],
        'kwh_in'     =>  $row[2]
      ]);
    }
  }

  /**
   * Retorna el periodo formateado. $month/$year (MM/YY)
   * @param  String $month  Mes
   * @param  String $year Año
   * @return String       Mes formateado
   * @author Carlos Anselmi <Carlos Anselmi>
   */
  private function getPeriodo($month,$year)
  {
    return $month . '/' . $year;
  }
}