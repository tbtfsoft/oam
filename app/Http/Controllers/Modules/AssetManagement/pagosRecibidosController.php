<?php

namespace App\Http\Controllers\Modules\AssetManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class pagosRecibidosController extends Controller
{

  /**
  * [Controlador de la vista de pagos recibidos]
  * @return View
  */
  public function index() {
		return view('modules.asset-management.pagos.recibidos');
  }
}