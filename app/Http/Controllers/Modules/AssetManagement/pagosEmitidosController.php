<?php

namespace App\Http\Controllers\Modules\AssetManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class pagosEmitidosController extends Controller
{

  /**
  * [Controlador de la vista de pagos emitidos]
  * @return View
  */
  public function index() {
		return view('modules.asset-management.pagos.emitidos');
  }
}