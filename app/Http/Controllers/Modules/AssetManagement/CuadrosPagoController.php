<?php

namespace App\Http\Controllers\Modules\AssetManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\AssetManagement\Energia;
use App\Models\AssetManagement\Potencia;
use App\Models\AssetManagement\ServicioComplementario;
use App\Models\AssetManagement\Empresa;
use App\Models\AssetManagement\TipoDocCen;
use Illuminate\Support\Collection as Collection;

use DB;

class CuadrosPagoController extends Controller
{	

	/**
	 * View of MatrizAcredor
	 * @return View
	 */
	public function matrizAcredor(Request $request)
	{
		return view('modules.asset-management.cuadros-pagos.matriz-acredor')
			->with('data',$this->getData($request,'id_empresa_acreedora'));
	}
	
	/**
	 * View of MatrizDeudor
	 * @return View
	 */
	public function matrizDeudor(Request $request)
	{
		return view('modules.asset-management.cuadros-pagos.matriz-deudor')
			->with('data',$this->getData($request,'id_empresa_deudora'));
	}

	/**
	 * [getData description]
	 * @param  Request $request    [description]
	 * @param  String  $cuadroPago [description]
	 * @return [type]              [description]
	 */
	public function getData(Request $request, String $cuadroPago)
	{

		$currentEmpresas = $request->user()->empresas->pluck('id');

		if ($request->user()->hasRole(['super-admin'])) {
			$where = "";
		}

		else {
			if (count($currentEmpresas)) {
				$where    = "WHERE $cuadroPago IN (".implode(",", $currentEmpresas->toArray()).")";
			} else {
				$where    = "WHERE $cuadroPago IN (-55555555)";
			}
		}
		if ( $request->empresa_xls != null ) {
			$NameIDEmpresa = 'id_empresa_acreedora';
			if ($cuadroPago == 'id_empresa_deudora')
				$NameIDEmpresa = 'id_empresa_deudora';
			$where = "WHERE $NameIDEmpresa IN (".$request->empresa_xls.")";
		}
		if ($where != '')
			$where = $request->tipo_doc_cen == null ? $where : "$where AND id_tipo_doc_cen IN ($request->tipo_doc_cen)";
		else
			$where = $request->tipo_doc_cen == null ? $where : "WHERE id_tipo_doc_cen IN ($request->tipo_doc_cen)";

		if ( $request->mes != null && $request->anio != null ) {
			if ($where != '')
				$where = "$where AND periodo = '$request->mes/$request->anio'";
			else
				$where = "WHERE periodo = '$request->mes/$request->anio'";
		}
		$request->modules = isset($request->modules) ? $request->modules : [1=>1,2=>2,3=>3];
		
		$columns  = 'id_empresa_deudora, id_empresa_acreedora, id_tipo_doc_cen, costo, periodo, created_at';
		$interna = '';
		if (in_array(1,$request->modules)) {
			$interna = "SELECT $columns, name 'ENERGIA' FROM ".Energia::getTableModel()." $where";
		}
		if (in_array(2,$request->modules)) {
			$potencia = "SELECT $columns, name 'POTENCIA' From ".Potencia::getTableModel()." $where";
			$interna = !$interna ? $potencia : $interna.' UNION '.$potencia;
		}
		if (in_array(3,$request->modules)) {
			$complementario = "SELECT $columns, name 'SERVICIOS COMPLEMENTARIOS' FROM ".ServicioComplementario::getTableModel()." $where";
			$interna = !$interna ? $complementario : $interna.' UNION '.$complementario;
		}

		$page = $request->page ? $request->page : 1;

		$size = $request->size ? $request->size : 20;

		$data = DB::select("SELECT $columns , name, periodo, created_at FROM ($interna) RESULTS ORDER BY created_at");

		$collect = collect($data);

		$paginationData = new LengthAwarePaginator($collect->forPage($page, $size), $collect->count(),$size,$page);

		$currentEmpresas = [];
		foreach ($paginationData as $key => $item) {
			$currentEmpresas[$item->id_empresa_deudora] = $item->id_empresa_deudora;
			$currentEmpresas[$item->id_empresa_acreedora] = $item->id_empresa_acreedora;
		}

		return [
			'collects' => $paginationData,
			'empresas' => $this->getNombresEmpresas($currentEmpresas),
			'typesDocuments' => $this->getNombresDocumentS(),
			'total' => Collection::make($data)->sum('costo')
		];
	}

	/**
	 * [getNombresEmpresas description]
	 * @param  [type] $currentEmpresas [description]
	 * @return [type]                  [description]
	 */
	public function getNombresEmpresas ($currentEmpresas) {

		$empresas = Empresa::select('id','nombre_xls')->findMany($currentEmpresas);

		$nombresEmpresas = [];

		foreach ($empresas as $empresa) {
			$nombresEmpresas[$empresa->id] = $empresa->nombre_xls;
		}

		return $nombresEmpresas;
	}

	/**
	 * [getNombresDocumentS description]
	 * @return [type] [description]
	 */
	public function getNombresDocumentS ()
	{
		$typesDocuments = TipoDocCen::all();

		$nombresTypesDocuments = [];

		foreach ($typesDocuments as $document) {
			$nombresTypesDocuments[$document->id] = $document->nombre;
		}

		return $nombresTypesDocuments;
	}
}
