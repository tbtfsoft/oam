<?php

namespace App\Http\Controllers\Modules\AssetManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\GuzzleHttp\GuzzlesSipub;

class SipubCoodinadorController extends GuzzlesSipub
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
		$request 	= 	$this->client->request('GET', 'recursos/infotecnica/empresas/');

    	$response 	=	$request->getBody();
    	
   		return $response;
    }
}
