<?php

namespace App\Http\Controllers\Modules\AssetManagement;

use App\Http\Controllers\Controller;
use App\Mail\EmailTransferenciaEconomicas;
use Illuminate\Http\Request;
use Lang;
use DB;
use Mail;

class TemplateEmailController extends Controller
{

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
    public function index ()
    {
        return view('modules.asset-management.cargar-datos.template-email');
    }

/*  "_token" => "u7iRLdWpGDJqQ9qxb3oH1ea1AEhvgah8EzgtaVu3"
  "empresa_xls" => "474"
  "tipo_doc_cen" => "2"
  "mes" => "01"
  "anio" => "2021"*/
    public function filter (Request $request)
    {
        // $query = "SELECT * FROM balance_economico_energia_cen WHERE id_empresa_acreedora IN (".$request->empresa_xls.")
        //          AND id_tipo_doc_cen IN (".$request->tipo_doc_cen.")
        //          AND periodo = '".$this->getPeriodo($request->mes, $request->anio)."' AND costo > 9";
                 // dd($query);
        // return collect(DB::select($query))->count();
        $empresa = \App\Models\AssetManagement\Empresa::find($request->empresa_xls);

        $energiaCen = \App\Models\AssetManagement\Energia::where('id_empresa_acreedora', $request->empresa_xls)
                            ->where('id_tipo_doc_cen', $request->tipo_doc_cen)
                            ->where('periodo', $this->getPeriodo($request->mes, $request->anio))
                            ->where('costo','>','10')
                            ->get();

        $potenciaCen = \App\Models\AssetManagement\Potencia::where('id_empresa_acreedora', $request->empresa_xls)
                            ->where('id_tipo_doc_cen', $request->tipo_doc_cen)
                            ->where('periodo', $this->getPeriodo($request->mes, $request->anio))
                            ->where('costo','>','10')
                            ->get();

        return view('modules.asset-management.cargar-datos.template-email')
            ->with('empresa', $empresa)
            ->with('html', view('modules.asset-management.cargar-datos.htmlEmail')->with('data', [
                'empresa' => $empresa,
                'energiaCenCount' => $energiaCen->count(),
                'energiaCenCosto' => $this->asDollars($energiaCen->sum('costo')),
                'potenciaCenCount' => $potenciaCen->count(),
                'potenciaCenCosto' => $this->asDollars($potenciaCen->sum('costo')),
                'costoTotal' => $this->asDollars($potenciaCen->sum('costo') + $energiaCen->sum('costo')),
                'mes' => $this->getMonth($request->mes),
                'anio' => $request->anio
            ]));
    }

    /**
     * Envio del correo
     * @param  Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function sentEmail (Request $request) {

        $data =  [
            'html' => $request->html,
            'subject' => $request->subject,
            'emails' => explode(',', str_replace(' ', '', $request->emails))
        ];

        Mail::send(new EmailTransferenciaEconomicas($data));

        return response()->json([
            'message' => 'Se enviarón correctamente.'
        ]);
    }

    /**
     * Retorna el periodo formateado. $month/$year (MM/YY)
     * @param  String $month  Mes
     * @param  String $year Año
     * @return String       Mes formateado
     * @author Carlos Anselmi <Carlos Anselmi>
     */
    private function getPeriodo ($month,$year)
    {
        return $month . '/' . $year;
    }

    private function asDollars($value) {
      return '$ ' . number_format($value, 2, ',', '.');
    }

    /**
     * La consulta se filtra por deudora o acredora dependiendo si es 'servicio complementario'
     * @param  Inter $id tipo de documento
     * @return String
     */
    private function whereAcredoraOrDeudora ($id) {
        return 'id_empresa_deudora';
    }

    private function getMonth ($m) {
        return [
            'Enero',
            'Febrero',
            'Marzo',
            'Abril',
            'Mayo',
            'Junio',
            'Julio',
            'Agosto',
            'Septiembre',
            'Octubre',
            'Noviembre',
            'Diciembre'
        ][(int)$m-1];
    }
}
