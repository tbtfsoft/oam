<?php

namespace App\Http\Controllers\Modules\AssetManagement;

use Illuminate\Http\Request;
use App\Helpers\TimeHelper;
use App\Http\Controllers\Controller;
use App\Models\Conexion;
use App\Models\Barras;
use App\Models\BarrasEmpresas;
use App\Models\ModalidadCalculo;
use App\Models\ModalidadCalculoEmpresas;
use App\Models\PrecioEstabilizado;
use App\Models\AssetManagement\Empresa;
use App\Models\AssetManagement\EnergiaMedidoresCEN as EnegiaCEN;
use App\Models\AssetManagement\EnergiaMedidoresPFV as EnergiaPFV;
use App\Models\AssetManagement\Energia as BalanceEconomicoEnergiaCEN;
use App\Models\AssetManagement\Potencia as BalanceEconomicoPotenciaCEN;
use App\Models\AssetManagement\ServicioComplementario as BalanceEconomicoSSCCCEN;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection as Collection;

class PrmteController  extends Controller
{
	public function index () {
		return view('modules.asset-management.prmte.index');
	}	

	public function GraficaPotenciaDiariaPRMTE(Request $request){
        date_default_timezone_set('America/Santiago');
        $data = Array();
        $empresa_id = $request->empresa;
        $conexion = Conexion::where('id_empresa', $empresa_id)->first();
        $host = $conexion == null ? 0 : $conexion->host;
        $pfv = $conexion == null ? 0 : $conexion->id_parq;
        $medidor = $conexion == null ? 0 : $conexion->id_medidor_prmte;
        $database_name = $conexion == null ? 0 : $conexion->db_prmte;
        $subtitulo = $conexion == null ? 'no existe conexion' : $conexion->nombre_parq;						

        $inicio = date('Y-m-d').' 00:00:00';
        $fin = date('Y-m-d').' 23:59:59';

		$data = Array();
        $ptRaw = Array();
		$irRaw = Array();
		$prRaw = Array();
        $dataRaw = Array();
		$horasRaw = Array();
		
        //DB CONNECTION
		$host = config('app.host_3');
		$port = config('app.host_port_3');
		$password = config('app.host_password_3');

        if($host != 0){
            //INICIO DE LA CONSULTA
            $strConnect = "host = ".$host." port = ".$port." dbname = '".$database_name."' user = 'postgres' password = ".$password;
            $con = pg_connect($strConnect);
    
			//DB QUERY STRING
			$strQuery = "SELECT med.fch_dato AS fecha, abs(med.pot_act_total) AS pt
            FROM tbl_datos_medidor AS med
            WHERE id_disp = $medidor
            AND fch_dato BETWEEN '$inicio' AND '$fin'		
			";	

			//DB QUERY
			$resRAW = pg_query($con,$strQuery);				

			while($dataFetch = pg_fetch_assoc($resRAW))
			{
				$dataRaw[] = $dataFetch;
			}	
			
			for($i=0; $i < count($dataRaw) ; $i++){
				$ptRaw[] = abs((float)($dataRaw[$i]['pt']));
				$irRaw[] = 0;
				$prRaw[] = 0;
				$horasRaw[] = (double)(TimeHelper::getDateTime($dataRaw[$i]['fecha'])*1000);
			}		

			if(count($ptRaw) == 0){
				//DB QUERY STRING
				$strQuery = "SELECT fecha.fecha, med.pot_act_total AS pt FROM 
				(
				SELECT (dias.dia + horas.hora) fecha FROM
				(
				SELECT dia::date FROM generate_series('$inicio'::date, '$fin'::date, '1 day'::interval) dia
				) dias
				INNER JOIN 
				(
				SELECT dia.hora, 1 AS relac_dias
				FROM
				(
				SELECT horas.serieh + ('''' || date_part('minutes', minutos.seriem) || ' minutes''') ::interval AS hora
				FROM 
				(
				SELECT ('''' || generate_series(0,23) || ' hours''') ::interval  AS serieh
				) AS horas
				INNER JOIN 
				(
				SELECT ('''' || generate_series(0,45,15) || ' minutes''') ::interval AS seriem, 1 AS relac_horas
				) AS minutos
				ON minutos.relac_horas=1
				) dia
				) AS horas ON horas.relac_dias=1
				) fecha 
				LEFT JOIN tbl_datos_medidor med ON med.fch_dato= fecha.fecha AND med.id_disp=$medidor
				WHERE fecha.fecha <= CURRENT_TIMESTAMP
				ORDER BY fecha.fecha ASC		
				";

				//DB QUERY
				$resRAW = pg_query($con,$strQuery);				

				while($dataFetch = pg_fetch_assoc($resRAW))
				{
					$dataRaw[] = $dataFetch;
				}	
				
				for($i=0; $i < count($dataRaw) ; $i++){
					$ptRaw[] = abs((float)($dataRaw[$i]['pt']));
					$irRaw[] = (float)($dataRaw[$i]['ir']);
					$prRaw[] = 0;
					$horasRaw[] = (double)(TimeHelper::getDateTime($dataRaw[$i]['fecha'])*1000);
				}			
			}
		}		
		
		//dd(count($ptRaw));
		$hora = date('H');
		$minutos = date ('i');

		$prmte_registros_db = count($ptRaw);
		$prmte_horas = $hora * 4;
		$prmte_minutos = 0;		

		if($minutos < 30){
			$prmte_minutos = 1;
		}else if($minutos >= 30 && $minutos < 45){
			$prmte_minutos = 2;
		}else if($minutos >= 45 && $minutos < 60){
			$prmte_minutos = 3;
		}else if($minutos >= 59 && $minutos <= 60){
			$prmte_minutos = 4;
		}

		$prmte_registros_act = $prmte_horas + $prmte_minutos;

		if($prmte_registros_db > $prmte_registros_act){
			$prmte_registros_db = $prmte_registros_act;
		}

		$prmte_porcentaje = ($prmte_registros_db * 100) / $prmte_registros_act; 

        $data = Array(
            'pt' => $ptRaw,
			'ir' => $irRaw,
			'pr' => $prRaw,
            'horas' => $horasRaw,
			'subtitulo' => $subtitulo,
			'prmte_registros_act' => $prmte_registros_act,
			'prmte_registros_db' => $prmte_registros_db,
			'prmte_porcentaje' => $prmte_porcentaje,
        );

        return $data;
    }
}