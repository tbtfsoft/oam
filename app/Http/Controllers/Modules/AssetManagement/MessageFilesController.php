<?php

namespace App\Http\Controllers\Modules\AssetManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;
use Lang;

class MessageFilesController extends Controller
{
	public function destroy (Request $request, $module, $id, $file)
	{
		Storage::delete('public/'.$module.'/'.$id.'/'.$file);

		return response()->json([
			'mensagee' => Lang::get('app.Archivo eliminado correctamente')
		]);
	}
}
