<?php

namespace App\Http\Controllers\Modules\AssetManagement;

use App\Models\AssetManagement\Energia;
use App\Models\AssetManagement\Empresa;
use App\Models\AssetManagement\EnergiaMedidoresCEN as EnegiaCEN;
use App\Models\AssetManagement\EnergiaMedidoresOE as EnegiaOE;
use App\Models\AssetManagement\EnergiaMedidoresOEoriginal as EnergiaOEoriginal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class EnergiaMedidoresController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
	public function energiaMedidores()
	{
		return view('modules.asset-management.cargar-datos.energia-medidores');
	}
}