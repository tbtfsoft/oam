<?php

namespace App\Http\Controllers\Modules\AssetManagement\Operaciones\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\Exportable;
use App\Models\AssetManagement\Energia;
use App\Models\AssetManagement\ServicioComplementario;
use App\Models\AssetManagement\Potencia;
use App\Models\AssetManagement\Empresa;
use App\Models\AssetManagement\PeriodoCen;
use App\Models\PPagoCen\PPEmpresa;
use Illuminate\Support\Collection as Collection;
use DB;


class ExportTortolas implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize, WithHeadingRow, WithChunkReading
{
    use Exportable;

    protected $request;

    public function __construct($request) {

        ini_set('memory_limit', '-1');

        set_time_limit(500);

        $this->request = $request;
    }

    /**
     * La consulta para general la Collection para el excel.
     *
     * @author Carlos Asnelmi <carlosanselmi2@gmail.com>
     * @return Collection
     */
    public function collection() {

        return collect([]);
    }

    /**
    * @var Collection` $row
    */
    public function map($row): array{
        // Aqui se genera el row de cada excel, siendo $row cada collection.
        // Cada posicion representa una columna del excel, mirar la funcion headings() de este mismo controlador para ver el orden de cada columna
        return [];

    }

    /**
     * Coloca los head de las columnas
     *
     * @author Carlos Asnelmi <carlosanselmi2@gmail.com>
     * @return Array()
     */
    public function headings(): array{
        return [
            'Destino del Documento',
            'Documento impreso se genera nulo',
            'Documento impreso Genera Rebaja Stock',
            'Tipo de Documento',
            'Número',
            'Número final, sólo boletas',
            'Fecha',
            'Local',
            'Vendedor',
            'Moneda Referencia',
            'Tasa Referencia',
            'Condición de Pago',
            'Fecha de Vencimiento',
            'Código del Cliente',
            'Tipo de Cliente',
            'Centro de Negocios',
            'Clasificador 1',
            'Clasificador 2',
            'Origen del Documento',
            'Lista de Precio',
            'Código del Proyecto',
            'Afecto',
            'Exento',
            'Total',
            'Bodega Inventario',
            'Motivo de movimiento Inventario',
            'Centro de Negocios Inventario',
            'Tipo de Cuenta Inventario',
            'Proveedor Inventario',
            'Dirección de Despacho',
            'Clasificador 1 Inventario',
            'Clasificador 2 Inventario',
            'Código Legal',
            'Nombre',
            'Giro',
            'Dirección',
            'Ciudad',
            'Rubro',
            'Glosa',
            'Línea de Detalle',
            'Articulo / Servicio',
            'Código del Producto',
            'Cantidad',
            'Tipo de Recargo y Descuento',
            'Precio Unitario',
            'Descuento',
            'Tipo de Descuento',
            'Tipo de Venta',
            'Total del Producto',
            'Precio Lista',
            'Total Neto',
            'Ficha Producto',
            'Centro de Negocios Producto',
            'Clasificador 1 Producto',
            'Clasificador 2 Producto',
            'Cantidad de Unidad Equivalente',
            'Cantidad de Periodos',
            'Comentario Producto',
            'Análisis Atributo1 Producto',
            'Análisis Atributo2 Producto',
            'Análisis Atributo3 Producto',
            'Análisis Atributo4 Producto',
            'Análisis Atributo5 Producto',
            'Análisis Lote Producto',
            'Fecha de Vencimiento Lote',
            'Ingreso Manual',
            'Tipo de Inventario',
            'Clasificador 1 Inventario Línea',
            'Clasificador 2 Inventario Línea',
            'Número de Descuento',
            'Descuento',
            'Tipo de Descuento',
            'Numero de Impuesto',
            'Código de Impuesto',
            'Valor de Impuesto',
            'Monto de Impuesto',
            'Centro de Negocios Producto',
            'Clasificador 1 Impuesto',
            'Clasificador 2 Impuesto',
            'Número de Cuota',
            'Fecha de Cuota',
            'Monto de Cuota',
            'Relación linea Series',
            'Sufijo Artículo Inventario',
            'Prefijo Artículo Inventario',
            'Serie Artículo Inventario',
            'Distrito',
            'Transacción',
            'Fecha Facturación Desde',
            'Fecha Facturación Hasta',
            'Vía de Transporte',
            'País Destino Receptor',
            'País Destino Embarque',
            'Modalidad Venta',
            'Tipo Despacho',
            'Indicador de Servicio',
            'Claúsula de Venta',
            'Total Claúsula de Venta',
            'Puerto Embarque',
            'Puerto Desembarque',
            'Unidad de Medida Tara',
            'Total Medida Tara',
            'Unidad Peso Bruto',
            'Total Peso Bruto',
            'Unidad Peso Neto',
            'Total Peso Neto',
            'Tipo de Bulto',
            'Total de Bultos',
            'Forma de Pago',
            'Tipo Documento Asociado',
            'Folio Documento Asociado',
            'Fecha Documento Asociado',
            'Comentario Documento Asociado',
            'Email',
            'Es documento de traspaso',
            'Contacto'
        ];
    }

    /**
     * Retorna el periodo formateado. $month/$year (MM/YY)
     * @param  String $month  Mes
     * @param  String $year Año
     * @return String       Mes formateado
     * @author Carlos Anselmi <Carlos Anselmi>
     */
    private function getPeriodo ($month,$year){
        return $month . '/' . $year;
    }

    /**
     * Aqui busca la empresa en el array de $this->PPEmpresas, si no la consigue tendra que buscar la empresa en el modelo de Empresa para obtener el rut.
     * Ddespues de tener el rut de empresa, en la tabla empresas, se separa el rut del digito verificadoy y se eliminan los puntos.
     * Para despues buscar con ese rut en PPEmpresas, y asi obtener los datos de dicha empresa deudora.
     * @param  Integer $currentEmpresas [Id de la empresa deudora a buscar]
     * @return Array
     */
    private function getPPEmpresa ($id_empresa_deudora){
        try {
            if (!in_array($id_empresa_deudora, $this->PPEmpresas)) {
                $empresa = Empresa::select('rut')->find($id_empresa_deudora);
                $empresa->verification_code = substr($empresa->rut, 10);
                $empresa->rut = str_replace('.','',str_replace($empresa->verification_code,'',$empresa->rut));
                $PPEmpresaDeudora = PPEmpresa::where('rut',$empresa->rut)->first();
                $this->PPEmpresas[$id_empresa_deudora] = $PPEmpresaDeudora ? $PPEmpresaDeudora->toArray() : null;
            }
            return $this->PPEmpresas[$id_empresa_deudora];
        } catch (Exception $e) {
            return null;
        }
    }

    private function getPeriodoCen () {
        return PeriodoCen::where('periodo', $this->getPeriodo($this->request->mes,$this->request->anio))
                    ->where('tipo_doc', $this->request->tipo_doc_cen)
                    ->where('tipo_item', $this->request->module)
                    ->first();
    }

    public function batchSize(): int{
        return 800;
    }

    public function chunkSize(): int{
        return 800;
    }
}
