<?php

namespace App\Http\Controllers\Modules\AssetManagement\Operaciones\Exports;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Helpers\TimeHelper;
use App\Http\Controllers\Controller;
use App\Models\Conexion;
use App\Models\Barras;
use App\Models\BarrasEmpresas;
use App\Models\ModalidadCalculo;
use App\Models\ModalidadCalculoEmpresas;
use App\Models\FactorReferencia;
use App\Models\PrecioEstabilizado;
use App\Models\AssetManagement\Empresa;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection as Collection;

class ExportFacturasPPCEN implements FromCollection,WithHeadings
{
    protected $request;
    
    public function __construct($request)
    {
        ini_set('memory_limit', '-1');

        set_time_limit(500);

        $this->request = $request;
    }

    public function collection()
    {
        $empresa_id = $this->request->empresa_xls;
        $modulo = $this->request->module;
		$conexion = Conexion::where('id_empresa', $empresa_id)->first();
        $host = 'localhost';
        $pfv = $conexion == null ? 0 : $conexion->id_parq;
        $medidor = $conexion == null ? 0 : $conexion->id_medidor;
        $radsensor = $conexion == null ? 0 : $conexion->id_radsensor;
        $database_name = 'oam';
        $periodo = $this->request->mes.'/'.$this->request->anio;

        $dataRaw = Array();

        //DB CONNECTION
        if($host == 1){
            $host = config('app.host_1');
            $port = config('app.host_port_1');
            $password = config('app.host_password_1');
        }else if($host == 2){

        }else{
            $host = 'localhost';
            $port = '5432';
            $password = '856952';            
        }


        //INICIO DE LA CONSULTA
        $strConnect = "host = ".$host." port = ".$port." dbname = '".$database_name."' user = 'postgres' password = ".$password;
        $con = pg_connect($strConnect);

        $strQuery = "SELECT empresas.rut AS rut_empresa_acreedora, facturas.folio, facturas.id_tipo_dte, facturas.monto_bruto, facturas.monto_neto, facturas.fecha_emision, facturas.fecha_aceptacion AS fecha_entrega_folio, facturas.fecha_aceptacion_ts AS fecha_entrega_folio_ts
        FROM facturas
        INNER JOIN empresas ON empresas.id = facturas.id_empresa_deudora
        WHERE periodo = '$periodo' AND id_modulo = '$modulo' AND id_empresa_acreedora = $empresa_id
        ORDER BY empresas.rut ASC";
        
        //DB QUERY
        $resRAW = pg_query($con,$strQuery);		

        while($dataFetch = pg_fetch_assoc($resRAW))
        {
            $dataRaw[] = $dataFetch;
        }

        return collect($dataRaw);
    }

    public function headings(): array{

        $column_name = Array();

        $column_name = [
            'RUT Empresa Deudora',
            'Folio',
            'Codigo DTE SII',
            'Monto Bruto',
            'Monto Neto',            
            'Fecha Emision',           
            'Fecha Entrega Folio',
            'Fecha Entrega Folio TS',         
        ];

        return $column_name;
    }
}