<?php

namespace App\Http\Controllers\Modules\AssetManagement\Operaciones\Exports;

//NO SE USA.
use Maatwebsite\Excel\Concerns\WithMapping;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\Models\AssetManagement\Energia;
use App\Models\AssetManagement\Potencia;
use App\Models\AssetManagement\Empresa;
use App\Models\AssetManagement\ServicioComplementario;
use App\Models\AssetManagement\TipoDocCen;
use Illuminate\Support\Collection as Collection;
use DB;

class ExportCollection implements FromCollection, WithHeadings, ShouldAutoSize, WithHeadingRow, WithChunkReading
{
    use Exportable;

    protected $request;
    
    public function __construct($request)
    {
        ini_set('memory_limit', '-1');

        set_time_limit(500);

        $this->request = $request;
    }

    /**
     * La consulta para general la query para el excel.
     *
     * @author Carlos Asnelmi <carlosanselmi2@gmail.com>
     * @return Query
     */
    public function collection()
    {

        $where = "WHERE ".$this->whereAcredoraOrDeudora($this->request->tipo_doc_cen)." IN (".$this->request->empresa_xls.") AND id_tipo_doc_cen IN (".$this->request->tipo_doc_cen.") AND periodo = '".$this->getPeriodo($this->request->mes,$this->request->anio)."' AND costo > 9";

        // Si selecciona el modulo de Energia
        if (in_array(1,$this->request->modules)) {
            $interna = "SELECT * FROM ".Energia::getTableModel()." $where";
        }
        // Si selecciona el modulo de Potencia
        if (in_array(2,$this->request->modules)) {
            $potencia = "SELECT * From ".Potencia::getTableModel()." $where";
            $interna = "SELECT * FROM ".Potencia::getTableModel()." $where";
            //$interna = !$interna ? $potencia : $interna.' UNION '.$potencia;
        }
        // Si selecciona el modulo de Servicios Complementarios
        if (in_array(3,$this->request->modules)) {
            $complementario = "SELECT * FROM ".ServicioComplementario::getTableModel()." $where";
            $interna = !$interna ? $complementario : $interna.' UNION '.$complementario;
        }

        $data = DB::select("SELECT ".$this->columnsSQL()." FROM ($interna) RESULTS ORDER BY created_at");

        $collect = collect($data);

        return $collect;
    }

    private function columnsSQL ()
    {
        $tipo = 1;
        if($tipo == 1){
            return "
            periodo as Fecha_proceso,
            'RLQTAR'   as Codigo,
            'Energia' as Descripcion,
            NULL as Razon_Social,
            NULL as Giro,
            NULL as Direccion_Postal,
            NULL as Comuna,
            NULL as Ciudad,
            (SELECT REPLACE(SUBSTRING(rut,1,10),'.','') FROM empresas WHERE id IN (id_empresa_deudora)) as Rut,
            (SELECT SUBSTRING(rut,12,1) FROM empresas WHERE id IN (id_empresa_deudora)) as Dig_Ver,
            costo as Monto_Neto,
            'SEN_[TEE_][Ene19][L][V01]' as Glosa_Item,
            'DE05969A18C58S0294' as Folio_doc_Referencia_1,
            'SEN' as Tipo_Doc_Referencia_1,
            '2019-07-01' as Fecha_Doc_Referencia_1,
            'SEN_[TEE_][Ene19][L][V01]' as Rzn_de_referencia_1,
            NULL as Folio_docReferencia_2,
            NULL as Tipo_DocReferencia_2,
            NULL as Fecha_Doc_Referencia_2,
            NULL as Rzn_de_referencia_2,
            NULL as Folio_doc_Referencia_3,
            NULL as Tipo_Doc_Referencia_3,
            NULL as Fecha_Doc_Referencia_3,
            NULL as Rzn_de_referencia_3,
            NULL as contacto,
            NULL as mail,
            NULL as contacto,
            NULL as mai
        ";
        }else{
            return "
            periodo as Fecha_proceso,
            'RLQTAR'   as Codigo,
            'Transferencia de Potencia' as Descripcion,
            NULL as Razon_Social,
            NULL as Giro,
            NULL as Direccion_Postal,
            NULL as Comuna,
            NULL as Ciudad,
            (SELECT REPLACE(SUBSTRING(rut,1,10),'.','') FROM empresas WHERE id IN (id_empresa_deudora)) as Rut,
            (SELECT SUBSTRING(rut,12,1) FROM empresas WHERE id IN (id_empresa_deudora)) as Dig_Ver,
            costo as Monto_Neto,
            'SEN_[BP__][Ene19][L][V01]' as Glosa_Item,
            'DE05969A18C56S0292' as Folio_doc_Referencia_1,
            'SEN' as Tipo_Doc_Referencia_1,
            '2019-07-01' as Fecha_Doc_Referencia_1,
            'SEN_[BP__][Ene19][L][V01]' as Rzn_de_referencia_1,
            NULL as Folio_docReferencia_2,
            NULL as Tipo_DocReferencia_2,
            NULL as Fecha_Doc_Referencia_2,
            NULL as Rzn_de_referencia_2,
            NULL as Folio_doc_Referencia_3,
            NULL as Tipo_Doc_Referencia_3,
            NULL as Fecha_Doc_Referencia_3,
            NULL as Rzn_de_referencia_3,
            NULL as contacto,
            NULL as mail,
            NULL as contacto,
            NULL as mai
        ";
        }
    }

    /**
     * Coloca los head de las columnas
     *
     * @author Carlos Asnelmi <carlosanselmi2@gmail.com>
     * @return Array()
     */
    public function headings(): array
    {
        return [
            'Fecha Proceso',
            'Codigo',
            'Descripcion',
            'Razon Social',
            'Giro',
            'Dirección Postal',
            'Comuna',
            'Ciudad',
            'Rut',
            'Dig. Ver',
            'Monto Neto',
            'Glosa Item',
            'Folio doc.Referencia 1',
            'Tipo Doc. Referencia 1',
            'Fecha Doc Referencia 1',
            'Rzn de referencia 1',
            'Folio doc. Referencia 2',
            'Tipo Doc. Referencia 2',
            'Fecha Doc Referencia 2',
            'Rzn de referencia 2',
            'Folio doc. Referencia 3',
            'Tipo Doc. Referencia 3',
            'Fecha Doc Referencia 3',
            'Rzn de referencia 3',
            'contacto',
            'mail',
            'contacto',
            'mail'
        ];
    }

    public function batchSize(): int
    {
        return 800;
    }
    
    public function chunkSize(): int
    {
        return 800;
    }

    /**
     * Retorna el periodo formateado. $month/$year (MM/YY)
     * @param  String $month  Mes
     * @param  String $year Año
     * @return String       Mes formateado
     * @author Carlos Anselmi <Carlos Anselmi>
     */
    private function getPeriodo ($month,$year)
    {
        return $month . '/' . $year;
    }

    /**
     * La consulta se filtra por deudora o acredora dependiendo si es 'servicio complementario'
     * @param  Inter $id tipo de documento
     * @return String
     */
    private function whereAcredoraOrDeudora ($id)
    {
        if ($id != 3) {
            return 'id_empresa_acreedora';
        }
        return 'id_empresa_deudora';
    }
}
