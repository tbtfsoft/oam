<?php

namespace App\Http\Controllers\Modules\AssetManagement\Operaciones\Exports;

//NO SE USA.
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\Exportable;
use App\Models\AssetManagement\Energia;
use App\Models\AssetManagement\Potencia;
use App\Models\AssetManagement\ServicioComplementario;
use App\Models\AssetManagement\Empresa;
use App\Models\PPagoSen\PPEmpresa;
use Illuminate\Support\Collection as Collection;
use DB;


class ExportPiquero implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize, WithHeadingRow, WithChunkReading
{
    use Exportable;

    protected $request;

    protected $PPEmpresas;
    
    public function __construct($request)
    {
        ini_set('memory_limit', '-1');

        set_time_limit(500);

        $this->request = $request;

        $this->PPEmpresas = [];
    }

    /**
     * La consulta para general la Collection para el excel.
     *
     * @author Carlos Asnelmi <carlosanselmi2@gmail.com>
     * @return Collection
     */
    public function collection()
    {

        $where = "WHERE ".$this->whereAcredoraOrDeudora($this->request->tipo_doc_cen)." IN (".$this->request->empresa_xls.")
                 AND id_tipo_doc_cen IN (".$this->request->tipo_doc_cen.")
                 AND periodo = '".$this->getPeriodo($this->request->mes,$this->request->anio)."' AND costo > 9";

        // Si selecciona el modulo de Energia
        if ($this->request->module == 1) {
            $union = "SELECT * FROM ".Energia::getTableModel()." $where";
        }
        // Si selecciona el modulo de Potencia
        if ($this->request->module == 2) {
            $union = "SELECT * FROM ".Potencia::getTableModel()." $where";
        }
        // Si selecciona el modulo de Potencia
        if ($this->request->module == 3) {
            $union = "SELECT * FROM ".ServicioComplementario::getTableModel()." $where";
        }

        return collect(DB::select("SELECT * FROM ($union) RESULTS ORDER BY created_at"));
    }

    /**
    * @var Collection` $row
    */
    public function map($row): array
    {
        // Aqui busca la empresa en el array de $this->PPEmpresas, si no la consigue tendra que buscar la empresa en el modelo de Empresa para obtener el rut.
        // Ddespues de tener el rut de empresa, en la tabla empresas, se separa el rut del digito verificadoy y se eliminan los puntos.
        // Para despues buscar con ese rut en PPEmpresas, y asi obtener los datos de dicha empresa deudora.
        $tipo_doc = 1;
        $tipo_item = 1;
        $tipo_item_name = '';
        $tipo_item_descrip = '';
        $glosa = '';
        $folio = '';
        $tipo_doc_ref = 'SEN';
        $fecha_doc_ref = '2019-05-09';

        $empresa_deudora =  $this->getPPEmpresa($row->id_empresa_deudora);
        

        if($this->request->module == 1){            
            if ($this->request->module != 3) {
                $tipo_item_name = 'ENERGIA';
                $tipo_item_descrip = 'Energia';
                $glosa = 'SEN_[TEE_][Ene19][L][V01]';
                $folio = 'DE05969A18C58S0294';
            }else {
                $tipo_item_name = 'OTROS';
                $tipo_item_descrip = 'OTROS';
            }            
        }elseif ($this->request->module == 2) {
            if ($this->request->module != 3) {
                $tipo_item_name = 'POTENCIA';
                $tipo_item_descrip = 'Transferencia de potencia';
                $glosa = 'SEN_[BP__][Ene19][L][V01]';
                $folio = 'DE05969A18C56S0292';
            }else {
                $tipo_item_name = 'OTROS';
                $tipo_item_descrip = 'OTROS';
            }              
        }

        // Aqui se genera el row de cada excel, siendo $row cada collection.
        // Cada posicion representa una columna del excel, mirar la funcion headings() de este mismo controlador para ver el orden de cada columna 
        return [
            $row->periodo,
            $tipo_item_name,
            $tipo_item_descrip,
            $empresa_deudora ? $empresa_deudora['name'] : NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            $empresa_deudora ? $empresa_deudora['rut'] : NULL,
            $empresa_deudora ? $empresa_deudora['verification_code'] : NULL,
            $row->costo,
            $glosa,
            $folio,
            $tipo_doc_ref,
            $fecha_doc_ref,
            $glosa,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            $empresa_deudora ? $empresa_deudora['payments_contact_phones'] : NULL,
            $empresa_deudora ? $empresa_deudora['payments_contact_email'] : NULL,
            NULL,
            NULL
        ];

    }

    /**
     * Coloca los head de las columnas
     *
     * @author Carlos Asnelmi <carlosanselmi2@gmail.com>
     * @return Array()
     */
    public function headings(): array
    {
        return [
            'Fecha Proceso',
            'Codigo',
            'Descripcion',
            'Razon Social',
            'Giro',
            'Dirección Postal',
            'Comuna',
            'Ciudad',
            'Rut',
            'Dig. Ver',
            'Monto Neto',
            'Glosa Item',
            'Folio doc.Referencia 1',
            'Tipo Doc. Referencia 1',
            'Fecha Doc Referencia 1',
            'Rzn de referencia 1',
            'Folio doc. Referencia 2',
            'Tipo Doc. Referencia 2',
            'Fecha Doc Referencia 2',
            'Rzn de referencia 2',
            'Folio doc. Referencia 3',
            'Tipo Doc. Referencia 3',
            'Fecha Doc Referencia 3',
            'Rzn de referencia 3',
            'contacto',
            'mail',
            'contacto',
            'mail'
        ];
    }

    /**
     * Retorna el periodo formateado. $month/$year (MM/YY)
     * @param  String $month  Mes
     * @param  String $year Año
     * @return String       Mes formateado
     * @author Carlos Anselmi <Carlos Anselmi>
     */
    private function getPeriodo ($month,$year)
    {
        return $month . '/' . $year;
    }

    /**
     * Aqui busca la empresa en el array de $this->PPEmpresas, si no la consigue tendra que buscar la empresa en el modelo de Empresa para obtener el rut.
     * Ddespues de tener el rut de empresa, en la tabla empresas, se separa el rut del digito verificadoy y se eliminan los puntos.
     * Para despues buscar con ese rut en PPEmpresas, y asi obtener los datos de dicha empresa deudora.
     * @param  Integer $currentEmpresas [Id de la empresa deudora a buscar]
     * @return Array
     */
    private function getPPEmpresa ($id_empresa_deudora)
    {
        try {
            if (!in_array($id_empresa_deudora, $this->PPEmpresas)) {
                $empresa = Empresa::select('rut')->find($id_empresa_deudora);
                $empresa->verification_code = substr($empresa->rut, 10);
                $empresa->rut = str_replace('.','',str_replace($empresa->verification_code,'',$empresa->rut));
                $PPEmpresaDeudora = PPEmpresa::where('rut',$empresa->rut)->first();
                $this->PPEmpresas[$id_empresa_deudora] = $PPEmpresaDeudora ? $PPEmpresaDeudora->toArray() : null;
            }
            return $this->PPEmpresas[$id_empresa_deudora];
        } catch (Exception $e) {
            return null;
        }
    }

    public function batchSize(): int
    {
        return 800;
    }
    
    public function chunkSize(): int
    {
        return 800;
    }

    /**
     * La consulta se filtra por deudora o acredora dependiendo si es 'servicio complementario'
     * @param  Inter $id tipo de documento
     * @return String
     */
    private function whereAcredoraOrDeudora ($id)
    {
        if ($id != 3) {
            return 'id_empresa_acreedora';
        }
        return 'id_empresa_deudora';
    }
}
