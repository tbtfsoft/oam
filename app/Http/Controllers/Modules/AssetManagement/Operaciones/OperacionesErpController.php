<?php
namespace App\Http\Controllers\Modules\AssetManagement\Operaciones;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AssetManagement\Energia;
use App\Models\AssetManagement\Potencia;
use App\Models\AssetManagement\PeriodoCen;
use App\Models\AssetManagement\ServicioComplementario;
use App\Models\AssetManagement\Empresa;
use App\Models\AssetManagement\TipoDocCen;
use Illuminate\Support\Collection as Collection;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class OperacionesErpController extends Controller
{

    protected $disk = 'erp';

    /**
     * [Controlador de la vista de ERP]
     * @return View vista del modulo de operaciones especiales del ERP
     * @author Carlos Anselmi <carlosanselmi2@gmail.com>
     */
    public function index()
    {
        return view('modules.asset-management.operaciones.ERP');
    }

    /**
     * Peticion para generar o obtener, dependiendo del caso, el excel de
     * operaciones especiales ERP
     * @param  Request $request
     * @return Maatwebsite\Excel\Facades\Excel
     * @author Carlos Anselmi <carlosanselmi2@gmail.com>
     */
    public function export(Request $request)
    {
        //Para construir el nombre del scv
        $empresa = Empresa::select('nombre_xls', 'id')->find($request->empresa_xls);

        $document = TipoDocCen::select('nombre')->find($request->tipo_doc_cen);

        $periodo = $this->getPeriodo($request->mes, $request->anio);

        $filename = $this->getNameFileStorage([$empresa->id . '/' . strtoupper($empresa->nombre_xls) , strtoupper($document->nombre) , $this->getNameModule($request->module) , $periodo], '.xlsx');

        if ($this->verifyData($request))
        {
            return response()->json(["error" => 400, "message" => "No se encontro datos"], 400);
        }

        return $this->ExportDocument((object)['request' => $request, 'empresa' => $empresa, 'document' => $document, 'periodo' => $periodo]);
    }

    /**
     * Obtiene el un array con todas las empresas, siendo la key el id y el
     * value el nombre_xls.
     * @return Array [1 => 'nombre_xls_1', '2' => 'nombre_xls_2' ... 'n' =>
     * 'nombre_xls_n']
     * @author Carlos Anselmi <carlosanselmi2@gmail.com>
     */
    public function getNombresEmpresas($currentEmpresas)
    {

        $empresas = Empresa::select('id', 'nombre_xls')->findMany($currentEmpresas);

        $nombresEmpresas = [];

        foreach ($empresas as $empresa)
        {
            $nombresEmpresas[$empresa
                ->id] = $empresa->nombre_xls;
        }

        return $nombresEmpresas;
    }

    /**
     * Obtiene el un array con todos los tipos de documentos, siendo la key el
     * id y el value el nombre.
     * @return Array [1 => 'name_1', '2' => 'name_2' ... 'n' => 'name_n']
     * @author Carlos Anselmi <carlosanselmi2@gmail.com>
     */
    public function getNombresDocumentS()
    {
        $typesDocuments = TipoDocCen::all();

        $nombresTypesDocuments = [];

        foreach ($typesDocuments as $document)
        {
            $nombresTypesDocuments[$document
                ->id] = $document->nombre;
        }

        return $nombresTypesDocuments;
    }

    /**
     * Retorna el periodo formateado. $month/$year (MM/YY)
     * @param  String $month  Mes
     * @param  String $year Año
     * @return String       Mes formateado
     * @author Carlos Anselmi <Carlos Anselmi>
     */
    private function getPeriodo($month, $year, $chart = '-')
    {
        return $month . $chart . $year;
    }

    public function getDocumentsDirectory(Request $request, $id)
    {
        $files = Storage::disk($this->disk)
            ->files("/" . $id);

        return response()->json(['response' => view('modules.asset-management.operaciones.response.files')
            ->with('files', $files)->render() ], 200);
    }

    private function getNameModule($id)
    {
        switch ($id)
        {
            case 1:
                return 'ENERGIA';
            break;
            case 2:
                return 'POTENCIA';
            break;
            case 3:
                return 'SERVICIO COMPLEMENTARIO';
            break;
            default:
                return 'MODULO-NO-DEFINIDO';
            break;
        }
    }

    /**
     * Descarga el Documento de operaciones ERP mediante el nombre de la ubicacion en el storage.
     * @return File
     * @author Carlos Anselmi <Carlos Anselmi>
     */
    public function downloadDocument($filename, $filenameExit = null, $ajax = true)
    {
        if ($ajax)
        {
            return response()->json(["url" => Storage::disk($this->disk)
                ->url($filename) , "message" => "Descargando..."], 200);
        }

        return $filenameExit ? Storage::disk($this->disk)
            ->download($filename, $filenameExit) : Storage::disk($this->disk)
            ->download($filename);
    }

    public function downloadDocumentHTTP(Request $request, $id_empresa, $empresa, $modulo, $tipo_documento, $periodo)
    {

        $periodos_new = explode("-", $periodo);

        $periodos_new = $this->getPeriodo($periodos_new[0], $periodos_new[1], null);

        $filename = $this->getNameFileStorage([$id_empresa . '/' . strtoupper($empresa) , strtoupper($tipo_documento) , $modulo, $periodo], '.xlsx');

        if (Storage::disk($this->disk)
            ->has($filename))
        {
            $filenameExit = $this->getNameFileStorage([strtoupper($empresa) , $modulo, $periodos_new], '.xlsx');
            return $this->downloadDocument($filename, $filenameExit, false);
        }
    }

    private function ExportDocument($dataExport)
    {
        $filenameExit = null;

        $periodos_new = explode("-", $dataExport->periodo);

        $periodos_new = $periodos_new[0] . '' . $periodos_new[1];

        $nameFile = $this->getNameFileStorage([$dataExport
            ->empresa->id . '/' . strtoupper($dataExport
            ->empresa
            ->nombre_xls) , strtoupper($dataExport
            ->document
            ->nombre) , $this->getNameModule($dataExport
            ->request
            ->module) , $dataExport->periodo], '.xlsx');

        switch ($dataExport
            ->empresa
            ->id)
        {
            case 414: //LECHUZAS
                Excel::store(new Exports\ExportLechuzas($dataExport->request) , $nameFile, $this->disk);
            break;
            case 512: //TORTALAS
				Excel::store(new Exports\ExportOperacionesCollection($dataExport->request) , $nameFile, $this->disk);
                //Excel::store(new Exports\ExportTortolas($dataExport->request) , $nameFile, $this->disk);
            break;
            default: // POR DEFECTO
                Excel::store(new Exports\ExportOperacionesCollection($dataExport->request) , $nameFile, $this->disk);
            break;
        }
        // Genera el nombre de salida
        if (!$filenameExit) $filenameExit = $this->getNameFileStorage([strtoupper($dataExport
            ->empresa
            ->nombre_xls) , $this->getNameModule($dataExport
            ->request->module, null) , $dataExport->periodo], '.xlsx');
        // Busca el csv y lo descarga
        return response()
            ->json(["message" => "Generado correctamente"], 200);
        //return $this->downloadDocument($nameFile, $filenameExit);

    }

    /**
     * Gestiona el nombre del archivo, une el nombre de la empresa, modulo ,
     * documento (opcional), periodo y extencion del archivo (opcional)
     * @param  Array $elements  El array que se le aplicara el implode para
     * generar la ruta del archivo
     * @param  String $extenxion La extencion del archivo a generar o
     * descargarse
     * @return String
     * @author Carlos Anselmi <carlosanselmi2@gmail.com>
     */
    private function getNameFileStorage($elements, $extenxion = null)
    {
        if (is_array($elements))
        {
            return empty($extenxion) ? implode("_", $elements) : implode("_", $elements) . $extenxion;
        }
        else
        {
            return empty($extenxion) ? $elements : $elements . $extenxion;
        }
    }

    private function verifyData($request)
    {
        if ($request->tipo_doc_cen === '1') return false;
        $row = PeriodoCen::where('tipo_doc', (int)$request->tipo_doc_cen)
            ->where('tipo_item', (int)$request->module)
            ->where('periodo', $this->getPeriodo($request->mes, $request->anio, '/'))
            ->first();

        return $row === null;
    }
}

