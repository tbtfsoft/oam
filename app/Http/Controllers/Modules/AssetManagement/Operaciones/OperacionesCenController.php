<?php

namespace App\Http\Controllers\Modules\AssetManagement\Operaciones;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AssetManagement\Energia;
use App\Models\AssetManagement\Potencia;
use App\Models\AssetManagement\ServicioComplementario;
use App\Models\AssetManagement\Empresa;
use App\Models\AssetManagement\TipoDocCen;
use Illuminate\Support\Collection as Collection;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class OperacionesCenController extends Controller
{

  /**
   * [Controlador de la vista de CEN]
   * @return View [description]
   */    
  public function index() {
      return view('modules.asset-management.operaciones.CEN');
  }

	/**
	 * [getData description]
	 * @param  Request $request    [description]
	 * @param  String  $cuadroPago [description]
	 * @return [type]              [description]
	 */
	public function export(Request $request)
	{
		//Para construir el nombre del scv
		$empresa = Empresa::select('nombre_xls')->find($request->empresa_xls);
		$empresa = strtoupper($empresa->nombre_xls);
		$document = TipoDocCen::select('nombre')->find($request->tipo_doc_cen);
		$document = strtoupper($document->nombre);
		$periodo = $this->getPeriodo($request->mes,$request->anio);
		$nameFile = $empresa.'_'.$document.'_'.$periodo.'.xlsx';
		// Crea el csv y lo almacena en storage/app/public/uploads
		Excel::store(new Exports\ExportCollection($request), $nameFile, 'cen');

		// Busca el csv y lo descarga
		return Storage::response("public/uploads/cen/$nameFile");
	}

	/**
	 * [getNombresEmpresas description]
	 * @param  [type] $currentEmpresas [description]
	 * @return [type]                  [description]
	 */
	public function getNombresEmpresas ($currentEmpresas) {

		$empresas = Empresa::select('id','nombre_xls')->findMany($currentEmpresas);

		$nombresEmpresas = [];

		foreach ($empresas as $empresa) {
			$nombresEmpresas[$empresa->id] = $empresa->nombre_xls;
		}

		return $nombresEmpresas;
	}

	/**
	 * [getNombresDocumentS description]
	 * @return [type] [description]
	 */
	public function getNombresDocumentS ()
	{
		$typesDocuments = TipoDocCen::all();

		$nombresTypesDocuments = [];

		foreach ($typesDocuments as $document) {
			$nombresTypesDocuments[$document->id] = $document->nombre;
		}

		return $nombresTypesDocuments;
	}

  /**
   * Retorna el periodo formateado. $month/$year (MM/YY)
   * @param  String $month  Mes
   * @param  String $year Año
   * @return String       Mes formateado
   * @author Carlos Anselmi <Carlos Anselmi>
   */
  private function getPeriodo ($month,$year)
  {
      return $month . '-' . $year;
  }
}
