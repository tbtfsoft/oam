<?php

namespace App\Http\Controllers\Modules\AssetManagement\Operaciones;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection as Collection;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use App\Helpers\TimeHelper;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Modules\AssetManagement\Operaciones\Exports\ExportFacturasPPCEN;

class OperacionesFacturasPPECEN extends Controller
{

	/**
	 * Controlador
	 * @return View
	 */ 
  public function index() {
	return view('modules.asset-management.operaciones.facturasPPCEN');
}

	public function generarPlanillaDTEPPCEN (Request $request){
		$empresa_id = $request->empresa;
		$modulo = $request->module;

		if ($modulo == 1) {
			return Excel::download(new ExportFacturasPPCEN($request), 'Planilla DTE PPCEN - Energia.xlsx');					
		}else if ($modulo == 2) {
			return Excel::download(new ExportFacturasPPCEN($request), 'Planilla DTE PPCEN - Potencia.xlsx');					
		}
		
	}

}