<?php

namespace App\Http\Controllers\Modules\AssetManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AssetManagement\Empresa;
use App\Models\AssetManagement\EmpresasPadres;
use App\Models\AssetManagement\EmpresasRelacion;
use App\Models\AssetManagement\Correspondencia;
use Lang;

class GeneralesController extends Controller
{

    /**
     * Busca la data para las graficas
     * @param  Request $request [description]
     * @return JSON
     * @author Morris Sosa <morris.sosa@oenergy.cl>
     */
    public function EmpresasPadre(Request $request)
    {
        if ($request->has('empresa_hija')) {
            $relation = EmpresasRelacion::where('id_empresa_hija', $request->empresa_hija)->first();
            
            $parent = $relation ? EmpresasPadres::find($relation->id_empresa_padre) : null;

            $child = Empresa::find($request->empresa_hija);

            return response()->json([
                "data"     => [
                    'parent' => $parent,
                    'child'  => $child
                ],
                'response' => view('layouts.partials.assets.userLabel', compact(['parent','child']))->render(),
                'sidebar'  => view('layouts.partials.assets.userLabel')->with('parent', $parent)->with('child', $child)->with('type', true)->render()
            ],200);
        }

        return response()->json([
            'message' => Lang::get('app.Se requiere el ID de la empresa')
        ], 404);
    }

    public function getEmpresasByIds (Request $request)
    {
        if (!isset($request->empresas_ids) || (count($request->empresas_ids) && $request->empresas_ids[0] == null)) {
            $user = \App\User::find($request->user_id);
            return response()->json([
                'empresas' => $user->empresas
            ],200);
        }

        $empresas = Empresa::find($request->empresas_ids);
        
        return response()->json([
            'empresas' => $empresas
        ],200);
    }
}
