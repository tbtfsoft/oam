<?php

namespace App\Http\Controllers\Modules\Reportes\Exports;

use App\Helpers\TimeHelper;
use App\Models\Conexion;
use App\Traits\ExportCsv;

class ReportesTracker{
	use ExportCsv;

	protected $request;

	protected $conexion;

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct($request, $config = []){
  	$this->request  = $request;

    $this->conexion = Conexion::where('id_empresa', $this->request->empresa_xls)->first();
  }


	public function verification (){
		return TRUE;
	}

	/**
	 * Sentencia SQL para la exportacion del Excel.
	 * @return String Sentencia Sql
	 */
	public function sql (){
    $empresa_id = $this->request->empresa_xls;
    $reporte_id = $this->request->id_reporte;
    $conexion = $this->conexion;
    $host = $conexion == null ? 0 : $conexion->host;
    $pfv = $conexion == null ? 0 : $conexion->id_parq;
    $medidor = $conexion == null ? 0 : $conexion->id_medidor;
    $radsensor = $conexion == null ? 0 : $conexion->id_radsensor;
    $database_name = $conexion == null ? 0 : $conexion->db;
    $subtitulo = $conexion == null ? 'no existe conexion' : $conexion->nombre_parq;			
    $dateRange = explode("-", $this->request->dates);
    $inicio = TimeHelper::formatDateRange($dateRange[0]);
    $fin = TimeHelper::formatDateRange($dateRange[1]);

    $data = Array();
    $inversor = Array();
    $potencia = Array();
    $dataRaw = Array();
    $horasRaw = Array();

    $strQuery = "";

    if($pfv == 11){
      //PIQUERO
      //DB QUERY STRING
      $strQuery = "SELECT tracker.fch_dato AS fecha, disp.id_local AS tracker_nro, tracker.dir_bus, tracker.pos_angle, tracker.error_code, tracker.estatus_back_track,tracker.pv_volt,tracker.bat_volt,tracker.cte_motor,tracker.temp_pcb,tracker.current_pos,tracker.mode_track,tracker.wind_speed,tracker.fch_update,tracker.conex_valor,tracker.stow_valor,tracker.clean_valor,tracker.fch_utc_dato,tracker.fch_audit,tracker.enviado_remoto,tracker.fch_remoto,tracker.index_dato,tracker.raw_valor
      FROM   tbl_datos_tracker_idematec AS tracker
      INNER JOIN tbl_dispositivos disp ON disp.id_disp=tracker.id_disp
      WHERE tracker.id_parq = $pfv
      AND tracker.fch_dato BETWEEN '$inicio' AND '$fin'
      ORDER  BY tracker.fch_dato, disp.id_local ASC			
      ";
  }elseif ($pfv == 12 || $pfv == 13 || $pfv == 14 || $pfv == 15 || $pfv == 17) {
        // CODORCINES SANTA LAURA PERDICES LAS LECHUZAS
        $strQuery = "SELECT tracker.fch_dato AS fecha, disp.id_local AS tracker_nro,id_spc,estatus_com,mode_track,fault_valor,smps_volt,bat_volt,bat_soc,target_pos,current_pos,cte_motor,aux1,aux2,aux3,fch_utc_dato,fch_audit,enviado_remoto,fch_remoto,index_dato,id_local,marca_disp,modelo_disp,desc_disp,databits_port,parity_port,stop_bits,baudrate_port,activo_disp,id_tipo_disp,port_disp,id_com,ip_disp,time_disp
        FROM   tbl_datos_tracker_artech AS tracker
        INNER JOIN tbl_dispositivos disp ON disp.id_disp=tracker.id_disp
        WHERE tracker.id_parq = $pfv
        AND tracker.fch_dato BETWEEN '$inicio' AND '$fin'
        ORDER  BY tracker.fch_dato, disp.id_local ASC			
        ";         
    }elseif ($pfv == 16) {

    }else {
        //AD CAPITAL
        //DB QUERY STRING
        $strQuery = "SELECT tracker.fch_dato AS fecha, disp.id_local AS tracker_nro, tracker.target_pos, tracker.current_pos,tracker.current_pos,tracker.serial_valor,tracker.fault_valor,tracker.estatus_valor,tracker.estatus_syst_op,tracker.pv_volt,tracker.bat_volt,tracker.cte_motor,tracker.temp_pcb,tracker.mode_track,tracker.wind_speed,tracker.fch_update,tracker.conex_valor,tracker.stow_valor,tracker.clean_valor,tracker.fch_utc_dato,tracker.fch_audit,tracker.enviado_remoto,tracker.fch_remoto,tracker.index_dato
        FROM   tbl_datos_tracker AS tracker
        INNER JOIN tbl_dispositivos disp ON disp.id_disp=tracker.id_disp
        WHERE tracker.id_parq = $pfv
        AND tracker.fch_dato BETWEEN '$inicio' AND '$fin'
        ORDER  BY tracker.fch_dato, disp.id_local ASC			
        ";
    }

    return $strQuery;
	}

	public function configPDO (){   
    $conexion = $this->conexion;
    $host = $conexion == null ? 0 : $conexion->host;
    $database_name = $conexion == null ? 0 : $conexion->db;
    //DB CONNECTION
    if($host == 1){
        $host = config('app.host_1');
        $port = config('app.host_port_1');
        $password = config('app.host_password_1');
    }else if($host == 2){

    }else{
        $host = 0;
        $port = 0;
        $password = 0;            
    }
    
		return [
			'driver'   => 'pgsql',
			'host'     =>  $host,
			'dbname'   =>  $database_name,
			'user'     => 'postgres',
			'password' =>  $password
		];
	}

  public function outputFileName (){
  	return 'Trackers.csv';
  }

  public function headings(): array{
		$conexion = $this->conexion;
    $pfv = $conexion == null ? 0 : $conexion->id_parq;

    $column_name = Array();

    if($pfv == 11){
      //PIQUERO
      $column_name = [
          'Fecha',
          'Tracker Nro',
          'dir_bus',
          'pos_angle',
          'error_code',        
          'estatus_back_track',
          'pv_volt','bat_volt','cte_motor','temp_pcb','current_pos','mode_track','wind_speed','fch_update','conex_valor','stow_valor','clean_valor','fch_utc_dato','fch_audit','enviado_remoto','fch_remoto','index_dato','raw_valor'];
  }elseif ($pfv == 12 || $pfv == 13 || $pfv == 14 || $pfv == 15) {
      //CODORCINES ||  SANTA LAURA|| PERDICES || LAS LECHUZAS
      $column_name = ['Fecha','Tracker Nro', 'id_spc','estatus_com','mode_track','fault_valor','smps_volt','bat_volt','bat_soc','target_pos','current_pos','cte_motor','aux1','aux2','aux3','fch_utc_dato','fch_audit','enviado_remoto','fch_remoto','index_dato','id_local','marca_disp','modelo_disp','desc_disp','databits_port','parity_port','stop_bits','baudrate_port','activo_disp','id_tipo_disp','port_disp','id_com','ip_disp','time_disp'];   
  }elseif ($pfv == 16) {
      //ILLALOLEN NO TRACKER
  }else {
      //AD CAPITAL
      $column_name = [
          'Fecha',
          'Tracker Nro',
          'Target Position',
          'Current Position',
          'serial_valor','fault_valor','estatus_valor','estatus_syst_op','pv_volt','bat_volt','cte_motor','temp_pcb','mode_track','wind_speed','fch_update','conex_valor','stow_valor','clean_valor','fch_utc_dato','fch_audit','enviado_remoto','fch_remoto','index_dato'];
  }

    return $column_name;
  }
}