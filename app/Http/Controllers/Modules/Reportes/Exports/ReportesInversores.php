<?php

namespace App\Http\Controllers\Modules\Reportes\Exports;

use App\Helpers\TimeHelper;
use App\Models\Conexion;
use App\Traits\ExportCsv;

class ReportesInversores
{
	use ExportCsv;

	protected $request;

	protected $conexion;

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct($request, $config = [])
  {
  	$this->request  = $request;

    $this->conexion = Conexion::where('id_empresa', $this->request->empresa_xls)->first();
  }


	public function verification ()
	{
		return TRUE;
	}

	/**
	 * Sentencia SQL para la exportacion del Excel.
	 * @return String Sentencia Sql
	 */
	public function sql ()
	{
    $empresa_id = $this->request->empresa_xls;
    $reporte_id = $this->request->id_reporte;
    $conexion = $this->conexion;
    $host = $conexion == null ? 0 : $conexion->host;
    $pfv = $conexion == null ? 0 : $conexion->id_parq;
    $medidor = $conexion == null ? 0 : $conexion->id_medidor;
    $radsensor = $conexion == null ? 0 : $conexion->id_radsensor;
    $database_name = $conexion == null ? 0 : $conexion->db;
    $subtitulo = $conexion == null ? 'no existe conexion' : $conexion->nombre_parq;			
    $dateRange = explode("-", $this->request->dates);
    $inicio = TimeHelper::formatDateRange($dateRange[0]);
    $fin = TimeHelper::formatDateRange($dateRange[1]);

    $data = Array();
    $inversor = Array();
    $potencia = Array();
    $dataRaw = Array();
    $horasRaw = Array();

    $strQuery = "";

    //CERNICALO
    if($pfv == 4){
      if($medidor == 301){
        //CGE
        //DB QUERY STRING
        $strQuery = "SELECT inv.fch_dato AS fecha, disp.id_local AS inversor, inv.id_disp, inv.pot_act_total AS potencia, est.nom_estatus AS inversor_status, inv.cte_string1, inv.cte_string2, inv.cte_string3, inv.cte_string4, inv.cte_string5, inv.cte_string6, inv.cte_string7, inv.cte_string8,inv.volt_string1,inv.volt_string2,inv.volt_string3,inv.volt_string4,inv.volt_string5,inv.volt_string6,inv.volt_string7,inv.volt_string8,inv.volt_a,inv.volt_b,inv.volt_c,inv.vlin_ab,inv.vlin_bc,inv.vlin_ca,inv.cte_a,inv.cte_b,inv.cte_c,inv.pot_act_total,inv.pot_rec_total,inv.pot_dc_total,inv.pot_mppt1_total,inv.pot_mppt2_total,inv.pot_mppt3_total,inv.pot_mppt4_total,inv.fac_pot,inv.ener_hora,inv.ener_dia,inv.ener_mes,inv.ener_year,inv.ener_total,inv.frec_valor,inv.efic_valor,inv.temp_valor,inv.id_estatus,inv.fch_utc_dato,inv.fch_audit,inv.enviado_remoto,inv.fch_remoto,inv.index_dato
        FROM   tbl_datos_inversor inv
        INNER JOIN tbl_dispositivos disp ON disp.id_disp=inv.id_disp
        INNER JOIN tbl_estatus_inversores est ON est.id_estatus=inv.id_estatus
        WHERE  inv.id_disp BETWEEN 307 AND 346
        AND
        inv.fch_dato between '$inicio'  AND '$fin' 
        ORDER  BY inv.fch_dato, inv.id_disp ASC
        ";				
      }else if($medidor == 391) {
        //COPELEC
        //DB QUERY STRING
        $strQuery = "SELECT inv.fch_dato AS fecha, disp.id_local AS inversor, inv.id_disp, inv.pot_act_total AS potencia, est.nom_estatus AS inversor_status, inv.cte_string1, inv.cte_string2, inv.cte_string3, inv.cte_string4, inv.cte_string5, inv.cte_string6, inv.cte_string7, inv.cte_string8,inv.volt_string1,inv.volt_string2,inv.volt_string3,inv.volt_string4,inv.volt_string5,inv.volt_string6,inv.volt_string7,inv.volt_string8,inv.volt_a,inv.volt_b,inv.volt_c,inv.vlin_ab,inv.vlin_bc,inv.vlin_ca,inv.cte_a,inv.cte_b,inv.cte_c,inv.pot_act_total,inv.pot_rec_total,inv.pot_dc_total,inv.pot_mppt1_total,inv.pot_mppt2_total,inv.pot_mppt3_total,inv.pot_mppt4_total,inv.fac_pot,inv.ener_hora,inv.ener_dia,inv.ener_mes,inv.ener_year,inv.ener_total,inv.frec_valor,inv.efic_valor,inv.temp_valor,inv.id_estatus,inv.fch_utc_dato,inv.fch_audit,inv.enviado_remoto,inv.fch_remoto,inv.index_dato
        FROM   tbl_datos_inversor inv
        INNER JOIN tbl_dispositivos disp ON disp.id_disp=inv.id_disp
        INNER JOIN tbl_estatus_inversores est ON est.id_estatus=inv.id_estatus
        WHERE  inv.id_disp BETWEEN 348 AND 390
        AND
        inv.fch_dato between '$inicio'  AND '$fin' 
        ORDER  BY inv.fch_dato, inv.id_disp ASC
        ";				
      }			
    }elseif($pfv == 11){
      //PIQUERO
      //DB QUERY STRING
      $strQuery = "SELECT inv.fch_dato AS fecha, disp.id_local AS inversor, inv.id_disp, inv.pot_act_total AS potencia,volt_dc,cte_dc_1,cte_dc_2,cte_dc_3,cte_dc_total,pot_dc_total,volt_dc_1,volt_dc_2,volt_dc_3,cte_ac_lin1,cte_ac_lin2,cte_ac_lin3,fact_pot,pot_ap_total,pot_rec_total,vlin_12,vlin_23,vlin_31,frec_valor,temp_ac,temp_dc,temp_pcb,temp_ext,temp_transf,estatus_dc1,estatus_dc2,estatus_dc3,estatus_ac,ener_dc_total,ener_ac_total,ener_dc_dia,ener_ac_dia,estatus_op_inv,aux_1,aux_2,aux_3,aux_4,aux_5,aux_6,aux_7,aux_8,fch_utc_dato,fch_audit,enviado_remoto,fch_remoto,index_dato
      FROM   tbl_datos_inversor_sma inv
      INNER JOIN tbl_dispositivos disp ON disp.id_disp=inv.id_disp
      WHERE  inv.id_disp = ANY 
      (SELECT id_disp FROM tbl_dispositivos WHERE id_parq=$pfv) AND
      inv.fch_dato between '$inicio'  AND '$fin' 
      ORDER  BY inv.fch_dato, inv.id_disp ASC	
      ";	
    }elseif($pfv == 13 || $pfv == 15 || $pfv == 17){
      //LECHUZAS
      //DB QUERY STRING
      $strQuery = "SELECT inv.fch_dato AS fecha, disp.id_local AS inversor, inv.id_disp, inv.pot_act_total AS potencia, est.nom_estatus AS inversor_status, inv.cte_string1, inv.cte_string2, inv.cte_string3, inv.cte_string4, inv.cte_string5, inv.cte_string6, inv.cte_string7,  inv.cte_string8, inv.cte_string9, inv.cte_string10, inv.cte_string11, inv.cte_string12
      FROM   tbl_datos_inversor_huawei100 inv
      INNER JOIN tbl_dispositivos disp ON disp.id_disp=inv.id_disp
      INNER JOIN tbl_estatus_inversores est ON est.id_estatus=inv.id_estatus
      WHERE  inv.id_disp = ANY 
      (SELECT id_disp FROM tbl_dispositivos WHERE id_parq=$pfv AND id_tipo_disp=5) AND
      inv.fch_dato between '$inicio'  AND '$fin' 
      ORDER  BY inv.fch_dato, inv.id_disp ASC	
      ";	
    }elseif($pfv == 16){
      //ILLALOLEN
      //DB QUERY STRING
      $strQuery = "SELECT inv.fch_dato AS fecha, disp.id_local AS inversor, inv.id_disp, inv.pot_act_total AS potencia,cte_ac,cte_dc,cte_a,cte_b,cte_c,volt_a,volt_b,volt_c,vlin_ab,vlin_bc,vlin_ca,pot_act_total,pot_ap_total,pot_rec_total,pot_dc,frec_valor,ener_total,volt_dc,temp_interna,temp_disip,temp_trafo,temp_other,id_estatus_op,id_estatus_specific,num_event1,num_event2,aux1,aux2,aux3,aux4,aux5,aux6,fch_utc_dato,fch_audit,enviado_remoto,fch_remoto,enviado_remoto2,fch_remoto2,index_dato
      FROM   tbl_datos_inversor_stp60 inv
      INNER JOIN tbl_dispositivos disp ON disp.id_disp=inv.id_disp
      WHERE  inv.id_disp = ANY 
      (SELECT id_disp FROM tbl_dispositivos WHERE id_parq=16 AND id_tipo_disp=5) AND
      inv.fch_dato between '$inicio'  AND '$fin' 
      ORDER  BY inv.fch_dato, disp.id_local ASC
      ";	
    }else{
      //ALL OTHER PFV
      //DB QUERY STRING
      $strQuery = "SELECT inv.fch_dato AS fecha, disp.id_local AS inversor, inv.id_disp, inv.pot_act_total AS potencia, est.nom_estatus AS inversor_status, inv.cte_string1, inv.cte_string2, inv.cte_string3, inv.cte_string4, inv.cte_string5, inv.cte_string6, inv.cte_string7, inv.cte_string8,inv.volt_string1,inv.volt_string2,inv.volt_string3,inv.volt_string4,inv.volt_string5,inv.volt_string6,inv.volt_string7,inv.volt_string8,inv.volt_a,inv.volt_b,inv.volt_c,inv.vlin_ab,inv.vlin_bc,inv.vlin_ca,inv.cte_a,inv.cte_b,inv.cte_c,inv.pot_act_total,inv.pot_rec_total,inv.pot_dc_total,inv.pot_mppt1_total,inv.pot_mppt2_total,inv.pot_mppt3_total,inv.pot_mppt4_total,inv.fac_pot,inv.ener_hora,inv.ener_dia,inv.ener_mes,inv.ener_year,inv.ener_total,inv.frec_valor,inv.efic_valor,inv.temp_valor,inv.id_estatus,inv.fch_utc_dato,inv.fch_audit,inv.enviado_remoto,inv.fch_remoto,inv.index_dato
      FROM   tbl_datos_inversor inv
      INNER JOIN tbl_dispositivos disp ON disp.id_disp=inv.id_disp
      INNER JOIN tbl_estatus_inversores est ON est.id_estatus=inv.id_estatus
      WHERE  inv.id_disp = ANY 
      (SELECT id_disp FROM tbl_dispositivos WHERE id_parq=$pfv AND id_tipo_disp=5) AND
      inv.fch_dato between '$inicio'  AND '$fin' 
      ORDER  BY inv.fch_dato, inv.id_disp ASC	
      ";
    }

    return $strQuery;
	}

	public function configPDO ()
	{   
    $conexion = $this->conexion;
    $host = $conexion == null ? 0 : $conexion->host;
    $database_name = $conexion == null ? 0 : $conexion->db;
    //DB CONNECTION
    if($host == 1){
        $host = config('app.host_1');
        $port = config('app.host_port_1');
        $password = config('app.host_password_1');
    }else if($host == 2){

    }else{
        $host = 0;
        $port = 0;
        $password = 0;            
    }
    
		return [
			'driver'   => 'pgsql',
			'host'     =>  $host,
			'dbname'   =>  $database_name,
			'user'     => 'postgres',
			'password' =>  $password
		];
	}

  public function outputFileName ()
  {
  	return 'Inversores.csv';
  }

  public function headings(): array
  {
		$conexion = $this->conexion;
    $pfv = $conexion == null ? 0 : $conexion->id_parq;

    $column_name = Array();

    if($pfv == 11){
        //PIQUERO
        $column_name = [
            'Fecha',
            'Combiner Box Nro',
            'Potencia Activa Total', 
            'volt_dc','cte_dc_1','cte_dc_2','cte_dc_3','cte_dc_total','pot_dc_total','volt_dc_1','volt_dc_2','volt_dc_3','cte_ac_lin1','cte_ac_lin2','cte_ac_lin3','fact_pot','pot_ap_total','pot_rec_total','vlin_12','vlin_23','vlin_31','frec_valor','temp_ac','temp_dc','temp_pcb','temp_ext','temp_transf','estatus_dc1','estatus_dc2','estatus_dc3','estatus_ac','ener_dc_total','ener_ac_total','ener_dc_dia','ener_ac_dia','estatus_op_inv','aux_1','aux_2','aux_3','aux_4','aux_5','aux_6','aux_7','aux_8','fch_utc_dato','fch_audit','id_parq','enviado_remoto','fch_remoto','index_dato'];
    }elseif($pfv == 13 || $pfv == 15){
      //SANTA LAURA || LAS LECHUZAS
      $column_name = [
          'Fecha',
          'Inversor Nro Terreno',
          'Inversor Nro DB',
          'Potencia',
          'Inversor Status',                
          'Cte String 1',
          'Cte String 2',
          'Cte String 3',
          'Cte String 4',
          'Cte String 5',
          'Cte String 6',
          'Cte String 7',
          'Cte String 8',
          'Cte String 9',
          'Cte String 10',
          'Cte String 11',
          'Cte String 12',
      ];
    }elseif($pfv == 16){
        //ILLALOLEN
        $column_name = [
            'Fecha',
            'Inversor Nro Terreno',
            'Inversor Nro DB',
            'Potencia Activa Total',
            'cte_ac','cte_dc','cte_a','cte_b','cte_c','volt_a','volt_b','volt_c','vlin_ab','vlin_bc','vlin_ca','pot_act_total','pot_ap_total','pot_rec_total','pot_dc','frec_valor','ener_total','volt_dc','temp_interna','temp_disip','temp_trafo','temp_other','id_estatus_op','id_estatus_specific','num_event1','num_event2','aux1','aux2','aux3','aux4','aux5','aux6','fch_utc_dato','fch_audit','enviado_remoto','fch_remoto','enviado_remoto2','fch_remoto2','index_dato'];
    }else{
        //ALL OTHER PFV
        $column_name = [
            'Fecha',
            'Inversor Nro Terreno',
            'Inversor Nro DB',
            'Potencia',
            'Inversor Status',                
            'Cte String 1',
            'Cte String 2',
            'Cte String 3',
            'Cte String 4',
            'Cte String 5',
            'Cte String 6',
            'Cte String 7',
            'Cte String 8',
            'volt_string1','volt_string2','volt_string3','volt_string4','volt_string5','volt_string6','volt_string7','volt_string8','volt_a','volt_b','volt_c','vlin_ab','vlin_bc','vlin_ca','cte_a','cte_b','cte_c','pot_act_total','pot_rec_total','pot_dc_total','pot_mppt1_total','pot_mppt2_total','pot_mppt3_total','pot_mppt4_total','fac_pot','ener_hora','ener_dia','ener_mes','ener_year','ener_total','frec_valor','efic_valor','temp_valor','id_estatus','fch_utc_dato','fch_audit','enviado_remoto','fch_remoto','index_dato'];
    }

    return $column_name;
  }
}