<?php

namespace App\Http\Controllers\Modules\Reportes\Exports;

use App\Helpers\TimeHelper;
use App\Models\Conexion;
use App\Traits\ExportCsv;

class ReportesCB{
	use ExportCsv;

	protected $request;

	protected $conexion;

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct($request, $config = []){
  	$this->request  = $request;

    $this->conexion = Conexion::where('id_empresa', $this->request->empresa_xls)->first();
  }


	public function verification (){
		return TRUE;
	}

	/**
	 * Sentencia SQL para la exportacion del Excel.
	 * @return String Sentencia Sql
	 */
	public function sql (){
    $empresa_id = $this->request->empresa_xls;
    $reporte_id = $this->request->id_reporte;
    $conexion = $this->conexion;
    $host = $conexion == null ? 0 : $conexion->host;
    $pfv = $conexion == null ? 0 : $conexion->id_parq;
    $medidor = $conexion == null ? 0 : $conexion->id_medidor;
    $radsensor = $conexion == null ? 0 : $conexion->id_radsensor;
    $database_name = $conexion == null ? 0 : $conexion->db;
    $subtitulo = $conexion == null ? 'no existe conexion' : $conexion->nombre_parq;			
    $dateRange = explode("-", $this->request->dates);
    $inicio = TimeHelper::formatDateRange($dateRange[0]);
    $fin = TimeHelper::formatDateRange($dateRange[1]);

    $data = Array();
    $inversor = Array();
    $potencia = Array();
    $dataRaw = Array();
    $horasRaw = Array();

    $strQuery = "";

    if($pfv == 11){
      //PIQUERO
      //DB QUERY STRING
      $strQuery = "SELECT datos.fch_dato AS fecha, datos.id_local AS inversor, round(((datos.volt_dc * datos.cte_total) / 1000)::numeric,2)  AS potencia, datos.cte_dc_in1, datos.cte_dc_in2, datos.cte_dc_in3, datos.cte_dc_in4, datos.cte_dc_in5, datos.cte_dc_in6, datos.cte_dc_in7, datos.cte_dc_in8, datos.cte_dc_in9, datos.cte_dc_in10, datos.cte_dc_in11, datos.cte_dc_in12, datos.cte_dc_in13, datos.cte_dc_in14, datos.cte_dc_in15, datos.cte_dc_in16, datos.cte_dc_in17, datos.cte_dc_in18, datos.cte_dc_in19, datos.cte_dc_in20, datos.cte_dc_in21, datos.cte_dc_in22, datos.cte_dc_in23, datos.cte_dc_in24 
      FROM 
      (
      SELECT cbx.fch_dato, disp.id_local, volt_dc, cte_dc_in1, cte_dc_in2, cte_dc_in3, cte_dc_in4, cte_dc_in5, cte_dc_in6, cte_dc_in7, cte_dc_in8, cte_dc_in9, cte_dc_in10, cte_dc_in11, cte_dc_in12, cte_dc_in13, cte_dc_in14, cte_dc_in15, cte_dc_in16, cte_dc_in17, cte_dc_in18, cte_dc_in19, cte_dc_in20, cte_dc_in21, cte_dc_in22, cte_dc_in23, cte_dc_in24,
      (cte_dc_in1 + cte_dc_in2 + cte_dc_in3 + cte_dc_in4 + cte_dc_in5 + cte_dc_in6 + cte_dc_in7 + cte_dc_in8 +
      cte_dc_in9 + cte_dc_in10 + cte_dc_in11 + cte_dc_in12 + cte_dc_in13 + cte_dc_in14 + cte_dc_in15 + cte_dc_in16 +
      cte_dc_in17 + cte_dc_in18 + cte_dc_in19 + cte_dc_in20 + cte_dc_in21 + cte_dc_in22 + cte_dc_in23 + cte_dc_in24) cte_total
      FROM tbl_datos_combinerbox_sma cbx
      INNER JOIN tbl_dispositivos disp ON disp.id_disp=cbx.id_disp
      WHERE cbx.id_parq=$pfv AND disp.id_tipo_disp=9 AND
      cbx.fch_dato between '$inicio' AND 
      '$fin' 
      ORDER  BY cbx.fch_dato, disp.id_local
      ) datos";
    }

    return $strQuery;
	}

	public function configPDO (){   
    $conexion = $this->conexion;
    $host = $conexion == null ? 0 : $conexion->host;
    $database_name = $conexion == null ? 0 : $conexion->db;
    //DB CONNECTION
    if($host == 1){
        $host = config('app.host_1');
        $port = config('app.host_port_1');
        $password = config('app.host_password_1');
    }else if($host == 2){

    }else{
        $host = 0;
        $port = 0;
        $password = 0;            
    }
    
		return [
			'driver'   => 'pgsql',
			'host'     =>  $host,
			'dbname'   =>  $database_name,
			'user'     => 'postgres',
			'password' =>  $password
		];
	}

  public function outputFileName (){
  	return 'Combiner Box.csv';
  }

  public function headings(): array{
		$conexion = $this->conexion;
    $pfv = $conexion == null ? 0 : $conexion->id_parq;

    $column_name = Array();

    if($pfv == 11){
      //PIQUERO
      $column_name = [
          'Fecha',
          'Combiner Box Nro',
          'Potencia',           
          'Cte String 1',
          'Cte String 2',
          'Cte String 3',
          'Cte String 4',
          'Cte String 5',
          'Cte String 6',
          'Cte String 7',
          'Cte String 8',
          'Cte String 9',
          'Cte String 10',
          'Cte String 11',
          'Cte String 12',
          'Cte String 13',
          'Cte String 14',
          'Cte String 15',
          'Cte String 16',
          'Cte String 17',
          'Cte String 18',
          'Cte String 19',
          'Cte String 20',
          'Cte String 21',
          'Cte String 22',
          'Cte String 23',
          'Cte String 24',
      ];
    }

    return $column_name;
  }
}