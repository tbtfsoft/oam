<?php

namespace App\Http\Controllers\Modules\Reportes;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection as Collection;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use App\Helpers\TimeHelper;
use App\Http\Controllers\Controller;
use App\Models\Conexion;
use App\Models\Barras;
use App\Models\BarrasEmpresas;
use App\Models\ModalidadCalculo;
use App\Models\ModalidadCalculoEmpresas;
use App\Models\FactorReferencia;
use App\Models\PrecioEstabilizado;
use App\Models\AssetManagement\Empresa;
use App\Exports\ReportesMedidor;
//use App\Exports\ReportesInversores;
//use App\Exports\ReportesTracker;
//use App\Exports\ReportesCB;
use App\Exports\ReportesCalidad;
use App\Http\Controllers\Modules\Reportes\Exports\ReportesInversores;
use App\Http\Controllers\Modules\Reportes\Exports\ReportesTracker;
use App\Http\Controllers\Modules\Reportes\Exports\ReportesCB;

class ReportesController extends Controller
{

	/**
	 * Controlador
	 * @return View
	 */
	public function homeElectrico ()
	{
		return view('modules.reportes.electrico.home');
	}

	public function generarReporteElectrico (Request $request)
	{
		$empresa_id = $request->empresa;
		$reporte_id = $request->id_reporte;

		if ($reporte_id == 1) {
			return Excel::download(new ReportesMedidor($request), 'Medidor.csv');					
		}elseif ($reporte_id == 2) {
			$export = new ReportesInversores($request);
			return $export->export();
		}elseif ($reporte_id == 3) {
			$export = new ReportesTracker($request);
			return $export->export();
		}elseif ($reporte_id == 4) {
			return Excel::download(new ReportesCalidad($request), 'Calidad.csv');
		}elseif ($reporte_id == 5) {
			$export = new ReportesCB($request);
			return $export->export();
		}
		
	}

}