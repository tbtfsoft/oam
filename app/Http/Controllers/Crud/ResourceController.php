<?php

namespace App\Http\Controllers\CRUD;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \IzyTech\Validator\Exceptions\ValidatorException;
use \IzyTech\Validator\Contracts\ValidatorInterface;
use App\Repositories\BaseRepositoryEloquent as Repository;
use App\DataTables\GenericDataTable as DataTable;
use View;
use Flash;
use Lang;

/**
 * Interface ResourceController.
 *
 * @package namespace App\Repositories;
 */
class ResourceController extends Controller
{

    /**
     * @var model
     */
    protected $model;

    /**
     * @var class_basename
     */
    protected $class_basename;

    /**
     * @var PostRepository
     */
    protected $repository;

    /**
     * @var dataTable
     */
    protected $dataTable;

    /**
     * @var dataTable
     */
    protected $relations;
    
    /**
     * @var prefix_view
     */
    protected $prefix_view;


    public function __construct($model, Array $options = [])
    {
        $this->model = $model;

        $this->dataTable  = isset($options['datatable']) ? $options['datatable'] : $this->dataTable();
        
        $this->repository = isset($options['repository']) ? $options['repository'] : $this->repository();

        $this->class_basename  = isset($options['class_basename']) ? $options['class_basename'] : $model->getTableModel();

        $this->relations  = isset($options['relations']) ? $options['relations'] : [];

        $this->prefix_view  = isset($options['prefix_view']) ? $options['prefix_view'] : $model->getTableModel();

        $this->prefix_router  = isset($options['prefix_router']) ? $options['prefix_router'] : $model->getTableModel();

        View::share('class_basename',  $this->class_basename);

        View::share('prefix_router',  $this->prefix_router);

        View::share('prefix_view',  $this->prefix_view);
    }

	/**
	* Display a listing of the resource.
	*
    * @return \Illuminate\Http\Response
    */
    public function index (Request $request)
    {
        $datatable = $this->dataTable();

        return $datatable->render($this->view('index'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create ()
    {

        $view = view($this->view('create'));
        
    	foreach ($this->relations as $key => $repository) {
    		$view->with($key, $repository);
    	}

        return $view;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $currency = $this->repository->create( $request->input() );

            Flash::success(Lang::get('crud.store'));
    
            return redirect(route($this->as('index')));
        }
        catch (ValidatorException $e) {

            Flash::error(Lang::get('crud.error'));

            return back()
                    ->withInput($request->input())
                    ->withErrors($e->getMessageBag()->getMessages());
        }
    }

    /**
     * Store a newly Show resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = $this->repository->find($id);

        if ($model) {
            return redirect(route($this->as('show'), $id));
        }
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = $this->repository->find($id);

        if ($model) {
            $view = view($this->view('edit'))->with('model', $model);
    
            foreach ($this->relations as $key => $repository) {
                $view->with($key, $repository);
            }
    
            return $view;
        }

        return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $currency = $this->repository->update( $request->input(), $id );

            Flash::success(Lang::get('crud.update'));

            return redirect(route($this->as('index')));
        }
        catch (ValidatorException $e) {

            Flash::error(Lang::get('crud.error'));

            return back()
                    ->withInput($request->input())
                    ->withErrors($e->getMessageBag()->getMessages());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->delete($id);

        Flash::warning(Lang::get('crud.destroy'));

        return redirect(route($this->as('index')));
    }

    /**
     * Retorna la vista por defecto si no existe $v
     * @param  String $v    La vista que se desea buscar. 'index', 'edit', 'show'.. 'etc'.
     * @return string
     */
    protected function view($v)
    {
        $view = $this->prefix_view .'.'. $v;

        if(!View::exists($view))
            return 'layouts.crud.'.$v;

        return $view;
    }

    /**
     * Obtiene el prefix para armar la ruta de la vista deseada.
     *
     * @param   String  $as  'index', 'edit', 'show'.. 'etc'
     *
     * @return  String       Cadena de la ruta.
     */
    public function as($as)
    {
        if ($this->prefix_router) {
            return $this->prefix_router.'.'.$as;
        }
        return $this->model->getTable().'.'.$as;
    }

    /**
     * Retorna el basename del Modelo
     * @return String
     */
    public function class_basename ()
    {
        return strtolower(class_basename($this->model));
    }

    public function repository()
    {
        return new Repository($this->model);
    }

    public function dataTable()
    {
        return new DataTable($this->model, \Auth::user());
    }
}