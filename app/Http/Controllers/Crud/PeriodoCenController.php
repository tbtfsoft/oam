<?php

namespace App\Http\Controllers\CRUD;

use Illuminate\Http\Request;
use App\Http\Controllers\CRUD\GenericResourceController;
use App\Models\AssetManagement\PeriodoCen as model;
use View;
use Flash;
use Lang;

class PeriodoCenController extends GenericResourceController {

  public function __construct(Model $model) {
    parent::__construct($model, [
        'class_basename' => 'periodos_cen'
    ]);
  }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $currency = $this->repository->update( $request->input(), $id );

            Flash::success(Lang::get('crud.update'));

            return redirect(route($this->as('index')));
        }
        catch (ValidatorException $e) {

            Flash::error(Lang::get('crud.error'));

            return back()
                    ->withInput($request->input())
                    ->withErrors($e->getMessageBag()->getMessages());
        }
    }
}
