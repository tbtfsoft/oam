<?php

namespace App\Http\Controllers\CRUD;

use Illuminate\Http\Request;
use App\Http\Controllers\CRUD\GenericResourceController;
use App\DataTables\ModalidadCalculoEmpresasDataTable as DataTable;
use App\Models\ModalidadCalculoEmpresas as model;

class ModalidadCalculoEmpresasController extends GenericResourceController
{
	
  public function __construct(Model $model)
  {
    parent::__construct($model, [
      'datatable'  => new DataTable($model, \Auth::user()),
			'class_basename' => 'modalidad_calculo_empresas'
    ]);
  }
}