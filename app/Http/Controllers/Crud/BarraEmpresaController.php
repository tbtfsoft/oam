<?php

namespace App\Http\Controllers\CRUD;

use Illuminate\Http\Request;
use App\Http\Controllers\CRUD\GenericResourceController;
use App\DataTables\BarrasEmpresasDataTable as DataTable;
use App\Models\BarrasEmpresas as model;
use View;
use Flash;
use Lang;

class BarraEmpresaController extends GenericResourceController
{
  public function __construct(Model $model)
  {
    parent::__construct($model, [
      'datatable'  => new DataTable($model, \Auth::user()),
      'class_basename' => 'barras_empresas'
    ]);
  }
}
