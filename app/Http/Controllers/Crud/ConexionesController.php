<?php

namespace App\Http\Controllers\CRUD;

use Illuminate\Http\Request;
use \IzyTech\Validator\Exceptions\ValidatorException;
use App\Http\Controllers\CRUD\GenericResourceController;
use App\Models\Conexion as model;
use Flash;
use Lang;

class ConexionesController extends GenericResourceController
{
  public function __construct(Model $model)
  {
		parent::__construct($model, [
      'class_basename' => 'conexiones'
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    try {

      $currency = $this->repository->create( $request->input() );

      Flash::success(Lang::get('crud.store'));

      return redirect(route($this->as('index')));
    }
    catch (ValidatorException $e) {

      Flash::error(Lang::get('crud.error'));

      return back()
              ->withInput($request->input())
              ->withErrors($e->getMessageBag()->getMessages());
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    try {

      $currency = $this->repository->update( $request->input(), $id );

      Flash::success(Lang::get('crud.update'));

      return redirect(route($this->as('index')));
    }
    catch (ValidatorException $e) {

      Flash::error(Lang::get('crud.error'));

      return back()
              ->withInput($request->input())
              ->withErrors($e->getMessageBag()->getMessages());
    }
  }
}
