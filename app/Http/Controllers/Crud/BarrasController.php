<?php

namespace App\Http\Controllers\CRUD;

use Illuminate\Http\Request;
use App\Http\Controllers\CRUD\GenericResourceController;
use App\Models\Barras as model;
use View;
use Flash;
use Lang;

class BarrasController extends GenericResourceController {

  public function __construct(Model $model) {
    parent::__construct($model, [
      'class_basename' => 'barras'
    ]);
  }

}
