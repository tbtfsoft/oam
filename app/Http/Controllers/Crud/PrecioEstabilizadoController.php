<?php

namespace App\Http\Controllers\CRUD;

use Illuminate\Http\Request;
use \IzyTech\Validator\Exceptions\ValidatorException;
use App\Http\Controllers\CRUD\GenericResourceController;
use App\DataTables\PrecioEstablecidoDataTable as DataTable;
use App\Repositories\PrecioEstabilizadoRepositoryEloquent as Repository;
use App\Models\PrecioEstabilizado as model;
use Flash;
use Lang;

class PrecioEstabilizadoController extends GenericResourceController
{
  public function __construct(Model $model)
  {
	  parent::__construct($model, [
      'repository' => new Repository($model),
      'datatable'  => new DataTable($model, \Auth::user()),
      'class_basename' => 'precio_estabilizado'
    ]);
  }
}
