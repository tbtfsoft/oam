<?php

namespace App\Http\Controllers\CRUD;

use App\Http\Controllers\CRUD\GenericResourceController;
use App\Models\Permission as model;
use App\DataTables\PermissionsDataTable as DataTable;
use App\Repositories\BaseRepositoryEloquent as Repository;
use Spatie\Permission\Models\Permission;
use Illuminate\Http\Request;
use View;
use Flash;
use Lang;


class PermissionsController extends GenericResourceController
{
  public function __construct(Model $model)
  {
	  parent::__construct($model, [
      'repository' => new Repository($model),
      'datatable'  => new DataTable($model, \Auth::user()),
      'class_basename' => 'permissions'
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    try {
      Permission::firstOrCreate([
        'name'          => $request->name,
        'display_name'  => $request->display_name,
        'module'        => $request->module,
        'description'   => $request->description
      ]);

      // $currency = $this->repository->create( $request->input() );

      Flash::success(Lang::get('crud.store'));

      return redirect(route($this->as('index')));
    }
    catch (ValidatorException $e) {

      Flash::error(Lang::get('crud.error'));

      return back()
              ->withInput($request->input())
              ->withErrors($e->getMessageBag()->getMessages());
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    try {

      $currency = $this->repository->update( $request->input(), $id );

      Flash::success(Lang::get('crud.update'));

      return redirect(route($this->as('index')));
    }
    catch (ValidatorException $e) {

      Flash::error(Lang::get('crud.error'));

      return back()
              ->withInput($request->input())
              ->withErrors($e->getMessageBag()->getMessages());
    }
  }
}