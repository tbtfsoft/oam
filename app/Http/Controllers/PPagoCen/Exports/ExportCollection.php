<?php

namespace App\Http\Controllers\PPagoCen\Exports;

//NO SE USA.
use Maatwebsite\Excel\Concerns\WithMapping;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\Models\PPagoCen\PPEmpresa;

class ExportCollection implements FromCollection, WithHeadings, ShouldAutoSize, WithHeadingRow, WithChunkReading, ShouldQueue
{
    use Exportable;

    protected $model;
    
    public function __construct($model)
    {
        ini_set('memory_limit', '-1');

        $this->model = $model;
    }

    /**
     * La consulta para general la query para el excel.
     *
     * @author Carlos Asnelmi <carlosanselmi2@gmail.com>
     * @return Query
     */
    public function collection()
    {
        set_time_limit(500);
		
        return $this->model::all();
        // return $this->model::whereIn('id', $this->ids())->get();
        // return $this->model::whereIn('rut', $this->ruts())->get();
    }
    
    /**
     * Coloca los head de las columnas
     *
     * @author Carlos Asnelmi <carlosanselmi2@gmail.com>
     * @return Array()
     */
    public function headings(): array
    {
        return [
        'ID',
        'Nombre Fantasia',
        'RUT',
        'Digito Verificador',
        'Razon Social',
        'Giro',
        'Email Recepccion DTE',
        'Numero de Cuenta Bancaria',
        'Banco',
        'Direccion Comercial',
        'Direccion Postal',
        'Gerente',
        'Contacto Pagos Nombre',
        'Contacto Pagos Apellido',
        'Contacto Pagos Direccion',
        'Contacto Pagos Telefono',
        'Contacto Pagos Email',
        'Contacto Facturacion Nombre',
        'Contacto Facturacion Apellido',
        'Contacto Facturacion Direccion',
        'Contacto Facturacion Telefono',
        'Contacto Facturacion Email',
        'Fecha de Creacion',
        'Fecha de Modificacion',
    ];
    }

    public function batchSize(): int
    {
        return 800;
    }
    
    public function chunkSize(): int
    {
        return 800;
    }

    public function ruts() 
    {
        return [
            76006855,76009698,76019239,76030638,76037036,76052206,76055356,76059578,76060441,76067373,76068557,76071891,76106835,76126507,76145769,76149809,76179024,76189274,76203788,76248798,76249099,76257379,76265287,76281947,76284294,76318056,76319883,76321458,76337599,76349223,76350250,76376635,76376829,76412562,76416891,76418918,76427498,76437712,76459637,76459845,76468419,76489426,76550580,76555400,76708710,76787690,76819440,76832212,76840310,77277800,77302440,77683400,87756500,88006900,91066000,91081000,96504980,96524140,96637520,96774300,96827870,96928510,96971330,96990040,76004976,76085254,76131355,76218856,76411321,76594660,76896732,77683400,78932860,96576920,96790240,99528750
        ];
    }

    public function ids() 
    {
        return [ 2,3,4,5];
    }
}
