<?php

namespace App\Http\Controllers\PPagoCen;

use Illuminate\Support\Collection as Collection;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\PPagoCen\PPEmpresa;
use App\Models\PPagoCen\PPEmpresaOld;
use App\Models\PPagoCen\PPBanco;
use GuzzleHttp\Client;
use File;
use Lang;

class PpController extends Controller
{

	protected $client;

  protected $base_uri = "https://ppagos-sen.coordinador.cl/api/v1/resources/";

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
  	$this->client = new Client([
  		'base_uri'	=> $this->base_uri,
	    'headers' => [
	        'Content-Type' => 'application/x-www-form-urlencoded',
	    ],
  	]);
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function index()
  {

    $empresas = PPEmpresa::all();

    $bancos = PPBanco::all();

		return view('modules.ppago.index', compact('empresas', 'bancos'));
  }

  /**
   * Lee el archivo Empresa.json para obtener las empresas y registralas en la base de datos.
   * @author Carlos Anselmi <Carlosanselmi2@gmail.com> 
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function empresas()
  {
  	 //$response = $this->client->get('participants/?limit=5');
  	$bancos = $this->getNombresBancos();

    ini_set('max_execution_time', 300);

    $response = $this->peticion_get('participants');

    $results = $response['results'];

    //$this->checkPPempresa($results);

    //$this->SetOldEmpresa($results);

    PPEmpresa::truncate();

  	foreach ($results as $row) {
  		PPEmpresa::Create([
				'id'   => $row['id'],
        'name' => $row['name'] ? $row['name'] : 'Sin Informacion en PP CEN',
				'rut'	 => $row['rut'] ? $row['rut'] : 'Sin Informacion en PP CEN',
				'verification_code'		=> $row['verification_code'] ? $row['verification_code'] : '0',
				'business_name'				=> $row['business_name'] ? $row['business_name'] : 'Sin Informacion en PP CEN',
				'commercial_business'	=> $row['commercial_business'] ? $row['commercial_business'] : 'Sin Informacion en PP CEN',
				'dte_reception_email'	=> $row['dte_reception_email'] ? $row['dte_reception_email'] : 'Sin Informacion en PP CEN',
				'bank_account'				=> $row['bank_account'] ? $row['bank_account'] : 'Sin Informacion en PP CEN',
				'bank'								=> array_key_exists($row['bank'],$bancos) ? $bancos[$row['bank']] : '',
				'commercial_address'=> $row['commercial_address'],
				'postal_address'		=> $row['postal_address'],
				'manager'						=> $row['manager'],
				'payments_contact_first_name'	=> $row['payments_contact']['first_name'],
				'payments_contact_last_name'	=> $row['payments_contact']['last_name'],
				'payments_contact_address'		=> $row['payments_contact']['address'],
				'payments_contact_phones'			=> $this->contact_phone($row['payments_contact']['phones']),
				'payments_contact_email'			=> $row['payments_contact']['email'] ? $row['payments_contact']['email'] : 'Sin Informacion en PP CEN',
				'bills_contact_first_name'		=> $row['bills_contact']['first_name'],
				'bills_contact_last_name'			=> $row['bills_contact']['last_name'],
				'bills_contact_address'				=> $row['bills_contact']['address'],
				'bills_contact_phones'				=> $this->contact_phone($row['bills_contact']['phones']),
				'bills_contact_email'					=> $row['bills_contact']['email'],
        'created_at'         => date("Y-m-d H:i:s", strtotime($row['created_ts'])),
        'updated_at'         => date("Y-m-d H:i:s", strtotime($row['updated_ts']))
  		]);
  	}

  	return response()->json(['status' => 200, 'message' => lang::get('app.Data procesada correctamente'), 'modelo' => 'Empresa'],200);
  }

  public function contact_phone($contact_phones)
  {
    if (is_array ($contact_phones)) {
      $phones = implode(" , ", $contact_phones);
    }else{
      $phones = '';
    }    

    return $phones;
  }

  /**
   * Lee el archivo bancos.json para obtener los bancos y agragarlo en la base de datos.
   * @author Carlos Anselmi <Carlosanselmi2@gmail.com> 
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function bancos()
  {
  	// $response = $this->client->get('participants/?limit=5');
    $response = $this->peticion_get('banks');

    $results = $response['results'];

    PPBanco::truncate();

  	foreach ($results as $row) {
  		PPBanco::firstOrCreate([
  			'code' => $row['code'],
  			'name' => $row['name'],
  			'sbif' => $row['sbif'],
  			'type' => $row['type']
  		]);
  	}

    return response()->json(['status' => 200, 'message' => lang::get('app.Data procesada correctamente'), 'modelo' => 'Empresa'],200);
  }


	/**
	 * Genera un array con los bancos, siendo el key del array el id del banco y el value el nombre
	 * @return Array
   * @author Carlos Anselmi <Carlosanselmi2@gmail.com> 
	 */
	private function getNombresBancos () {

		$bancos = $this->getBancos();

		$nombresBancos = [];

		foreach ($bancos as $banco) {
			$nombresBancos[$banco->id] = $banco->name;
		}

		return $nombresBancos;
	}

  /**
   * Exportar data de PPempres
   * @return Excel
   */
  public function export ($option = null)
  {
    $nameFile =  'pagos-sen-'.date('y-m-d_h-m-s') .'.xlsx';

    return Excel::download(new Exports\ExportCollection(new PPEmpresa()),$nameFile);
  }

  public function peticion_get ($path)
  {
    $url = $this->getUrl($path);

    $conexion = curl_init();
    // --- Url
    curl_setopt($conexion, CURLOPT_URL,$url);
    // --- Petición GET.
    curl_setopt($conexion, CURLOPT_HTTPGET, TRUE);
    // --- Cabecera HTTP.
    curl_setopt($conexion, CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
    // --- Para recibir respuesta de la conexión.
    curl_setopt($conexion, CURLOPT_RETURNTRANSFER, 1);

    $response = curl_exec($conexion);

    return collect(json_decode($response, true));
  }

  /**
   * Concaten el path y base_uri
   * @param  String $path
   * @return Json
   */
  public function getUrl ($path)
  {
    return $this->base_uri . $path . '/?limit=900';
  }

  private function getBancos () 
  {
    return PPBanco::all();
  }

  private function getEmpresas ()
  {
    return PPEmpresa::all();
  }

  public function process ()
  {
    $bancos = $this->bancos();
    $bancos = $this->getBancos();

    $empresas = $this->empresas();
    $empresas = $this->getEmpresas();

    return response()->json(['status' => 200,
                             'message' => lang::get('app.Data procesada correctamente'),
                             'response' => view('modules.ppago.response.table',compact('empresas','bancos'))->render()
                           ],200);
  }

  private function checkPPempresa ($collect) {
    $empresaOld = PPEmpresaOld::all();
    if (!count($empresaOld)) {
      $this->SetOldEmpresa($collect);
    }
  
    $diff = $empresaOld->diffKeys($collect);
    dd($diff);
  }
  private function SetOldEmpresa ($collect) {
    $bancos = $this->getNombresBancos();
    PPEmpresaOld::truncate();
    foreach ($collect as $row) {
      PPEmpresaOld::Create([
        'name' => $row['name'] ? $row['name'] : 'Sin Informacion en PP CEN',
        'rut'  => $row['rut'] ? $row['rut'] : 'Sin Informacion en PP CEN',
        'verification_code'   => $row['verification_code'] ? $row['verification_code'] : '0',
        'business_name'       => $row['business_name'] ? $row['business_name'] : 'Sin Informacion en PP CEN',
        'commercial_business' => $row['commercial_business'] ? $row['commercial_business'] : 'Sin Informacion en PP CEN',
        'dte_reception_email' => $row['dte_reception_email'] ? $row['dte_reception_email'] : 'Sin Informacion en PP CEN',
        'bank_account'        => $row['bank_account'] ? $row['bank_account'] : 'Sin Informacion en PP CEN',
        'bank'                => array_key_exists($row['bank'],$bancos) ? $bancos[$row['bank']] : '',
        'commercial_address'=> $row['commercial_address'],
        'postal_address'    => $row['postal_address'],
        'manager'           => $row['manager'],
        'payments_contact_first_name' => $row['payments_contact']['first_name'],
        'payments_contact_last_name'  => $row['payments_contact']['last_name'],
        'payments_contact_address'    => $row['payments_contact']['address'],
        'payments_contact_phones'			=> $this->contact_phone($row['payments_contact']['phones']),
        'payments_contact_email'      => $row['payments_contact']['email'] ? $row['payments_contact']['email'] : 'Sin Informacion en PP CEN',
        'bills_contact_first_name'    => $row['bills_contact']['first_name'],
        'bills_contact_last_name'     => $row['bills_contact']['last_name'],
        'bills_contact_address'       => $row['bills_contact']['address'],
        'bills_contact_phones'				=> $this->contact_phone($row['bills_contact']['phones']),
        'bills_contact_email'         => $row['bills_contact']['email']
      ]);
    }
  }
}
