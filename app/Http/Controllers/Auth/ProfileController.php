<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Auth\AuthResourceController;
use App\Http\Controllers\Auth\Requests\updateUserPassword;
use App\Http\Controllers\Auth\Requests\updateUser;
use App\User as Model;
use Flash;
use Validator;
use Hash;
use Lang;

class ProfileController extends AuthResourceController
{

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function profile()
    {
        return view('auth.profile.index');
    }

    public function updateProfile(updateUser $request)
    {
        $user = \App\User::find(\Auth::id());

        $user->name = $request->name;

        $user->save();

        Flash::success(Lang::get('auth.profile_updated'));

        return redirect(route('profile.index'));
    }

    /**
     * Show accounts.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function updateAuthUserPassword(updateUserPassword $request)
    {
        $user = \App\User::find(\Auth::id());

        if (!Hash::check($request->password_current, $user->password)) {

            Flash::success(Lang::get('auth.profile_updated'));

            return back()->withInput($request->input());
        }

        // if ($request->password_current != $request->password_confirmation) {

        //     Flash::error('La nueva contraseña no coincide.');

        //     return back()->withInput($request->input());
        // }

        $user->password = Hash::make($request->password);

        $user->save();

        Flash::success(Lang::get('auth.profile_updated'));

        return redirect(route('profile.index'));
    }
}
