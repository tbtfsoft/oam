<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        if (!\Auth::check()) {
            return redirect()->route('login');
        }

        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/');
    }

    public function username()
    {
        return 'rut';
    }
    
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'rut' => 'required|max:15|unique:users',
            'password' => 'required|min:4|confirmed',
        ]);
    }


    public function ajaxLogin(Request $request)
    {
        $auth = false;

        $credentials = $request->only($this->username(), 'password');

        $credentials['rut'] = mb_strtoupper($credentials['rut']);

        if (\Auth::attempt($credentials, $request->has('remember'))) {
            $auth = true;
        }

        if ($request->ajax()) {
            return response()->json([
                'auth'      => $auth,
                'path'  => !$auth ?: url($this->redirectPath()),
                'message'   => $auth ? 'Bien!' : 'Mal!'
            ]);
        }
    }
}