<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \IzyTech\Validator\Exceptions\ValidatorException;
use \IzyTech\Validator\Contracts\ValidatorInterface;
use App\Repositories\UserRepositoryEloquent as Repository;
use App\DataTables\GenericDataTable as DataTable;
use App\Mail\CreateUserMail;
use View;
use Flash;
use Lang;

/**
 * Interface CountryRepository.
 *
 * @package namespace App\Repositories;
 */
class AuthResourceController extends Controller
{

    /**
     * @var model
     */
    protected $model;

    /**
     * @var class_basename
     */
    protected $class_basename;

    /**
     * @var PostRepository
     */
    protected $repository;

    /**
     * @var dataTable
     */
    protected $dataTable;

    /**
     * @var dataTable
     */
    protected $relations;


    public function __construct($model, $relations = [])
    {
        $this->model = $model;

        $this->dataTable  = $this->dataTable();
        
        $this->repository = $this->repository();

        $this->class_basename  = $this->class_basename();

        $this->relations  = $relations;

        View::share('class_basename', $this->model->getTable());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function index()
    {
        $this->dataTable  = $this->dataTable();

        return $this->dataTable->render($this->view('index'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $view = view($this->view('create'));

    	foreach ($this->relations as $key => $repository) {
    		$view->with($key, $repository);
    	}

        return $view;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $user = $this->repository->create( $request->input() );

            $user->empresas()->sync($request->empresas);

            $parent = \Auth::user();
            
            if($parent->hasRole(['client-admin'])) {
                $parent->childers()->attach($user);
            }

            Flash::success(Lang::get('crud.store'));
    
            // \Mail::to($user->email)->send(new CreateUserMail($user));

            return redirect(route($this->as('index')));
        }
        catch (ValidatorException $e) {

            Flash::error(Lang::get('crud.error'));
            return back()
                    ->withInput($request->input())
                    ->withErrors($e->getMessageBag()->getMessages());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = $this->repository->find($id);

		$view = view($this->view('edit'))->with('model', $model);

    	foreach ($this->relations as $key => $repository) {
    		$view->with($key, $repository);
    	}

        return $view;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            
            $currency = $this->repository->update( $request->input(), $id );

            $currency_empresas = $currency->empresas->map(function ($empresa) {
                return $empresa->id;
            })->toArray();


            if ($currency->hasRole(['client-admin'])) {
                foreach ($currency->childers as $userChilder) {
                    $childer_empresas = $userChilder->empresas->filter(function ($empresa) use ($currency_empresas) {
                        return in_array($empresa->id, $currency_empresas);
                    })->map(function ($empresa) {
                        return $empresa->id;
                    })->toArray();

                    $userChilder->empresas()->sync($childer_empresas);
                }
            }

            $currency->empresas()->sync($request->empresas);

            Flash::success(Lang::get('crud.update'));

            return redirect(route($this->as('index')));
        }
        catch (ValidatorException $e) {

            Flash::error(Lang::get('crud.error'));

            return back()
                    ->withInput($request->input())
                    ->withErrors($e->getMessageBag()->getMessages());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->delete($id);

        Flash::warning(Lang::get('crud.destroy'));

        return redirect(route($this->as('index')));
    }

    /**
     * Retorna la vista por defecto si no existe $view
     * @param  String $view
     * @return string
     */
    public function view($view)
    {
        $view_edit = $this->dir_view($view);

        if(!View::exists($view_edit))
            return 'layouts.crud.'.$view;

        return $view_edit;
    }


    public function as($as)
    {
        return $this->model->getTable().'.'.$as;
    }

    /**
     * Retorna el basename del Modelo
     * @return String
     */
    public function class_basename()
    {
        return strtolower(class_basename($this->model));
    }
    
    /**
     * Retorna la vista por defecto del modelo
     * @param  String $view [edit,index,show,deleted,...]
     * @return String
     */
    public function dir_view($view)
    {
        return 'layouts.crud.'.$this->model->getTable().'.'.$view;
    }

    /**
     * Retorna el repositorio del modelo
     */
    public function repository()
    {
        return new Repository($this->model);
    }

    public function dataTable()
    {
        return new DataTable($this->model,\Auth::user());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return abort(404);
    }

}