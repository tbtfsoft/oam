<?php

namespace App\Http\Controllers\Auth\Requests;

use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;
use Flash;
use Lang;

class updateUserPassword extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password_current' => 'required|min:5',
            'password' => 'required|confirmed|min:5',
            'password_confirmation' => 'required|min:5'
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails()) {
            $validator->after(function ($validator) {
                Flash::error(Lang::get('auth.error.check_fields'));
            });
        }
    }
}
