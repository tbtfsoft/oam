<?php

namespace App\Http\Controllers\GuzzleHttp;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;

class GuzzlesSipub extends Controller
{

	protected $client;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    	$this->client = new Client([
    		'base_uri'	=> config('guzzle.conexions.sipub.base_uri'),
			'headers'	=> [
		    	'Authorization'     => config('guzzle.conexions.sipub.authorization')
		    ]
    	]);
    }
}
