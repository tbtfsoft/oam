<?php

namespace App\Http\Middleware;

use Closure;
use Config;
use Illuminate\Http\Response;

class CheckForMaintenaceModule
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {
        if (app('auth')->guest()) {
            throw UnauthorizedException::notLoggedIn();
        }

        $maintenancePermissions = Config::get('permission.maintenance');

        $maintenancePermissions = is_array($maintenancePermissions)
            ? $maintenancePermissions
            : explode('|', $maintenancePermissions);

        $permissions = is_array($permission)
            ? $permission
            : explode('|', $permission);

        foreach ($permissions as $p) {
            if (in_array($p, $maintenancePermissions)) {
                return new response(view('errors.503'));
            }
        }

        return $next($request);
    }
}
