<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Blade;

class BladeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('implode', function ($expression) {
                return "<?php echo strtoupper($expression); ?>";
            });

        Blade::component('layouts.components.scripts-highcharts', 'highcharts');
        
        Blade::component('layouts.components.content', 'content');

        Blade::include('layouts.components.selects.typeDocument', 'SelectTypeDocument');

        Blade::include('layouts.components.selects.companys', 'SelectCompanys');

        Blade::include('layouts.components.selects.months', 'SelectMonths');

        Blade::include('layouts.components.selects.years', 'SelectYears');

        Blade::include('layouts.components.selects.modules', 'SelectModules');

        Blade::include('layouts.components.progressBar', 'progressBar');

        Blade::include('layouts.partials.assets.components.jquery-confirm', 'jqueryConfirm');

        Blade::include('layouts.partials.assets.components.datatables', 'datatables');

        Blade::component('layouts.partials.assets.components.datapicker', 'datapicker');

        Blade::component('layouts.partials.assets.components.fileinput', 'fileinput');

        Blade::component('layouts.partials.assets.components.treeMultiselect', 'multiselect');
    }
}
