<?php

namespace App\Repositories;

use IzyTech\Repository\Eloquent\BaseRepository;
use Illuminate\Container\Container as Application;

/**
 * Class ParametroRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class PrecioEstabilizadoRepositoryEloquent extends BaseRepository
{

    protected $rules;

    protected $model;

    public function __construct($model) {

        $this->model = $model;

        $this->rules = $model->rules();

        $app = Application::getInstance();

        parent::__construct($app);
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return get_class($this->model);
    }
}
