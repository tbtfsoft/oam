<?php

namespace App\Repositories;

use App\Repositories\BaseRepositoryEloquent;
use Illuminate\Container\Container as Application;
use IzyTech\Validator\Contracts\ValidatorInterface;
use IzyTech\Repository\Events\RepositoryEntityCreated;
use IzyTech\Repository\Events\RepositoryEntityUpdated;
use Hash;
/**
 * Class ParametroRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class UserRepositoryEloquent extends BaseRepositoryEloquent
{

    /**
     * Save a new entity in repository
     *
     * @throws ValidatorException
     *
     * @param array $attributes
     *
     * @return mixed
     */
    public function create(array $attributes)
    {
        if (!is_null($this->validator)) {
            // we should pass data that has been casts by the model
            // to make sure data type are same because validator may need to use
            // this data to compare with data that fetch from database.
            if( $this->versionCompare($this->app->version(), "5.2.*", ">") ){
                $attributes = $this->model->newInstance()->forceFill($attributes)->makeVisible($this->model->getHidden())->toArray();
            }else{
                $model = $this->model->newInstance()->forceFill($attributes);
                $model->addVisible($this->model->getHidden());
                $attributes = $model->toArray();
            }

            $this->validator->with($attributes)->passesOrFail(ValidatorInterface::RULE_CREATE);
        }

        unset($attributes['role']);

        $attributes['password'] = Hash::make($attributes['password']);

        $model = $this->model->newInstance($attributes);

        $attributes = $this->syncRolesAndPermissions($model,$attributes);

        $model->save();

        $this->resetModel();

        event(new RepositoryEntityCreated($this, $model));

        return $this->parserResult($model);
    }


    /**
     * Update a entity in repository by id
     *
     * @throws ValidatorException
     *
     * @param array $attributes
     * @param       $id
     *
     * @return mixed
     */
    public function update(array $attributes, $id)
    {
        $this->applyScope();

        if (!is_null($this->validator)) {
            // we should pass data that has been casts by the model
            // to make sure data type are same because validator may need to use
            // this data to compare with data that fetch from database.
            if( $this->versionCompare($this->app->version(), "5.2.*", ">") ){
                $attributes = $this->model->newInstance()->forceFill($attributes)->makeVisible($this->model->getHidden())->toArray();
            }else{
                $model = $this->model->newInstance()->forceFill($attributes);
                $model->addVisible($this->model->getHidden());
                $attributes = $model->toArray();
            }

            $this->validator->with($attributes)->setId($id)->passesOrFail(ValidatorInterface::RULE_UPDATE);
        }

        $temporarySkipPresenter = $this->skipPresenter;

        $this->skipPresenter(true);

        $model = $this->model->findOrFail($id);

        unset($attributes['role']);
        
        $attributes['password'] = isset($attributes['password']) ?  Hash::make($attributes['password']) : $model->password;

        $this->syncRolesAndPermissions($model,$attributes);

        $model->fill($attributes);

        $model->save();

        $this->skipPresenter($temporarySkipPresenter);
        $this->resetModel();

        event(new RepositoryEntityUpdated($this, $model));

        return $this->parserResult($model);
    }

    public function syncRolesAndPermissions($model,$attributes)
    {

        if (isset($attributes['roles']) && !is_null($attributes['roles'])) {
            $model->syncRoles([$attributes['roles']]);
        }

        if (isset($attributes['permissions']) && !is_null($attributes['permissions'])) {
            if($model->hasRole(['client-admin'])) {
                foreach ($model->childers as $key => $child) {
                    $this->syncPermissions($child,$attributes);
                }
            }
            $model->syncPermissions([$attributes['permissions']]);
        }else
            $model->syncPermissions([]);

    }

    public function syncPermissions($model,$attributes)
    {
        if (isset($attributes['permissions']) && !is_null($attributes['permissions'])) {
            $model->syncPermissions([$attributes['permissions']]);
        }else
            $model->syncPermissions([]);
    }
}
