<?php

namespace App\DataTables;

use View;
use App\Datatables\GenericDataTable;
use App\Datatables\Traits\DatatableTrait;
use App\Models\Permission as model;

class PermissionsDataTable extends GenericDataTable
{
  use DatatableTrait;

    
  /**
   * Get query source of dataTable.
   *
   * @param \App\User $model
   * @return \Illuminate\Database\Eloquent\Builder
   */
  public function query()
  {
  	return $this->model->newQuery();
  }
}
