<?php

namespace App\DataTables;

use Yajra\DataTables\Services\DataTable;
use View;
use Lang;

class GenericDataTable extends DataTable
{

    protected $model;

    protected $class_model;

    protected $user;

    public function __construct($model, $user = null) {

        $this->model = $model;

        $this->user = $user;

        $this->class_model = strtolower(class_basename($this->model));
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {

        $view_actions = 'layouts.crud.'.$this->class_model.'.actions';

        return datatables($query)
                    ->setRowId('id')
                    ->addColumn('action', View::exists($view_actions) ? $view_actions : 'layouts.crud.actions');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->addAction($this->getActionsParameters())
                    ->minifiedAjax()
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $columns = [
            ['title' => 'id', 'sWidth' => '1%', 'data' => 'id', 'orderable' => true]
        ];

        $files = method_exists($this->model, 'getfillDatatable') ? $this->model->getfillDatatable() : $this->model->getFillable();

        if (is_array($files)) {
            foreach ($files as $fillable) {
                if ($fillable !== 'password') {
                    array_push($columns, [
                        'title'     =>  Lang::get('validation.attributes.'.$fillable),
                        'data'      => $fillable,
                        'orderable' => config('datatables.orderable')
                    ]);
                }
            }
        }

        return $columns;
    }

    /**
     * Get Parameters.
     *
     * @return array
     */
    protected function getBuilderParameters()
    {
        $locale = session()->get('locale') ? session()->get('locale') : 'es';
        
        return [
            'paging'        => true,
            'searching'     => config('datatables.searching'),
            "processing"    => true,
            'responsive'    => true,
            'autoWidth'     => false,
            "rowGroup"      => true,
            'info'          => true,
            'searchDelay'   => 350,
            'lengthMenu' =>  [[15, 100, 200], [15, 100, 200]],
            'dom'     => 'Bfrtip',
            'language' => [ 'url' => url('plugins/dataTables/lang/'.$locale.'.json') ],
            'buttons' => [
                [
                    'extend' => 'pageLength'
                ],
                [
                    'extend' => 'excel',
                    'text'   => '<span class="fa fa-file-excel"></span> Excel Export',
                    'exportOptions' => [
                        'modifer' => [
                            'page' => 'All',
                            'search' => 'none'
                        ],
                        'modifier' => [
                            'page' => 'All',
                            'search' => 'none'
                        ]
                    ]
                ],
                [
                    'extend' => 'pdf',
                    'text'   => '<span class="fa fa-file-pdf"></span> PDF Export',
                    'exportOptions' => [
                        'modifer' => [
                            'page' => 'All',
                            'search' => 'none'
                        ],
                        'modifier' => [
                            'page' => 'All',
                            'search' => 'none'
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * Get Actions Parameters.
     *
     * @return array
     */
    protected function getActionsParameters()
    {
        return [
            'width'          => '1%',
            'exportable'     => false,
            'printable'      => false,
            'searchable'     => false,
            'orderable'      => false,
            'responsivePriority' => 2,
            'title'          => Lang::get('crud.action'),
            'defaultContent' => ''
        ];
    }
    
    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        if(!is_null($this->user) && $this->user->hasRole(['client-admin']))
            return $this->user->childers;

        return $this->model->newQuery();
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return $this->class_model.'_' . date();
    }
}
