<?php

namespace App\Datatables\Traits;

use \App\User;
use \App\Models\Barras;
use \App\Models\ModalidadCalculo;
use \App\Models\AssetManagement\Empresa;

trait DatatableTrait {
 
  public function getUserLabel ($id)
  {
    $e = User::find($id);
    return $e ? $e->name : 'N/D';
  }
  public function getFilterUser ($keyword)
  {
    return User::where('name','LIKE', ["%{$keyword}%"])->pluck('id')->toArray();
  }
  public function getBarraLabel ($id)
  {
    $e = Barras::find($id);
    return $e ? $e->barra_human : 'N/D';
  }

  public function getFilterBarras ($keyword)
  {
  	return Barras::where('barra_human','LIKE', ["%{$keyword}%"])->pluck('id')->toArray();
  }

  public function getEmpresaLabel ($id)
  {
    $e = Empresa::find($id);
    return $e ? $e->nombre_xls : 'N/D';
  }

  public function getFilterEmpresa ($keyword)
  {
  	return Empresa::where('nombre_xls','LIKE', ["%{$keyword}%"])->pluck('id')->toArray();
  }

  public function getModalidadCalculoLabel ($id)
  {
    $e = ModalidadCalculo::find($id);
    return $e ? $e->nombre : "$id (ID)";
  }

  public function getFilterModalidadCalculo ($keyword)
  {
    return ModalidadCalculo::where('nombre','LIKE', ["%{$keyword}%"])->pluck('id')->toArray();
  }
}