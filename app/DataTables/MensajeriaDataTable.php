<?php

namespace App\DataTables;

use View;
use App\Datatables\GenericDataTable;
use App\Datatables\Traits\DatatableTrait;

class MensajeriaDataTable extends GenericDataTable
{
    use DatatableTrait;

    public function __construct($model, $user = null) {
        
        $this->model = $model;

        $this->user = $user;

        $this->class_model = strtolower(class_basename($this->model));
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $view_actions = 'modules.asset-management.resources.mensajeria.row';

        return datatables($query)
                ->setRowId('id')
                ->addColumn('user', function ($row) {
                    return $this->getUserLabel($row->id_user);
                })
                ->addColumn('correspondencia', $view_actions)
                ->filterColumn('correspondencia', function($query, $keyword) {
                    $sql = "correspondencias.titulo like ?";
                    $query->whereRaw($sql, ["%{$keyword}%"]);
                })
                ->rawColumns(['correspondencia']);
    }

    public function query ()
    {
        $ids = $this->user->empresas->pluck('id');
        if ( $this->user->hasRole(['super-admin', 'backoffice']) ) {
            $query = $this->model::orderBy('fecha', 'DESC')->doesntHave('empresas')->orWhereHas('empresas', function ($q) use ($ids) {
                $q->whereHas('empresa', function ($r) use ($ids) {
                    $r->whereIn('id', $ids);
                });
            })->select();
        }

        $query = $this->model::orderBy('fecha', 'DESC');
    
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters($this->getBuilderParameters());
    }
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return ['correspondencia'];
    }
    /**
     * Get Parameters.
     *
     * @return array
     */
    protected function getBuilderParameters()
    {
        $locale = session()->get('locale') ? session()->get('locale') : 'es';
        
        return [
            'paging'        => true,
            'searching'     => config('datatables.searching'),
            "processing"    => true,
            'responsive'    => true,
            'autoWidth'     => false,
            "rowGroup"      => true,
            'info'          => true,
            'searchDelay'   => 350,
            'lengthMenu' =>  [[15, 100, 200], [15, 100, 200]],
            'language' => [ 'url' => url('plugins/dataTables/lang/'.$locale.'.json') ]
        ];
    }
}
