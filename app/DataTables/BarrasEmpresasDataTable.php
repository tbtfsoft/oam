<?php

namespace App\DataTables;

use View;
use App\Datatables\GenericDataTable;
use App\Datatables\Traits\DatatableTrait;

class BarrasEmpresasDataTable extends GenericDataTable
{
    use DatatableTrait;

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $view_actions = 'layouts.crud.'.$this->class_model.'.actions';

        return datatables($query)
                ->setRowId('id')
                ->editColumn('id_empresa', function ($row) {
                    return $this->getEmpresaLabel($row->id_empresa);
                })
                ->filterColumn('id_empresa', function($query, $keyword) {
                    $query->whereIn('id_empresa', $this->getFilterEmpresa($keyword));
                })
                ->addColumn('action', View::exists($view_actions) ? $view_actions : 'layouts.crud.actions')
                ->rawColumns(['id_barra','action']);
    }
}
