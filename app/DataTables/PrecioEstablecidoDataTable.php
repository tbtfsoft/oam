<?php

namespace App\DataTables;

use View;
use App\Datatables\GenericDataTable;
use App\Datatables\Traits\DatatableTrait;

class PrecioEstablecidoDataTable extends GenericDataTable
{
    use DatatableTrait;

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $view_actions = 'layouts.crud.'.$this->class_model.'.actions';

        return datatables($query)
                ->setRowId('id')
                ->editColumn('id_barra', function ($row) {
                    return $this->getBarraLabel($row->id_barra);
                })
                ->filterColumn('id_barra', function($query, $keyword) {
                    $query->whereIn('id_barra', $this->getFilterBarras($keyword));
                })
                ->editColumn('precio_energia', function ($row) {
                    return '$' . number_format($row->precio_energia);
                })
                ->editColumn('precio_potencia', function ($row) {
                    return '$' . number_format($row->precio_potencia);
                })
                ->addColumn('action', View::exists($view_actions) ? $view_actions : 'layouts.crud.actions')
                ->rawColumns(['id_barra','action']);
    }
}
