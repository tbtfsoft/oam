<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Helpers\TimeHelper;
use App\Http\Controllers\Controller;
use App\Models\Conexion;
use App\Models\Barras;
use App\Models\BarrasEmpresas;
use App\Models\ModalidadCalculo;
use App\Models\ModalidadCalculoEmpresas;
use App\Models\FactorReferencia;
use App\Models\PrecioEstabilizado;
use App\Models\AssetManagement\Empresa;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection as Collection;

class ReportesInversores implements FromCollection,WithHeadings
{
    protected $request;
    
    public function __construct($request)
    {
        ini_set('memory_limit', '-1');

        set_time_limit(500);

        $this->request = $request;
    }

    public function collection()
    {
        $empresa_id = $this->request->empresa_xls;
		$reporte_id = $this->request->id_reporte;
		$conexion = Conexion::where('id_empresa', $empresa_id)->first();
        $host = $conexion == null ? 0 : $conexion->host;
        $pfv = $conexion == null ? 0 : $conexion->id_parq;
        $medidor = $conexion == null ? 0 : $conexion->id_medidor;
        $radsensor = $conexion == null ? 0 : $conexion->id_radsensor;
        $database_name = $conexion == null ? 0 : $conexion->db;
		$subtitulo = $conexion == null ? 'no existe conexion' : $conexion->nombre_parq;			
		$dateRange = explode("-", $this->request->dates);
		$inicio = TimeHelper::formatDateRange($dateRange[0]);
        $fin = TimeHelper::formatDateRange($dateRange[1]);

        $data = Array();
		$inversor = Array();
		$potencia = Array();
        $dataRaw = Array();
		$horasRaw = Array();

        //DB CONNECTION
        if($host == 1){
            $host = config('app.host_1');
            $port = config('app.host_port_1');
            $password = config('app.host_password_1');
        }else if($host == 2){

        }else{
            $host = 0;
            $port = 0;
            $password = 0;            
        }

        if($host != 0){
            //INICIO DE LA CONSULTA
            $strConnect = "host = ".$host." port = ".$port." dbname = '".$database_name."' user = 'postgres' password = ".$password;
            $con = pg_connect($strConnect);

			//CERNICALO
			if($pfv == 4){
				if($medidor == 301){
					//CGE
					//DB QUERY STRING
					$strQuery = "SELECT inv.fch_dato AS fecha, disp.id_local AS inversor, inv.id_disp, inv.pot_act_total AS potencia, est.nom_estatus AS inversor_status, inv.cte_string1, inv.cte_string2, inv.cte_string3, inv.cte_string4, inv.cte_string5, inv.cte_string6, inv.cte_string7, inv.cte_string8
					FROM   tbl_datos_inversor inv
					INNER JOIN tbl_dispositivos disp ON disp.id_disp=inv.id_disp
					INNER JOIN tbl_estatus_inversores est ON est.id_estatus=inv.id_estatus
					WHERE  inv.id_disp BETWEEN 307 AND 346
					AND
					inv.fch_dato between '$inicio'  AND 
				'$fin' 
					ORDER  BY inv.fch_dato, inv.id_disp ASC
					";				
				}else if($medidor == 391) {
					//COPELEC
					//DB QUERY STRING
					$strQuery = "SELECT inv.fch_dato AS fecha, disp.id_local AS inversor, inv.id_disp, inv.pot_act_total AS potencia, est.nom_estatus AS inversor_status, inv.cte_string1, inv.cte_string2, inv.cte_string3, inv.cte_string4, inv.cte_string5, inv.cte_string6, inv.cte_string7, inv.cte_string8
					FROM   tbl_datos_inversor inv
					INNER JOIN tbl_dispositivos disp ON disp.id_disp=inv.id_disp
					INNER JOIN tbl_estatus_inversores est ON est.id_estatus=inv.id_estatus
					WHERE  inv.id_disp BETWEEN 348 AND 390
					AND
					inv.fch_dato between '$inicio'  AND 
				'$fin' 
					ORDER  BY inv.fch_dato, inv.id_disp ASC
					";				
				}			
			}elseif($pfv == 11){
				//PIQUERO
				//DB QUERY STRING
				$strQuery = "SELECT inv.fch_dato AS fecha, disp.id_local AS inversor, inv.id_disp, inv.pot_act_total AS potencia,volt_dc,cte_dc_1,cte_dc_2,cte_dc_3,cte_dc_total,pot_dc_total,volt_dc_1,volt_dc_2,volt_dc_3,cte_ac_lin1,cte_ac_lin2,cte_ac_lin3,fact_pot,pot_ap_total,pot_rec_total,vlin_12,vlin_23,vlin_31,frec_valor,temp_ac,temp_dc,temp_pcb,temp_ext,temp_transf,estatus_dc1,estatus_dc2,estatus_dc3,estatus_ac,ener_dc_total,ener_ac_total,ener_dc_dia,ener_ac_dia,estatus_op_inv,aux_1,aux_2,aux_3,aux_4,aux_5,aux_6,aux_7,aux_8,fch_utc_dato,fch_audit,enviado_remoto,fch_remoto,index_dato
				FROM   tbl_datos_inversor_sma inv
				INNER JOIN tbl_dispositivos disp ON disp.id_disp=inv.id_disp
				WHERE  inv.id_disp = ANY 
				(SELECT id_disp FROM tbl_dispositivos WHERE id_parq=$pfv) AND
				inv.fch_dato between '$inicio'  AND '$fin' 
				ORDER  BY inv.fch_dato, inv.id_disp ASC	
				";	
			}elseif($pfv == 13){
				//SANTA LAURA
				//DB QUERY STRING
				$strQuery = "SELECT inv.fch_dato AS fecha, disp.id_local AS inversor, inv.id_disp, inv.pot_act_total AS potencia, est.nom_estatus AS inversor_status, inv.cte_string1, inv.cte_string2, inv.cte_string3, inv.cte_string4, inv.cte_string5, inv.cte_string6, inv.cte_string7,  inv.cte_string8, inv.cte_string9, inv.cte_string10, inv.cte_string11, inv.cte_string12
				FROM   tbl_datos_inversor_huawei100 inv
				INNER JOIN tbl_dispositivos disp ON disp.id_disp=inv.id_disp
				INNER JOIN tbl_estatus_inversores est ON est.id_estatus=inv.id_estatus
				WHERE  inv.id_disp = ANY 
				(SELECT id_disp FROM tbl_dispositivos WHERE id_parq=$pfv AND id_tipo_disp=5) AND
				inv.fch_dato between '$inicio'  AND '$fin' 
				ORDER  BY inv.fch_dato, inv.id_disp ASC	
				";	
			}elseif($pfv == 15){
				//LECHUZAS
				//DB QUERY STRING
				$strQuery = "SELECT inv.fch_dato AS fecha, disp.id_local AS inversor, inv.id_disp, inv.pot_act_total AS potencia, est.nom_estatus AS inversor_status, inv.cte_string1, inv.cte_string2, inv.cte_string3, inv.cte_string4, inv.cte_string5, inv.cte_string6, inv.cte_string7,  inv.cte_string8, inv.cte_string9, inv.cte_string10, inv.cte_string11, inv.cte_string12
				FROM   tbl_datos_inversor_huawei100 inv
				INNER JOIN tbl_dispositivos disp ON disp.id_disp=inv.id_disp
				INNER JOIN tbl_estatus_inversores est ON est.id_estatus=inv.id_estatus
				WHERE  inv.id_disp = ANY 
				(SELECT id_disp FROM tbl_dispositivos WHERE id_parq=$pfv AND id_tipo_disp=5) AND
				inv.fch_dato between '$inicio'  AND '$fin' 
				ORDER  BY inv.fch_dato, inv.id_disp ASC	
				";	
			}elseif($pfv == 16){
				//ILLALOLEN
				//DB QUERY STRING
				$strQuery = "SELECT inv.fch_dato AS fecha, disp.id_local AS inversor, inv.id_disp, inv.pot_act_total AS potencia,cte_ac,cte_dc,cte_a,cte_b,cte_c,volt_a,volt_b,volt_c,vlin_ab,vlin_bc,vlin_ca,pot_act_total,pot_ap_total,pot_rec_total,pot_dc,frec_valor,ener_total,volt_dc,temp_interna,temp_disip,temp_trafo,temp_other,id_estatus_op,id_estatus_specific,num_event1,num_event2,aux1,aux2,aux3,aux4,aux5,aux6,fch_utc_dato,fch_audit,enviado_remoto,fch_remoto,enviado_remoto2,fch_remoto2,index_dato
                FROM   tbl_datos_inversor_stp60 inv
                INNER JOIN tbl_dispositivos disp ON disp.id_disp=inv.id_disp
                WHERE  inv.id_disp = ANY 
                (SELECT id_disp FROM tbl_dispositivos WHERE id_parq=16 AND id_tipo_disp=5) AND
                inv.fch_dato between '$inicio'  AND '$fin' 
                ORDER  BY inv.fch_dato, disp.id_local ASC
				";	
			}else{
				//ALL OTHER PFV
				//DB QUERY STRING
				$strQuery = "SELECT inv.fch_dato AS fecha, disp.id_local AS inversor, inv.id_disp, inv.pot_act_total AS potencia, est.nom_estatus AS inversor_status, inv.cte_string1, inv.cte_string2, inv.cte_string3, inv.cte_string4, inv.cte_string5, inv.cte_string6, inv.cte_string7, inv.cte_string8
				FROM   tbl_datos_inversor inv
				INNER JOIN tbl_dispositivos disp ON disp.id_disp=inv.id_disp
				INNER JOIN tbl_estatus_inversores est ON est.id_estatus=inv.id_estatus
				WHERE  inv.id_disp = ANY 
				(SELECT id_disp FROM tbl_dispositivos WHERE id_parq=$pfv AND id_tipo_disp=5) AND
				inv.fch_dato between '$inicio'  AND '$fin' 
				ORDER  BY inv.fch_dato, inv.id_disp ASC	
				";
            }
            
            //DB QUERY
            $resRAW = pg_query($con,$strQuery);		

            while($dataFetch = pg_fetch_assoc($resRAW))
            {
                $dataRaw[] = $dataFetch;
            }
        }

        return collect($dataRaw);
    }

    public function headings(): array
    {
        $empresa_id = $this->request->empresa_xls;
		$conexion = Conexion::where('id_empresa', $empresa_id)->first();
        $pfv = $conexion == null ? 0 : $conexion->id_parq;

        $column_name = Array();

        if($pfv == 11){
            //PIQUERO
            $column_name = [
                'Fecha',
                'Combiner Box Nro',
                'Potencia Activa Total', 
                'volt_dc','cte_dc_1','cte_dc_2','cte_dc_3','cte_dc_total','pot_dc_total','volt_dc_1','volt_dc_2','volt_dc_3','cte_ac_lin1','cte_ac_lin2','cte_ac_lin3','fact_pot','pot_ap_total','pot_rec_total','vlin_12','vlin_23','vlin_31','frec_valor','temp_ac','temp_dc','temp_pcb','temp_ext','temp_transf','estatus_dc1','estatus_dc2','estatus_dc3','estatus_ac','ener_dc_total','ener_ac_total','ener_dc_dia','ener_ac_dia','estatus_op_inv','aux_1','aux_2','aux_3','aux_4','aux_5','aux_6','aux_7','aux_8','fch_utc_dato','fch_audit','id_parq','enviado_remoto','fch_remoto','index_dato'          

            ];
        }elseif($pfv == 13){
            //SANTA LAURA
            $column_name = [
                'Fecha',
                'Inversor Nro Terreno',
                'Inversor Nro DB',
                'Potencia',
                'Inversor Status',                
                'Cte String 1',
                'Cte String 2',
                'Cte String 3',
                'Cte String 4',
                'Cte String 5',
                'Cte String 6',
                'Cte String 7',
                'Cte String 8',
                'Cte String 9',
                'Cte String 10',
                'Cte String 11',
                'Cte String 12',
            ];
        }elseif($pfv == 15){
            //LECHUZAS
            $column_name = [
                'Fecha',
                'Inversor Nro Terreno',
                'Inversor Nro DB',
                'Potencia',
                'Inversor Status',                
                'Cte String 1',
                'Cte String 2',
                'Cte String 3',
                'Cte String 4',
                'Cte String 5',
                'Cte String 6',
                'Cte String 7',
                'Cte String 8',
                'Cte String 9',
                'Cte String 10',
                'Cte String 11',
                'Cte String 12',
            ];
        }elseif($pfv == 16){
            //LECHUZAS
            $column_name = [
                'Fecha',
                'Inversor Nro Terreno',
                'Inversor Nro DB',
                'Potencia Activa Total',
                'cte_ac','cte_dc','cte_a','cte_b','cte_c','volt_a','volt_b','volt_c','vlin_ab','vlin_bc','vlin_ca','pot_act_total','pot_ap_total','pot_rec_total','pot_dc','frec_valor','ener_total','volt_dc','temp_interna','temp_disip','temp_trafo','temp_other','id_estatus_op','id_estatus_specific','num_event1','num_event2','aux1','aux2','aux3','aux4','aux5','aux6','fch_utc_dato','fch_audit','enviado_remoto','fch_remoto','enviado_remoto2','fch_remoto2','index_dato'
            ];
        }else{
            //ALL OTHER PFV
            $column_name = [
                'Fecha',
                'Inversor Nro Terreno',
                'Inversor Nro DB',
                'Potencia',
                'Inversor Status',                
                'Cte String 1',
                'Cte String 2',
                'Cte String 3',
                'Cte String 4',
                'Cte String 5',
                'Cte String 6',
                'Cte String 7',
                'Cte String 8',
            ];
        }

        return $column_name;
    }
}