<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Helpers\TimeHelper;
use App\Http\Controllers\Controller;
use App\Models\Conexion;
use App\Models\Barras;
use App\Models\BarrasEmpresas;
use App\Models\ModalidadCalculo;
use App\Models\ModalidadCalculoEmpresas;
use App\Models\FactorReferencia;
use App\Models\PrecioEstabilizado;
use App\Models\AssetManagement\Empresa;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection as Collection;

class ReportesCB implements FromCollection,WithHeadings
{
    protected $request;
    
    public function __construct($request)
    {
        ini_set('memory_limit', '-1');

        set_time_limit(500);

        $this->request = $request;
    }

    public function collection()
    {
        $empresa_id = $this->request->empresa_xls;
		$reporte_id = $this->request->id_reporte;
		$conexion = Conexion::where('id_empresa', $empresa_id)->first();
        $host = $conexion == null ? 0 : $conexion->host;
        $pfv = $conexion == null ? 0 : $conexion->id_parq;
        $medidor = $conexion == null ? 0 : $conexion->id_medidor;
        $radsensor = $conexion == null ? 0 : $conexion->id_radsensor;
        $database_name = $conexion == null ? 0 : $conexion->db;
		$subtitulo = $conexion == null ? 'no existe conexion' : $conexion->nombre_parq;			
		$dateRange = explode("-", $this->request->dates);
		$inicio = TimeHelper::formatDateRange($dateRange[0]);
        $fin = TimeHelper::formatDateRange($dateRange[1]);

        $data = Array();
		$inversor = Array();
		$potencia = Array();
        $dataRaw = Array();
        $horasRaw = Array();
        $strQuery = "";

        //DB CONNECTION
        if($host == 1){
            $host = config('app.host_1');
            $port = config('app.host_port_1');
            $password = config('app.host_password_1');
        }else if($host == 2){

        }else{
            $host = 0;
            $port = 0;
            $password = 0;            
        }

        if($host != 0){
            //INICIO DE LA CONSULTA
            $strConnect = "host = ".$host." port = ".$port." dbname = '".$database_name."' user = 'postgres' password = ".$password;
            $con = pg_connect($strConnect);

            if($pfv == 11){
				//PIQUERO
				//DB QUERY STRING
				$strQuery = "SELECT datos.fch_dato AS fecha, datos.id_local AS inversor, round(((datos.volt_dc * datos.cte_total) / 1000)::numeric,2)  AS potencia, datos.cte_dc_in1, datos.cte_dc_in2, datos.cte_dc_in3, datos.cte_dc_in4, datos.cte_dc_in5, datos.cte_dc_in6, datos.cte_dc_in7, datos.cte_dc_in8, datos.cte_dc_in9, datos.cte_dc_in10, datos.cte_dc_in11, datos.cte_dc_in12, datos.cte_dc_in13, datos.cte_dc_in14, datos.cte_dc_in15, datos.cte_dc_in16, datos.cte_dc_in17, datos.cte_dc_in18, datos.cte_dc_in19, datos.cte_dc_in20, datos.cte_dc_in21, datos.cte_dc_in22, datos.cte_dc_in23, datos.cte_dc_in24 
				FROM 
				(
				SELECT cbx.fch_dato, disp.id_local, volt_dc, cte_dc_in1, cte_dc_in2, cte_dc_in3, cte_dc_in4, cte_dc_in5, cte_dc_in6, cte_dc_in7, cte_dc_in8, cte_dc_in9, cte_dc_in10, cte_dc_in11, cte_dc_in12, cte_dc_in13, cte_dc_in14, cte_dc_in15, cte_dc_in16, cte_dc_in17, cte_dc_in18, cte_dc_in19, cte_dc_in20, cte_dc_in21, cte_dc_in22, cte_dc_in23, cte_dc_in24,
				(cte_dc_in1 + cte_dc_in2 + cte_dc_in3 + cte_dc_in4 + cte_dc_in5 + cte_dc_in6 + cte_dc_in7 + cte_dc_in8 +
				cte_dc_in9 + cte_dc_in10 + cte_dc_in11 + cte_dc_in12 + cte_dc_in13 + cte_dc_in14 + cte_dc_in15 + cte_dc_in16 +
				cte_dc_in17 + cte_dc_in18 + cte_dc_in19 + cte_dc_in20 + cte_dc_in21 + cte_dc_in22 + cte_dc_in23 + cte_dc_in24) cte_total
				FROM tbl_datos_combinerbox_sma cbx
				INNER JOIN tbl_dispositivos disp ON disp.id_disp=cbx.id_disp
				WHERE cbx.id_parq=$pfv AND disp.id_tipo_disp=9 AND
				cbx.fch_dato between '$inicio' AND 
				'$fin' 
				ORDER  BY cbx.fch_dato, disp.id_local
                ) datos";
            }	
            
            //DB QUERY
            $resRAW = pg_query($con,$strQuery);		

            while($dataFetch = pg_fetch_assoc($resRAW))
            {
                $dataRaw[] = $dataFetch;
            }
        }

        return collect($dataRaw);
    }

    public function headings(): array
    {
        $empresa_id = $this->request->empresa_xls;
		$conexion = Conexion::where('id_empresa', $empresa_id)->first();
        $pfv = $conexion == null ? 0 : $conexion->id_parq;

        $column_name = Array();

        if($pfv == 11){
            //PIQUERO
            $column_name = [
                'Fecha',
                'Combiner Box Nro',
                'Potencia',           
                'Cte String 1',
                'Cte String 2',
                'Cte String 3',
                'Cte String 4',
                'Cte String 5',
                'Cte String 6',
                'Cte String 7',
                'Cte String 8',
                'Cte String 9',
                'Cte String 10',
                'Cte String 11',
                'Cte String 12',
                'Cte String 13',
                'Cte String 14',
                'Cte String 15',
                'Cte String 16',
                'Cte String 17',
                'Cte String 18',
                'Cte String 19',
                'Cte String 20',
                'Cte String 21',
                'Cte String 22',
                'Cte String 23',
                'Cte String 24',
            ];
        }

        return $column_name;
    }
}