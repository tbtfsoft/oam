<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Helpers\TimeHelper;
use App\Http\Controllers\Controller;
use App\Models\Conexion;
use App\Models\Barras;
use App\Models\BarrasEmpresas;
use App\Models\ModalidadCalculo;
use App\Models\ModalidadCalculoEmpresas;
use App\Models\FactorReferencia;
use App\Models\PrecioEstabilizado;
use App\Models\AssetManagement\Empresa;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection as Collection;

class ReportesMedidor implements FromCollection,WithHeadings
{
    protected $request;
    
    public function __construct($request)
    {
        ini_set('memory_limit', '-1');

        set_time_limit(500);

        $this->request = $request;
    }

    public function collection()
    {
        $empresa_id = $this->request->empresa_xls;
		$reporte_id = $this->request->id_reporte;
		$conexion = Conexion::where('id_empresa', $empresa_id)->first();
        $host = $conexion == null ? 0 : $conexion->host;
        $pfv = $conexion == null ? 0 : $conexion->id_parq;
        $medidor = $conexion == null ? 0 : $conexion->id_medidor;
        $radsensor = $conexion == null ? 0 : $conexion->id_radsensor;
        $database_name = $conexion == null ? 0 : $conexion->db;
		$subtitulo = $conexion == null ? 'no existe conexion' : $conexion->nombre_parq;			
		$dateRange = explode("-", $this->request->dates);
		$inicio = TimeHelper::formatDateRange($dateRange[0]);
        $fin = TimeHelper::formatDateRange($dateRange[1]);

        $data = Array();
		$inversor = Array();
		$potencia = Array();
        $dataRaw = Array();
		$horasRaw = Array();

        //DB CONNECTION
        if($host == 0){
            $host = config('app.host_3');
            $port = config('app.host_port_3');
            $password = config('app.host_password_3');
            $database_name = 'PRMTE';
            $medidor = $conexion == null ? 0 : $conexion->id_medidor_prmte;
        }else if($host == 1){
            $host = config('app.host_1');
            $port = config('app.host_port_1');
            $password = config('app.host_password_1');
        }else if($host == 2){

        }else{
            $host = 0;
            $port = 0;
            $password = 0;            
        }

        if($host != 0){
            //INICIO DE LA CONSULTA
            $strConnect = "host = ".$host." port = ".$port." dbname = '".$database_name."' user = 'postgres' password = ".$password;
            $con = pg_connect($strConnect);

            //DB QUERY STRING
            $strQuery = "SELECT fch_dato, id_parq, id_disp,volt_a, volt_b, volt_c, volt_prom, vlin_ab, vlin_bc, vlin_ca, vlin_prom, pot_act_a, pot_act_b, pot_act_c, pot_act_total, pot_rec_a, pot_rec_b, pot_rec_c, pot_rec_total, pot_ap_a, pot_ap_b, pot_ap_c, pot_ap_total, fac_pot_a, fac_pot_b, fac_pot_c, fac_pot_total, cte_a, cte_b, cte_c, cte_prom, ener_in, ener_out, ener_rec_in, ener_rec_out, frec_valor
            FROM tbl_datos_medidor	
            WHERE id_disp = $medidor
            AND fch_dato BETWEEN '$inicio' AND '$fin'		
            ORDER  BY fch_dato ASC";
            
            //DB QUERY
            $resRAW = pg_query($con,$strQuery);		

            while($dataFetch = pg_fetch_assoc($resRAW))
            {
                $dataRaw[] = $dataFetch;
            }
        }

        return collect($dataRaw);
    }

    public function headings(): array
    {
        $empresa_id = $this->request->empresa_xls;
		$conexion = Conexion::where('id_empresa', $empresa_id)->first();
        $pfv = $conexion == null ? 0 : $conexion->id_parq;

        $column_name = Array();

        $column_name = [
            'Fecha',
            'ID PFV',
            'ID Medidor',
            'Voltaje A',
            'Voltaje B',           
            'Voltaje C',
            'Voltaje Promedio',
            'Voltaje Linea AB',
            'Voltaje Linea BC',
            'Voltaje Linea CA',
            'Voltaje Linea Promedio',
            'Potencia Activa A',
            'Potencia Activa B',
            'Potencia Activa C',
            'Potencia Activa Total',
            'Potencia Reactiva A',
            'Potencia Reactiva B',
            'Potencia Reactiva C',
            'Potencia Reactiva Total',
            'Potencia Aparente A',
            'Potencia Aparente B',
            'Potencia Aparente C',
            'Potencia Aparente Total',
            'Factor de Potencia A',
            'Factor de Potencia B',
            'Factor de Potencia C',
            'Factor de Potencia Total',
            'Corriente A',
            'Corriente B',
            'Corriente C',
            'Corriente Promedio',
            'Energia Consumida',
            'Energia Inyectada',
            'Energia Reactiva Consumida',
            'Energia Reactiva Inyectada',
            'Frecuencia',            
        ];

        return $column_name;
    }
}