<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Helpers\TimeHelper;
use App\Http\Controllers\Controller;
use App\Models\Conexion;
use App\Models\Barras;
use App\Models\BarrasEmpresas;
use App\Models\ModalidadCalculo;
use App\Models\ModalidadCalculoEmpresas;
use App\Models\FactorReferencia;
use App\Models\PrecioEstabilizado;
use App\Models\AssetManagement\Empresa;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection as Collection;

class ReportesCalidad implements FromCollection,WithHeadings
{
    protected $request;
    
    public function __construct($request)
    {
        ini_set('memory_limit', '-1');

        set_time_limit(500);

        $this->request = $request;
    }

    public function collection()
    {
        $empresa_id = $this->request->empresa_xls;
		$reporte_id = $this->request->id_reporte;
		$conexion = Conexion::where('id_empresa', $empresa_id)->first();
        $host = $conexion == null ? 0 : $conexion->host;
        $pfv = $conexion == null ? 0 : $conexion->id_parq;
        $medidor = $conexion == null ? 0 : $conexion->id_medidor;
        $radsensor = $conexion == null ? 0 : $conexion->id_radsensor;
        $database_name = $conexion == null ? 0 : $conexion->db;
		$subtitulo = $conexion == null ? 'no existe conexion' : $conexion->nombre_parq;			
		$dateRange = explode("-", $this->request->dates);
		$inicio = TimeHelper::formatDateRange($dateRange[0]);
        $fin = TimeHelper::formatDateRange($dateRange[1]);

        $data = Array();
		$inversor = Array();
		$potencia = Array();
        $dataRaw = Array();
		$horasRaw = Array();

        //DB CONNECTION
        if($host == 1){
            $host = config('app.host_1');
            $port = config('app.host_port_1');
            $password = config('app.host_password_1');
        }else if($host == 2){

        }else{
            $host = 0;
            $port = 0;
            $password = 0;            
        }

        if($host != 0){
            //INICIO DE LA CONSULTA
            $strConnect = "host = ".$host." port = ".$port." dbname = '".$database_name."' user = 'postgres' password = ".$password;
            $con = pg_connect($strConnect);

            //DB QUERY STRING
            $strQuery = "SELECT fch_dato, id_parq, id_disp_med, id_disp_rad, id_disp_temp, temp_panel, rad_valor, rad_avg, pot_act_total, ener_out, ener_delta, coef_temp_panel, valor_pr, pp_rad_real, pp_rad_avg, pr_rad_real, pr_rad_avg, pp2_rad_real, pp2_rad_avg, pr2_rad_real, pr2_rad_avg 
            FROM tbl_pr_calculos	
            WHERE id_disp_med = $medidor
            AND fch_dato BETWEEN '$inicio' AND '$fin'		
            ORDER  BY fch_dato ASC";
            
            //DB QUERY
            $resRAW = pg_query($con,$strQuery);		

            while($dataFetch = pg_fetch_assoc($resRAW))
            {
                $dataRaw[] = $dataFetch;
            }
        }

        return collect($dataRaw);
    }

    public function headings(): array
    {
        $empresa_id = $this->request->empresa_xls;
		$conexion = Conexion::where('id_empresa', $empresa_id)->first();
        $pfv = $conexion == null ? 0 : $conexion->id_parq;

        $column_name = Array();

        $column_name = [
            'Fecha',
            'ID PFV',
            'ID MEDIDOR',
            'ID DISP RADIACION',
            'ID DISP TEMP',
            'Temperatura Panel',
            'Radiacion',           
            'Radiacion Promedio',
            'Potencia Activa Total',
            'Energia Inyectada',
            'Energia Delta',
            'Coef Temperatura Panel',
            'PR',
            'PP RAD REAL',
            'PP RAD AVG',
            'PR RAD REAL',
            'PR RAD AVG',
            'PP2 RAD REAL',
            'PP2 RAD AVG',
            'PR2 RAD REAL',
            'PR2 RAD AVG'           
        ];

        return $column_name;
    }
}