<?php 

namespace App\Injections;

use Exception;
use Illuminate\Http\Request;
use App\Models\AssetManagement\Empresa;
use App\Models\Permission;
use App\Models\Barras;
use App\Models\ModalidadCalculo;

class PermissionTrait {

	/**
	 * Obtiene todo los modulo de los permisos. Creando un Array donde el key es el nombre del Modulo
	 * @return Array 	 [name_module_1 => name_module_1, name_module_2 => name_module_2.. name_module_n => name_module_n]
	 * @author Carlos Anselmi <Carlos Anselmi>
	 */
	public function modules ()
	{
		$permissions = Permission::select('module')->where('module','!=',null)->groupBy('module')->get();

		return array_reduce($permissions->toArray(), function ($result, $item) {
	    $result[$item['module']] = $item['module'];
	    return $result;
		}, array());
	}

	public function empresas ()
	{
    $empresas =  Empresa::select(['id', 'nombre_xls'])->get();

		return array_reduce($empresas->toArray(), function ($result, $item) {
	    $result[$item['id']] = $item['nombre_xls'];
	    return $result;
		}, array());
	}

	public function barras ()
	{
		$barras = Barras::select(['id', 'barra_xls'])->get();

		return array_reduce($barras->toArray(), function ($result, $item) {
	    $result[$item['id']] = $item['barra_xls'];
	    return $result;
		}, array());
	}

	public function modalidadCalculos ()
	{
		$rows = ModalidadCalculo::select(['id', 'nombre'])->get();

		return array_reduce($rows->toArray(), function ($result, $item) {
	    $result[$item['id']] = $item['nombre'];
	    return $result;
		}, array());
	}

}