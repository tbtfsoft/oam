<?php 

namespace App\Injections;

use Exception;
use Illuminate\Http\Request;
use App\Models\AssetManagement\Empresa;
use App\Models\Permission;
use App\Models\Barras;
use App\Models\ModalidadCalculo;

class CorrespondenciaTrait {


	protected $statuses;

  public function __construct()
  {
  	$this->statuses = $this->getStatuses();
	}

	/**
	 * @author Carlos Anselmi <Carlos Anselmi>
	 */
	public function nextStatus ($status)
	{
		return $status < count($this->statuses) ? (Object)$this->statuses[$status + 1] : (Object)$this->statuses[$status];
	}

	/**
	 * Retorna todos los estados de la correspondencia.
	 * @return Array
	 * @author Carlos Anselmi <Carlos Anselmi>
	 */
  public function getStatuses ()
  {
    return [
      [
        'label' => 'Abierto',
        'class' => 'status-open'
      ],
      [
        'label'  => 'Acuse recibo cliente',
        'action' => 'Acuse recibo cliente',
        'class'  => 'status-acuse'
      ],
      [
        'label'  => 'cerrado',
        'action' => 'Cerrar',
        'class'  => 'status-close'
      ],
    ];
  }

  public function getStatus ($status)
  {
		return (Object)$this->statuses[$status];
  }
}