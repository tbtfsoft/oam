<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TBL_DatosMedidor extends Model
{
	
	protected $connection = 'PARKS2';

  protected $table = 'tbl_datos_medidor';

  protected $fillable = [
		'fch_dato',
		'ener_in',
		'ener_out'
  ];
}