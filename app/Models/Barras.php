<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use IzyTech\Validator\Contracts\ValidatorInterface;

class Barras extends Model
{

  protected $table = 'barras';
  
  protected $fillable = [
    'barra_xls',
    'barra_human'
  ];

  protected $datatable = [
    'barra_xls',
    'barra_human'
  ];

  protected $rules = [
    ValidatorInterface::RULE_CREATE => [
      'barra_xls'    =>   'required',
      'barra_human'  =>   'required'
    ],

    ValidatorInterface::RULE_UPDATE => [
      'barra_xls'    =>   'required',
      'barra_human'  =>   'required'
    ] 
  ];

  /**
   * Retorna la tabla del modelos
   * @return String tabla
   */
	static function getTableModel()
	{
		return (new static)->getTable();
	}

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return $this->rules;
  }

  public function getfillTable()
  {
    return $this->datatable;
  }
}