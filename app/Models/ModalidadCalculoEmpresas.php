<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use IzyTech\Validator\Contracts\ValidatorInterface;

class ModalidadCalculoEmpresas extends Model
{

  protected $table = 'modalidad_calculo_empresas';

  protected $fillable = [
    'id_modalidad_calculo',
    'id_empresa',
    'inicial_date',
    'final_date'
  ];

  protected $rules = [
    ValidatorInterface::RULE_CREATE => [
      'id_modalidad_calculo' =>  'required',
      'id_empresa' =>  'required',
      'inicial_date' =>  'required',
      'final_date' =>  'required'
    ],

    ValidatorInterface::RULE_UPDATE => [
      'id_modalidad_calculo' =>  'required',
      'id_empresa' =>  'required',
      'inicial_date' =>  'required',
      'final_date' =>  'required'
    ] 
  ];

  /**
   * Retorna la tabla del modelos
   * @return String tabla
   */
  static function getTableModel()
  {
    return (new static)->getTable();
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return $this->rules;
  }

  public function getfillTable()
  {
    return $this->datatable;
  }

   /**
   * Set inicial_date & final_date
   *
   * @param  string  $value
   * @return void
   */
  public function setDatesAttribute($value)
  {
    $dates = explode(" - ", $value);

    $this->attributes['inicial_date'] = $dates[0];

    $this->attributes['final_date'] = $dates[1];
  }
}