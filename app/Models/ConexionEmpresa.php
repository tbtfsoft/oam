<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConexionEmpresa extends Model
{

  protected $table = 'conexion-empresa';
  
  protected $fillable = [
  	'id',
  	'empresa_id',
  	'conexion_id'
  ];

  /**
   * Retorna la tabla del modelos
   * @return String tabla
   */
	static function getTableModel()
	{
		return (new static)->getTable();
	}
}
