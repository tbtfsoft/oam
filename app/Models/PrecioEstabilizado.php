<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use IzyTech\Validator\Contracts\ValidatorInterface;

class PrecioEstabilizado extends Model
{
  protected $table = 'precio_estabilizado';
  
  protected $appends = ['barra'];

  protected $fillable = [
    'id_barra',
    'precio_energia',
    'precio_potencia',
    'inicial_date',
    'final_date'
  ];

  protected $rules = [
    ValidatorInterface::RULE_CREATE => [
      'id_barra' => 'required',
      'precio_energia' => 'required',
      'precio_potencia' => 'required',
      'inicial_date' => 'required',
      'final_date' => 'required'
    ],
    ValidatorInterface::RULE_UPDATE => [
      'id_barra' => 'required',
      'precio_energia' => 'required',
      'precio_potencia' => 'required',
      'inicial_date' => 'required',
      'final_date' => 'required'
    ] 
  ];

  /**
   * Retorna la tabla del modelos
   * @return String tabla
   */
	static function getTableModel()
	{
		return (new static)->getTable();
	}

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return $this->rules;
  }
  
  public function getBarraAttribute()
  {
    $barra = \App\Models\Barras::where('id', $this->id_barra)->first();
    return $barra ? $barra->barra_human : 'N/D';
  }

   /**
   * Set inicial_date & final_date
   *
   * @param  string  $value
   * @return void
   */
  public function setDatesAttribute($value)
  {
    $dates = explode(" - ", $value);

    $this->attributes['inicial_date'] = $dates[0];

    $this->attributes['final_date'] = $dates[1];
  }
}