<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModalidadCalculo extends Model
{

  protected $table = 'modalidad_calculo';
  
  protected $fillable = [
    'nombre',
    'tipo_calculo'
  ];

  /**
   * Retorna la tabla del modelos
   * @return String tabla
   */
	static function getTableModel()
	{
		return (new static)->getTable();
	}
}