<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FactorReferencia extends Model
{

  protected $table = 'factor_referencia';
  
  protected $fillable = [
		'id_empresa',
		'id_user',
		'id_tipo_doc_cen',
		'hora',
		'dia',
		'fr_generado',
		'fr_consumido',
		'periodo'
  ];

  /**
   * Retorna la tabla del modelos
   * @return String tabla
   */
	static function getTableModel()
	{
		return (new static)->getTable();
	}
}
