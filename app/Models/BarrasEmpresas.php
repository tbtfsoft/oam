<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use IzyTech\Validator\Contracts\ValidatorInterface;
use \App\Models\AssetManagement\Empresa;
use \App\Models\Barras;

class BarrasEmpresas extends Model
{
  protected $table = 'barras_empresas';
  
  protected $appends = ['barra', 'empresa'];

  protected $fillable = [
    'id_barras',
    'id_empresa',
    'tipo_calculo',
    'inicial_date',
    'final_date'
  ];

  protected $datatable = [
    'id_barras',
    'empresa',
    'tipo_calculo',
    'inicial_date',
    'final_date'
  ];

  protected $rules = [
    ValidatorInterface::RULE_CREATE => [
      'id_barras'     =>   'required',
      'id_empresa'    =>   'required',
      'tipo_calculo'  =>   'required',
      'inicial_date'  =>   'required',
      'final_date'    =>   'required'
    ],

    ValidatorInterface::RULE_UPDATE => [
      'id_barras'     =>   'required',
      'id_empresa'    =>   'required',
      'tipo_calculo'  =>   'required',
      'inicial_date'  =>   'required',
      'final_date'    =>   'required'
    ] 
  ];

  /**
   * Retorna la tabla del modelos
   * @return String tabla
   */
  static function getTableModel()
  {
    return (new static)->getTable();
  }

 /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return $this->rules;
  }

  public function getfillTable()
  {
    return $this->datatable;
  }

  public function getBarraAttribute()
  {
    $e = Barras::findOrFail($this->id_barras);
    return $e ? $e->barra_human : 'N/D';
  }

  public function getEmpresaAttribute()
  {
    $e = Empresa::findOrFail($this->id_empresa);
    return $e ? $e->nombre_xls : 'N/D';
  }

   /**
   * Set inicial_date & final_date
   *
   * @param  string  $value
   * @return void
   */
  public function setDatesAttribute($value)
  {
    $dates = explode(" - ", $value);

    $this->attributes['inicial_date'] = $dates[0];

    $this->attributes['final_date'] = $dates[1];
  }
}