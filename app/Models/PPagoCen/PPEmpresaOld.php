<?php

namespace App\Models\PPagoCen;

use Illuminate\Database\Eloquent\Model;

class PPEmpresaOld extends Model
{

  protected $table = 'pp_empresas_old';

	protected $fillable = [
		'id',
		'name',
		'rut',
		'verification_code',
		'business_name',
		'commercial_business',
		'dte_reception_email',
		'bank_account',
		'bank',
		'bank_id',
		'commercial_address',
		'postal_address',
		'manager',
		'payments_contact_first_name',
		'payments_contact_last_name',
		'payments_contact_address',
		'payments_contact_phones',
		'payments_contact_email',
		'bills_contact_first_name',
		'bills_contact_last_name',
		'bills_contact_address',
		'bills_contact_phones',
		'bills_contact_email'
	];
	
}