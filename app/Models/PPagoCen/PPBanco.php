<?php

namespace App\Models\PPagoCen;

use Illuminate\Database\Eloquent\Model;

class PPBanco extends Model
{

  protected $table = 'pp_bancos';

	protected $fillable = [
		'id',
		'code',
		'name',
		'sbif',
		'type'
	];
	
}
