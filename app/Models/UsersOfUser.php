<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersOfUser extends Model
{
    protected $table = 'users_of_users';

    protected $fillable = [
			'id',
			'parent_id',
			'child_id'
    ];

    function parent()
    {
			return $this->belongsTo('App\User');
    }
}
