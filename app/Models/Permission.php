<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use IzyTech\Validator\Contracts\ValidatorInterface;

class Permission extends Model
{

  protected $fillable = [
  	'name',
  	'module',
  	'description',
  	'display_name',
  	'guard_name',
  	'description'
  ];

  protected $datatable = [
		'name',
  	'display_name',
		'module',
  	'description'
  ];

  protected $rules = [
    ValidatorInterface::RULE_CREATE => [
      'display_name' =>  'required',
      'description' =>  'required',
      'name' =>  'required',
      'module' =>  'required'
    ],

    ValidatorInterface::RULE_UPDATE => [
      'display_name' =>  'required',
      'description' =>  'required',
      'name' =>  'required',
      'module' =>  'required'
    ] 
  ];
  /**
   * Retorna la tabla del modelos
   * @return String tabla
   */
  static function getTableModel()
  {
    return (new static)->getTable();
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return $this->rules;
  }

  public function getfillTable()
  {
    return $this->datatable;
  }
}
