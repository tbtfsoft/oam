<?php

namespace App\Models\AssetManagement;

use Illuminate\Database\Eloquent\Model;

class EnergiaMedidoresPFV extends Model
{
	
  protected $table = 'energia_medidores_pfv';

  protected $fillable = [
		'id',
		'id_empresa',
		'id_user',
		'periodo',
		'kwh_in',
		'kwh_out',
		'medidor_date',
		'created_at',
		'updated_at'
  ];
}
