<?php

namespace App\Models\AssetManagement;

use Illuminate\Database\Eloquent\Model;

class TipoDocCen extends Model
{
	
  protected $table = 'tipo_doc_cen';

  protected $fillable = [
  	'nombre',
  	'deleted_at'
  ];

  public function getNombreAttribute($nombre) {
  	return trans('app.' . strtolower($nombre));
  }
}
