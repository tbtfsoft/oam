<?php

namespace App\Models\AssetManagement;

use App\Models\AssetManagement\ModelMensajeria;

class InformesOM extends ModelMensajeria
{
  protected $table = 'om_informes';
}
