<?php

namespace App\Models\AssetManagement;

use Illuminate\Database\Eloquent\Model;

class EmpresaIntegracion extends Model
{
	
  protected $table = "empresa_integracion";

  protected $fillable = [
  	'empresa_id',
  	'tipo_integracion_id',
  	'deleted_at'
  ];
}
