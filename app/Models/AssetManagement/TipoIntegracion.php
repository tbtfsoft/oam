<?php

namespace App\Models\AssetManagement;

use Illuminate\Database\Eloquent\Model;

class TipoIntegracion extends Model
{

  protected $table = 'tipo_integracion';

  protected $fillable = [
  	'nombre',
  	'deleted_at'
  ];
}
