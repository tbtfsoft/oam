<?php

namespace App\Models\AssetManagement;

use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use App\Models\Conexion as conexion;
use App\Models\Dispositivo as dispositivo;
use IzyTech\Validator\Contracts\ValidatorInterface;

class Empresa extends Model
{
    use HasSlug;

    protected $fillable = [
        'id',
        'rut',
        'slug',
        'nombre_xls',
        'label',
        'order',
        'potencia_instalada',
        'coordenadas_lat',
        'coordenadas_lng',
        'url_gmaps',
        'fecha_inicio'
        // 'razon_social',
        // 'giro',
        // 'acteco',
        // 'cdgsiisucur',
        // 'direccion',
        // 'comuna',
        // 'ciudad'
    ];

    protected $datatable = [
        'rut',
        'nombre_xls',
        'label',
        'order',
        'potencia_instalada',
        'coordenadas_lat',
        'coordenadas_lng',
        'url_gmaps',
        'fecha_inicio'
    ];

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'rut' => 'required',
            'nombre_xls' => 'min:3|unique:empresas',
            'order' => 'required',
            'fecha_inicio' => 'required'
        ],

        ValidatorInterface::RULE_UPDATE => [
            'rut' => 'required',
            'nombre_xls' => 'min:3|unique:empresas, nombre_xls,id,:id',
            'order' => 'required',
            'fecha_inicio' => 'required'
        ]
    ];

    /**
    * Get the validation rules that apply to the request.
    *
    * @return array
    */
    public function rules()
    {
        return $this->rules;
    }

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('nombre_xls')
            ->saveSlugsTo('slug');
    }

    public function __construct(array $empresa = [])
    {
        parent::__construct($empresa);

        static::retrieved(function ($empresa) {
            $empresa->conexion;
            $empresa->dispositivo;
        });
    }

    public function users ()
    {
        return $this->belongsToMany('App\User','empresas_users', 'empresa_id', 'user_id')
                    ->withTimestamps();
    }

}
