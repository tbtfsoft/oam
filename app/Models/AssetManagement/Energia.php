<?php

namespace App\Models\AssetManagement;

use Illuminate\Database\Eloquent\Model;

class Energia extends Model
{
	
  protected $table = 'balance_economico_energia_cen';
  
  protected $fillable = [
  	'id_empresa_deudora',
  	'id_empresa_acreedora',
  	'id_tipo_doc_cen',
  	'costo',
  	'periodo'
  ];

  /**
   * Retorna la tabla del modelos
   * @return String tabla
   */
	static function getTableModel()
	{
		return (new static)->getTable();
	}
}
