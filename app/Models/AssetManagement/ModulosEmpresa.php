<?php

namespace App\Models\AssetManagement;

use App\Models\AssetManagement\Empresa;
use Illuminate\Database\Eloquent\Model;
use IzyTech\Validator\Contracts\ValidatorInterface;

class ModulosEmpresa extends Model
{
	
  protected $tabla = 'modulos_empresas';

  public $timestamps = false;

  protected $fillable = [
    'modulo',
		'id_user',
    'id_empresa',
		'id_modulo'
  ];

  protected $datatable = [
    'modulo',
		'id_user',
    'id_empresa',
    'id_modulo'
  ];

  /**
   * Retorna la tabla del modelos
   * @return String tabla
   */
	static function getTableModel()
	{
		return (new static)->getTable();
	}

  public function user()
  {
    return $this->hasOne('App\User');
  }

  public function empresa()
  {
    return $this->hasOne('App\Models\AssetManagement\Empresa', 'id', 'id_empresa');
  }

  public function getEmpresaXlsAttribute ()
  {
    return $this->empresa->nombre_xls;
  }
  
  public function correspondencia() 
  {
    return $this->belongsToMany('App\Models\AssetManagement\Correspondencia')->withTimestamps();
  }

  public function informe() 
  {
    return $this->belongsToMany('App\Models\AssetManagement\Informe')->withTimestamps();
  }

  public function incidencia() 
  {
    return $this->belongsToMany('App\Models\AssetManagement\Incidencia')->withTimestamps();
  }

  public function informeOM() 
  {
    return $this->belongsToMany('App\Models\AssetManagement\InformeOM')->withTimestamps();
  }
}