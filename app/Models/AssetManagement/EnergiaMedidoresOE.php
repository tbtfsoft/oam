<?php

namespace App\Models\AssetManagement;

use Illuminate\Database\Eloquent\Model;

class EnergiaMedidoresOE extends Model
{
	
  protected $table = 'energia_medidores_oe';

  protected $fillable = [
		'id_empresa',
		'id_user',
		'periodo',
		'hora',
		'kwh_in',
		'kwh_out'
  ];
}
