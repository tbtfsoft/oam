<?php

namespace App\Models\AssetManagement;

use App\Models\AssetManagement\Empresa;
use Illuminate\Database\Eloquent\Model;
use IzyTech\Validator\Contracts\ValidatorInterface;

class ModulosStatus extends Model
{
    protected $table = 'modulos_status';

    protected $fillable = [
        'modulo',
	    'id_user',
	    'id_modulo',
        'description',
        'status'
    ];

    protected $datatable = [
        'modulo',
	    'id_user',
        'id_modulo',
        'description',
        'status'
    ];

    /**
    * Retorna la tabla del modelos
    * @return String tabla
   */
    public function user()
    {
      return $this->hasOne('App\User', 'id', 'id_user');
    }

    public function correspondencia() 
    {
      return $this->belongsToMany('App\Models\AssetManagement\Correspondencia')->withTimestamps();
    }
  
    public function informe() 
    {
      return $this->belongsToMany('App\Models\AssetManagement\Informe')->withTimestamps();
    }
  
    public function incidencia() 
    {
      return $this->belongsToMany('App\Models\AssetManagement\Incidencia')->withTimestamps();
    }
  
    public function informeOM() 
    {
      return $this->belongsToMany('App\Models\AssetManagement\InformeOM')->withTimestamps();
    }
}