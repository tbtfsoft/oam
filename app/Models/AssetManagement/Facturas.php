<?php

namespace App\Models\AssetManagement;

use Illuminate\Database\Eloquent\Model;

class Facturas extends Model
{
	
  protected $table = 'facturas';
  
  protected $fillable = [
    'id',
    'periodo',
    'id_empresa_deudora',
    'id_empresa_acreedora',
    'id_modulo',
    'id_tipo_dte',
    'folio',
    'fecha_emision',
    'fecha_aceptacion',
    'fecha_aceptacion_ts',
    'fecha_vencimiento',
    'forma_pago',
    'monto_neto',
    'monto_bruto',
    'iva',
    'pago_codigo',
    'pago_fecha',
    'created_at',
    'updated_at'
  ];

  /**
   * Retorna la tabla del modelos
   * @return String tabla
   */
	static function getTableModel()
	{
		return (new static)->getTable();
  }
  
  public function empresas() 
  {
      return $this->belongsToMany('App\Models\AssetManagement\Empresa');
  }

  public function getPagoAttribute()
  {
    return $this->pago_fecha;
  }

  /**
   * Get columns.
   *
   * @return array
   */
  static function getColumns()
  {
    return [
      ['title' => 'id', 'data' => 'id'],
      ['title' => 'periodo', 'data' => 'periodo'],
      ['title' => 'deudora', 'data' => 'id_empresa_deudora'],
      ['title' => 'acreedora', 'data' => 'id_empresa_acreedora'],
      ['title' => 'tipo_dte', 'data' => 'id_tipo_dte'],
      ['title' => 'aceptacion', 'data' => 'fecha_aceptacion'],
      ['title' => 'aceptacion_ts', 'data' => 'fecha_aceptacion_ts'],
      ['title' => 'fecha_vencimiento', 'data' => 'fecha_vencimiento'],
      ['title' => 'forma_pago', 'data' => 'forma_pago'],
      ['title' => 'monto_neto', 'data' => 'monto_neto'],
      ['title' => 'monto_bruto', 'data' => 'monto_bruto'],
      ['title' => 'iva','data' => 'iva'],
      ['title' => 'pago_fecha', 'data' => 'pago_fecha'],
      ['title' => 'created_at', 'data' => 'created_at'],
      ['title' => 'updated_at', 'data' => 'updated_at']
    ];
  }
}
