<?php

namespace App\Models\AssetManagement;

use Illuminate\Database\Eloquent\Model;
use IzyTech\Validator\Contracts\ValidatorInterface;

class ComentariosEmpresa extends Model
{
	
  protected $tabla = 'comentarios_empresas';
  
  public $timestamps = false;

  protected $fillable = [
		'id_empresa',
		'id_comentario'
  ];

  protected $datatable = [
    'id_empresa',
    'id_comentario'
  ];
  
  protected $rules = [
    ValidatorInterface::RULE_CREATE => [
      'id_empresa' => 'required',
      'id_comentario' => 'required'
    ],

    ValidatorInterface::RULE_UPDATE => [
      'id_empresa' => 'required',
      'id_comentario' => 'required'
    ] 
  ];

  /**
  * Get the validation rules that apply to the request.
  *
  * @return array
  */
  public function rules()
  {
    return $this->rules;
  }
  /**
   * Retorna la tabla del modelos
   * @return String tabla
   */
	static function getTableModel()
	{
		return (new static)->getTable();
	}

  public function empresa ()
  {
    return $this->hasOne('App\Models\AssetManagement\Empresa', 'id', 'id_empresa');
  }

  public function comentario ()
  {
    return $this->belongsTo('App\Models\AssetManagement\ModulosComentario', 'id_comentario', 'id');
  }
}
