<?php

namespace App\Models\AssetManagement;

use Illuminate\Database\Eloquent\Model;
use IzyTech\Validator\Contracts\ValidatorInterface;
use Storage;

abstract class ModelMensajeria extends Model
{

  protected $model;

  protected $fillable = [
		'titulo',
		'descripcion',
		'fecha',
    'status',
    'id_user'
  ];

  protected $datatable = [
		'titulo',
		'descripcion',
		'fecha',
		'id_user'
  ];  
  
  protected $rules = [
    ValidatorInterface::RULE_CREATE => [
			'titulo' => 'required',
			'descripcion' => 'required|min:3',
			'fecha' => 'required',
			'id_user' => 'required'
    ],

    ValidatorInterface::RULE_UPDATE => [
			'titulo' => 'required',
			'descripcion' => 'required|min:3',
			'fecha' => 'required',
			'id_user' => 'required'
    ] 
  ];

  /**
  * Get the validation rules that apply to the request.
  *
  * @return array
  */
  public function rules()
  {
    return $this->rules;
  }

  /**
   * Retorna la tabla del modelos
   * @return String tabla
   */
	static function getTableModel()
	{
		return (new static)->getTable();
  }
  
  /**
   * Obtenga el Usuario asociados con la Correspondencia.
   *
   * @return  Collect
   */
  public function user()
  {
    return $this->hasOne('App\User', 'id', 'id_user');
  }

  /**
   * Obtiene los IDs de las empresas asociados a la Correspondencia.
   *
   * @return  Array
   */
  public function getEmpresasIdsAttribute()
  {
    $empresas = [];
    foreach ($this->empresas as $key =>  $e) {
      array_push($empresas, $e->empresa->id);
    }
    return $empresas;
  }

   /**
   * Obtenga la fecha formateada.
   *
   * @param  string  $value
   * @return void
   */
  public function getFechaAttribute ($value)
  {
    return date('d/m/Y', strtotime($value));
  }


  /**
   * Se obtiene todos los archivos de Modelo (Correspondencia) mediante el su $id.
   *
   * @return Collect
   */
  public function getFilesAttribute ()
  {
    $files = Storage::disk($this->getTableModel())->allFiles($this->id);

    return collect($files)->map(function ($file) {
      return [
        'url'      => Storage::url($this->getTableModel() .'/'. $file),
        'delete'   => route('message-files.delete', [$this->getTableModel(), $this->id, basename($file)]),
        'type'     => pathinfo(basename($file), PATHINFO_EXTENSION),
        'basename' => basename($file)
      ];
    });
  }

  /**
   * Obtenga los Estados posibles para la Correspondencia.
   *
   * @return  Array
   */
  public function getEstadosAttribute ()
  {
    return [
      [
        'label' => 'Abierto',
        'class'  => 'status-open'
      ],
      [
        'label' => 'Acuse recibo cliente',
        'class'  => 'status-acuse'
      ],
      [
        'label' => 'cerrado',
        'class'  => 'status-close'
      ],
    ];
  }

  public function getEstadoAttribute ()
  {
    return array_key_exists($this->status, $this->estados) ? (object)$this->estados[$this->status] : (object)$this->estados[0];
  }

  /**
   * Obtenga las Empresas asociadas.
   *
   * @return  Collect
   */
  public function empresas ()
  {
    return $this->hasMany(ModulosEmpresa::class, 'id_modulo', 'id')->where('modulo', $this->getTableModel());
  }

  /**
   * Obtenga los Comentarios asociados.
   *
   * @return  Collect
   */
  public function comentarios ()
  {
    return $this->hasMany(ModulosComentario::class, 'id_modulo', 'id')->where('modulo', $this->getTableModel());
  }

  public function informes() 
  {
    return $this->hasMany('App\Models\AssetManagement\Informe','id_modulo', 'id')->where('modulo', $this->getTableModel());
  }

  public function incidencias() 
  {
    return $this->hasMany('App\Models\AssetManagement\Incidencia','id_modulo', 'id')->where('modulo', $this->getTableModel());
  }

  public function informeOMs() 
  {
    return $this->hasMany('App\Models\AssetManagement\InformeOM','id_modulo', 'id')->where('modulo', $this->getTableModel());
  }
  
  /**
   * Obtenga los status asociados.
   *
   * @return  Collect
   */
  public function status ()
  {
    return $this->hasMany('App\Models\AssetManagement\ModulosStatus', 'id_modulo', 'id')->where('modulo', $this->getTableModel());
  }

  public function isClose ()
  {
    $idClose = $this->status()->where('status', 2);
    return  $idClose ? $idClose->first() : null;
  }

  public function getStatusModel ($id_user)
  {
    if ($this->isClose()) {
      return $this->isClose();
    }
    
    $statusUser = $this->getStatusModelUser($id_user);

    return $statusUser ? $statusUser : $this->status()->where('status', 0)->first();
  }
  
  public function getStatusModelUser ($id_user)
  {
    return $this->status()->where('id_user', $id_user)->first();
  }
  
  public function getLabelStatus ($index) 
  {
    return array_key_exists($index, $this->estados) ? (object)$this->estados[$index] : (object)$this->estados[0];
  }
}