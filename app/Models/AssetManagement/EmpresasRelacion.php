<?php

namespace App\Models\AssetManagement;

use Illuminate\Database\Eloquent\Model;

class EmpresasRelacion extends Model
{
	
  protected $table = 'empresas_relacion';
  
  protected $fillable = [
  	'id_empresa_padre',
  	'id_empresa_hija'
  ];

  /**
   * Retorna la tabla del modelos
   * @return String tabla
   */
	static function getTableModel()
	{
		return (new static)->getTable();
	}
}
