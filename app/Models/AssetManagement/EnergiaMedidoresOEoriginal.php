<?php

namespace App\Models\AssetManagement;

use Illuminate\Database\Eloquent\Model;

class EnergiaMedidoresOEoriginal extends Model
{
	
  protected $table = 'energia_medidores_oe_original';

	protected $fillable = [
		'id_empresa',
		'id_user',
		'periodo',
		'hora',
		'kwh_in',
		'kwh_out'
	];
}
