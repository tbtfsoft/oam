<?php

namespace App\Models\AssetManagement;

use Illuminate\Database\Eloquent\Model;
use IzyTech\Validator\Contracts\ValidatorInterface;
use App\Models\AssetManagement\ComentariosEmpresa;

class ModulosComentario extends Model
{
	
  protected $fillable = [
    'modulo',
    'id_user',
    'id_modulo',
		'comentario'
  ];

  protected $datatable = [
    'modulo',
    'id_user',
    'id_modulo',
		'comentario'
  ];
  
  /**
   * Retorna la tabla del modelos
   * @return String tabla
   */
	static function getTableModel()
	{
		return (new static)->getTable();
	}

  public function user()
  {
    return $this->hasOne('App\User', 'id', 'id_user');
  }

  public function correspondencia () 
  {
    return $this->hasOne('App\Models\AssetManagement\Correspondencia', 'id', 'id_modulo');
  }

  public function informe () 
  {
    return $this->hasOne('App\Models\AssetManagement\Informe', 'id', 'id_modulo');
  }

  public function informeOM () 
  {
    return $this->hasOne('App\Models\AssetManagement\InformesOM', 'id', 'id_modulo');
  }

  public function incidencia () 
  {
    return $this->hasOne('App\Models\AssetManagement\Incidencia', 'id', 'id_modulo');
  }

  public function empresas ()
  {
    return $this->hasMany(ComentariosEmpresa::class, 'id_comentario', 'id');
  }

  public function getEmpresasParent ()
  {
    switch ($this->modulo) {
      case 'correspondencias':
        return $this->correspondencia->empresas;
      break;
      case 'informes':
        return $this->informe->empresas;
      break;
      case 'incidencias':
        return $this->incidencia->empresas;
      break;
      case 'informeOM':
        return $this->informeOM->empresas;
      break;  
      default:
        return [];
        break;
    }
  }

  public function getEmpresasIdsAttribute()
  {
    $empresas = [];
    foreach ($this->empresas as $key =>  $e) {
      array_push($empresas, $e->empresa->id);
    }
    return $empresas;
  }
}
