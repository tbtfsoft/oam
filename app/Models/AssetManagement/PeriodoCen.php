<?php

namespace App\Models\AssetManagement;

use Illuminate\Database\Eloquent\Model;
use IzyTech\Validator\Contracts\ValidatorInterface;

class PeriodoCen extends Model
{
  protected $table = 'periodos_cen';
  
  public $timestamps = false;

  protected $fillable = [
    'periodo',
    'tipo_doc',
    'tipo_item',
    'glosa',
    'folio',
    'fecha'
  ];

  protected $rules = [
    ValidatorInterface::RULE_CREATE => [
      'periodo'     =>  'required',
      'tipo_doc'    =>  'required',
      'tipo_item'   =>  'required',
      'glosa'       =>  'required',
      'folio'       =>  'required',
      'fecha'       =>  'required'
    ],

    ValidatorInterface::RULE_UPDATE => [
      'periodo'     =>  'required',
      'tipo_doc'    =>  'required',
      'tipo_item'   =>  'required',
      'glosa'       =>  'required',
      'folio'       =>  'required',
      'fecha'       =>  'required'
    ] 
  ];

  /**
   * Retorna la tabla del modelos
   * @return String tabla
   */
  static function getTableModel()
  {
    return (new static)->getTable();
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return $this->rules;
  }
}
