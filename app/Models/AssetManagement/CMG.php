<?php

namespace App\Models\AssetManagement;

use Illuminate\Database\Eloquent\Model;

class CMG extends Model
{
	
  protected $table = 'cmg';
  
  protected $fillable = [
    'id',
    'id_empresa',
    'id_user',
    'id_tipo_doc_cen',
    'hora',
    'dia',
    'barra',
    'cmg_mills_kwh',
    'usd',
    'cmg_usd_kwh',
    'periodo',
    'created_at',
    'updated_at'
  ];

  /**
   * Retorna la tabla del modelos
   * @return String tabla
   */
	static function getTableModel()
	{
		return (new static)->getTable();
	}
}
