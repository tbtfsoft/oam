<?php

namespace App\Models\AssetManagement;

use Illuminate\Database\Eloquent\Model;

class EmpresasPadres extends Model
{
	
  protected $table = 'empresas_padres';
  
  protected $fillable = [
    'id',
  	'nombre',
  	'logo'
  ];

  /**
   * Retorna la tabla del modelos
   * @return String tabla
   */
	static function getTableModel()
	{
		return (new static)->getTable();
	}
}
