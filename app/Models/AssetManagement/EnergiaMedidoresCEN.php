<?php

namespace App\Models\AssetManagement;

use Illuminate\Database\Eloquent\Model;

class EnergiaMedidoresCEN extends Model
{
	
  protected $table = 'energia_medidores_cen';

  protected $fillable = [
		'id_empresa',
		'id_user',
		'id_tipo_doc_cen',
		'periodo',
		'dia',
		'hora',
		'kwh_in',
		'kwh_out'
  ];
}
