<?php

namespace App\Models\AssetManagement;

use Illuminate\Database\Eloquent\Model;

class FacturasRecibidas extends Model
{
	
  protected $table = 'facturas_recibidas';
  
  protected $fillable = [
    'id',
    'periodo',
    'id_empresa',
    'id_empresa_deudora',
    'id_empresa_acreedora',
    'id_tipo_dte',
    'folio',
    'fecha_emision',
    'fecha_aceptacion',
    'fecha_aceptacion_ts',
    'fecha_vencimiento',
    'forma_pago',
    'monto_neto',
    'monto_bruto',
    'iva',
    'created_at',
    'updated_at'
  ];

  /**
   * Retorna la tabla del modelos
   * @return String tabla
   */
	static function getTableModel()
	{
		return (new static)->getTable();
	}
}
