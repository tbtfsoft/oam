<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use IzyTech\Validator\Contracts\ValidatorInterface;
use App\Models\AssetManagement\Empresa;

class Dispositivo extends Model
{
  use HasSlug;

  protected $table = 'dispositivos';

  protected $fillable = [
		'id_disp',
    'tipo',
    'slug'
  ];

  protected $rules = [
    ValidatorInterface::RULE_CREATE => [
      'id_disp'         =>  'required|unique:dispositivos|min:1',
      'tipo'            =>  'required'
    ],

    ValidatorInterface::RULE_UPDATE => [
      'id_disp'         =>  'required|unique:dispositivos,id_disp,id,:id|min:1',
      'tipo'            =>  'required'
    ] 
 	];

  /**
   * Get the options for generating the slug.
   */
  public function getSlugOptions() : SlugOptions
  {
    return SlugOptions::create()
        ->generateSlugsFrom(['id_disp','tipo'])
        ->saveSlugsTo('slug');
  }

 /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return $this->rules;
  }
}