<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use IzyTech\Validator\Contracts\ValidatorInterface;
use App\Models\AssetManagement\Empresa;

class Conexion extends Model
{

  protected $table = 'conexiones';

  protected $appends = ['empresa'];

  protected $datatable = [
    'db',
    'empresa',
    'id_parq',
    'id_medidor',
    'id_radsensor',
    'host',
    'nombre_parq',
    'db_prmte',
    'id_parq_prmte',
    'id_medidor_prmte'
  ];

  protected $fillable = [
		'db',
    'id_empresa',
    'id_parq',
    'id_medidor',
    'id_radsensor',
    'host',
    'nombre_parq',
    'db_prmte',
    'id_parq_prmte',
    'id_medidor_prmte',
  ];

  protected $rules = [
    ValidatorInterface::RULE_CREATE => [
      'db'            =>  'required|min:2',
      'id_parq'       =>  'required',
      'id_medidor'    =>  'required',
      'id_radsensor'  =>  'required',
      'nombre_parq'   =>  'required|min:1',
      'host'          =>  'required|min:1',
      'db_prmte'      =>  'min:1',
      'id_parq_prmte' =>  'min:1',
      'id_medidor_prmte' =>  'min:1',
    ],

    ValidatorInterface::RULE_UPDATE => [
      'db'            =>  'required|min:2',
      'id_parq'       =>  'required',
      'id_medidor'    =>  'required',
      'id_radsensor'  =>  'required',
      'nombre_parq'   =>  'required|min:1',
<<<<<<< HEAD
      'host'          =>  'required|min:1',
      'db_prmte'      =>  'min:1',
      'id_parq_prmte'     =>  'min:1',
      'id_medidor_prmte'      =>  'min:1',
=======
      'host'          =>  'required|min:1'
>>>>>>> 6bba57b09923102656598136b90d82f9927ccb19
    ]
 	];

 /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return $this->rules;
  }

  public function getfillTable()
  {
    return $this->datatable;
  }

  public function getEmpresaAttribute()
  {
    $empresa = \App\Models\AssetManagement\Empresa::where('id',$this->id_empresa)->first();
    if ($empresa) {
      return $empresa->nombre_xls;
    }
    return 'N/D';
  }
}
