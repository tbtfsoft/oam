<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use IzyTech\Validator\Contracts\ValidatorInterface;
use App\Models\AssetManagement\Empresa;

class User extends Authenticatable
{
    use HasRoles;
    use Notifiable;
    
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['role'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'rut', 'password', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $rules = [

        ValidatorInterface::RULE_CREATE => [
            'name'          =>  'required|min:3',
            'email'         =>  'required|min:6',
            'rut'           =>  'required|unique:users|min:9',
            'password'      =>  'required|string|min:6|confirmed'
        ],

        ValidatorInterface::RULE_UPDATE => [
            'name'          =>  'required|min:3',
            'email'         =>  'required|min:6',
            'rut'           =>  'required|unique:users,rut,id,:id|min:9',
            'password'      =>  'sometimes|nullable|min:6|confirmed'
        ] 
   ];

   /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->rules;
    }

    /**
     * Get rol for the user.
     *
     * @return bool
     */
    public function getRoleAttribute()
    {
        $role = $this->roles()->first();

        return $role ? $role->name : 'N/D';
    }

    public function empresas() 
    {
        return $this->belongsToMany(Empresa::class,'empresas_users', 'user_id', 'empresa_id')->orderBy('order', 'asc')
                    ->withTimestamps();
    }

    /**
     * The Childers(user) that belong to the user.
     */
    public function childers()
    {
        return $this->belongsToMany('App\User', 'users_users', 'parent_id', 'child_id','id')
                    ->withTimestamps();
    }

    /**
     * The parent(user) that belong to the user.
     */
    public function parent()
    {
        return $this->belongsToMany('App\User', 'users_users', 'child_id', 'parent_id','id')
                    ->take(1)
                    ->withTimestamps();
    }
}
