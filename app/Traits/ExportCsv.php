<?php 

namespace App\Traits;

use League\Csv\Reader;
use League\Csv\Writer;

trait ExportCsv {

	public function export () {

		if (!$this->verification())
		{
			return null;
		}

    $dbh = $this->PDO();

    $sth = $dbh->prepare($this->sql());

    //because we don't want to duplicate the data for each row
    // PDO::FETCH_NUM could also have been used
    $sth->setFetchMode(\PDO::FETCH_ASSOC);
    $sth->execute();

    //we create the CSV into memory
    $csv = Writer::createFromFileObject(new \SplTempFileObject());

    //we insert the CSV header
    $csv->insertOne($this->headings());

    // The PDOStatement Object implements the Traversable Interface
    // that's why Writer::insertAll can directly insert
    // the data into the CSV
    $csv->insertAll($sth);

    // Because you are providing the filename you don't have to
    // set the HTTP headers Writer::output can
    // directly set them for you
    // The file is downloadable
    $csv->output($this->outputFileName());
	}

	/**
	 * Nomnre del archivo de salida. Se debe agregar el formato.
	 * @return String Nombre del archivo.
	 */
	public function outputFileName ()
	{
		return 'file.csv';
	}

	public function verification ()
	{
		return true;
	}

	/**
	 * Header del csv.
	 * @return Array
	 */
	public function headings ()
	{
		return [];
	}

	/**
	 * Sentencia SQL para la exportacion del Excel.
	 * @return String Sentencia Sql
	 */
	public function sql ()
	{
		return null;
	}

	/**
	 * Configuracion del PDO. La conexion de la base de datos.
	 * @return Array
	 */
	public function configPDO ()
	{
		return [
			'driver'   => 'pgsql',
			'host'     => '186.10.119.4',
			'dbname'   => 'PARKS',
			'user'     => 'postgres',
			'password' => '8e5DbZZmnZ'
		];
	}

	/**
	 * Objeto de conexion PDO.
	 */
	public function PDO ()
	{
		$confi = $this->configPDO();

		$dsn = $confi['driver'].":host=".$confi['host'].";dbname=".$confi['dbname'];

		return new \PDO($dsn, $confi['user'], $confi['password']);
	}

}