<?php 

namespace App\Traits;

use Exception;
use Illuminate\Http\Request;

trait MessagesResponseTrait {

	public static function simple($data, $code = 200) {
		return response()->json([
			'response' => view('layouts.components.response.simple')->with('data',$data)->render(),
			'message'  => $data['message'],
			'catch'		 => $data['catch'] ? $data['catch'] : []
		],$code);
	}
}